#!/bin/bash

set -e
set -o pipefail

TMP_DIR="tree-sitter"
mkdir -p "$TMP_DIR"

add_repo()
{
    local URL="$1"
    local NAME="$2"

    local DIR="$TMP_DIR/$NAME"
    if [ -d "$DIR" ]
    then
        git -C "$DIR" pull
    else
        git clone "$URL" "$DIR"
    fi

    make -C "$DIR"
}

create_targz()
{
    local DST=$(realpath $1)

    cd "$TMP_DIR"
    find . -name 'libtree-sitter-*.so' -o -name 'highlights.scm' |
        tar cfz "$DST" --files-from=-
}

add_repo "https://github.com/tree-sitter/tree-sitter-rust"              rust
add_repo "https://github.com/tree-sitter/tree-sitter-c"                 c
add_repo "https://github.com/tree-sitter/tree-sitter-cpp"               cpp
add_repo "https://github.com/tree-sitter/tree-sitter-python"            python
add_repo "https://github.com/tree-sitter/tree-sitter-json"              json
add_repo "https://github.com/tree-sitter-grammars/tree-sitter-toml"     toml
add_repo "https://github.com/tree-sitter-grammars/tree-sitter-markdown" markdown
add_repo "https://github.com/tree-sitter-grammars/tree-sitter-make"     make

create_targz "tree-sitter.tar.gz"
