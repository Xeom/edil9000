define HELP
                 === MAIN MAKEFILE ===
---- Targets ----
 test            Run all tests.
 common_lib      Build the common C library.
 plugin_lib      Build the plugin C libraries.
 run             Run Edil.
 install         Install Edil using cargo.
 help            Show this message.

endef
include common.mk

common_lib:
	$(MAKE) -C common lib

plugin_lib:
	$(MAKE) -C plugin_py lib

run: common_lib plugin_lib
	$(CARGO) run -p edil

install: common_lib plugin_lib
	$(CARGO) install --path edil --profile dev --locked

test_common:
	$(MAKE) -C common test

test_edil_lib: common_lib
	$(MAKE) -C edil_lib test

test_edil: common_lib plugin_lib
	$(MAKE) -C edil_lib test
	$(MAKE) -C edil test

test: test_common test_edil

.PHONY: test common_lib plugin_lib run install
.DEFAULT_GOAL := test
