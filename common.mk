ROOT_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

TEST_RUST_FLAGS ?= --test-threads 1 $(if $(VERBOSE_TESTS),,-q)
export TEST_RUST_FLAGS := $(TEST_RUST_FLAGS)

TEST_C_FLAGS ?= $(if $(VERBOSE_TESTS),,-q)
export TEST_C_FLAGS := $(TEST_C_FLAGS)

VALGRIND ?= valgrind
export VALGRIND := $(VALGRIND)

VGFLAGS ?= --track-origins=yes --leak-check=full --error-exitcode=1 \
           --suppressions=$(ROOT_DIR)/valgrind.supp --gen-suppressions=all \
           $(if $(VERBOSE_TESTS),,-q)
export VGFLAGS := $(VGFLAGS)

ifeq (,$(findstring B,$(MAKEFLAGS)))
    CARGO ?= cargo
else
    CARGO ?= cargo clean ; cargo
endif
export CARGO := $(CARGO)

AT := $(if $(VERBOSE),,@)

compile_flags.txt: ALWAYS
	$(AT) echo === Generating compile_flags.txt ===
	$(AT) printf '%s\n' $(CFLAGS) $(EXTRA_CFLAGS) > $@

define COMMON_HELP
--- Common Variables ---
 VALGRIND        Valgrind executable.
 VGFLAGS         Flags to pass to valgrind.
 CARGO           Cargo executable.
 TEST_RUST_FLAGS Flags to pass to Rust test executables.
 TEST_C_FLAGS    Flags to pass to C test executables.
 VERBOSE         If set, to print all executed commands.
 VERBOSE_TESTS   If set, print verbose test output.

endef

help:
	$(info $(HELP))
	$(info $(COMMON_HELP))
	@:

.PHONY: help ALWAYS
