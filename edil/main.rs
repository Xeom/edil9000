use edil_lib::*;
use std::path::PathBuf;

/// My editor :)
#[derive(clap::Parser)]
#[command(version, about, long_about=None)]
struct CliArgs
{
    /// Paths to open.
    ///
    /// If no paths are specified, then if STDIN is a terminal, the current
    /// directory is opened, or if STDIN is not a terminal, STDIN is opened as a
    /// file.
    paths: Vec<PathBuf>,
    /// Path for saved logfile.
    #[arg(long, env="EDIL_LOG_FILE")]
    log_file: Option<PathBuf>,
    /// The format to use for saved logfile.
    #[arg(long, default_value="pretty")]
    log_format: LogFormat,
    /// Logging filter.
    #[arg(default_value="info", long, env="EDIL_LOG_LEVEL")]
    log_level: String,

    /// Turn off the notification buffer at the bottom of the screen.
    #[arg(long, action)]
    disable_notifications: bool
}

#[derive(clap::ValueEnum, Clone, Debug)]
enum LogFormat
{
    /// Pretty logging with ANSI escape codes for formatting.
    Pretty,
    /// Human-readable UTF-8 text logging.
    Text,
    /// Machine-readable JSON formatted logging.
    Json
}

#[tokio::main]
async fn main()
{
    let args = <CliArgs as clap::Parser>::parse();

    let log_buffer = Buffer::default();
    let fonts = Fonts::default();

    // Create the in-editor log buffer
    logging::Logger::instance()
        .add_sink(
            &args.log_level,
            logging::BufferSink::new(&log_buffer, &fonts)
        );

    // Create a log file
    if let Some(file_name) = &args.log_file
    {
        let format = match args.log_format
            {
                LogFormat::Pretty =>
                    logging::FileFormat::Text { ansi: true, timestamps: true },
                LogFormat::Json =>
                    logging::FileFormat::Json,
                LogFormat::Text =>
                    logging::FileFormat::Text { ansi: false, timestamps: true }
            };

        let file = std::io::BufWriter::new(
            std::fs::File::create(file_name).expect("Could not create log file")
        );

        logging::Logger::instance()
            .add_sink(&args.log_level, logging::FileSink::new(file, format));
    }

    let config = Config::load().await;

    info!("Starting editor...");

    debug!("Opening TTY...");
    let (input, output) = tty::open_tty(tty::vt220());

    debug!("Creating editor...");
    let editor = Editor::new(input.win_size(), config, fonts);

    logging::redirect_panics_to_log();

    debug!("Redirecting Stdout to log...");
    let stdout_handle =
        tokio::task::spawn(logging::redirect_stdstream_to_log_task(
            editor.clone(), logging::StdStream::StdOut, info_span!("Stdout")
        ));

    debug!("Redirecting Stderr to log...");
    let stderr_handle =
        tokio::task::spawn(logging::redirect_stdstream_to_log_task(
            editor.clone(), logging::StdStream::StdErr, info_span!("Stderr")
        ));

    if args.disable_notifications
    {
        editor.disable_notifications();
    }

    editor.open_window(LogParams(log_buffer));

    if !args.paths.is_empty()
    {
        for path in args.paths.iter()
        {
            editor.clone().open_path(path, 0).await.unwrap();
        }
    }
    else if nix::unistd::isatty(nix::libc::STDIN_FILENO) == Ok(false)
    {
        info!("Reading from STDIN...");
        editor.open_pipe(&nix::libc::STDIN_FILENO)
            .unwrap();
    }
    else
    {
        info!("Opening current directory...");
        editor.clone().open_path(".", 0).await
            .unwrap();
    }

    debug!("Starting input task...");
    let py = Box::new(plugin_py::PyPlugin);
    let input_handle =
        tokio::task::spawn(InputTask::new(editor.clone(), input, py).task());

    debug!("Starting display task...");
    let draw_handle =
        tokio::task::spawn(DisplayTask::new(editor.clone(), output).task());

    Config::setup_dotfiles().await.ok();

    draw_handle.await.unwrap();
    input_handle.await.unwrap();
    stdout_handle.await.unwrap();
    stderr_handle.await.unwrap();

    info!("Editor terminated");
}
