use test_harness::*;

const EXE_PATH: &'static str = env!("CARGO_BIN_EXE_edil");

fn assert_mode(pty_harness: &mut PtyHarness, mode: &str)
{
    println!("assert_mode({mode})");
    pty_harness.assert(
        |harness| harness.contents().contains(mode),
        std::time::Duration::from_secs(10)
    )
}

#[test]
fn test_switches_between_modes()
{
     let (mut pty_harness, _child) = ChildBuilder::new(&[EXE_PATH])
        .using_stdin_pipe()
        .using_arg("--disable-notifications")
        .build();

    assert_mode(&mut pty_harness, "Read");
    pty_harness.type_bytes("/");
    assert_mode(&mut pty_harness, "Prompt");
    pty_harness.type_bytes("\r");
    assert_mode(&mut pty_harness, "Read");
}
