use test_harness::*;

const EXE_PATH: &'static str = env!("CARGO_BIN_EXE_edil");

#[test]
fn test_scrolls_with_big_text()
{
     let (mut pty_harness, mut child_harness) = ChildBuilder::new(&[EXE_PATH])
        .using_stdin_pipe()
        .using_arg("--disable-notifications")
        .build();

    // Type a massive block of 'x' that won't fit on the screen.
    for i in 0..1000
    {
        child_harness.send_stdin_bytes("x".repeat(i) + "\n");
        child_harness.send_stdin_bytes("Last line");
        pty_harness.type_bytes("G");
        pty_harness.assert_ignoring_bottom(|s| s.trim().contains("Last line"));
        child_harness.send_stdin_bytes("\n");
    }
}
