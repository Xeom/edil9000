use test_harness::*;
use rand::Rng;

const EXE_PATH: &'static str = env!("CARGO_BIN_EXE_edil");

#[test]
fn test_resize()
{
    fn expect_screen(rows: u16, cols: u16) -> String
    {
        let mut rtn = String::new();

        for _ in 0..(rows - 1)
        {
            rtn.push_str(
                &std::iter::repeat("#").take(cols.into())
                    .chain(std::iter::once("\n"))
                    .collect::<String>()
            );
        }

        rtn.extend(
            "Read/Untitled File/1 Task".chars()
                .chain(std::iter::repeat('_'))
                .take(cols as usize)
        );

        rtn
    }

    fn resize_randomly(pty_harness: &mut PtyHarness)
    {
        let rows = rand::thread_rng().gen_range(10..40);
        let cols = rand::thread_rng().gen_range(10..40);

        pty_harness.resize(rows, cols);
    }

     let (mut pty_harness, mut child_harness) = ChildBuilder::new(&[EXE_PATH])
        .using_stdin_pipe()
        .using_arg("--disable-notifications")
        .build();

    child_harness.send_stdin_bytes(
        &std::iter::repeat(b'#').take(40 * 40)
            .collect::<Vec<_>>()
    );

    for _ in 0..200
    {
        for _ in 0..4
        {
            resize_randomly(&mut pty_harness);
        }
        let size = pty_harness.size();
        pty_harness.assert_shows(expect_screen(size.0, size.1));
    }
}
