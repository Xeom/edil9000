extern crate proc_macro;
use proc_macro::TokenStream;

struct Function
{
    name:   syn::Ident,
    result: Option<syn::Type>,
    param_names: Vec<syn::Ident>,
    param_types: Vec<syn::Type>
}

fn gen_symbol(func: &Function) -> syn::Ident
{
    quote::format_ident!("api_{}", func.name)
}

fn gen_api_info(func: &Function) -> TokenStream
{
    let param_names = func.param_names.iter().map(|id| id.to_string());
    let param_types = &func.param_types;

    let name   = func.name.to_string();
    let symbol = gen_symbol(func).to_string();
    let info_name = quote::format_ident!(
        "API_{}", func.name.to_string().to_uppercase()
    );

    let result = match &func.result
        {
            Some(result_type) =>
                quote::quote!
                {
                    Some(&plugin::api::OutputImpl::<#result_type>::new())
                },
            None => quote::quote! { None }
        };

    quote::quote!
    {
        #[linkme::distributed_slice(plugin::api::PLUGIN_API)]
        static #info_name: plugin::api::Info::<'static> =
            plugin::api::Info::<'static>
            {
                symbol: #symbol,
                name: #name,
                result: #result,
                args: &[ #(
                    (
                        #param_names,
                        &plugin::api::InputImpl::<#param_types>::new()
                    )
                ),* ]
            };
    }
        .into()
}

fn gen_extern_api(func: &Function) -> TokenStream
{
    let param_names = &func.param_names;
    let param_types = &func.param_types
        .iter()
        .map(|ty| quote::quote! { <#ty as plugin::api::Input> })
        .collect::<Vec<_>>();

    let inner_name = &func.name;
    let symbol = gen_symbol(func);

    if let Some(result_type) = func.result
        .as_ref()
        .map(|ty| quote::quote! { <#ty as plugin::api::Output> })
    {
        quote::quote!
        {
            #[no_mangle]
            pub unsafe extern "C" fn #symbol(
                #( #param_names: #param_types::CType ),*
            )
                -> #result_type::CType
            {
                #result_type::from_rust(
                    #inner_name( #( &* #param_types::from_c(&#param_names) ),* )
                )
            }
        }
            .into()
    }
    else
    {
        quote::quote!
        {
            #[no_mangle]
            pub unsafe extern "C" fn #symbol(
                #( #param_names: #param_types::CType ),*
            )
            {
                #inner_name( #( &* #param_types::from_c(&#param_names) ),* );
            }
        }
            .into()
    }
}

#[proc_macro_attribute]
pub fn plugin_api(attr: TokenStream, item: TokenStream) -> TokenStream
{
    assert!(attr.is_empty(), "This macro does not accept any arguments.");
    let mut result = item.clone();
    let item_fn = syn::parse_macro_input!(item as syn::ItemFn);

    let mut funct = Function
    {
        name: item_fn.sig.ident.clone(),
        result: match item_fn.sig.output
        {
            syn::ReturnType::Default => None,
            syn::ReturnType::Type(_, ty) => Some(*ty)
        },
        param_names: Vec::new(),
        param_types: Vec::new()
    };

    for input in item_fn.sig.inputs.iter()
    {
        if let syn::FnArg::Typed( syn::PatType { pat, ty, .. }) = input
        {
            if let syn::Pat::Ident(name) = pat.as_ref()
            {
                if let syn::Type::Reference(syn::TypeReference { elem, .. })
                    = ty.as_ref()
                {
                    funct.param_names.push(name.ident.clone());
                    funct.param_types.push(elem.as_ref().clone());
                }
                else
                {
                    panic!(
                        "This macro expects all parameters to be immutable \
                        references."
                    );
                }
            }
            else
            {
                panic!(
                    "This macro expects all parameters to have a simple \
                    identifier as a name."
                );
            }
        }
        else
        {
            panic!("This macro does not expect a self argument.");
        }
    }

    result.extend(gen_extern_api(&funct));
    result.extend(gen_api_info(&funct));

    result
}
