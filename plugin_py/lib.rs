use edil_lib::*;
use std::{
    ptr::NonNull,
    marker::PhantomData,
    ffi::CString
};

mod c_api
{
    #![allow(non_upper_case_globals)]
    #![allow(non_camel_case_types)]
    #![allow(non_snake_case)]
    #![allow(unused)]

    include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
}

pub struct PyPlugin;

impl plugin::Plugin for PyPlugin
{
    fn create<'a>(&self, context: &'a Context) -> Box<dyn plugin::PluginHandle + 'a>
    {
        Box::new(PyHandle::new(context))
    }
}

pub struct PyHandle<'a>
{
    c_ptr: NonNull<c_api::txt_py_s>,
    _spooky: PhantomData<&'a ()>
}

impl<'a> OwnedPtr for PyHandle<'a>
{
    type Ptr = *mut c_api::txt_py_s;

    unsafe fn ptr(&self) -> *mut c_api::txt_py_s { self.c_ptr.clone().as_mut() }

    unsafe fn from_owned_ptr(ptr: *mut c_api::txt_py_s) -> Self
    {
        Self
        {
            c_ptr:   NonNull::new(ptr).expect("Could not create Python"),
            _spooky: Default::default()
        }
    }
}

impl<'a> PyHandle<'a>
{
    fn new(context: &'a Context) -> Self
    {
        unsafe
        {
            let ctx_ptr = context as *const _ as *mut c_api::txt_context_s;
            Self::from_owned_ptr(c_api::txt_py_create(ctx_ptr))
        }
    }
}

impl<'a> Drop for PyHandle<'a>
{
    fn drop(&mut self) { unsafe { c_api::txt_py_destroy(self.ptr()) } }
}

impl<'a> plugin::PluginHandle for PyHandle<'a>
{
    fn eval(&mut self, code: &str)
    {
        // TODO: Error handling
        let c_str = CString::new(code).expect("I haven't sorted this out yet");
        let res = unsafe { c_api::txt_py_eval(self.ptr(), c_str.as_ptr()) };

        assert_eq!(res, c_api::txt_err_TXT_OK);
    }
}
