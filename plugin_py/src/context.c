#include "py_types.h"

typedef struct txt_py_context txt_py_context_s;

struct txt_py_context
{
    PyObject_HEAD
    txt_context_s *ptr;
};

static PyObject *get_default_context()
{
    PyObject *rtn;

    if (TXT_PY_CONTEXT_CONTEXTVAR == NULL)
    {
        PyErr_Format(
            PyExc_RuntimeError,
            "No Edil context ContextVar found."
        );
        return NULL;
    }

    if (PyContextVar_Get(TXT_PY_CONTEXT_CONTEXTVAR, NULL, &rtn) < 0)
        return NULL;

    if (rtn == NULL)
    {
        PyErr_Format(
            PyExc_RuntimeError,
            "Could not find default Edil context handle in ContextVar"
        );
        return NULL;
    }

    return rtn;
}

bool txt_context_from_pyobj(PyObject *py, txt_context_s **out)
{
    if (py == Py_None)
    {
        // If no context was specified, try and find the python context.
        Py_DECREF(py);
        py = get_default_context();
        if (!py)
            return false;
    }

    if (!PyType_IsSubtype(Py_TYPE(py), &TXT_PYTYPE_CONTEXT))
    {
        PyErr_Format(
            PyExc_TypeError, "Expected pyedil.Context, found %s.",
            Py_TYPE(py)->tp_name
        );
        return false;
    }

    *out = ((txt_py_context_s *)py)->ptr;
    return true;
}

TXT_IMPL_GENERIC_TO_PYOBJ(context, txt_context_s, TXT_PYTYPE_CONTEXT)

static void py_context_dealloc(PyObject *self_obj)
{
    txt_py_context_s *self = (txt_py_context_s *)self_obj;
    self->ptr = NULL;
}

PyTypeObject TXT_PYTYPE_CONTEXT =
{
    .ob_base      = PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name      = "pyedil.Context",
    .tp_doc       = PyDoc_STR(
        "An opaque handle for the context of a plugin. This must be passed as"
        " an argument to some API calls."
    ),
    .tp_basicsize = sizeof(txt_py_context_s),
    .tp_itemsize  = 0,
    .tp_flags     = Py_TPFLAGS_DEFAULT,
    .tp_dealloc   = py_context_dealloc
};
