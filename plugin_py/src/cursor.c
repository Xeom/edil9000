#include "py_types.h"

typedef struct txt_py_cursor txt_py_cursor_s;

struct txt_py_cursor
{
    PyObject_HEAD
    txt_cursor_s *ptr;
};

TXT_IMPL_GENERIC_FROM_PYOBJ(cursor, txt_cursor_s, TXT_PYTYPE_CURSOR)
TXT_IMPL_GENERIC_TO_PYOBJ(cursor, txt_cursor_s, TXT_PYTYPE_CURSOR)

static void py_cursor_dealloc(PyObject *self_obj)
{
    txt_py_cursor_s *self = (txt_py_cursor_s *)self_obj;
    if (self->ptr)
    {
        txt_cursor_destroy(self->ptr);
        self->ptr = NULL;
    }
}

static PyMethodDef CURSOR_METHODS[] =
{
    { 0 }
};

PyTypeObject TXT_PYTYPE_CURSOR =
{
    .ob_base      = PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name      = "pyedil.Cursor",
    .tp_doc       = PyDoc_STR(
        "An object representing a Cursor within a Buffer."
    ),
    .tp_basicsize = sizeof(txt_py_cursor_s),
    .tp_itemsize  = 0,
    .tp_flags     = Py_TPFLAGS_DEFAULT,
    .tp_dealloc   = py_cursor_dealloc,
    .tp_methods   = CURSOR_METHODS
};
