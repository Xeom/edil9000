#if !defined(LIBTXT_PY_MODULE_H)
# define LIBTXT_PY_MODULE_H

#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <stdbool.h>

#include "txt_cursor.h"
#include "txt_str.h"

PyObject *PyInit_pyedil(void);

/** PyMethodDef */
extern PyMethodDef TXT_PY_METHODS[];

/** Python binding for txt_context_s */
typedef struct txt_context txt_context_s;

extern PyTypeObject TXT_PYTYPE_CONTEXT;
extern PyObject *TXT_PY_CONTEXT_CONTEXTVAR;

PyObject *txt_context_to_pyobj(txt_context_s *context);
bool txt_context_from_pyobj(PyObject *self_obj, txt_context_s **out);

/** Python binding for txt_cursor_s */
extern PyTypeObject TXT_PYTYPE_CURSOR;

PyObject *txt_cursor_to_pyobj(txt_cursor_s *cursor);
bool txt_cursor_from_pyobj(PyObject *self_obj, txt_cursor_s **out);

/** Misc. conversions */
static inline bool txt_utf8_from_pyobj(PyObject *obj, txt_utf8_s **out)
{
    ssize_t len;
    const char *bytes = PyUnicode_AsUTF8AndSize(obj, &len);
    if (!bytes)
        return false;

    *out = txt_utf8_create((uint8_t *)bytes, (size_t)len);

    return true;
}

static inline PyObject *txt_utf8_to_pyobj(txt_utf8_s *s)
{
    return PyUnicode_FromStringAndSize(
        (const char *)txt_utf8_bytes(s),
        txt_utf8_len(s)
    );
}

static inline bool txt_size_t_from_pyobj(PyObject *obj, size_t *out)
{
    *out = PyLong_AsSize_t(obj);
    return PyErr_Occurred() == NULL;
}

#define TXT_CONVERT_ARG_FROM_PY(_inp, _out) \
    _Generic( \
        (*_out), \
        txt_context_s *: txt_context_from_pyobj, \
        txt_cursor_s *:  txt_cursor_from_pyobj, \
        txt_utf8_s *:    txt_utf8_from_pyobj, \
        size_t:          txt_size_t_from_pyobj \
    )(_inp, _out)

#define TXT_CONVERT_RESULT_TO_PY(_inp) \
    _Generic( \
        (_inp), \
        txt_cursor_s *: txt_cursor_to_pyobj, \
        txt_utf8_s *:   txt_utf8_to_pyobj, \
        uint64_t:       PyLong_FromUnsignedLongLong \
    )(_inp)

#define TXT_IMPL_GENERIC_TO_PYOBJ(_name, _c_type, _py_type_obj) \
    PyObject *txt_ ## _name ## _to_pyobj(_c_type *c) \
    { \
        if (!c) Py_RETURN_NONE; \
        txt_py_ ## _name ## _s *py = \
            (txt_py_ ## _name ## _s *)_py_type_obj.tp_alloc(&_py_type_obj, 0); \
        if (!py) return NULL; \
        py->ptr = c; \
        return (PyObject *)py; \
    }

#define TXT_IMPL_GENERIC_FROM_PYOBJ(_name, _c_type, _py_type_obj) \
    bool txt_ ## _name ## _from_pyobj(PyObject *py, _c_type **out) \
    { \
        if (!PyType_IsSubtype(Py_TYPE(py), &_py_type_obj)) \
        { \
            PyErr_Format( \
                PyExc_TypeError, "Expected %s, found %s.", \
                _py_type_obj.tp_name, \
                Py_TYPE(py)->tp_name \
            ); \
            return false; \
        } \
        *out = ((txt_py_ ## _name ## _s *)py)->ptr; \
        return true; \
    }

#endif
