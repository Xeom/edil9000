#include "py_types.h"
#include "plugin.h"

/* === Embedded python init/deinit === */
static pthread_mutex_t MTX = PTHREAD_MUTEX_INITIALIZER;
static wchar_t *PROGRAM = NULL;
static int NUM_PY = 0;
PyObject *TXT_PY_CONTEXT_CONTEXTVAR = NULL;

static void embed_start()
{
    pthread_mutex_lock(&MTX);
    if (NUM_PY++ == 0)
    {
        PROGRAM = Py_DecodeLocale("edil", NULL);
        PyImport_AppendInittab("pyedil", PyInit_pyedil);
        Py_Initialize();

        TXT_PY_CONTEXT_CONTEXTVAR = PyContextVar_New("pyedil_context", NULL);
    }
    pthread_mutex_unlock(&MTX);
}

static void embed_end()
{
    pthread_mutex_lock(&MTX);
    if (--NUM_PY == 0)
    {
        Py_DECREF(TXT_PY_CONTEXT_CONTEXTVAR);
        TXT_PY_CONTEXT_CONTEXTVAR = NULL;

        Py_FinalizeEx();
        PyMem_RawFree(PROGRAM);
    }
    pthread_mutex_unlock(&MTX);
}

/* Python state interpreter handle */
struct txt_py
{
    PyObject *globals;
    PyObject *locals;

    PyObject *py_ctx;
    PyObject *edil_ctx;
};

txt_py_s *txt_py_create(txt_context_s *context_handle)
{
    embed_start();

    txt_py_s *rtn = malloc(sizeof(*rtn));
    if (!rtn)
        return NULL;

    memset(rtn, '\0', sizeof(*rtn));

    rtn->globals = PyDict_New();
    if (!rtn->globals) goto err;

    rtn->locals = PyDict_New();
    if (!rtn->locals) goto err;

    // Set "pyedil" in python session (also inits the module, so do first!)
    PyObject *pyedil = PyImport_ImportModule("pyedil");
    if (!pyedil) goto err;

    if (PyDict_SetItemString(rtn->globals, "edil", pyedil) < 0)
        goto err;

    Py_DECREF(pyedil);

    // Set "context" in python session and context
    rtn->edil_ctx = txt_context_to_pyobj(context_handle);
    if (!rtn->edil_ctx) goto err;

    if (PyDict_SetItemString(rtn->globals, "context", rtn->edil_ctx) < 0)
        goto err;

    // Create a new python context
    rtn->py_ctx = PyContext_New();
    if (!rtn->py_ctx) goto err;

    PyContext_Enter(rtn->py_ctx);
    PyContextVar_Set(TXT_PY_CONTEXT_CONTEXTVAR, rtn->edil_ctx);
    PyContext_Exit(rtn->py_ctx);

    return rtn;
err:
    txt_py_destroy(rtn);
    return NULL;
}

void txt_py_destroy(txt_py_s *py)
{
    if (py->globals) Py_DECREF(py->globals);
    if (py->locals)  Py_DECREF(py->locals);
    if (py->py_ctx)  Py_DECREF(py->py_ctx);
    if (py->edil_ctx)
    {
        // Manually run the destructor on the context. It is no longer valid.
        //
        // This ensures that even if a reference to the edil context of this
        // session survives somewhere, it will become invalid and cannot be
        // used to break everything.
        PyGILState_STATE gs = PyGILState_Ensure();
        TXT_PYTYPE_CONTEXT.tp_dealloc(py->edil_ctx);
        PyGILState_Release(gs);

        Py_DECREF(py->edil_ctx);
    }

    free(py);

    embed_end();
}

txt_err_e txt_py_eval(txt_py_s *py, const char *s)
{
    PyContext_Enter(py->py_ctx);
    PyObject *rtn = PyRun_String(s, Py_single_input, py->globals, py->locals);
    PyContext_Exit(py->py_ctx);

    if (!rtn)
        return TXT_ERR_PYTHON;

    Py_DECREF(rtn);

    return TXT_OK;
}

