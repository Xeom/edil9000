#if !defined(LIBTXT_PYBIND_H)
# define LIBTXT_PYBIND_H
# include "txt_err.h"

typedef struct txt_py txt_py_s;
typedef struct txt_context txt_context_s;

txt_py_s *txt_py_create(txt_context_s *context);

void txt_py_destroy(txt_py_s *py);

txt_err_e txt_py_eval(txt_py_s *py, const char *s);

#endif
