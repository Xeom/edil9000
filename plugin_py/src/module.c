#include "py_types.h"

static PyModuleDef MODULE_DEF =
{
    PyModuleDef_HEAD_INIT,
    .m_name    = "pyedil",
    .m_doc     = "Python plugin bindings for the Edil editor.",
    .m_methods = TXT_PY_METHODS,
    .m_size    = -1
};

#define DIM(_x) (sizeof(_x) / sizeof((_x)[0]))
static struct { const char *name; PyTypeObject *type; } TYPES[] =
{
    { "Context",       &TXT_PYTYPE_CONTEXT },
    { "Cursor",        &TXT_PYTYPE_CURSOR }
};

PyObject *PyInit_pyedil(void)
{
    PyObject *module = NULL;
    size_t added_types = 0;

    for (size_t i = 0; i < DIM(TYPES); ++i)
    {
        if (PyType_Ready(TYPES[i].type) < 0)
            goto err;
    }

    module = PyModule_Create(&MODULE_DEF);
    if (module == NULL)
        goto err;

    for (size_t i = 0; i < DIM(TYPES); ++i)
    {
        PyTypeObject *t = TYPES[i].type;
        Py_INCREF(t);
        added_types++;
        if (PyModule_AddObject(module, TYPES[i].name, (PyObject *)t) < 0)
            goto err;
    }

    return module;

err:
    for (size_t i = 0; i < added_types; ++i)
        Py_DECREF(TYPES[i].type);

    if (module)
        Py_DECREF(module);

    return NULL;
}
