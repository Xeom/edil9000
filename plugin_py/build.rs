use std::{env, path::{PathBuf, Path}, process::Command};

fn option(opt: &str, path: impl AsRef<Path>)
{
    println!("cargo:{opt}={}", path.as_ref().as_os_str().to_str().unwrap());
}

fn add_lib(path: &Path)
{
    option("rerun-if-changed", path);

    if let Some(dir) = path.parent()
    {
        option("rustc-link-search", dir);
    }

    if let Some(lib) = path.file_stem()
        .and_then(|p| p.to_str().unwrap().strip_prefix("lib"))
    {
        option("rustc-link-lib", lib);
    }
}

fn add_python_libs()
{
    // "python3-config --libs --embed" should return a list of -l flags to link.
    let output_bytes = Command::new("python3-config")
        .arg("--libs")
        .arg("--embed")
        .output()
        .expect("Could not run python3-config")
        .stdout;

    for flag in String::from_utf8(output_bytes)
        .expect("Invalid UTF-8 from python3-config")
        .trim()
        .split_whitespace()
    {
        let lib = flag.strip_prefix("-l")
            .expect("Expected ldflag to start with -l");

        option("rustc-link-lib", lib);
    }
}

fn main()
{
    let plugin_py_dir = PathBuf::from(env::var("CARGO_MANIFEST_DIR").unwrap())
        .canonicalize()
        .expect("Could not canonicalize plugin_py directory");

    let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap())
        .canonicalize()
        .expect("Could not canonicalize out dir");

    let header_file = plugin_py_dir.join("src/plugin.h");

    option("rerun-if-changed", &plugin_py_dir.join("build.rs"));
    option("rerun-if-changed", &header_file);

    add_lib(&plugin_py_dir.join("build/libpyedil.a"));
    add_python_libs();

    bindgen::Builder::default()
        .clang_arg("-I../common/inc")
        .header(header_file.to_str().unwrap())
        .parse_callbacks(Box::new(bindgen::CargoCallbacks::new()))
        .generate()
        .expect("Could not generate bindings")
        .write_to_file(out_dir.join("bindings.rs"))
        .expect("Could not write bindings");
}
