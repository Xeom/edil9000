use super::*;

pub struct ApiUtf8
{
    c_ptr: NonNull<txt_utf8_s>
}

impl OwnedPtr for ApiUtf8
{
    type Ptr = *mut txt_utf8_s;

    unsafe fn ptr(&self) -> *mut txt_utf8_s { self.c_ptr.clone().as_mut() }

    unsafe fn from_owned_ptr(ptr: *mut txt_utf8_s) -> Self
    {
        Self { c_ptr: NonNull::new(ptr).expect("Could not create utf8 str") }
    }
}

impl ApiUtf8
{
    pub fn new(s: &str) -> Self
    {
        let bytes = s.as_bytes();
        unsafe
        {
            Self::from_owned_ptr(
                txt_utf8_create(bytes.as_ptr(), bytes.len())
            )
        }
    }
}

impl<'a> NonOwned<'a, ApiUtf8>
{
    pub fn as_str(&self) -> std::borrow::Cow<'a, str>
    {
        String::from_utf8_lossy(unsafe { std::slice::from_raw_parts(
            txt_utf8_bytes(self.ptr()),
            txt_utf8_len(self.ptr())
        ) })
    }
}
