mod c_api;
use c_api::*;

use std::{
    ptr::NonNull,
    marker::PhantomData,
    mem::{ManuallyDrop, MaybeUninit, size_of},
    ffi::CString
};

mod utf8;
pub use utf8::*;

mod fonts;
pub use fonts::*;

mod event;
pub use event::*;

mod buffer;
pub use buffer::*;

mod cursor;
pub use cursor::*;

mod non_owned;
pub use non_owned::{OwnedPtr, NonOwned};

#[macro_export]
macro_rules! expect_event
{
    ($evt:expr, $exp:expr, $cause:block) =>
    {
        let tout = std::time::Duration::from_millis(10);
        let exec = Executor::default();
        let (tx, rx) = std::sync::mpsc::channel();
        let handler = Handler::builder(&exec)
            .with_event($evt)
            .with_sync()
            .build_thread_safe(move |i| tx.send(*i).unwrap());

        $cause;

        for e in $exp
        {
            assert_eq!(rx.recv_timeout(tout).as_ref(), Ok(e));
        }
        assert!(rx.try_recv().is_err());

        let _ = handler;
    };
    ($evt:expr, $cause:block) =>
    {
        let tout = std::time::Duration::from_millis(10);
        let exec = Executor::default();
        let (tx, rx) = std::sync::mpsc::channel();
        let handler = Handler::builder(&exec)
            .with_event($evt)
            .with_sync()
            .build_thread_safe(move |i| tx.send(*i).unwrap());

        $cause;

        assert!(rx.recv_timeout(tout).is_ok());
        assert!(rx.recv_timeout(tout).is_err());

        let _ = handler;
    };
}

#[macro_export]
macro_rules! expect_no_event
{
    ($evt:expr, $cause:block) =>
    {
        expect_event!($evt, std::iter::empty(), $cause);
    }
}
