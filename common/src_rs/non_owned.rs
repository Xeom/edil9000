use super::*;

#[derive(Copy, Clone)]
pub struct NonOwned<'a, T>
{
    inner:   ManuallyDrop<T>,
    _spooky: PhantomData<&'a ()>
}

pub trait OwnedPtr
{
    type Ptr;

    unsafe fn ptr(&self) -> Self::Ptr;

    unsafe fn to_owned_ptr(self) -> Self::Ptr
        where Self: Sized
    {
        let ptr = self.ptr();
        std::mem::forget(self);
        ptr
    }

    unsafe fn from_owned_ptr(ptr: Self::Ptr) -> Self
        where Self: Sized;

    unsafe fn from_non_owned_ptr<'a>(ptr: Self::Ptr) -> NonOwned<'a, Self>
        where Self: Sized
    {
        NonOwned
        {
            inner:   ManuallyDrop::new(Self::from_owned_ptr(ptr)),
            _spooky: Default::default()
        }
    }

    unsafe fn as_immortal_non_owned(&self) -> NonOwned<'static, Self>
        where Self: Sized
    {
        unsafe { Self::from_non_owned_ptr(self.ptr()) }
    }

    fn as_non_owned<'a>(&'a self) -> NonOwned<'a, Self>
        where Self: Sized
    {
        unsafe { Self::from_non_owned_ptr(self.ptr()) }
    }
}

impl<'a, T> AsRef<T> for NonOwned<'a, T>
{
    fn as_ref(&self) -> &T { &self.inner }
}

impl<'a, T> std::ops::Deref for NonOwned<'a, T>
{
    type Target = T;

    fn deref(&self) -> &T { &self.inner }
}

impl<'a, T> From<&'a T> for NonOwned<'a, T>
    where T: OwnedPtr
{
    fn from(owned: &'a T) -> NonOwned<'a, T> { owned.as_non_owned() }
}
