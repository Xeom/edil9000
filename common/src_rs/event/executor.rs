use super::*;

pub struct Executor(NonNull<txt_executor_s>);

unsafe impl Sync for Executor {}
unsafe impl Send for Executor {}

impl Executor
{
    pub(super) unsafe fn ptr(&self) -> *mut txt_executor_s
    {
        self.0.clone().as_mut()
    }
}

impl Default for Executor
{
    fn default() -> Self
    {
        Self(
            NonNull::new(unsafe { txt_executor_create() })
                .expect("Could not create executor")
        )
    }
}

impl Drop for Executor
{
    fn drop(&mut self) { unsafe { txt_executor_destroy(self.ptr()) } }
}

