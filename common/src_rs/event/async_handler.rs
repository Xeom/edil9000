use super::*;

use std::{
    collections::VecDeque,
    task::{Waker, Poll, Context},
    pin::Pin,
    sync::{Arc, Mutex},
    future::Future
};

#[derive(Default)]
struct Inner<E>
{
    vec:             VecDeque<E>,
    notify:          Option<Waker>,
    ignore_multiple: bool
}

pub struct AsyncHandler<E>
{
    _handler: Handler,
    inner:    Arc<Mutex<Inner<E>>>
}

pub struct Next<'a, E>(&'a AsyncHandler<E>);

fn handle_cb<E>(inner: &Arc<Mutex<Inner<E>>>, params: E)
{
    let maybe_waker;
    {
        let mut inner = inner.lock().unwrap();
        if inner.ignore_multiple
        {
            inner.vec.clear();
        }

        inner.vec.push_back(params);

        maybe_waker = inner.notify.take();
    }

    if let Some(waker) = maybe_waker
    {
        waker.wake();
    }
}

impl<E> AsyncHandler<E>
{
    pub fn next(&self) -> Next<'_, E> { Next(self) }
}

impl<'a, E> From<handler::Builder<'a, E>> for AsyncHandler<E>
    where E: Sync + Send + Copy + 'static
{
    fn from(bldr: handler::Builder<'a, E>) -> AsyncHandler<E>
    {
        let inner = Arc::new(Mutex::new(
            Inner {
                vec:             Default::default(),
                notify:          None,
                ignore_multiple: (bldr.get_flags() & IGNORE_MULTIPLE) != 0
            }
        ));

        let inner_2 = inner.clone();

        Self
        {
            _handler: bldr.build_thread_safe(move |e| handle_cb(&inner_2, *e)),
            inner
        }
    }
}

impl<'a, E> Future for Next<'a, E>
{
    type Output = E;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<E>
    {
        let mut inner = self.0.inner.lock().unwrap();
        match inner.vec.pop_front()
        {
            Some(rtn) => Poll::Ready(rtn),
            None =>
            {
                inner.notify = Some(cx.waker().clone());
                Poll::Pending
            }
        }
    }
}
