use super::*;

pub(super) type Cb<E> = Box<dyn FnMut(&E) + Send + 'static>;

pub(super) extern "C" fn c_callback<E>(
    handler:        *mut txt_handler_s,
    handler_params: *mut std::ffi::c_void,
    event_params:   *const std::ffi::c_void
)
{
    let _ = handler;

    // Get event parameters.
    let params: &E =
        if size_of::<E>() == 0
        {
            // Zero-sized types are passed from C as NULL-pointers.
            //
            // Make a dangling pointer to handle this.
            assert!(event_params.is_null(), "Expected NULL event params.");
            let non_null = NonNull::<E>::dangling();
            unsafe { non_null.as_ref() }
        }
        else
        {
            // For normal types, simply cast the pointer to a ref.
            assert!(!event_params.is_null(), "Expected Non-NULL event params.");
            unsafe { &*(event_params as *const E) }
        };

    // Get handler parameters.
    assert!(!handler_params.is_null(), "Expected Non-NULL handler params.");
    let cb: &mut Cb<E> = unsafe { &mut *(handler_params as *mut Cb<E>) };

    // Call the event handler.
    cb(params)
}
