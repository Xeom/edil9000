use super::*;

pub struct Builder<'a, E: 'static>
{
    exec:  &'a Executor,
    event: Option<NonOwned<'a, Event<E>>>,
    flags: i32
}

impl<'a, E> Builder<'a, E>
{
    pub fn new(exec: &'a Executor) -> Self
    {
        Self { exec, flags: SYNC | ASYNC, event: None }
    }

    pub fn with_ignore_multiple(mut self) -> Self
    {
        self.flags |= IGNORE_MULTIPLE;
        self
    }

    pub fn with_async(mut self) -> Self
    {
        self.flags &= !SYNC;
        self.flags |= ASYNC;
        self
    }

    pub fn with_sync(mut self) -> Self
    {
        self.flags &= !ASYNC;
        self.flags |= SYNC;
        self
    }

    pub fn with_event<'evt, T>(mut self, event: T)
        -> Self
        where
            T: Into<Option<NonOwned<'evt, Event<E>>>>,
            'evt: 'a
    {
        self.event = event.into();
        self
    }

    pub fn build_thread_safe(mut self, cb: impl Fn(&E) + Sync + Send + 'static)
        -> Handler
    {
        self.flags |= THREAD_SAFE;
        self.build(cb)
    }

    pub fn build(self, cb: impl FnMut(&E) + Send + 'static) -> Handler
    {
        let mut c_ptr = NonNull::new(
                unsafe { txt_handler_create(self.exec.ptr()) }
            )
            .expect("Could not create handler");

        let mut cb: Box<Cb<E>> = Box::new(Box::new(cb));

        unsafe
        {
            txt_handler_set_cb(
                c_ptr.as_mut(),
                Some(c_callback::<E>),
                &mut *cb as *mut Cb<E> as *mut std::ffi::c_void
            )
        }

        unsafe { txt_handler_set_flags(c_ptr.as_mut(), self.flags) }

        if let Some(evt) = self.event
        {
            unsafe { txt_event_listen(evt.ptr(), c_ptr.as_mut()); }
        }

        Handler { c_ptr, _cb: cb }
    }

    pub fn get_flags(&self) -> i32 { self.flags }
}
