use super::*;

mod builder;
pub use builder::Builder;

mod callback;
use callback::{Cb, c_callback};

pub struct Handler
{
    c_ptr:   NonNull<txt_handler_s>,
    _cb:     Box<dyn std::any::Any>
}

unsafe impl Send for Handler {}
unsafe impl Sync for Handler {}

impl Handler
{
    pub fn builder<'a, E>(exec: &'a Executor) -> Builder<'a, E>
    {
        Builder::new(exec)
    }

    unsafe fn ptr(&self) -> *mut txt_handler_s { self.c_ptr.clone().as_mut() }

    pub fn get_flags(&self) -> i32
    {
        unsafe { txt_handler_get_flags(self.ptr()) }
    }
}

impl Drop for Handler
{
    fn drop(&mut self) { unsafe { txt_handler_destroy(self.ptr()) } }
}

// Allow Optional handlers to be used in Builder.with_event.
impl<'a, E> From<&'a Event<E>> for Option<NonOwned<'a, Event<E>>>
{
    fn from(event: &'a Event<E>) -> Option<NonOwned<'a, Event<E>>>
    {
        Some(event.as_non_owned())
    }
}

