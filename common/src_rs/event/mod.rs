use super::*;

pub mod handler;
pub use handler::Handler;

mod async_handler;
pub use async_handler::AsyncHandler;

mod executor;
pub use executor::Executor;

pub struct Event<E>
{
    c_ptr:   NonNull<txt_event_s>,
    _spooky: PhantomData<E>
}

pub const SYNC:            i32 = txt_handler_flags_TXT_EVENT_SYNC;
pub const ASYNC:           i32 = txt_handler_flags_TXT_EVENT_ASYNC;
pub const THREAD_SAFE:     i32 = txt_handler_flags_TXT_EVENT_THREAD_SAFE;
pub const IGNORE_MULTIPLE: i32 = txt_handler_flags_TXT_EVENT_IGNORE_MULTIPLE;

impl<E> OwnedPtr for Event<E>
{
    type Ptr = *mut txt_event_s;

    unsafe fn ptr(&self) -> *mut txt_event_s { self.c_ptr.clone().as_mut() }

    unsafe fn from_owned_ptr(ptr: *mut txt_event_s) -> Self
    {
        Self
        {
            c_ptr:   NonNull::new(ptr).expect("Could not create event"),
            _spooky: Default::default()
        }
    }
}

impl<E> Event<E>
    where E: Sync + Send + Sized
{
    pub fn trigger(&self, params: &E)
    {
        unsafe
        {
            // TODO: Error handling
            txt_event_trigger(
                self.ptr(),
                params as *const E as *const std::ffi::c_void,
                size_of::<E>()
            );
        }
    }

    pub fn new_sync() -> Self
    {
        unsafe { Self::from_owned_ptr(txt_event_create(size_of::<E>(), SYNC)) }
    }
}

impl<E> Default for Event<E>
    where E: Sync + Send + Copy + 'static
{
    fn default() -> Self
    {
        unsafe
        {
            Self::from_owned_ptr(txt_event_create(size_of::<E>(), SYNC | ASYNC))
        }
    }
}

// It is safe to send events between threads, or access the same event from
// different threads.
unsafe impl<E> Sync for Event<E> {}
unsafe impl<E> Send for Event<E> {}

impl<E> Drop for Event<E>
{
    fn drop(&mut self) { unsafe { txt_event_destroy(self.ptr()) } }
}

#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    fn from_owned_ptr()
    {
        // The pointer should be free'd automatically when the event is dropped.
        let _ = unsafe
            { Event::<()>::from_owned_ptr(txt_event_create(0, SYNC | ASYNC)) };
    }

    #[test]
    fn from_non_owned_pointer()
    {
        // The pointer should not be free'd when the event is dropped.
        let c_ptr = unsafe { txt_event_create(0, SYNC | ASYNC) };
        {
            let _ = unsafe { Event::<()>::from_non_owned_ptr(c_ptr) };
        }
        unsafe { txt_event_destroy(c_ptr) };
    }
}
