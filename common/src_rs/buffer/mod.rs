use super::*;

mod transaction;

pub struct Buffer
{
    c_ptr: NonNull<txt_buffer_s>,
}

pub type Modification = txt_modification_s;

unsafe impl Send for Buffer {}
unsafe impl Sync for Buffer {}

impl Default for Buffer
{
    fn default() -> Self
    {
        unsafe { Self::from_owned_ptr(txt_buffer_create()) }
    }
}

impl Drop for Buffer
{
    fn drop(&mut self)
    {
        unsafe { txt_buffer_destroy(self.ptr()); }
    }
}

impl OwnedPtr for Buffer
{
    type Ptr = *mut txt_buffer_s;

    unsafe fn ptr(&self) -> *mut txt_buffer_s { self.c_ptr.clone().as_mut() }

    unsafe fn from_owned_ptr(ptr: *mut txt_buffer_s) -> Self
    {
        Self { c_ptr: NonNull::new(ptr).expect("Could not create buffer") }
    }
}

impl Buffer
{
    pub fn contents(&self) -> String
    {
        self.cursor(&StartOfText).read().to_string()
    }

    pub fn num_lines(&self) -> usize
    {
        unsafe { txt_buffer_num_lines(self.ptr()) }
    }

    pub fn line_len(&self, row: usize) -> usize
    {
        unsafe { txt_buffer_line_len(self.ptr(), row) }
    }

    pub fn cursor<L>(&self, loc: &L) -> Cursor
        where L: CreateLocation
    {
        loc.create_cursor(&self.as_non_owned())
    }

    pub fn on_modify_font<'buf>(&'buf self) -> NonOwned<'buf, Event<()>>
    {
        unsafe
        {
            Event::from_non_owned_ptr(txt_buffer_on_modify_font(self.ptr()))
        }
    }

    pub fn on_modify_text<'buf>(&'buf self)
        -> NonOwned<'buf, Event<Modification>>
    {
        unsafe
        {
            Event::from_non_owned_ptr(txt_buffer_on_modify_text(self.ptr()))
        }
    }

    pub fn transaction(&self) -> transaction::Transaction<'_>
    {
        transaction::Transaction::new(self)
    }

    pub fn revision_id(&self) -> usize
    {
        unsafe { txt_buffer_revision_id(self.ptr()) }
    }
}

impl std::fmt::Write for Buffer
{
    fn write_str(&mut self, s: &str) -> std::fmt::Result
    {
        self.cursor(&EndOfText).write_str(s)
    }
}
