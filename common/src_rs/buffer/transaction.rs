use super::*;

type DontSend = PhantomData<std::sync::MutexGuard<'static, ()>>;

pub struct Transaction<'buf>
{
    buf:     &'buf Buffer,
    _spooky: DontSend
}

impl<'buf> Transaction<'buf>
{
    pub(super) fn new(buf: &'buf Buffer) -> Self
    {
        unsafe { txt_buffer_transaction_begin(buf.ptr()) }
        Self { buf, _spooky: Default::default() }
    }

    pub fn run<T>(self, cb: impl FnOnce() -> T) -> T
    {
        let res = cb();
        drop(self);
        res
    }
}

impl<'buf> Drop for Transaction<'buf>
{
    fn drop(&mut self)
    {
        unsafe { txt_buffer_transaction_end(self.buf.ptr()) }
    }
}
