use super::*;

#[derive(Clone, Copy)]
pub struct Colour(pub(super) txt_colour_s);

impl std::cmp::PartialEq for Colour
{
    fn eq(&self, oth: &Self) -> bool
    {
        self.0.r == oth.0.r && self.0.g == oth.0.g && self.0.b == oth.0.b
    }
}

impl Colour
{
    pub fn r(&self) -> u8 { self.0.r }

    pub fn g(&self) -> u8 { self.0.g }

    pub fn b(&self) -> u8 { self.0.b }

    pub fn to_3bit(&self) -> u8 { unsafe { txt_colour_to_3bit(self.0) } }

    pub fn to_8bit(&self) -> u8 { unsafe { txt_colour_to_8bit(self.0) } }

    pub fn is_bright(&self) -> bool { unsafe { txt_colour_is_bright(self.0) } }

    pub fn black()          -> Self { unsafe { Self(TXT_BLACK) } }
    pub fn red()            -> Self { unsafe { Self(TXT_RED) } }
    pub fn green()          -> Self { unsafe { Self(TXT_GREEN) } }
    pub fn yellow()         -> Self { unsafe { Self(TXT_YELLOW) } }
    pub fn blue()           -> Self { unsafe { Self(TXT_BLUE) } }
    pub fn magenta()        -> Self { unsafe { Self(TXT_MAGENTA) } }
    pub fn cyan()           -> Self { unsafe { Self(TXT_CYAN) } }
    pub fn white()          -> Self { unsafe { Self(TXT_WHITE) } }
    pub fn grey()           -> Self { unsafe { Self(TXT_GREY) } }
    pub fn bright_red()     -> Self { unsafe { Self(TXT_BRIGHT_RED) } }
    pub fn bright_green()   -> Self { unsafe { Self(TXT_BRIGHT_GREEN) } }
    pub fn bright_yellow()  -> Self { unsafe { Self(TXT_BRIGHT_YELLOW) } }
    pub fn bright_blue()    -> Self { unsafe { Self(TXT_BRIGHT_BLUE) } }
    pub fn bright_magenta() -> Self { unsafe { Self(TXT_BRIGHT_MAGENTA) } }
    pub fn bright_cyan()    -> Self { unsafe { Self(TXT_BRIGHT_CYAN) } }
    pub fn bright_white()   -> Self { unsafe { Self(TXT_BRIGHT_WHITE) } }
}

#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    fn colours_3bit()
    {
        assert_eq!(Colour::black()         .to_3bit(), 0);
        assert_eq!(Colour::red()           .to_3bit(), 1);
        assert_eq!(Colour::green()         .to_3bit(), 2);
        assert_eq!(Colour::yellow()        .to_3bit(), 3);
        assert_eq!(Colour::blue()          .to_3bit(), 4);
        assert_eq!(Colour::magenta()       .to_3bit(), 5);
        assert_eq!(Colour::cyan()          .to_3bit(), 6);
        assert_eq!(Colour::white()         .to_3bit(), 7);
        assert_eq!(Colour::grey()          .to_3bit(), 0);
        assert_eq!(Colour::bright_red()    .to_3bit(), 1);
        assert_eq!(Colour::bright_green()  .to_3bit(), 2);
        assert_eq!(Colour::bright_yellow() .to_3bit(), 3);
        assert_eq!(Colour::bright_blue()   .to_3bit(), 4);
        assert_eq!(Colour::bright_magenta().to_3bit(), 5);
        assert_eq!(Colour::bright_cyan()   .to_3bit(), 6);
        assert_eq!(Colour::bright_white()  .to_3bit(), 7);
    }

    #[test]
    fn colours_bright()
    {
        assert_eq!(Colour::black()         .is_bright(), false);
        assert_eq!(Colour::red()           .is_bright(), false);
        assert_eq!(Colour::green()         .is_bright(), false);
        assert_eq!(Colour::yellow()        .is_bright(), false);
        assert_eq!(Colour::blue()          .is_bright(), false);
        assert_eq!(Colour::magenta()       .is_bright(), false);
        assert_eq!(Colour::cyan()          .is_bright(), false);
        assert_eq!(Colour::white()         .is_bright(), false);
        assert_eq!(Colour::grey()          .is_bright(), true);
        assert_eq!(Colour::bright_red()    .is_bright(), true);
        assert_eq!(Colour::bright_green()  .is_bright(), true);
        assert_eq!(Colour::bright_yellow() .is_bright(), true);
        assert_eq!(Colour::bright_blue()   .is_bright(), true);
        assert_eq!(Colour::bright_magenta().is_bright(), true);
        assert_eq!(Colour::bright_cyan()   .is_bright(), true);
        assert_eq!(Colour::bright_white()  .is_bright(), true);
    }
}
