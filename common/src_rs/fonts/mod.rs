use super::*;

mod colour;
pub use colour::*;

mod font_attrs;
pub use font_attrs::*;

#[derive(Default, Clone, Copy)]
#[repr(transparent)]
pub struct FontId(u16);

impl FontId
{
    pub fn as_u16(&self) -> u16 { self.0 }
}

pub struct Fonts(NonNull<txt_fonts_s>);

unsafe impl Send for Fonts {}
unsafe impl Sync for Fonts {}

impl OwnedPtr for Fonts
{
    type Ptr = *mut txt_fonts_s;

    unsafe fn ptr(&self) -> *mut txt_fonts_s { self.0.clone().as_mut() }

    unsafe fn from_owned_ptr(ptr: *mut txt_fonts_s) -> Self
    {
        Self(NonNull::new(ptr).expect("Could not create fonts"))
    }
}

impl Default for Fonts
{
    fn default() -> Self { unsafe { Self::from_owned_ptr(txt_fonts_create()) } }
}

impl Drop for Fonts
{
    fn drop(&mut self) { unsafe { txt_fonts_destroy(self.ptr()) } }
}

impl Fonts
{
    pub fn get_id(&self, name: impl AsRef<str>) -> FontId
    {
        let name_c = CString::new(name.as_ref())
            .expect("Invalid name for font");

        FontId(unsafe { txt_fonts_get_id(self.ptr(), name_c.as_ptr()) })
    }

    pub fn set_attrs(&self, id: FontId, attrs: &FontAttrs)
    {
        unsafe { txt_fonts_set_attrs(self.ptr(), id.0, &attrs.0); }
    }

    pub fn get_attrs(&self, id: FontId) -> FontAttrs
    {
        let mut rtn = MaybeUninit::<txt_font_attrs_s>::uninit();
        unsafe { txt_fonts_get_attrs(self.ptr(), id.0, rtn.as_mut_ptr()); }

        FontAttrs(unsafe { rtn.assume_init() })
    }

    pub fn on_set_attrs(&self) -> NonOwned<'_, Event<u16>>
    {
        unsafe { Event::from_non_owned_ptr(txt_fonts_on_set_attrs(self.ptr())) }
    }
}

