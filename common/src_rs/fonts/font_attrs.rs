use super::*;

#[derive(Clone, Copy)]
pub struct FontAttrs(pub(super) txt_font_attrs_s);

impl Default for FontAttrs
{
    fn default() -> Self { unsafe { MaybeUninit::zeroed().assume_init() } }
}

impl std::cmp::PartialEq for FontAttrs
{
    fn eq(&self, oth: &Self) -> bool
    {
        unsafe { txt_font_attrs_eq(self.0, oth.0) }
    }
}

macro_rules! impl_flag
{
    // TODO: Use $concat_idents when stable.
    ($flag:ident, $set_flag:ident) =>
    {
        pub fn $flag(&self) -> bool { self.0.$flag }
        pub fn $set_flag(&mut self, val: bool)
        {
            self.0.$flag = val;
        }
    }
}

macro_rules! impl_colour
{
    // TODO: Use $concat_idents when stable.
    ($col:ident, $col_flag:ident, $set_col:ident) =>
    {
        pub fn $col(&self) -> Option<Colour>
        {
            if self.0.$col_flag { Some(Colour(self.0.$col)) } else { None }
        }

        pub fn $set_col(&mut self, col: impl Into<Option<Colour>>)
        {
            match col.into()
            {
                Some(c) =>
                {
                    self.0.$col_flag = true;
                    self.0.$col = c.0;
                }
                None =>
                {
                    self.0.$col_flag = false;
                }
            }
        }

    }
}

impl FontAttrs
{
    impl_flag!(bold,   set_bold);
    impl_flag!(dull,   set_dull);
    impl_flag!(under,  set_under);
    impl_flag!(blink,  set_blink);
    impl_flag!(invert, set_invert);
    impl_flag!(italic, set_italic);

    impl_colour!(fg, has_fg, set_fg);
    impl_colour!(bg, has_bg, set_bg);
}

#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    fn test_font_attr_eq()
    {
        let mut a = FontAttrs::default();
        a.set_bg(Colour::red());
        let b = FontAttrs::default();

        assert!(a != b);
        assert!(a == a);
        assert!(b == b);
    }
}
