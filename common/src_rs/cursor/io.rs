use super::*;

pub(super) struct CursorIo<'cur>
{
    c_ptr:   NonNull<txt_cursor_io_s>,
    _spooky: PhantomData<&'cur ()>
}

pub enum Encoding
{
    Ascii,
    Utf8,
    Utf32
}

pub enum LineEnding
{
    Unix,
    Dos,
    No
}

unsafe impl<'cur> Sync for CursorIo<'cur> {}
unsafe impl<'cur> Send for CursorIo<'cur> {}

impl<'cur> CursorIo<'cur>
{
    pub unsafe fn ptr(&self) -> *mut txt_cursor_io_s
    {
        self.c_ptr.clone().as_mut()
    }

    pub fn from_ptr(ptr: *mut txt_cursor_io_s) -> Self
    {
        Self
        {
            c_ptr:   NonNull::new(ptr).expect("Could not create cursor"),
            _spooky: Default::default()
        }
    }

    pub fn set_until(&mut self, end: &'cur Cursor)
    {
        unsafe { txt_cursor_io_set_until(self.ptr(), end.ptr()); }
    }

    pub fn set_until_eol(&mut self)
    {
        unsafe { txt_cursor_io_set_until_eol(self.ptr()); }
    }

    pub fn set_encoding(&mut self, enc: Encoding)
    {
        let val = match enc
            {
                Encoding::Ascii => txt_encoding_TXT_ENCODING_ASCII,
                Encoding::Utf8  => txt_encoding_TXT_ENCODING_UTF8,
                Encoding::Utf32 => txt_encoding_TXT_ENCODING_UTF32
            };

        unsafe { txt_cursor_io_set_encoding(self.ptr(), val); }
    }

    pub fn set_line_ending(&mut self, le: LineEnding)
    {
        let val = match le
            {
                LineEnding::Unix => txt_line_ending_TXT_LINE_ENDING_UNIX,
                LineEnding::Dos  => txt_line_ending_TXT_LINE_ENDING_DOS,
                LineEnding::No   => txt_line_ending_TXT_LINE_ENDING_NONE
            };

        unsafe { txt_cursor_io_set_line_ending(self.ptr(), val); }
    }

    pub fn set_font(&mut self, font: FontId)
    {
        unsafe { txt_cursor_io_set_font(self.ptr(), font.as_u16()); }
    }
}

impl<'cur> Drop for CursorIo<'cur>
{
    fn drop(&mut self)
    {
        unsafe { txt_cursor_io_destroy(self.ptr()); }
    }
}

