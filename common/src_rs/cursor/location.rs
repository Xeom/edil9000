use super::*;

pub trait CreateLocation
{
    fn create_cursor(&self, buffer: &NonOwned<'_, Buffer>) -> Cursor;
}

pub trait QueryLocation
{
    fn query_cursor(cursor: &Cursor) -> Self;
}

pub struct StartOfText;
pub struct EndOfText;
pub struct Row(pub usize);

#[derive(PartialEq, Debug, Clone, Copy)]
pub struct Coord { pub row: usize , pub col: usize }

#[derive(PartialEq, Debug)]
pub struct Offset(pub usize);

#[derive(PartialEq, Debug)]
pub struct Utf8Byte(pub usize);

impl CreateLocation for Row
{
    fn create_cursor(&self, buffer: &NonOwned<'_, Buffer>) -> Cursor
    {
        Coord { row: self.0, col: 0 }.create_cursor(buffer)
    }
}

impl CreateLocation for EndOfText
{
    fn create_cursor(&self, buffer: &NonOwned<'_, Buffer>) -> Cursor
    {
        unsafe
        {
            Cursor::from_owned_ptr(txt_cursor_create_end_of_text(buffer.ptr()))
        }
    }
}

impl CreateLocation for StartOfText
{
    fn create_cursor(&self, buffer: &NonOwned<'_, Buffer>) -> Cursor
    {
        Offset(0).create_cursor(buffer)
    }
}

impl QueryLocation for Coord
{
    fn query_cursor(cursor: &Cursor) -> Self
    {
        let coord = unsafe { txt_cursor_coord(cursor.ptr()) };
        Self { row: coord.row, col: coord.col }
    }
}

impl CreateLocation for Coord
{
    fn create_cursor(&self, buffer: &NonOwned<'_, Buffer>) -> Cursor
    {
        let Self { row, col } = *self;
        unsafe
        {
            Cursor::from_owned_ptr(
                txt_cursor_create_coord(buffer.ptr(), txt_coord_s { row, col })
            )
        }
    }
}

impl QueryLocation for Offset
{
    fn query_cursor(cursor: &Cursor) -> Self
    {
        Self(unsafe { txt_cursor_offset(cursor.ptr()) })
    }
}

impl CreateLocation for Offset
{
    fn create_cursor(&self, buffer: &NonOwned<'_, Buffer>) -> Cursor
    {
        unsafe
        {
            Cursor::from_owned_ptr(
                txt_cursor_create_offset(buffer.ptr(), self.0)
            )
        }
    }
}

impl QueryLocation for Utf8Byte
{
    fn query_cursor(cursor: &Cursor) -> Self
    {
        Self(unsafe { txt_cursor_utf8_byte(cursor.ptr()) })
    }
}

impl CreateLocation for Utf8Byte
{
    fn create_cursor(&self, buffer: &NonOwned<'_, Buffer>) -> Cursor
    {
        unsafe
        {
            Cursor::from_owned_ptr(
                txt_cursor_create_utf8_byte(buffer.ptr(), self.0)
            )
        }
    }
}
