use super::*;

pub struct CursorRead<'cur>(pub(super) CursorIo<'cur>);

impl<'cur> CursorRead<'cur>
{
    pub fn with_encoding(mut self, enc: Encoding) -> Self
    {
        self.0.set_encoding(enc); self
    }

    pub fn with_line_ending(mut self, le: LineEnding) -> Self
    {
        self.0.set_line_ending(le); self
    }

    pub fn until(mut self, end: &'cur Cursor) -> Self
    {
        self.0.set_until(end); self
    }

    pub fn until_eol(mut self) -> Self
    {
        self.0.set_until_eol(); self
    }

    pub fn to_bytes(mut self) -> Vec<u8>
    {
        let mut out = Vec::<u8>::new();
        std::io::copy(&mut self, &mut out)
            .expect("Could not read cursor to string");

        out
    }

    pub fn to_string(mut self) -> String
    {
        self.0.set_encoding(Encoding::Utf8);
        String::from_utf8(self.to_bytes())
            .unwrap_or_else(
                |e| String::from_utf8_lossy(e.as_bytes()).into_owned()
            )
    }

    pub fn to_codepoints(mut self) -> Vec<char>
    {
        self.0.set_encoding(Encoding::Utf32);

        let mut out = Vec::<char>::new();
        loop
        {
            out.reserve(2048);
            let to_fill = out.spare_capacity_mut();
            let num_bytes = unsafe
                {
                    txt_cursor_io_read(
                        self.0.ptr(),
                        to_fill.as_mut_ptr() as *mut char as *mut u8,
                        to_fill.len() * size_of::<char>()
                    )
                };

            if num_bytes == 0 { break; }

            assert!(num_bytes % 4 == 0);
            unsafe { out.set_len(out.len() + num_bytes / 4); }
        }

        out
    }
}

impl<'cur> std::io::Read for CursorRead<'cur>
{
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize>
    {
        Ok(unsafe {
            txt_cursor_io_read(self.0.ptr(), buf.as_mut_ptr(), buf.len())
        })
    }
}

