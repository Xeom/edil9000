use super::*;

mod location;
pub use location::*;

mod read;
pub use read::CursorRead;

mod write;
pub use write::CursorWrite;

mod io;
pub use io::*;

pub struct Cursor
{
    c_ptr:   NonNull<txt_cursor_s>
}

unsafe impl Sync for Cursor {}
unsafe impl Send for Cursor {}

impl OwnedPtr for Cursor
{
    type Ptr = *mut txt_cursor_s;

    unsafe fn ptr(&self) -> *mut txt_cursor_s { self.c_ptr.clone().as_mut() }

    unsafe fn from_owned_ptr(ptr: *mut txt_cursor_s) -> Self
    {
        Self { c_ptr: NonNull::new(ptr).expect("Could not create cursor") }
    }
}

impl PartialEq for Cursor
{
    fn eq(&self, oth: &Self) -> bool
    {
        self.partial_cmp(oth) == Some(std::cmp::Ordering::Equal)
    }
}

impl PartialOrd for Cursor
{
    fn partial_cmp(&self, oth: &Self) -> Option<std::cmp::Ordering>
    {
        let mut ordering: i32 = 0;
        let res =
            unsafe { txt_cursor_cmp(self.ptr(), oth.ptr(), &mut ordering) };
        if res == txt_err_TXT_OK
        {
            PartialOrd::partial_cmp(&ordering, &0)
        }
        else
        {
            None
        }
    }
}

impl std::fmt::Debug for Cursor
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        f.debug_struct("Cursor")
            .field("col", &self.col())
            .field("row", &self.row())
            .finish()
    }
}

impl Cursor
{
    pub fn dup(&self) -> Self
    {
        unsafe { Self::from_owned_ptr(txt_cursor_create_dup(self.ptr())) }
    }

    pub fn next_line(&self) -> Option<Self> { self.relative_line(1) }

    pub fn prev_line(&self) -> Option<Self> { self.relative_line(-1) }

    pub fn start_of_line(&self) -> Self
    {
        self.relative_line(0)
            .unwrap_or_else(|| self.dup())
    }

    pub fn relative_line(&self, n: i64) -> Option<Self>
    {
        let ptr = unsafe { txt_cursor_create_relative_line(self.ptr(), n) };

        if !ptr.is_null()
        {
            Some(unsafe { Self::from_owned_ptr(ptr) })
        }
        else
        {
            None
        }
    }

    pub fn location<L: QueryLocation>(&self) -> L { L::query_cursor(self) }

    pub fn row(&self) -> usize { self.location::<Coord>().row }

    pub fn col(&self) -> usize { self.location::<Coord>().col }

    pub fn moved(&self, n: i64) -> Self
    {
        let rtn = self.dup();
        rtn.move_by(n);
        rtn
    }

    pub fn move_by(&self, n: i64)
    {
        unsafe { txt_cursor_move_by(self.ptr(), n) }
    }

    pub fn move_to(&self, oth: &Cursor)
    {
        // TODO: Check error
        unsafe { txt_cursor_move_to(self.ptr(), oth.ptr()); }
    }

    pub fn insert_forward(&self, ch: char)
    {
        unsafe { txt_cursor_insert_forward(self.ptr(), ch as u32) }
    }

    pub fn insert_backward(&self, ch: char)
    {
        unsafe { txt_cursor_insert_backward(self.ptr(), ch as u32) }
    }

    pub fn delete_forward(&self)
    {
        unsafe { txt_cursor_delete_forward(self.ptr()) }
    }

    pub fn delete_backward(&self)
    {
        unsafe { txt_cursor_delete_backward(self.ptr()) }
    }

    fn create_io<'cur>(&'cur self) -> CursorIo<'cur>
    {
        unsafe { CursorIo::from_ptr(txt_cursor_io_create(self.ptr())) }
    }

    pub fn read<'cur>(&'cur self) -> CursorRead<'cur>
    {
        CursorRead(self.create_io())
    }

    pub fn write<'cur>(&'cur self) -> CursorWrite<'cur>
    {
        CursorWrite(self.create_io())
    }

    pub fn on_move<'cur>(&'cur self) -> NonOwned<'cur, Event<()>>
    {
        unsafe { Event::from_non_owned_ptr(txt_cursor_on_move(self.ptr())) }
    }

    pub fn delete_between(a: &Self, b: &Self)
    {
        unsafe
        {
            // TODO Handle errors
            txt_cursor_delete_between(a.ptr(), b.ptr());
        }
    }

    pub fn set_font(&self, font: FontId, len: usize)
    {
        unsafe { txt_cursor_set_font(self.ptr(), font.as_u16(), len); }
    }

    pub fn get_fonts(&self, n: usize) -> Vec<FontId>
    {
        let mut rtn = Vec::with_capacity(n);

        let ptr = rtn.as_mut_ptr() as *mut u16;
        unsafe { txt_cursor_get_font(self.ptr(), ptr, n); }
        unsafe { rtn.set_len(n); }

        rtn
    }

    pub fn buffer<'cur>(&'cur self) -> NonOwned<'cur, Buffer>
    {
        unsafe { Buffer::from_non_owned_ptr(txt_cursor_buffer(self.ptr())) }
    }
}

impl std::fmt::Write for Cursor
{
    fn write_str(&mut self, s: &str) -> std::fmt::Result
    {
        use std::io::Write;
        self.write().write_all(s.as_bytes())
            .expect("Could not write string to buffer");

        Ok(())
    }
}

impl Drop for Cursor
{
    fn drop(&mut self) { unsafe { txt_cursor_destroy(self.ptr()) } }
}

