use super::*;

pub struct CursorWrite<'cur>(pub(super) CursorIo<'cur>);

impl<'cur> CursorWrite<'cur>
{
    pub fn with_encoding(mut self, enc: Encoding) -> Self
    {
        self.0.set_encoding(enc); self
    }

    pub fn with_line_ending(mut self, le: LineEnding) -> Self
    {
        self.0.set_line_ending(le); self
    }

    pub fn set_font(&mut self, font: FontId)
    {
        self.0.set_font(font);
    }
}

impl<'cur> std::io::Write for CursorWrite<'cur>
{
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize>
    {
        unsafe { txt_cursor_io_write(self.0.ptr(), buf.as_ptr(), buf.len()) };
        Ok(buf.len())
    }

    fn flush(&mut self) -> std::io::Result<()> { Ok(()) }
}
