#include "txt_str.h"

#include <stdlib.h>
#include <string.h>

struct txt_utf8
{
    size_t len;
    uint8_t bytes[];
};

txt_utf8_s *txt_utf8_create(const uint8_t *bytes, size_t len)
{
    txt_utf8_s *s = malloc(sizeof(txt_utf8_s) + len);
    if (s)
    {
        s->len = len;
        memcpy(s->bytes, bytes, len);
    }

    return s;
}

void txt_utf8_destroy(txt_utf8_s *s)
{
    free(s);
}

size_t txt_utf8_len(const txt_utf8_s *s)
{
    return s->len;
}

const uint8_t *txt_utf8_bytes(const txt_utf8_s *s)
{
    return s->bytes;
}

