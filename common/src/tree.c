#include "tree.h"

#include <string.h>
#include <stdbool.h>
#if SNOW_ENABLED
# include <assert.h>
#endif

static const int DELTA = 3;
static const int GAMMA = 2;

__attribute__((warn_unused_result))
static txt_tree_node_s *insert_after(
    txt_tree_node_s *node,
    txt_tree_node_s *to_ins
);

__attribute__((warn_unused_result))
static txt_tree_node_s *insert_before(
    txt_tree_node_s *node,
    txt_tree_node_s *to_ins
);

static void recount_node(txt_tree_node_s *node);

__attribute__((warn_unused_result))
static txt_tree_node_s *balance_node(txt_tree_node_s *node);

static inline size_t node_size_by(const txt_tree_node_s *node, txt_weight_s w)
{
    return (node && w.id < TXT_TREE_MAX_WEIGHTS) ? node->weight_total[w.id] : 0;
}

// Get the size of a node.
static inline size_t node_size(const txt_tree_node_s *node)
{
    return node_size_by(node, TXT_TREE_NUM_NODES);
}

// Set a node as the left child of another, updating nothing.
static void set_left(txt_tree_node_s *parent, txt_tree_node_s *left)
{
#if SNOW_ENABLED
    assert(parent != left);
#endif

    parent->left = left;
    if (left)
        left->parent = parent;
    recount_node(parent);
}

// Set a child as the right child of another, updating nothing.
static void set_right(txt_tree_node_s *parent, txt_tree_node_s *right)
{
#if SNOW_ENABLED
    assert(parent != right);
#endif

    parent->right = right;
    if (right)
        right->parent = parent;
    recount_node(parent);
}

static txt_tree_node_s *insert_first(
    txt_tree_node_s *node,
    txt_tree_node_s *to_ins
)
{
    if (node)
    {
        set_left(node, insert_first(node->left, to_ins));
        return balance_node(node);
    }
    else
    {
        return to_ins;
    }
}

static txt_tree_node_s *insert_last(
    txt_tree_node_s *node,
    txt_tree_node_s *to_ins
)
{
    if (node)
    {
        set_right(node, insert_last(node->right, to_ins));
        return balance_node(node);
    }
    else
    {
        return to_ins;
    }
}

// Insert a node after another, and return the node that should replace it.
static txt_tree_node_s *insert_after(
    txt_tree_node_s *node,
    txt_tree_node_s *to_ins
)
{
#if SNOW_ENABLED
    assert(node != to_ins);
#endif
    if (node)
    {
        set_right(node, insert_first(node->right, to_ins));
        return balance_node(node);
    }
    else
    {
        return to_ins;
    }
}

// Insert a node before another, and return the node that should replace it.
static txt_tree_node_s *insert_before(
    txt_tree_node_s *node,
    txt_tree_node_s *to_ins
)
{
#if SNOW_ENABLED
    assert(node != to_ins);
#endif
    if (node)
    {
        set_left(node, insert_last(node->left, to_ins));
        return balance_node(node);
    }
    else
    {
        return to_ins;
    }
}

// Rotate a tree right.
static txt_tree_node_s *rotate_right(txt_tree_node_s *root)
{
    txt_tree_node_s *pivot = root->left;

    set_left(root, pivot->right);
    set_right(pivot, root);

    return pivot;
}

// Rotate a tree left.
static txt_tree_node_s *rotate_left(txt_tree_node_s *root)
{
    txt_tree_node_s *pivot = root->right;

    set_right(root, pivot->left);
    set_left(pivot, root);

    return pivot;
}

// Rotate a tree double right.
static txt_tree_node_s *rotate_double_right(txt_tree_node_s *root)
{
    set_left(root, rotate_left(root->left));
    return rotate_right(root);
}

// Rotate a tree double left.
static txt_tree_node_s *rotate_double_left(txt_tree_node_s *root)
{
    set_right(root, rotate_right(root->right));
    return rotate_left(root);
}

// Remove a node from the tree, and return the node that should replace it.
static txt_tree_node_s *remove_node(txt_tree_node_s *to_rem)
{
    txt_tree_node_s *new;

    bool left  = to_rem->left != NULL;
    bool right = to_rem->right != NULL;

    if (left && right)
    {
        // Snip off the right tree, and remove the first element. This is our
        // new root.
        to_rem->right->parent = NULL;
        txt_tree_s tmp_tree = { .root = to_rem->right };
        new = txt_tree_index(&tmp_tree, 0, TXT_TREE_NUM_NODES);
        txt_tree_remove(&tmp_tree, new);

        // Replace the subtrees.
        set_left(new, to_rem->left);
        set_right(new, tmp_tree.root);

        new = balance_node(new);
    }
    else if (left)
    {
        new = to_rem->left;
    }
    else if (right)
    {
        new = to_rem->right;
    }
    else
    {
        new = NULL;
    }

    to_rem->left = NULL;
    to_rem->right = NULL;
    to_rem->parent = NULL;

    return new;
}

static void recount_node(txt_tree_node_s *node)
{
    for (size_t w = 0; w < TXT_TREE_MAX_WEIGHTS; ++w)
    {
        node->weight_total[w] = node->weight_this[w] +
            ((node->left) ? node->left->weight_total[w] : 0) +
            ((node->right) ? node->right->weight_total[w] : 0);
    }
}

static inline size_t weight_l(const txt_tree_node_s *node)
{
    return 1 + node_size(node->left);
}

static inline size_t weight_r(const txt_tree_node_s *node)
{
    return 1 + node_size(node->right);
}

// Rebalance a single node and those below it.
static txt_tree_node_s *balance_node(txt_tree_node_s *node)
{
    while (DELTA * weight_l(node) < weight_r(node))
    {
        if (!node->right ||
            weight_l(node->right) < GAMMA * weight_r(node->right))
        {
            node = rotate_left(node);
        }
        else
        {
            node = rotate_double_left(node);
        }
    }

    while (DELTA * weight_r(node) < weight_l(node))
    {
        if (!node->left || weight_r(node->left) < GAMMA * weight_l(node->left))
        {
            node = rotate_right(node);
        }
        else
        {
            node = rotate_double_right(node);
        }
    }

    return node;
}

// Replace a node in a tree with another.
static void replace_in_tree(
    txt_tree_s      *tree,
    txt_tree_node_s *parent,
    txt_tree_node_s *old,
    txt_tree_node_s *new
)
{
    if (parent)
    {
        if (old != new)
        {
            if (old == parent->left)
                set_left(parent, new);
            else
                set_right(parent, new);
        }
        else
        {
            recount_node(parent);
        }

        txt_tree_node_s *grandpa = parent->parent;

        replace_in_tree(tree, grandpa, parent, balance_node(parent));
    }
    else
    {
        tree->root = new;

        if (new)
            new->parent = NULL;
    }
}

void txt_tree_init(txt_tree_s *tree)
{
    tree->root = NULL;
}

size_t txt_tree_size_by(const txt_tree_s *tree, txt_weight_s weight)
{
    return tree->root ? node_size_by(tree->root, weight) : 0;
}

size_t txt_tree_get_weight(const txt_tree_node_s *node, txt_weight_s weight)
{
    return (node && weight.id < TXT_TREE_MAX_WEIGHTS) ?
        node->weight_this[weight.id] : 0;
}

void txt_tree_set_weight(txt_tree_node_s *node, txt_weight_s weight, size_t v)
{
    if (weight.id < TXT_TREE_MAX_WEIGHTS)
    {
        node->weight_this[weight.id] = v;

        for (; node; node = node->parent)
            recount_node(node);
    }
}

void txt_tree_remove(txt_tree_s *tree, txt_tree_node_s *to_rem)
{
    txt_tree_node_s *parent = to_rem->parent;
    replace_in_tree(tree, parent, to_rem, remove_node(to_rem));
}

void txt_tree_insert_before(
    txt_tree_s      *tree,
    txt_tree_node_s *node,
    txt_tree_node_s *to_ins
)
{
    memset(to_ins, '\0', sizeof(*to_ins));
    txt_tree_set_weight(to_ins, TXT_TREE_NUM_NODES, 1);

    if (node)
    {
        txt_tree_node_s *parent = node->parent;
        replace_in_tree(tree, parent, node, insert_before(node, to_ins));
    }
    else if (tree->root)
    {
        txt_tree_insert_after(tree, txt_tree_last(tree), to_ins);
    }
    else
    {
        tree->root = to_ins;
        to_ins->parent = NULL;
    }
}

void txt_tree_insert_after(
    txt_tree_s      *tree,
    txt_tree_node_s *node,
    txt_tree_node_s *to_ins
)
{
    memset(to_ins, '\0', sizeof(*to_ins));
    txt_tree_set_weight(to_ins, TXT_TREE_NUM_NODES, 1);

    if (node)
    {
        txt_tree_node_s *parent = node->parent;
        replace_in_tree(tree, parent, node, insert_after(node, to_ins));
    }
    else if (tree->root)
    {
        txt_tree_insert_before(tree, txt_tree_first(tree), to_ins);
    }
    else
    {
        tree->root = to_ins;
        to_ins->parent = NULL;
    }
}

txt_tree_node_s *txt_tree_last(txt_tree_s *tree)
{
    txt_tree_node_s *iter = tree->root;
    while (iter && iter->right)
        iter = iter->right;

    return iter;
}

txt_tree_node_s *txt_tree_first(txt_tree_s *tree)
{
    txt_tree_node_s *iter = tree->root;
    while (iter && iter->left)
        iter = iter->left;

    return iter;
}

static inline txt_tree_node_s *index_by_node(
    txt_tree_node_s *node,
    size_t           index,
    txt_weight_s     weight
)
{
    if (node->left)
    {
        size_t sum_left = node_size_by(node->left, weight);
        if (sum_left > index)
            return index_by_node(node->left, index, weight);
    }

    if (node->right)
    {
        size_t sum_right = node_size_by(node->right, weight);
        size_t sum_this  = node_size_by(node, weight);

        size_t sum_before_right = sum_this - sum_right;

        if (sum_before_right <= index)
            return index_by_node(node->right, index - sum_before_right, weight);
    }

    return node;
}

txt_tree_node_s *txt_tree_index(
    txt_tree_s  *tree,
    size_t       index,
    txt_weight_s weight
)
{
    txt_tree_node_s *root = tree->root;

    /* The only case where this function returns NULL is an empty tree. */
    if (!root) return NULL;

    return index_by_node(root, index, weight);
}

int txt_tree_cmp(const txt_tree_node_s *a, const txt_tree_node_s *b)
{
    if (a == b)
        return 0;

    if (node_size(a) < node_size(b))
    {
        if (a->parent == b)
            return (b->left == a) ? -1 : 1;
        else
            return txt_tree_cmp(a->parent, b);
    }
    else
    {
        if (b->parent == a)
            return (a->left == b) ? 1 : -1;
        else
            return txt_tree_cmp(a, b->parent);
    }
}

/* find-by, but return a weight including the weight of the passed node. */
static inline size_t find_by_incl(
    const txt_tree_node_s *node,
    txt_weight_s           weight
)
{
    size_t weight_right = node->right ? node_size_by(node->right, weight) : 0;

    if (node->parent)
    {
        if (node == node->parent->left)
            return txt_tree_find(node->parent, weight) - weight_right;
        else
            return find_by_incl(node->parent, weight) +
                node_size_by(node, weight) - weight_right;
    }
    else
    {
        return node_size_by(node, weight) - weight_right;
    }
}

size_t txt_tree_find(const txt_tree_node_s *node, txt_weight_s weight)
{
    size_t weight_left = node->left ? node_size_by(node->left, weight) : 0;

    if (node->parent)
    {
        if (node == node->parent->left)
            return txt_tree_find(node->parent, weight) -
                (node_size_by(node, weight) - weight_left);
        else
            return find_by_incl(node->parent, weight) + weight_left;
    }
    else
    {
        return weight_left;
    }
}

txt_tree_node_s *txt_tree_next(txt_tree_node_s *node)
{
    if (node->right)
    {
        node = node->right;
        while (node->left) node = node->left;
        return node;
    }

    while (node->parent)
    {
        if (node->parent->left == node)
            return node->parent;
        else
            node = node->parent;
    }

    return NULL;
}

txt_tree_node_s *txt_tree_prev(txt_tree_node_s *node)
{
    if (node->left)
    {
        node = node->left;
        while (node->right) node = node->right;
        return node;
    }

    while (node->parent)
    {
        if (node->parent->right == node)
            return node->parent;
        else
            node = node->parent;
    }

    return NULL;
}

static txt_tree_node_s *bisect_node(
    txt_tree_node_s *node,
    int cmp_cb(const txt_tree_node_s *, const void *),
    const void *param
)
{
    if (!node)
        return NULL;

    int res = cmp_cb(node, param);

    if (res > 0)
    {
        return bisect_node(node->left, cmp_cb, param);
    }
    else /* res <= 0 */
    {
        txt_tree_node_s *closer = bisect_node(node->right, cmp_cb, param);

        return closer ? closer : node;
    }
}

txt_tree_node_s *txt_tree_bisect(
    txt_tree_s *tree,
    int cmp_cb(const txt_tree_node_s *, const void *),
    const void *param
)
{
    return bisect_node(tree->root, cmp_cb, param);
}

#include <snow/snow.h>
#if SNOW_ENABLED

static void verify_node(txt_tree_node_s *node)
{
    if (!node)
        return;

    asserteq(node_size(node), 1 + node_size(node->left) + node_size(node->right));
    assert(DELTA * weight_r(node) >= weight_l(node));
    assert(DELTA * weight_l(node) >= weight_r(node));

    if (node->left)
        asserteq_ptr(node->left->parent, node);

    if (node->right)
        asserteq_ptr(node->right->parent, node);

    verify_node(node->left);
    verify_node(node->right);
}

static void assert_tree_contents(txt_tree_s *tree, txt_tree_node_s **nodes)
{
    size_t idx;
    asserteq_ptr(txt_tree_first(tree), *nodes);
    for (idx = 0; nodes[idx]; ++idx)
    {
        asserteq_ptr(txt_tree_index(tree, idx, TXT_TREE_NUM_NODES), nodes[idx]);
        asserteq(txt_tree_find(nodes[idx], TXT_TREE_NUM_NODES), idx);
    }

    asserteq_ptr(txt_tree_last(tree), (idx == 0) ? NULL : nodes[idx - 1]);
    asserteq(txt_tree_size(tree), idx);

    verify_node(tree->root);
}

#define ASSERT_TREE_CONTENTS(_tree, ...) \
    assert_tree_contents(&_tree, (txt_tree_node_s*[]){ __VA_ARGS__, NULL });

#define ASSERT_TREE_EMPTY(_tree) \
    assert_tree_contents(&_tree, (txt_tree_node_s*[]){ NULL });

describe(tree)
{
    txt_tree_s tree;
    txt_tree_node_s n[16];

    before_each()
    {
        txt_tree_init(&tree);
    }

    it ("Inserts after NULL")
    {
        for (int i = 8; i >= 0; --i)
            txt_tree_insert_after(&tree, NULL, &n[i]);

        ASSERT_TREE_CONTENTS(
            tree, &n[0], &n[1], &n[2], &n[3], &n[4], &n[5], &n[6], &n[7], &n[8]
        );
    }

    it ("Inserts before NULL")
    {
        for (int i = 0; i <= 8; ++i)
            txt_tree_insert_before(&tree, NULL, &n[i]);

        ASSERT_TREE_CONTENTS(
            tree, &n[0], &n[1], &n[2], &n[3], &n[4], &n[5], &n[6], &n[7], &n[8]
        );
    }

    it ("Inserts after other")
    {
        for (int i = 0; i <= 8; ++i)
            txt_tree_insert_after(&tree, txt_tree_last(&tree), &n[i]);

        ASSERT_TREE_CONTENTS(
            tree, &n[0], &n[1], &n[2], &n[3], &n[4], &n[5], &n[6], &n[7], &n[8]
        );
    }

    it ("Inserts before other")
    {
        for (int i = 8; i >= 0; --i)
            txt_tree_insert_before(&tree, txt_tree_first(&tree), &n[i]);

        ASSERT_TREE_CONTENTS(
            tree, &n[0], &n[1], &n[2], &n[3], &n[4], &n[5], &n[6], &n[7], &n[8]
        );
    }

    it ("Removes elements")
    {
        for (size_t i = 0; i < 16; ++i)
            txt_tree_insert_after(&tree, NULL, &n[i]);

        for (size_t i = 0; i < 16; ++i)
            txt_tree_remove(&tree, &n[i]);

        ASSERT_TREE_EMPTY(tree);
    }

    it ("Iterates forward")
    {
        for (size_t i = 0; i < 16; ++i)
            txt_tree_insert_before(&tree, NULL, &n[i]);

        for (size_t i = 0; i < 15; ++i)
            asserteq_ptr(txt_tree_next(&n[i]), &n[i + 1]);
        asserteq_ptr(txt_tree_next(&n[15]), NULL);
    }

    it ("Iterates backward")
    {
        for (size_t i = 0; i < 16; ++i)
            txt_tree_insert_before(&tree, NULL, &n[i]);

        for (size_t i = 0; i < 15; ++i)
            asserteq_ptr(txt_tree_prev(&n[i + 1]), &n[i]);
        asserteq_ptr(txt_tree_prev(&n[0]), NULL);
    }

    it ("Finds indices of nodes")
    {
        for (size_t i = 0; i < 16; ++i)
            txt_tree_insert_before(&tree, NULL, &n[i]);

        for (size_t i = 0; i < 16; ++i)
            asserteq(txt_tree_find(&n[i], TXT_TREE_NUM_NODES), i);
    }

    it ("Compares nodes")
    {
        for (size_t a = 0; a < 16; ++a)
        for (size_t b = 0; b < 16; ++b)
        {
            int expect;
            if (a == b) expect =  0;
            if (a  > b) expect =  1;
            if (a  < b) expect = -1;

            asserteq(txt_tree_cmp(&n[a], &n[b]), expect);
        }
    }

    it ("Gets weight of nodes")
    {
        txt_weight_s w = { .id = 1 };
        asserteq(txt_tree_get_weight(&n[0], w), 0);
        txt_tree_set_weight(&n[0], w, 500);
        asserteq(txt_tree_get_weight(&n[0], w), 500);
    }

    it ("Gets size of tree by weights")
    {
        txt_weight_s w = { .id = 1 };

        for (size_t i = 0; i < 16; ++i)
            txt_tree_insert_before(&tree, NULL, &n[i]);

        for (size_t i = 0; i < 16; ++i)
            txt_tree_set_weight(&n[i], w, i);

        asserteq(txt_tree_size_by(&tree, w), 120);
    }

    it ("Finds by weights")
    {
        txt_weight_s w = { .id = 1 };

        for (size_t i = 0; i < 16; ++i)
            txt_tree_insert_before(&tree, NULL, &n[i]);

        txt_tree_set_weight(&n[5], w, 1);
        txt_tree_set_weight(&n[8], w, 2);

        asserteq_ptr(txt_tree_index(&tree, 0, w), &n[5]);
        asserteq_ptr(txt_tree_index(&tree, 1, w), &n[8]);
        asserteq_ptr(txt_tree_index(&tree, 2, w), &n[8]);
        asserteq_ptr(txt_tree_index(&tree, 3, w), &n[15]);
    }


    it ("Survives random fuzzing")
    {
        txt_tree_s second_tree;
        txt_tree_init(&second_tree);

        for (size_t i = 0; i < 16; ++i)
            txt_tree_insert_after(&tree, NULL, &n[i]);

        for (int i = 0; i < 100000; ++i)
        {
            bool swap = (rand() & 1);

            txt_tree_s *src = swap ? &tree : &second_tree;
            txt_tree_s *dst = swap ? &second_tree : &tree;

            if (txt_tree_size(src))
            {
                size_t src_ind = rand() % txt_tree_size(src);
                size_t dst_ind = rand() % (txt_tree_size(dst) + 1);

                txt_tree_node_s *src_node =
                    txt_tree_index(src, src_ind, TXT_TREE_NUM_NODES);

                txt_tree_node_s *dst_node =
                    txt_tree_index(dst, dst_ind, TXT_TREE_NUM_NODES);

                bool before = rand() & 1;

#if 0
                printf(
                    "Taking node %zu (%p) and putting it %s node %zu (%p)\n",
                    src_ind, src_node, before ? "before" : "after",
                    dst_ind, dst_node
                );
                printf(
                    "Source tree contains %zu, and destination contains %zu\n",
                    txt_tree_size(src), txt_tree_size(dst)
                );
#endif

                txt_tree_remove(src, src_node);

                if (before)
                {
                    txt_tree_insert_before(dst, dst_node, src_node);
                    if (dst_node)
                        asserteq_ptr(txt_tree_prev(dst_node), src_node);
                }
                else
                {
                    txt_tree_insert_after(dst, dst_node, src_node);
                    if (dst_node)
                        asserteq_ptr(txt_tree_next(dst_node), src_node);
                }

                asserteq(txt_tree_size(&tree) + txt_tree_size(&second_tree), 16);
            }

            verify_node(src->root);
            verify_node(dst->root);
        }
    }
}

#endif
