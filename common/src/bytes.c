#include "bytes.h"

#include "macros.h"

#include <stdlib.h>
#include <string.h>

txt_bytes_s txt_bytes_clone(txt_bytes_s to_dup)
{
    uint8_t *data = malloc(to_dup.len);
    memcpy(data, to_dup.data, to_dup.len);

    return (txt_bytes_s){ .len = to_dup.len, .data = data };
}

int txt_bytes_cmp(txt_bytes_s l, txt_bytes_s r)
{
    int min_len = MIN(l.len, r.len);
    int cmp = min_len ? memcmp(l.data, r.data, min_len) : 0;

    if (cmp != 0)
        return cmp;

    if (l.len > r.len)
        return 1;

    if (l.len < r.len)
        return -1;

    return 0;
}
