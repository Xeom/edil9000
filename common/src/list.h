#if !defined(TXT_LIST_H)
# define TXT_LIST_H
# include <stddef.h>
# include <stdbool.h>

typedef struct txt_list txt_list_s;

struct txt_list
{
    txt_list_s *next;
    txt_list_s *prev;
};

static inline void txt_list_init(txt_list_s *list)
{
    list->next = list;
    list->prev = list;
}

static inline void txt_list_grab(txt_list_s *list)
{
    list->prev->next = list->next;
    list->next->prev = list->prev;

    list->next = list;
    list->prev = list;
}

static inline void txt_list_join(txt_list_s *a, txt_list_s *b)
{
    txt_list_s *a_last = a->prev;
    txt_list_s *b_last = b->prev;

    a_last->next = b;
    b->prev = a_last;

    b_last->next = a;
    a->prev = b_last;
}

static inline void txt_list_join_after(txt_list_s *a, txt_list_s *b)
{
    txt_list_join(a->next, b);
}

static inline bool txt_list_empty(txt_list_s *list)
{
    return list->next == list;
}

#define TXT_LIST_FOREACH(_list, _iter) \
    for ( \
        txt_list_s *_iter = (_list)->next; \
        _iter != (_list); \
        _iter = _iter->next \
    )

#endif
