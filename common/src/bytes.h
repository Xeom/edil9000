#if !defined(TXT_BYTES_H)
# define TXT_BYTES_H
# include <stdint.h>
# include <stddef.h>

typedef struct txt_bytes txt_bytes_s;

struct txt_bytes
{
    size_t         len;
    const uint8_t *data;
};

txt_bytes_s txt_bytes_clone(txt_bytes_s to_dup);

int txt_bytes_cmp(txt_bytes_s l, txt_bytes_s r);

#endif
