#if !defined(TXT_REFCOUNT_H)
# define TXT_REFCOUNT_H
# include <stddef.h>

void *txt_refcount_create(size_t len);

void txt_refcount_set_destructor(void *mem, void (*destructor)(void *));

void txt_refcount_decr(void *mem);

void txt_refcount_incr(void *mem);

#endif

