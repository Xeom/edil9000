#include "refcount.h"

#include "macros.h"

#include <stdlib.h>
#include <stdatomic.h>

typedef struct txt_refcount txt_refcount_s;

struct txt_refcount
{
    atomic_int refs;
    void       (*destructor)(void *);
    char       data[];
};

void *txt_refcount_create(size_t len)
{
    txt_refcount_s *rc = malloc(sizeof(txt_refcount_s) + len);
    if (rc)
    {
        rc->refs = 1;
        rc->destructor = NULL;
        return rc->data;
    }
    else
    {
        return NULL;
    }
}

void txt_refcount_set_destructor(void *mem, void (*destructor)(void *))
{
    txt_refcount_s *rc = CONTAINER_OF(mem, txt_refcount_s, data);
    rc->destructor = destructor;
}

void txt_refcount_decr(void *mem)
{
    txt_refcount_s *rc = CONTAINER_OF(mem, txt_refcount_s, data);
    if (--(rc->refs) == 0)
    {
        if (rc->destructor)
            (rc->destructor)(mem);

        free(rc);
    }
}

void txt_refcount_incr(void *mem)
{
    txt_refcount_s *rc = CONTAINER_OF(mem, txt_refcount_s, data);
    ++(rc->refs);
}
