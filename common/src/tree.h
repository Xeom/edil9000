#if !defined(TXT_TREE_H)
# define TXT_TREE_H
# include <stddef.h>
# include <stdint.h>


typedef struct txt_tree        txt_tree_s;
typedef struct txt_tree_node   txt_tree_node_s;
typedef struct txt_weight      txt_weight_s;

struct txt_weight
{
    size_t id;
};

static const txt_weight_s TXT_TREE_NUM_NODES = { 0 };

struct txt_tree
{
    txt_tree_node_s *root;
};

/// A mixin to allow something to be a node in a tree.
# define TXT_TREE_MAX_WEIGHTS 4
struct txt_tree_node
{
    txt_tree_node_s  *parent;
    txt_tree_node_s  *left;
    txt_tree_node_s  *right;
    size_t            weight_total[TXT_TREE_MAX_WEIGHTS];
    size_t            weight_this[TXT_TREE_MAX_WEIGHTS];
};

void txt_tree_init(txt_tree_s *tree);

size_t txt_tree_get_weight(const txt_tree_node_s *node, txt_weight_s weight);

void txt_tree_set_weight(txt_tree_node_s *node, txt_weight_s weight, size_t v);

size_t txt_tree_size_by(const txt_tree_s *tree, txt_weight_s weight);

static inline size_t txt_tree_size(const txt_tree_s *tree)
{
    return txt_tree_size_by(tree, TXT_TREE_NUM_NODES);
}

void txt_tree_remove(txt_tree_s *tree, txt_tree_node_s *node);

void txt_tree_insert_before(
    txt_tree_s      *tree,
    txt_tree_node_s *node,
    txt_tree_node_s *to_ins
);

void txt_tree_insert_after(
    txt_tree_s      *tree,
    txt_tree_node_s *node,
    txt_tree_node_s *to_ins
);

txt_tree_node_s *txt_tree_last(txt_tree_s *tree);

txt_tree_node_s *txt_tree_first(txt_tree_s *tree);

/// Return the last node for which the sum of the weights of previous nodes is
/// less than or equal to index.
txt_tree_node_s *txt_tree_index(
    txt_tree_s  *tree,
    size_t       index,
    txt_weight_s weight
);

size_t txt_tree_find(const txt_tree_node_s *node, txt_weight_s weight);

int txt_tree_cmp(const txt_tree_node_s *a, const txt_tree_node_s *b);

txt_tree_node_s *txt_tree_next(txt_tree_node_s *node);

txt_tree_node_s *txt_tree_prev(txt_tree_node_s *node);

/// Run a bisection on a tree.
///
/// Return the final node in the tree for which cmp_cb returns a value less than
/// or equal to zero, or NULL if no such value exists.
///
/// Assume that the tree is properly ordered with respect to the comparison
/// callback.
///
txt_tree_node_s *txt_tree_bisect(
    txt_tree_s *tree,
    int cmp_cb(const txt_tree_node_s *, const void *),
    const void *param
);

#endif
