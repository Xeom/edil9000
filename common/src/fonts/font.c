#include "txt_fonts.h"

#include "macros.h"
#include "tree.h"
#include "event/event.h"

#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <stdatomic.h>

#define DEFAULT_FONT_ID 0
#define MAX_FONTS 2048
#define MAX_NAME_LEN 128

typedef struct entry entry_s;

struct entry
{
    txt_tree_node_s  thead;
    uint16_t         id;
    txt_font_attrs_s attrs;
    bool             seen;
    bool             is_set;
    uint16_t         parent_id;
    char             name[MAX_NAME_LEN + 1];
};

struct txt_fonts
{
    pthread_rwlock_t lock;
    txt_event_s      on_set_attrs;
    txt_tree_s       by_name;
    uint16_t         num_allocated;
    entry_s          entries[MAX_FONTS];
};

static inline entry_s *node_to_entry(txt_tree_node_s *node)
{
    return node ? CONTAINER_OF(node, entry_s, thead) : NULL;
}

static inline const entry_s *node_to_entry_const(const txt_tree_node_s *node)
{
    return node ? CONTAINER_OF(node, entry_s, thead) : NULL;
}

static int entry_cmp_cb(const txt_tree_node_s *node, const void *name)
{
    return strcmp(node_to_entry_const(node)->name, name);
}

txt_fonts_s *txt_fonts_create(void)
{
    txt_fonts_s *rtn = malloc(sizeof(*rtn));

    if (rtn)
    {
        pthread_rwlock_init(&(rtn->lock), NULL);
        txt_event_init(&(rtn->on_set_attrs), sizeof(uint16_t));
        txt_tree_init(&(rtn->by_name));
        rtn->num_allocated = 0;
    }

    txt_fonts_set_attrs(
        rtn,
        txt_fonts_get_id(rtn, "default"),
        &(txt_font_attrs_s) { 0 }
    );

    return rtn;
}

void txt_fonts_destroy(txt_fonts_s *fonts)
{
    pthread_rwlock_destroy(&(fonts->lock));
    txt_event_kill(&(fonts->on_set_attrs));
    free(fonts);
}

static uint16_t get_id(txt_fonts_s *fonts, const char *name);

static uint16_t get_parent_id(txt_fonts_s *fonts, const char *name)
{
    const char *dot = strrchr(name, '.');
    if (!dot)
        return DEFAULT_FONT_ID;

    char parent_name[MAX_NAME_LEN + 1];
    memcpy(parent_name, name, dot - name);
    parent_name[dot - name] = '\0';

    return get_id(fonts, parent_name);
}

static uint16_t get_id(txt_fonts_s *fonts, const char *name)
{
    txt_tree_node_s *bisect_node =
        txt_tree_bisect(&(fonts->by_name), entry_cmp_cb, name);

    // 1. The font exists and was found.
    entry_s *found = node_to_entry(bisect_node);
    if (found && strcmp(found->name, name) == 0)
    {
        return found->id;
    }
    // 2. The font does not exist, but we cannot create any more.
    else if (fonts->num_allocated == MAX_FONTS)
    {
        return DEFAULT_FONT_ID;
    }
    // 3. The font does not exist, so we create it.
    else
    {
        uint16_t id = fonts->num_allocated++;
        entry_s *new = &(fonts->entries[id]);

        txt_tree_insert_after(&(fonts->by_name), bisect_node, &(new->thead));

        new->id        = id;
        new->attrs     = (txt_font_attrs_s){ 0 };
        new->seen      = false;
        new->is_set    = false;
        strcpy(new->name, name);

        new->parent_id = get_parent_id(fonts, name);

        return id;
    }
}

uint16_t txt_fonts_get_id(txt_fonts_s *fonts, const char *name)
{
    uint16_t id;

    // Reject font names that are too long.
    if (strlen(name) > MAX_NAME_LEN)
        return DEFAULT_FONT_ID;

    WITH_WRLOCK(&(fonts->lock))
    {
        id = get_id(fonts, name);
    }

    return id;
}

void txt_fonts_set_attrs(
    txt_fonts_s            *fonts,
    uint16_t                id,
    const txt_font_attrs_s *attrs
)
{
    bool do_event = false;

    WITH_WRLOCK(&(fonts->lock))
    {
        if (id < fonts->num_allocated)
        {
            entry_s *entry = &(fonts->entries[id]);
            entry->attrs   = *attrs;

            entry->is_set = true;
            do_event = entry->seen;
        }
    }

    if (do_event)
    {
        txt_err_ignore(
            txt_event_trigger(&(fonts->on_set_attrs), &id, sizeof(id))
        );
    }
}

void txt_fonts_get_attrs(
    txt_fonts_s      *fonts,
    uint16_t          id,
    txt_font_attrs_s *attrs
)
{
    WITH_RDLOCK(&(fonts->lock))
    {
        if (id >= fonts->num_allocated)
        {
            *attrs = (txt_font_attrs_s){ 0 };
        }
        else
        {
            entry_s *entry;
            while (!(entry = &(fonts->entries[id]))->is_set)
                id = entry->parent_id;

            *attrs = entry->attrs;
            entry->seen = true;
        }
    }
}

txt_event_s *txt_fonts_on_set_attrs(txt_fonts_s *fonts)
{
    return &(fonts->on_set_attrs);
}

#include <snow/snow.h>

describe(fonts)
{
    txt_fonts_s *fonts;

    before_each()
    {
        fonts = txt_fonts_create();
    }

    after_each()
    {
        txt_fonts_destroy(fonts);
    }

    it ("Uses parent attributes")
    {
        const uint16_t a   = txt_fonts_get_id(fonts, "a");
        const uint16_t abc = txt_fonts_get_id(fonts, "a.b.c");
        const uint16_t ab  = txt_fonts_get_id(fonts, "a.b");

        const txt_font_attrs_s a_attr   = { .fg = { .r = 100 } };
        const txt_font_attrs_s ab_attr  = { .fg = { .r = 101 } };
        const txt_font_attrs_s abc_attr = { .fg = { .r = 102 } };

        txt_font_attrs_s q;
        txt_fonts_get_attrs(fonts, a, &q);
        asserteq(q.fg.r, 0);
        txt_fonts_get_attrs(fonts, ab, &q);
        asserteq(q.fg.r, 0);
        txt_fonts_get_attrs(fonts, abc, &q);
        asserteq(q.fg.r, 0);

        txt_fonts_set_attrs(fonts, a, &a_attr);
        txt_fonts_get_attrs(fonts, a, &q);
        asserteq(q.fg.r, 100);
        txt_fonts_get_attrs(fonts, ab, &q);
        asserteq(q.fg.r, 100);
        txt_fonts_get_attrs(fonts, abc, &q);
        asserteq(q.fg.r, 100);

        txt_fonts_set_attrs(fonts, ab, &ab_attr);
        txt_fonts_get_attrs(fonts, a, &q);
        asserteq(q.fg.r, 100);
        txt_fonts_get_attrs(fonts, ab, &q);
        asserteq(q.fg.r, 101);
        txt_fonts_get_attrs(fonts, abc, &q);
        asserteq(q.fg.r, 101);

        txt_fonts_set_attrs(fonts, abc, &abc_attr);
        txt_fonts_get_attrs(fonts, a, &q);
        asserteq(q.fg.r, 100);
        txt_fonts_get_attrs(fonts, ab, &q);
        asserteq(q.fg.r, 101);
        txt_fonts_get_attrs(fonts, abc, &q);
        asserteq(q.fg.r, 102);
    }

}
