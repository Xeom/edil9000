#include "txt_fonts.h"

#include "macros.h"

const txt_colour_s TXT_BLACK          = { .r =   0, .g =   0, .b =   0 };
const txt_colour_s TXT_RED            = { .r = 170, .g =   0, .b =   0 };
const txt_colour_s TXT_GREEN          = { .r =   0, .g = 170, .b =   0 };
const txt_colour_s TXT_YELLOW         = { .r = 170, .g = 170, .b =   0 };
const txt_colour_s TXT_BLUE           = { .r =   0, .g =   0, .b = 170 };
const txt_colour_s TXT_MAGENTA        = { .r = 170, .g =   0, .b = 170 };
const txt_colour_s TXT_CYAN           = { .r =   0, .g = 170, .b = 170 };
const txt_colour_s TXT_WHITE          = { .r = 170, .g = 170, .b = 170 };
const txt_colour_s TXT_GREY           = { .r =  85, .g =  85, .b =  85 };
const txt_colour_s TXT_BRIGHT_RED     = { .r = 255, .g =   0, .b =   0 };
const txt_colour_s TXT_BRIGHT_GREEN   = { .r =   0, .g = 255, .b =   0 };
const txt_colour_s TXT_BRIGHT_YELLOW  = { .r = 255, .g = 255, .b =   0 };
const txt_colour_s TXT_BRIGHT_BLUE    = { .r =   0, .g =   0, .b = 255 };
const txt_colour_s TXT_BRIGHT_MAGENTA = { .r = 255, .g =   0, .b = 255 };
const txt_colour_s TXT_BRIGHT_CYAN    = { .r =   0, .g = 255, .b = 255 };
const txt_colour_s TXT_BRIGHT_WHITE   = { .r = 255, .g = 255, .b = 255 };

static inline uint8_t scale(uint8_t val, uint32_t bins)
{
    return ((uint32_t)val * bins) / 256;
}

uint8_t txt_colour_to_3bit(txt_colour_s col)
{
    return
        ((col.r >= 128) ? 1 : 0) |
        ((col.g >= 128) ? 2 : 0) |
        ((col.b >= 128) ? 4 : 0);
}

uint8_t txt_colour_to_8bit(txt_colour_s col)
{
    uint8_t r = scale(col.r, 6);
    uint8_t g = scale(col.g, 6);
    uint8_t b = scale(col.b, 6);

    if (r == g && g == b)
    {
        return 216 + scale(col.r, 24);
    }
    else
    {
        return (r * 36) + (g * 6) + b;
    }
}

bool txt_colour_is_bright(txt_colour_s col)
{
    uint8_t min = MIN(MIN(col.r, col.g), col.b);
    uint8_t max = MAX(MAX(col.r, col.g), col.b);

    if (min >= 128)
    {
        /* Colour is a white - bright if minimum value >170 */
        return min > 170;
    }
    else if (max < 128)
    {
        /* Colour is a black - bright if minimum value >=85 */
        return max >= 85;
    }
    else
    {
        /* Colour is neither black nor white */
        return max > 170;
    }

}

static inline bool colour_eq(txt_colour_s a, txt_colour_s b)
{
    return a.r == b.r && a.g == b.g && a.b == b.b;
}

bool txt_font_attrs_eq(txt_font_attrs_s a, txt_font_attrs_s b)
{
    return
        (!!(a.bold)   == !!(b.bold))   &&
        (!!(a.dull)   == !!(b.dull))   &&
        (!!(a.under)  == !!(b.under))  &&
        (!!(a.blink)  == !!(b.blink))  &&
        (!!(a.invert) == !!(b.invert)) &&
        (!!(a.italic) == !!(b.italic)) &&
        (!!(a.has_fg) == !!(b.has_fg)) &&
        (!!(a.has_bg) == !!(b.has_bg)) &&
        (a.has_bg ? colour_eq(a.bg, b.bg) : true) &&
        (a.has_fg ? colour_eq(a.fg, b.fg) : true);
}
