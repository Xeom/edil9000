#include "cursor.h"

#include "text/buffer/buffer.h"
#include "text/buffer/modify.h"

void txt_cursor_move(txt_cursor_s *cursor, txt_atom_iter_s iter)
{
    WITH_BUFFER_XACT(cursor->buffer)
    {
        // Create a temporary atom to store the location of the iterator
        txt_atom_s tmp;
        txt_atom_init(&tmp, iter);

        // Move the cursor to the location of the temporary
        txt_atom_kill(&(cursor->atom));
        txt_atom_init(&(cursor->atom), txt_atom_iter(&tmp));

        // Destroy the temporary
        txt_atom_kill(&tmp);

        txt_err_ignore(txt_event_trigger(&(cursor->on_move), NULL, 0));
    }
}

txt_err_e txt_cursor_move_to(txt_cursor_s *cursor, txt_cursor_s *oth)
{
    if (cursor->buffer != oth->buffer)
        return TXT_ERR_WRONG_BUF;

    if (cursor != oth)
        txt_cursor_move(cursor, txt_atom_iter(&(oth->atom)));

    return TXT_OK;
}

void txt_cursor_move_by(txt_cursor_s *cursor, int64_t n)
{
    WITH_BUFFER_XACT(cursor->buffer)
    {
        txt_atom_iter_s iter = txt_atom_iter(&(cursor->atom));
        txt_atom_iter_seek(&iter, n);
        txt_cursor_move(cursor, iter);
    }
}

void txt_cursor_insert_forward(txt_cursor_s *cursor, uint32_t cp)
{
    WITH_BUFFER_XACT(cursor->buffer)
    {
        txt_buffer_insert_forward(cursor->buffer, &(cursor->atom), cp, 0);
    }
}

void txt_cursor_insert_backward(txt_cursor_s *cursor, uint32_t cp)
{
    WITH_BUFFER_XACT(cursor->buffer)
    {
        txt_buffer_insert_backward(cursor->buffer, &(cursor->atom), cp, 0);
    }
}

void txt_cursor_delete_forward(txt_cursor_s *cursor)
{
    WITH_BUFFER_XACT(cursor->buffer)
    {
        txt_buffer_pop_forward(cursor->buffer, &(cursor->atom));
    }
}

void txt_cursor_delete_backward(txt_cursor_s *cursor)
{
    WITH_BUFFER_XACT(cursor->buffer)
    {
        // TODO: txt_buffer_pop_backward? We shouldn't be doing the move event!
        txt_cursor_move_by(cursor, -1);
        txt_buffer_pop_forward(cursor->buffer, &(cursor->atom));
    }
}

txt_event_s *txt_cursor_on_move(txt_cursor_s *cursor)
{
    return &(cursor->on_move);
}

txt_err_e txt_cursor_delete_between(txt_cursor_s *a, txt_cursor_s *b)
{
    if (a->buffer != b->buffer)
        return TXT_ERR_WRONG_BUF;

    WITH_BUFFER_XACT(a->buffer)
    {
        txt_buffer_pop_between(a->buffer, &(a->atom), &(b->atom));
    }

    return TXT_OK;
}

void txt_cursor_get_font(txt_cursor_s *cur, uint16_t *fonts, size_t n)
{
    WITH_BUFFER_XACT(cur->buffer)
    {
        txt_atom_iter_s iter = txt_atom_iter(&(cur->atom));
        for (size_t idx = 0; idx < n; ++idx)
        {
            const txt_glyph_s *glyph = txt_atom_iter_next(&iter);
            fonts[idx] = glyph ? glyph->font : 0;
        }
    }
}

void txt_cursor_set_font(txt_cursor_s *cur, uint16_t font, size_t n)
{
    WITH_BUFFER_XACT(cur->buffer)
    {
        txt_buffer_set_font(cur->buffer, &(cur->atom), font, n);
    }
}

txt_buffer_s *txt_cursor_buffer(txt_cursor_s *cur)
{
    return cur->buffer;
}

#include <snow/snow.h>
#if SNOW_ENABLED

static void assert_cursor_reads(txt_cursor_s *cur, const char *exp)
{
    size_t idx;
    txt_atom_iter_s iter = txt_atom_iter(&(cur->atom));
    for (idx = 0; exp[idx]; ++idx)
    {
        const txt_glyph_s *glyph = txt_atom_iter_next(&iter);
        assert(glyph);
        asserteq(glyph->codepoint, exp[idx]);
    }

    assert(!txt_atom_iter_next(&iter));
}

static void assert_buffer(txt_buffer_s *buffer, const char *exp)
{
    txt_cursor_s *rdr = txt_cursor_create_offset(buffer, 0);
    assert_cursor_reads(rdr, exp);
    txt_cursor_destroy(rdr);
}

static void insert_str(txt_cursor_s *cur, const char *s)
{
    for (; *s; ++s)
        txt_cursor_insert_backward(cur, *s);
}

describe(cursor)
{
    txt_buffer_s *buffer;
    txt_cursor_s *cursor;

    before_each()
    {
        buffer = txt_buffer_create();
        cursor = txt_cursor_create_end_of_text(buffer);
    }

    after_each()
    {
        txt_cursor_destroy(cursor);
        txt_buffer_destroy(buffer);
    }

    it ("Inserts text")
    {
        insert_str(cursor, "Hello world");
        assert_buffer(buffer, "Hello world");
    }

    it ("Reads text when moved")
    {
        insert_str(cursor, "Hello world");

        txt_cursor_move_by(cursor, -11);
        assert_cursor_reads(cursor, "Hello world");

        txt_cursor_move_by(cursor, 6);
        assert_cursor_reads(cursor, "world");

        txt_cursor_move_by(cursor, 5);
        txt_cursor_move_by(cursor, -5);
        assert_cursor_reads(cursor, "world");
    }

    it ("Cannot move beyond end")
    {
        txt_cursor_move_by(cursor, -100);
        asserteq(txt_cursor_offset(cursor), 0);

        txt_cursor_move_by(cursor, 100);
        asserteq(txt_cursor_offset(cursor), 0);

        insert_str(cursor, "Hello world");

        txt_cursor_move_by(cursor, -100);
        asserteq(txt_cursor_offset(cursor), 0);

        txt_cursor_move_by(cursor, 100);
        asserteq(txt_cursor_offset(cursor), 11);
    }

    it ("Goes after other cursors when moved")
    {
        insert_str(cursor, "Hello world");

        txt_cursor_s *a = txt_cursor_create_offset(buffer, 5);
        txt_cursor_s *b = txt_cursor_create_offset(buffer, 5);

        int32_t res;
        asserteq(txt_cursor_cmp(a, b, &res), TXT_OK);
        assert(res < 0);

        txt_cursor_move_by(a, 0);
        asserteq(txt_cursor_cmp(a, b, &res), TXT_OK);
        assert(res > 0);

        txt_cursor_move_by(b, 0);
        asserteq(txt_cursor_cmp(a, b, &res), TXT_OK);
        assert(res < 0);

        txt_cursor_destroy(a);
        txt_cursor_destroy(b);
    }

    it ("Moves to other cursor")
    {
        insert_str(cursor, "Hello world");

        txt_cursor_s *start = txt_cursor_create_offset(buffer, 0);
        txt_cursor_s *end   = txt_cursor_create_offset(buffer, 11);

        asserteq(txt_cursor_move_to(cursor, start), TXT_OK);
        asserteq(txt_cursor_offset(cursor), 0);

        asserteq(txt_cursor_move_to(cursor, end), TXT_OK);
        asserteq(txt_cursor_offset(cursor), 11);

        asserteq(txt_cursor_move_to(cursor, start), TXT_OK);
        asserteq(txt_cursor_offset(cursor), 0);

        txt_cursor_destroy(start);
        txt_cursor_destroy(end);
    }

    it ("Inserts forwards and backwards")
    {
        txt_cursor_insert_backward(cursor, '1');
        txt_cursor_insert_forward(cursor,  '2');
        txt_cursor_insert_backward(cursor, '3');
        txt_cursor_insert_forward(cursor,  '4');

        assert_buffer(buffer, "1342");
    }

    it ("Deletes forwards")
    {
        insert_str(cursor, "LEFTRIGHT");
        txt_cursor_move_by(cursor, -5);

        txt_cursor_delete_forward(cursor);
        txt_cursor_delete_forward(cursor);
        txt_cursor_delete_forward(cursor);
        txt_cursor_delete_forward(cursor);
        txt_cursor_delete_forward(cursor);

        assert_buffer(buffer, "LEFT");
    }

    it ("Deletes backwards")
    {
        insert_str(cursor, "LEFTRIGHT");
        txt_cursor_move_by(cursor, -5);

        txt_cursor_delete_backward(cursor);
        txt_cursor_delete_backward(cursor);
        txt_cursor_delete_backward(cursor);
        txt_cursor_delete_backward(cursor);

        assert_buffer(buffer, "RIGHT");
    }

    it ("Moves before start")
    {
        txt_cursor_move_by(cursor, -10);
        insert_str(cursor, "Hello world");
        assert_buffer(buffer, "Hello world");
    }

    it ("Deletes before start")
    {
        insert_str(cursor, "123");
        txt_cursor_delete_backward(cursor);
        txt_cursor_delete_backward(cursor);
        txt_cursor_delete_backward(cursor);
        txt_cursor_delete_backward(cursor);
        insert_str(cursor, "123");
        assert_buffer(buffer, "123");
    }

    it ("Deletes past end")
    {
        insert_str(cursor, "123");
        txt_cursor_move_by(cursor, -3);
        txt_cursor_delete_forward(cursor);
        txt_cursor_delete_forward(cursor);
        txt_cursor_delete_forward(cursor);
        txt_cursor_delete_forward(cursor);
        insert_str(cursor, "123");
        assert_buffer(buffer, "123");
    }

    it ("Moves and inserts")
    {
        insert_str(cursor, "H world");
        txt_cursor_move_by(cursor, -6);
        insert_str(cursor, "ello");

        assert_buffer(buffer, "Hello world");
    }

    it ("Can be accessed after buffer destroyed")
    {
        txt_buffer_destroy(buffer);
        txt_cursor_move_by(cursor, 1);
        buffer = txt_buffer_create();
    }

    it ("Calls event on move")
    {
        EXPECT_EVENT(
            txt_cursor_on_move(cursor),
            txt_cursor_move_by(cursor, 1)
        );
        EXPECT_EVENT(
            txt_cursor_on_move(cursor),
            txt_cursor_move_by(cursor, -1)
        );
    }

    it ("Deletes between")
    {
        insert_str(cursor, "Hello\nworld");
        txt_cursor_s *a = txt_cursor_create_offset(buffer, 2);
        txt_cursor_s *b = txt_cursor_create_offset(buffer, 9);
        asserteq(txt_cursor_delete_between(a, b), TXT_OK);
        txt_cursor_destroy(a);
        txt_cursor_destroy(b);

        assert_buffer(buffer, "Held");
    }

    it ("Deletes between whole buffer")
    {
        insert_str(cursor, "Hello\nworld");
        txt_cursor_s *a = txt_cursor_create_offset(buffer, 0);
        txt_cursor_s *b = txt_cursor_create_offset(buffer, 11);
        asserteq(txt_cursor_delete_between(b, a), TXT_OK);
        txt_cursor_destroy(a);
        txt_cursor_destroy(b);

        assert_buffer(buffer, "");
    }

    it ("Gets and sets fonts")
    {
        insert_str(cursor, "Hello World");
        txt_cursor_move_by(cursor, -11);

        uint16_t exp[] = { 1, 1, 1, 1, 1, 0, 2, 2, 2, 2, 2 };
        uint16_t get[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        txt_cursor_set_font(cursor, 2, 11);
        txt_cursor_set_font(cursor, 0, 6);
        txt_cursor_set_font(cursor, 1, 5);
        txt_cursor_get_font(cursor, get, 11);

        asserteq_buf(exp, get, sizeof(exp));
    }

    it ("Creates cursor on next line")
    {
        insert_str(cursor, "Hello\nworld");
        txt_cursor_s *a = txt_cursor_create_offset(buffer, 0);
        assert(a);
        txt_cursor_s *b = txt_cursor_create_relative_line(a, 1);
        assert(b);
        txt_cursor_s *c = txt_cursor_create_relative_line(b, 1);

        asserteq(txt_cursor_coord(a).row, 0);
        asserteq(txt_cursor_coord(a).col, 0);
        asserteq(txt_cursor_coord(b).row, 1);
        asserteq(txt_cursor_coord(b).col, 0);
        assert(c == NULL);

        txt_cursor_destroy(a);
        txt_cursor_destroy(b);
    }

    it ("Creates cursor on previous line")
    {
        insert_str(cursor, "Hello\nworld");
        txt_cursor_s *a =
            txt_cursor_create_coord(buffer, (txt_coord_s){ .row = 1 });
        assert(a);
        txt_cursor_s *b = txt_cursor_create_relative_line(a, -1);
        assert(b);
        txt_cursor_s *c = txt_cursor_create_relative_line(b, -1);

        asserteq(txt_cursor_coord(a).row, 1);
        asserteq(txt_cursor_coord(a).col, 0);
        asserteq(txt_cursor_coord(b).row, 0);
        asserteq(txt_cursor_coord(b).col, 0);
        assert(c == NULL);

        txt_cursor_destroy(a);
        txt_cursor_destroy(b);
    }
}
#endif
