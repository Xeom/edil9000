#if !defined(TXT_BUFFER_CURSOR_H)
# define TXT_BUFFER_CURSOR_H
# include "txt_cursor.h"
# include "macros.h"

# include "event/event.h"
# include "text/atom/atom.h"

struct txt_cursor
{
    txt_buffer_s *buffer;
    txt_atom_s    atom;
    txt_event_s   on_move;
    uint16_t      insert_font;
};

void txt_cursor_move(txt_cursor_s *cursor, txt_atom_iter_s iter);

#endif
