#include "txt_cursor_io.h"

#include "cursor.h"
#include "text/buffer/buffer.h"
#include "text/buffer/modify.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>

#define PART_LEN 32

struct txt_cursor_io
{
    // Parameters.
    txt_cursor_s     *cursor;      // Cursor to perform IO on.
    bool              until_eol;   // When reading, stop at end-of-line.
    txt_atom_s       *until;       // When reading, stop at this atom.
    bool              done;        // When reading, we have reached the end.
    txt_encoding_e    encoding;    // Encoding to use.
    txt_line_ending_e line_ending; // Line ending to use.

    // Partially encoded/decoded buffer.
    //
    // When a read/write cannot be entirely completed due to a character being
    // encoded across multiple buffers, the leftover bytes are stored here.
    char           part[PART_LEN];
    size_t         part_used;

    // Buffer for current IO operation.
    uint8_t       *buf;
    size_t         buf_len;
    size_t         buf_used;

    uint16_t       font;
};

static inline bool is_newline(txt_glyph_s glyph)
{
    return glyph.codepoint == '\n';
}

txt_cursor_io_s *txt_cursor_io_create(txt_cursor_s *cursor)
{
    txt_cursor_io_s *rtn = malloc(sizeof(*rtn));
    if (rtn)
    {
        rtn->cursor      = cursor;
        rtn->until       = NULL;
        rtn->until_eol   = false;
        rtn->done        = false;
        rtn->encoding    = TXT_ENCODING_UTF8;
        rtn->line_ending = TXT_LINE_ENDING_UNIX;
        rtn->part_used   = 0;
        rtn->font        = 0;
    }

    return rtn;
}

void txt_cursor_io_destroy(txt_cursor_io_s *io)
{
    free(io);
}

void txt_cursor_io_set_encoding(txt_cursor_io_s *io, txt_encoding_e enc)
{
    io->encoding = enc;
}

void txt_cursor_io_set_line_ending(txt_cursor_io_s *io, txt_line_ending_e le)
{
    io->line_ending = le;
}

void txt_cursor_io_set_until(txt_cursor_io_s *io, txt_cursor_s *until)
{
    io->until = &(until->atom);
}

void txt_cursor_io_set_until_eol(txt_cursor_io_s *io)
{
    io->until_eol = true;
}

void txt_cursor_io_set_font(txt_cursor_io_s *io, uint16_t font)
{
    io->font = font;
}

static bool has_buffer(txt_cursor_io_s *io)
{
    return io->buf_len > io->buf_used;
}

static void write_byte(txt_cursor_io_s *io, uint8_t byte)
{
    if (has_buffer(io))
    {
        io->buf[(io->buf_used)++] = byte;
    }
    else if (io->part_used < PART_LEN)
    {
        io->part[(io->part_used)++] = byte;
    }
}

static void consume(txt_cursor_io_s *io, size_t n)
{
    if (n < io->part_used)
    {
        memmove(io->part, io->part + n, io->part_used - n);
        io->part_used -= n;
    }
    else
    {
        n -= io->part_used;
        io->part_used = 0;
        io->buf_used += n;
    }
}

static bool read_byte(txt_cursor_io_s *io, size_t idx, uint8_t *b)
{
    if (idx < io->part_used)
    {
        *b = io->part[idx];
        return true;
    }
    else
    {
        idx -= io->part_used;
        idx += io->buf_used;
        if (idx < io->buf_len)
        {
            *b = io->buf[idx];
            return true;
        }
        else
        {
            return false;
        }
    }
}

static void encode_ascii(txt_cursor_io_s *io, uint32_t codepoint)
{
    write_byte(io, (codepoint < 0x80) ? codepoint : '?');
}

static inline uint8_t upper(int n) { return ~(uint8_t)(0xff >> n); }
static inline uint8_t lower(int n) { return ~(uint8_t)(0xff << n); }

static void encode_utf8(txt_cursor_io_s *io, uint32_t codepoint)
{
    if (codepoint < upper(1))
        return write_byte(io, codepoint);

    uint8_t rev[8];
    size_t idx;

    for (idx = 0; ; ++idx)
    {
        rev[idx] = codepoint & lower(6);
        codepoint >>= 6;

        if (codepoint == 0)
        {
            if ((rev[idx] & upper(idx + 2)) == 0)
            {
                rev[idx] |= upper(idx + 1);
                break;
            }
        }

        rev[idx] |= upper(1);
    }

    do
    {
        write_byte(io, rev[idx]);
    } while (idx--);
}

static void encode_utf32(txt_cursor_io_s *io, uint32_t codepoint)
{
    write_byte(io, codepoint & 0xff);
    write_byte(io, (codepoint >> 8)  & 0xff);
    write_byte(io, (codepoint >> 16) & 0xff);
    write_byte(io, (codepoint >> 24) & 0xff);
}

static void encode_codepoint(txt_cursor_io_s *io, uint32_t codepoint)
{
    switch (io->encoding)
    {
    case TXT_ENCODING_ASCII:
        encode_ascii(io, codepoint);
        break;
    case TXT_ENCODING_UTF8:
        encode_utf8(io, codepoint);
        break;
    case TXT_ENCODING_UTF32:
        encode_utf32(io, codepoint);
        break;
    default:
        break;
    }
}

static void encode_glyph(txt_cursor_io_s *io, txt_glyph_s glyph)
{
    if (is_newline(glyph))
    {
        switch (io->line_ending)
        {
        case TXT_LINE_ENDING_DOS:
            encode_codepoint(io, '\r');
            encode_codepoint(io, '\n');
            break;

        case TXT_LINE_ENDING_UNIX:
            encode_codepoint(io, '\n');
            break;

        case TXT_LINE_ENDING_NONE:
        default:
            // Encode nothing!
            break;
        }
    }
    else
    {
        encode_codepoint(io, glyph.codepoint);
    }
}

size_t txt_cursor_io_read(txt_cursor_io_s *io, uint8_t *buf, size_t len)
{
    // Grab any partial codepoints leftover from last time...
    char   prev_part[PART_LEN];
    size_t prev_part_used = io->part_used;
    memcpy(prev_part, io->part, sizeof(io->part));

    // Reset the state of the IO for this OP...
    io->buf       = buf;;
    io->buf_len   = len;
    io->buf_used  = 0;
    io->part_used = 0;

    // Write any partial codepoints from last time...
    for (size_t idx = 0; idx < prev_part_used; ++idx)
        write_byte(io, prev_part[idx]);

    WITH_BUFFER_XACT(io->cursor->buffer)
    {
        txt_atom_iter_s iter = txt_atom_iter(&(io->cursor->atom));

        const txt_glyph_s *glyph;
        while (!io->done && has_buffer(io) && (glyph = txt_atom_iter_next(&iter)))
        {
            if (io->until &&
                txt_atom_iter_eq(txt_atom_iter(io->until), iter))
            {
                // Mark the IO as complete, and consume the character.
                io->done = true;
            }

            if (io->until_eol && is_newline(*glyph))
            {
                // Mark the IO as complete, but don't consume the newline.
                io->done = true;
                break;
            }

            encode_glyph(io, *glyph);
        }

        // Move the cursor to the iterated position.
        txt_cursor_move(io->cursor, iter);
    }

    if (io->buf_used != 0)
        txt_err_ignore(txt_event_trigger(&(io->cursor->on_move), NULL, 0));

    return io->buf_used;
}

static size_t decode_ascii(txt_cursor_io_s *io, uint32_t *res, size_t off)
{
    uint8_t b;
    if (!read_byte(io, off, &b))
        return 0;

    *res = b < 0x80 ? b : '?';
    return 1;
}

/**
 * Count the number of upper bits set in a byte.
 *
 */

static inline size_t num_upper_set(uint8_t b)
{
    static const uint8_t lookup[256] =
    {
        // 1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 0
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 1
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 2
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 3
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 4
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 5
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 6
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 7
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 8
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // 9
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // a
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, // b
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, // c
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, // d
        3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, // e
        4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 7, 8, // f
    };

    return lookup[b];
}

static size_t decode_utf8(txt_cursor_io_s *io, uint32_t *res, size_t off)
{
    uint8_t b;
    if (!read_byte(io, off, &b))
        return 0;

    size_t len = num_upper_set(b);

    // Fewer than 2 upper bits - treat like ASCII
    if (len < 2)
    {
        *res = b < 0x80 ? b : '?';
        return 1;
    }

    *res = b & ~upper(len);

    for (size_t idx = 1; idx < len; ++idx)
    {
        if (!read_byte(io, off + idx, &b))
            return 0;

        *res <<= 6;
        *res  |= b & lower(6);
    }

    return len;
}

static size_t decode_utf32(txt_cursor_io_s *io, uint32_t *res, size_t off)
{
    uint8_t b[4];
    for (int i = 0; i < 4; ++i)
    {
        if (!read_byte(io, off + i, &b[i]))
            return 0;
    }

    *res = b[0] | (b[1] << 8) | (b[2] << 16) | (b[3] << 24);
    return 4;
}

static size_t decode_codepoint(txt_cursor_io_s *io, uint32_t *res, size_t off)
{
    switch (io->encoding)
    {
    case TXT_ENCODING_ASCII:
        return decode_ascii(io, res, off);
    case TXT_ENCODING_UTF8:
        return decode_utf8(io, res, off);
    case TXT_ENCODING_UTF32:
        return decode_utf32(io, res, off);
    default:
        *res = '?';
        return 1;
    }
}

void txt_cursor_io_write(txt_cursor_io_s *io, const uint8_t *buf, size_t len)
{
    txt_buffer_s *buffer = io->cursor->buffer;
    txt_atom_s   *atom   = &(io->cursor->atom);

    // Setup the IO with the parameters for this operation.
    io->buf       = (uint8_t *)buf;
    io->buf_len   = len;
    io->buf_used  = 0;

    WITH_BUFFER_XACT(buffer)
    {
        for (;;)
        {
            uint32_t codepoint = 0;
            size_t len = decode_codepoint(io, &codepoint, 0);

            if (len == 0) break;

            if (io->line_ending == TXT_LINE_ENDING_DOS && codepoint == '\r')
            {
                uint32_t codepoint_2;
                size_t len_2 = decode_codepoint(io, &codepoint_2, len);

                if (len_2 == 0) break;

                if (codepoint_2 == '\n')
                {
                    codepoint = codepoint_2;
                    len      += len_2;
                }
            }

            txt_buffer_insert_backward(buffer, atom, codepoint, io->font);

            consume(io, len);
        }
    }

    // Copy any un-consumed buffer into the partial buffer.
    size_t to_copy = io->buf_len - io->buf_used;
    memcpy(io->part + io->part_used, io->buf + io->buf_used, to_copy);
    io->part_used += to_copy;
}

#include <snow/snow.h>
#if SNOW_ENABLED
# include "event/event.h"

static void assert_buffers_eq(txt_buffer_s *b1, txt_buffer_s *b2)
{
    txt_cursor_s *c1 = txt_cursor_create_offset(b1, 0);
    txt_cursor_s *c2 = txt_cursor_create_offset(b2, 0);

    txt_atom_iter_s it1 = txt_atom_iter(&(c1->atom));
    txt_atom_iter_s it2 = txt_atom_iter(&(c2->atom));

    for (;;)
    {
        const txt_glyph_s *g1 = txt_atom_iter_next(&it1);
        const txt_glyph_s *g2 = txt_atom_iter_next(&it2);

        assert((g1 && g2) || (!g1 && !g2));
        if (g1 && g2)
        {
            asserteq(g1->codepoint, g2->codepoint);
            asserteq(g1->font,      g2->font);
        }
        else
        {
            break;
        }
    }

    txt_cursor_destroy(c1);
    txt_cursor_destroy(c2);
}

static bool copy_between(txt_cursor_io_s *a, txt_cursor_io_s *b, size_t len)
{
    uint8_t buf[len];
    size_t n = txt_cursor_io_read(a, buf, len);

    txt_cursor_io_write(b, buf, n);

    return n > 0;
}

describe(cursor_io)
{
    txt_buffer_s    *buffer_1;
    txt_cursor_s    *cursor_1;
    txt_cursor_io_s *io_1;
    txt_buffer_s    *buffer_2;
    txt_cursor_s    *cursor_2;
    txt_cursor_io_s *io_2;

    before_each()
    {
        buffer_1 = txt_buffer_create();
        buffer_2 = txt_buffer_create();
        cursor_1 = txt_cursor_create_end_of_text(buffer_1);
        cursor_2 = txt_cursor_create_end_of_text(buffer_2);
        io_1     = txt_cursor_io_create(cursor_1);
        io_2     = txt_cursor_io_create(cursor_2);
    }

    after_each()
    {
        txt_cursor_io_destroy(io_1);
        txt_cursor_io_destroy(io_2);
        txt_cursor_destroy(cursor_1);
        txt_cursor_destroy(cursor_2);
        txt_buffer_destroy(buffer_1);
        txt_buffer_destroy(buffer_2);
    }

    it ("Copies ASCII text")
    {
        txt_cursor_io_set_encoding(io_1, TXT_ENCODING_ASCII);
        txt_cursor_io_set_encoding(io_2, TXT_ENCODING_ASCII);

        for (char c = ' '; c <= '~'; ++c)
            txt_cursor_insert_forward(cursor_1, c);

        while (copy_between(io_1, io_2, 32)) {}
        assert_buffers_eq(buffer_1, buffer_2);
    }

    it ("Encodes invalid ASCII as '?'")
    {
        txt_cursor_io_set_encoding(io_1, TXT_ENCODING_ASCII);
        txt_cursor_io_set_encoding(io_2, TXT_ENCODING_ASCII);

        uint8_t buf[8];
        txt_cursor_insert_forward(cursor_1, 1000);
        asserteq(txt_cursor_io_read(io_1, buf, sizeof(buf)), 1);
        asserteq(buf[0], '?');
    }

    it ("Decodes invalid ASCII as '?'")
    {
        txt_cursor_io_set_encoding(io_1, TXT_ENCODING_ASCII);
        txt_cursor_io_set_encoding(io_2, TXT_ENCODING_ASCII);

        // Insert invalid ASCII into buffer 1
        uint8_t buf[] = { 0xff };
        txt_cursor_io_write(io_1, buf, sizeof(buf));

        // Explicitly insert '?' into buffer 2
        txt_cursor_insert_forward(cursor_2, '?');

        assert_buffers_eq(buffer_1, buffer_2);
    }

    it ("Encodes UTF-8")
    {
        // Encode a pound symbol
        txt_cursor_insert_forward(cursor_1, 0xa3);

        uint8_t buf[32];
        asserteq(txt_cursor_io_read(io_1, buf, sizeof(buf)), 2);
        asserteq(buf[0], 0xc2);
        asserteq(buf[1], 0xa3);

        // Encode a euro symbol
        txt_cursor_insert_forward(cursor_1, 0x20ac);

        asserteq(txt_cursor_io_read(io_1, buf, sizeof(buf)), 3);
        asserteq(buf[0], 0xe2);
        asserteq(buf[1], 0x82);
        asserteq(buf[2], 0xac);
    }

    it ("Encodes UTF-32")
    {
        txt_cursor_io_set_encoding(io_1, TXT_ENCODING_UTF32);

        // Encode a pound symbol
        txt_cursor_insert_forward(cursor_1, 0xa3);

        uint8_t buf[32];
        asserteq(txt_cursor_io_read(io_1, buf, sizeof(buf)), 4);
        asserteq(buf[0], 0xa3);
        asserteq(buf[1], 0x0);
        asserteq(buf[2], 0x0);
        asserteq(buf[3], 0x0);

        // Encode a euro symbol
        txt_cursor_insert_forward(cursor_1, 0x20ac);

        asserteq(txt_cursor_io_read(io_1, buf, sizeof(buf)), 4);
        asserteq(buf[0], 0xac);
        asserteq(buf[1], 0x20);
        asserteq(buf[2], 0x00);
        asserteq(buf[3], 0x00);
    }

    it ("Decodes UTF-8")
    {
        // Decode a pound and euro symbol
        uint8_t buf[] = { 0xc2, 0xa3, 0xe2, 0x82, 0xac };
        txt_cursor_io_write(io_1, buf, sizeof(buf));

        txt_cursor_insert_forward(cursor_2, 0x20ac);
        txt_cursor_insert_forward(cursor_2, 0xa3);

        assert_buffers_eq(buffer_1, buffer_2);
    }

    it ("Decodes UTF-32")
    {
        txt_cursor_io_set_encoding(io_1, TXT_ENCODING_UTF32);

        // Decode a pound and euro symbol
        uint8_t buf[] = { 0xa3, 0x00, 0x00, 0x00, 0xac, 0x20, 0x00, 0x00 };
        txt_cursor_io_write(io_1, buf, sizeof(buf));

        txt_cursor_insert_forward(cursor_2, 0x20ac);
        txt_cursor_insert_forward(cursor_2, 0xa3);

        assert_buffers_eq(buffer_1, buffer_2);
    }

    it ("Copies UTF-8 characters of all sizes")
    {
        for (int bits = 0; bits <= 32; ++bits)
        {
            uint32_t codepoint = rand();

            if (bits == 0)
                codepoint = 0;
            else if (bits < 32)
                codepoint %= (1u << bits);

            txt_cursor_insert_forward(cursor_1, codepoint);
        }

        while (copy_between(io_1, io_2, 32)) {}
        assert_buffers_eq(buffer_1, buffer_2);
    }

    it ("Copies UTF-32 characters of all sizes")
    {
        txt_cursor_io_set_encoding(io_1, TXT_ENCODING_UTF32);
        txt_cursor_io_set_encoding(io_2, TXT_ENCODING_UTF32);

        for (int bits = 0; bits <= 32; ++bits)
        {
            uint32_t codepoint = rand();

            if (bits == 0)
                codepoint = 0;
            else if (bits < 32)
                codepoint %= (1u << bits);

            txt_cursor_insert_forward(cursor_1, codepoint);
        }

        while (copy_between(io_1, io_2, 32)) {}
        assert_buffers_eq(buffer_1, buffer_2);
    }

    it ("Copies UTF-8 characters of all sizes one byte at a time")
    {
        for (int bits = 0; bits < 16; ++bits)
            txt_cursor_insert_forward(cursor_1, 1 << bits);

        while (copy_between(io_1, io_2, 1)) {}
        assert_buffers_eq(buffer_1, buffer_2);
    }

    it ("Copies UTF-32 characters of all sizes one byte at a time")
    {
        txt_cursor_io_set_encoding(io_1, TXT_ENCODING_UTF32);
        txt_cursor_io_set_encoding(io_2, TXT_ENCODING_UTF32);

        for (int bits = 0; bits < 16; ++bits)
            txt_cursor_insert_forward(cursor_1, 1 << bits);

        while (copy_between(io_1, io_2, 1)) {}
        assert_buffers_eq(buffer_1, buffer_2);
    }

    it ("Calls event on write")
    {
        EXPECT_EVENT(
            txt_buffer_on_modify_text(buffer_1),
            txt_cursor_io_write(io_1, (const uint8_t *)"Hello", 5)
        );
    }

    it ("Calls event on read")
    {
        uint8_t buf[1] = { 0 };
        txt_cursor_insert_forward(cursor_1, 'x');
        EXPECT_EVENT(
            txt_cursor_on_move(cursor_1),
            txt_cursor_io_read(io_1, buf, sizeof(buf))
        );
        asserteq(buf[0], 'x');
    }

    it ("Copies until EOL")
    {
        txt_cursor_insert_forward(cursor_1, '\n');
        txt_cursor_insert_forward(cursor_1, 'B');
        txt_cursor_insert_forward(cursor_1, 'B');
        txt_cursor_insert_forward(cursor_1, '\n');
        txt_cursor_insert_forward(cursor_1, 'A');
        txt_cursor_insert_forward(cursor_1, 'A');

        txt_cursor_io_set_until_eol(io_1);

        uint8_t buf[32];

        asserteq(txt_cursor_io_read(io_1, buf, sizeof(buf)), 2);
        asserteq(buf[0], 'A');
        asserteq(buf[1], 'A');

        txt_cursor_io_destroy(io_1);
        io_1 = txt_cursor_io_create(cursor_1);
        txt_cursor_io_set_until_eol(io_1);

        asserteq(txt_cursor_io_read(io_1, buf, sizeof(buf)), 2);
        asserteq(buf[0], 'B');
        asserteq(buf[1], 'B');

        txt_cursor_io_destroy(io_1);
        io_1 = txt_cursor_io_create(cursor_1);
        txt_cursor_io_set_until_eol(io_1);

        asserteq(txt_cursor_io_read(io_1, buf, sizeof(buf)), 0);
    }

    // it ("Survives UTF-8 fuzzing")
    // it ("Decodes invalid UTF-8 as '?'")
    // it ("Encodes CRNL")
    // it ("Decodes CRNL")
}
#endif
