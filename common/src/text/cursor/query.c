#include "cursor.h"

#include "text/buffer/buffer.h"

txt_coord_s txt_cursor_coord(txt_cursor_s *cursor)
{
    txt_coord_s rtn;
    WITH_BUFFER_XACT(cursor->buffer)
    {
        rtn = txt_atom_coord(&(cursor->atom));
    }

    return rtn;
}

size_t txt_cursor_offset(txt_cursor_s *cursor)
{
    size_t rtn;
    WITH_BUFFER_XACT(cursor->buffer)
    {
        rtn = txt_atom_distance(
            txt_atom_tree_start(cursor->atom.tree),
            txt_atom_iter(&(cursor->atom))
        );
    }

    return rtn;
}

size_t txt_cursor_utf8_byte(txt_cursor_s *cursor)
{
    size_t rtn;
    WITH_BUFFER_XACT(cursor->buffer)
    {
        rtn = txt_atom_utf8_byte(&(cursor->atom));
    }

    return rtn;
}

txt_err_e txt_cursor_cmp(txt_cursor_s *a, txt_cursor_s *b, int32_t *res)
{
    if (a->buffer != b->buffer)
        return TXT_ERR_WRONG_BUF;

    WITH_BUFFER_XACT(a->buffer)
    {
        if (res)
            *res = txt_atom_cmp(&(a->atom), &(b->atom));
    }

    return TXT_OK;
}

