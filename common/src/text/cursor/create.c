#include "cursor.h"

#include "refcount.h"
#include "text/buffer/buffer.h"

#include <stdlib.h>

static txt_cursor_s *create_inner(txt_buffer_s *buffer, txt_atom_iter_s iter)
{
    txt_cursor_s *rtn = malloc(sizeof(*rtn));
    if (rtn)
    {
        txt_refcount_incr(buffer);
        rtn->buffer = buffer;

        txt_event_init(&(rtn->on_move), 0);
        txt_atom_init(&(rtn->atom), iter);
    }

    return rtn;
}

void txt_cursor_destroy(txt_cursor_s *cursor)
{
    WITH_BUFFER_XACT(cursor->buffer)
    {
        txt_atom_kill(&(cursor->atom));
        txt_event_kill(&(cursor->on_move));
    }

    txt_refcount_decr(cursor->buffer);
    free(cursor);
}

#define RETURN_CURSOR_AT_ITER(buffer_, iter_, code_) \
    txt_cursor_s *rtn;                                                         \
    WITH_BUFFER_XACT(buffer_)                                                  \
    {                                                                          \
        txt_atom_iter_s iter;                                                  \
        code_;                                                                 \
        rtn = create_inner(buffer_, iter);                                     \
    }                                                                          \
    return rtn;

txt_cursor_s *txt_cursor_create_end_of_text(txt_buffer_s *buffer)
{
    RETURN_CURSOR_AT_ITER(
        buffer, iter,
        iter = txt_atom_tree_end(&(buffer->atom_tree))
    )
}

txt_cursor_s *txt_cursor_create_coord(txt_buffer_s *buffer, txt_coord_s coord)
{
    RETURN_CURSOR_AT_ITER(
        buffer, iter,
        txt_atom_iter_at_coord(&iter, &(buffer->atom_tree), coord)
    )
}

txt_cursor_s *txt_cursor_create_offset(txt_buffer_s *buffer, size_t offset)
{
    RETURN_CURSOR_AT_ITER(
        buffer, iter,
        txt_atom_iter_at_offset(&iter, &(buffer->atom_tree), offset)
    )
}

txt_cursor_s *txt_cursor_create_utf8_byte(txt_buffer_s *buffer, size_t offset)
{
    RETURN_CURSOR_AT_ITER(
        buffer, iter,
        txt_atom_iter_at_utf8_byte(&iter, &(buffer->atom_tree), offset)
    )
}

txt_cursor_s *txt_cursor_create_dup(txt_cursor_s *oth)
{
    txt_buffer_s *buffer = oth->buffer;
    RETURN_CURSOR_AT_ITER(buffer, iter, iter = txt_atom_iter(&(oth->atom)));
}

txt_cursor_s *txt_cursor_create_relative_line(txt_cursor_s *oth, long n)
{
    txt_cursor_s *rtn    = NULL;
    txt_buffer_s *buffer = oth->buffer;

    WITH_BUFFER_XACT(buffer)
    {
        txt_atom_iter_s it;
        long row = n + (long)txt_atom_row(&(oth->atom));
        if (row >= 0)
        {
            bool valid = txt_atom_iter_at_coord(
                &it,
                &(buffer->atom_tree),
                (txt_coord_s){ .row = (size_t)row, .col = 0 }
            );

            if (valid)
                rtn = create_inner(buffer, it);
        }
    }

    return rtn;
}
