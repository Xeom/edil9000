#include "transaction.h"

void txt_transaction_init(txt_transaction_s *xact)
{
    pthread_mutexattr_t attrs;
    pthread_mutexattr_init(&attrs);
    pthread_mutexattr_settype(&attrs, PTHREAD_MUTEX_RECURSIVE);
    pthread_mutex_init(&(xact->lock), &attrs);

    txt_event_init(&(xact->on_modify_text), sizeof(txt_modification_s));
    txt_event_init(&(xact->on_modify_font), 0);

    xact->revision_id = 0;
    xact->depth = 0;
    xact->text_modified = false;
    xact->font_modified = false;
}

void txt_transaction_kill(txt_transaction_s *xact)
{
    txt_event_kill(&(xact->on_modify_text));
    txt_event_kill(&(xact->on_modify_font));
    pthread_mutex_destroy(&(xact->lock));
}

void txt_transaction_begin(txt_transaction_s *xact)
{
    pthread_mutex_lock(&(xact->lock));
    xact->depth++;
}

void txt_transaction_end(txt_transaction_s *xact)
{
    // If we're at a depth of zero, clean up the whole transaction.
    if (--xact->depth == 0)
    {
        txt_modification_s mod;
        bool text_modified = xact->text_modified;
        bool font_modified = xact->font_modified;

        if (text_modified)
            mod = txt_change_kill(&(xact->change), &(xact->revision_id));

        xact->text_modified = false;
        xact->font_modified = false;

        // Unlock the transaction before triggering the event, allowing SYNC
        // handlers to obtain a transaction if they'd like.
        pthread_mutex_unlock(&(xact->lock));

        if (font_modified)
            txt_err_ignore(txt_event_trigger(&(xact->on_modify_font), NULL, 0));

        if (text_modified)
        {
            txt_err_ignore(
                txt_event_trigger(&(xact->on_modify_text), &mod, sizeof(mod))
            );
        }
    }
    else
    {
        pthread_mutex_unlock(&(xact->lock));
    }
}

void txt_transaction_modify_text(txt_transaction_s *xact, txt_atom_s *atom)
{
    // Add this modification to the change keeper-tracker.
    // Create a new one if this is the first modification of the transaction.
    if (!xact->text_modified)
        txt_change_init(&(xact->change), atom);
    else
        txt_change_add(&(xact->change), atom);

    xact->text_modified = true;
}

void txt_transaction_modify_font(txt_transaction_s *xact)
{
    xact->font_modified = true;
}
