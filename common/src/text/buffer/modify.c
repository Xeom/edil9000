#include "modify.h"

#include "macros.h"
#include "text/atom/atom.h"
#include "text/buffer/buffer.h"

void txt_buffer_insert_forward(
    txt_buffer_s *buf,
    txt_atom_s   *atom,
    uint32_t      codepoint,
    uint16_t      font
)
{
    txt_glyph_s glyph = { font, codepoint };
    txt_transaction_modify_text(&(buf->xact), atom);
    txt_atom_insert_forward(atom, glyph);
}

void txt_buffer_insert_backward(
    txt_buffer_s *buf,
    txt_atom_s   *atom,
    uint32_t      codepoint,
    uint16_t      font
)
{
    txt_transaction_modify_text(&(buf->xact), atom);

    txt_glyph_s glyph = { font, codepoint };
    txt_atom_insert_backward(atom, glyph);
}

void txt_buffer_pop_forward(
    txt_buffer_s *buf,
    txt_atom_s   *atom
)
{
    txt_glyph_s glyph;
    txt_transaction_modify_text(&(buf->xact), atom);
    txt_atom_pop_front(atom, &glyph);
}

void txt_buffer_pop_between(
    txt_buffer_s *buf,
    txt_atom_s   *start,
    txt_atom_s   *end
)
{
    if (txt_atom_cmp(start, end) > 0)
        SWAP(start, end);

    txt_transaction_modify_text(&(buf->xact), start);
    txt_transaction_modify_text(&(buf->xact), end);
    txt_atom_clear(start, end);
}

void txt_buffer_set_font(
    txt_buffer_s *buf,
    txt_atom_s   *atom,
    uint16_t      font,
    size_t        n
)
{
    txt_glyph_s *glyph;
    txt_atom_iter_s iter = txt_atom_iter(atom);
    while (n-- && (glyph = txt_atom_iter_next(&iter)))
        glyph->font = font;

    txt_transaction_modify_font(&(buf->xact));

}
