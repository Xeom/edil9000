#if !defined(TXT_TEXT_BUFFER_MODIFY_H)
# define TXT_TEXT_BUFFER_MODIFY_H
# include <stdint.h>
# include <stddef.h>

typedef struct txt_buffer txt_buffer_s;
typedef struct txt_atom   txt_atom_s;

/**
 * @file
 * Editing primitives for buffers.
 *
 * These primitives should be the only things that modify a buffer! Using atom
 * APIs directly is verboten.
 *
 */

/**
 * Insert a codepoint at the front of an atom.
 *
 */
void txt_buffer_insert_forward(
    txt_buffer_s *buf,
    txt_atom_s   *atom,
    uint32_t      codepoint,
    uint16_t      font
);

/**
 * Insert a codepoint at the back of an atom.
 *
 */
void txt_buffer_insert_backward(
    txt_buffer_s *buf,
    txt_atom_s   *atom,
    uint32_t      codepoint,
    uint16_t      font
);

/**
 * Pop a codepoint from the front of an atom.
 *
 */
void txt_buffer_pop_forward(
    txt_buffer_s *buf,
    txt_atom_s   *atom
);

/**
 * Delete the text between the starts of two atoms.
 *
 */
void txt_buffer_pop_between(
    txt_buffer_s *buf,
    txt_atom_s   *start,
    txt_atom_s   *end
);

/**
 * Set the fonts starting at an atom.
 *
 */
void txt_buffer_set_font(
    txt_buffer_s *buf,
    txt_atom_s   *atom,
    uint16_t      fonts,
    size_t        n
);

#endif
