#if !defined(TXT_BUFFER_CHANGE_H)
# define TXT_BUFFER_CHANGE_H
# include "text/atom/atom.h"
# include "txt_cursor.h"

typedef struct txt_change txt_change_s;

struct txt_change
{
    txt_atom_s start;
    txt_atom_s end;
    txt_pos_s orig_end;
};

void txt_change_init(txt_change_s *change, txt_atom_s *atom);

void txt_change_add(txt_change_s *change, txt_atom_s *atom);

txt_modification_s txt_change_kill(txt_change_s *change, size_t *revision_id);

#endif
