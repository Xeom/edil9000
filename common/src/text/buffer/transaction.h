#if !defined(TXT_BUFFER_TRANSACTION_H)
# define TXT_BUFFER_TRANSACTION_H
# include <pthread.h>
# include "change.h"
# include "event/event.h"
# include "text/atom/atom.h"

typedef struct txt_transaction txt_transaction_s;

struct txt_transaction
{
    pthread_mutex_t lock;
    int depth;

    txt_change_s change;

    /// A revision that increments with each modification to the text of the
    /// buffer.
    size_t revision_id;

    bool font_modified;
    bool text_modified;
    txt_event_s on_modify_text;
    txt_event_s on_modify_font;
};

void txt_transaction_init(txt_transaction_s *xact);

void txt_transaction_kill(txt_transaction_s *xact);

void txt_transaction_begin(txt_transaction_s *xact);

void txt_transaction_end(txt_transaction_s *xact);

void txt_transaction_modify_text(txt_transaction_s *xact, txt_atom_s *atom);

void txt_transaction_modify_font(txt_transaction_s *xact);

#endif
