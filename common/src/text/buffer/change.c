#include "change.h"

#include "text/atom/atom_tree.h"

static inline txt_pos_s get_pos(txt_atom_s *atom)
{
    txt_atom_iter_s sot = txt_atom_tree_start(atom->tree);
    return (txt_pos_s)
        {
            .coord     = txt_atom_coord(atom),
            .glyph_off = txt_atom_distance(sot, txt_atom_iter(atom)),
            .utf8_off  = txt_atom_utf8_byte(atom)
        };
}

void txt_change_init(txt_change_s *change, txt_atom_s *atom)
{
    txt_atom_iter_s iter = txt_atom_iter(atom);
    txt_atom_init_nudged_backward(&(change->start), iter);
    txt_atom_init(&(change->end), iter);

    change->orig_end = get_pos(atom);
}

void txt_change_add(txt_change_s *change, txt_atom_s *atom)
{
    txt_atom_iter_s iter = txt_atom_iter(atom);

    if (txt_atom_cmp(atom, &(change->start)) < 0)
    {
        txt_atom_kill(&(change->start));
        txt_atom_init_nudged_backward(&(change->start), iter);
    }

    if (txt_atom_cmp(atom, &(change->end)) > 0)
    {
        txt_pos_s next = get_pos(atom);
        txt_pos_s curr = get_pos(&(change->end));

        if (next.coord.row == curr.coord.row)
        {
            change->orig_end.coord.col += next.coord.col - curr.coord.col;
        }
        else
        {
            change->orig_end.coord.col  = next.coord.col;
            change->orig_end.coord.row += next.coord.row - curr.coord.row;
        }

        change->orig_end.utf8_off  += next.utf8_off - curr.utf8_off;
        change->orig_end.glyph_off += next.glyph_off - curr.glyph_off;

        txt_atom_kill(&(change->end));
        txt_atom_init(&(change->end), iter);
    }
}

txt_modification_s txt_change_kill(txt_change_s *change, size_t *revision_id)
{
    txt_modification_s rtn = {
        .new_revision_id = ++(*revision_id),
        .start           = get_pos(&(change->start)),
        .end_new         = get_pos(&(change->end)),
        .end_orig        = change->orig_end
    };

    txt_atom_kill(&(change->start));
    txt_atom_kill(&(change->end));

    return rtn;
}
