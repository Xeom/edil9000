#if !defined(TXT_BUFFER_BUFFER_H)
# define TXT_BUFFER_BUFFER_H
# include "txt_buffer.h"
# include "transaction.h"
# include "text/atom/atom_tree.h"

struct txt_buffer
{
    txt_transaction_s xact;
    txt_atom_tree_s   atom_tree;
};

#define WITH_BUFFER_XACT(_buf) \
    BEFORE_AFTER( \
        txt_buffer_transaction_begin(_buf), txt_buffer_transaction_end(_buf))

#endif
