#include "buffer.h"

#include "macros.h"
#include "refcount.h"

#include <stdbool.h>
#include <stdlib.h>

static void destructor(void *ptr)
{
    txt_buffer_s *buffer = ptr;
    txt_transaction_kill(&(buffer->xact));
    txt_atom_tree_kill(&(buffer->atom_tree));
}

txt_buffer_s *txt_buffer_create(void)
{
    txt_buffer_s *rtn = txt_refcount_create(sizeof(*rtn));
    txt_refcount_set_destructor(rtn, destructor);

    if (rtn)
    {
        txt_atom_tree_init(&(rtn->atom_tree));
        txt_transaction_init(&(rtn->xact));
    }

    return rtn;
}

void txt_buffer_destroy(txt_buffer_s *buffer)
{
    txt_refcount_decr(buffer);
}

void txt_buffer_transaction_begin(txt_buffer_s *buffer)
{
    txt_transaction_begin(&(buffer->xact));
}

void txt_buffer_transaction_end(txt_buffer_s *buffer)
{
    txt_transaction_end(&(buffer->xact));
}

size_t txt_buffer_num_lines(txt_buffer_s *buffer)
{
    size_t rtn;
    WITH_BUFFER_XACT(buffer)
    {
        rtn = txt_atom_tree_rows(&(buffer->atom_tree));
    }

    return rtn;
}

size_t txt_buffer_line_len(txt_buffer_s *buffer, size_t row)
{
    size_t rtn;
    WITH_BUFFER_XACT(buffer)
    {
        rtn = txt_atom_tree_cols(&(buffer->atom_tree), row);
    }

    return rtn;
}

txt_event_s *txt_buffer_on_modify_text(txt_buffer_s *buffer)
{
    return &(buffer->xact.on_modify_text);
}

txt_event_s *txt_buffer_on_modify_font(txt_buffer_s *buffer)
{
    return &(buffer->xact.on_modify_font);
}

size_t txt_buffer_revision_id(txt_buffer_s *buffer)
{
    size_t rtn;
    WITH_BUFFER_XACT(buffer)
    {
        rtn = buffer->xact.revision_id;
    }

    return rtn;
}


#include <snow/snow.h>
#if defined(SNOW_ENABLED)
# include <stdatomic.h>
# include "text/cursor/cursor.h"
# include "txt_cursor_io.h"
# include "event/executor.h"

static void create_n_by_m(txt_buffer_s *buf, uint32_t n, uint32_t m)
{
    uint32_t g = 'a';

    txt_cursor_s *cur = txt_cursor_create_end_of_text(buf);
    for (uint32_t row = 0; row < n; ++row)
    {
        for (uint32_t col = 0; col < m; ++col)
            txt_cursor_insert_backward(cur, g++);

        txt_cursor_insert_backward(cur, '\n');
    }
    txt_cursor_destroy(cur);
}

static void verify_n_by_m(txt_buffer_s *buf, uint32_t n, uint32_t m)
{
    uint32_t expect = 'a';
    txt_atom_iter_s iter = txt_atom_tree_start(&(buf->atom_tree));

    for (uint32_t row = 0; row < n; ++row)
    {
        for (uint32_t col = 0; col < m; ++col)
        {
            const txt_glyph_s *g = txt_atom_iter_next(&iter);
            assert(g);
            asserteq(g->codepoint, expect++);
        }

        const txt_glyph_s *g = txt_atom_iter_next(&iter);
        assert(g);
        asserteq(g->codepoint, '\n');
    }

    assert(!txt_atom_iter_next(&iter));
}

describe(buffer)
{
    txt_buffer_s *buffer;

    before_each()
    {
        buffer = txt_buffer_create();
    }

    after_each()
    {
        txt_buffer_destroy(buffer);
    }

    it ("Has one line when initialized")
    {
        asserteq(txt_buffer_num_lines(buffer), 1);
    }

    subdesc(on_text_modified)
    {
        it ("Is called on insert backward")
        {
            txt_cursor_s *cursor = txt_cursor_create_end_of_text(buffer);
            EXPECT_EVENT(
                txt_buffer_on_modify_text(buffer),
                txt_cursor_insert_backward(cursor, '1')
            );
            txt_cursor_destroy(cursor);
        }

        it ("Is called on insert forward")
        {
            txt_cursor_s *cursor = txt_cursor_create_end_of_text(buffer);
            EXPECT_EVENT(
                txt_buffer_on_modify_text(buffer),
                txt_cursor_insert_forward(cursor, '1')
            );
            txt_cursor_destroy(cursor);
        }

        it ("Is called on delete backward")
        {
            txt_cursor_s *cursor = txt_cursor_create_end_of_text(buffer);
            txt_cursor_insert_backward(cursor, '1');
            EXPECT_EVENT(
                txt_buffer_on_modify_text(buffer),
                txt_cursor_delete_backward(cursor)
            );
            txt_cursor_destroy(cursor);
        }

        it ("Is called on delete forward")
        {
            txt_cursor_s *cursor = txt_cursor_create_end_of_text(buffer);
            txt_cursor_insert_forward(cursor, '1');
            EXPECT_EVENT(
                txt_buffer_on_modify_text(buffer),
                txt_cursor_delete_forward(cursor)
            );
            txt_cursor_destroy(cursor);
        }

        it ("Is called on delete between")
        {
            txt_cursor_s *a = txt_cursor_create_end_of_text(buffer);
            txt_cursor_insert_forward(a, '1');
            txt_cursor_s *b = txt_cursor_create_offset(buffer, 1);
            EXPECT_EVENT(
                txt_buffer_on_modify_text(buffer),
                asserteq(txt_cursor_delete_between(a, b), TXT_OK)
            );
            txt_cursor_destroy(a);
            txt_cursor_destroy(b);
        }
    }

    subdesc(on_font_modify)
    {
        it ("Is called on set font")
        {
            txt_cursor_s *a = txt_cursor_create_end_of_text(buffer);
            txt_cursor_insert_forward(a, '1');
            EXPECT_EVENT(
                txt_buffer_on_modify_font(buffer),
                txt_cursor_set_font(a, 1, 1)
            );
            txt_cursor_destroy(a);
        }
    }

    subdesc(line_len)
    {
        it ("Returns zero for empty buffer")
        {
            asserteq(txt_buffer_line_len(buffer, 0), 0);
        }

        it ("Measures line length")
        {
            txt_cursor_s *cur = txt_cursor_create_end_of_text(buffer);
            for (size_t ln = 0; ln < 256; ++ln)
            {
                for (size_t len = 0; len < ln; ++len)
                    txt_cursor_insert_backward(cur, '!');

                txt_cursor_insert_backward(cur, '\n');
            }
            txt_cursor_destroy(cur);

            for (size_t ln = 0; ln < 256; ++ln)
                asserteq(txt_buffer_line_len(buffer, ln), ln);
        }
    }

    subdesc(fuzzing)
    {
        it ("Creates and destroys cursors randomly")
        {
            const uint32_t DIMS = 8;
            const size_t   NUM_CURS = 15;
            const size_t   NUM_REPS = 10000;
            txt_cursor_s *curs[NUM_CURS];

            create_n_by_m(buffer, DIMS, DIMS);
            memset(curs, '\0', sizeof(curs));

            for (size_t rep = 0; rep < NUM_REPS; ++rep)
            {
                size_t sel = rand() % NUM_CURS;
                if (curs[sel])
                    txt_cursor_destroy(curs[sel]);

                txt_coord_s coord = { rand() % DIMS, rand() % DIMS };

                verify_n_by_m(buffer, DIMS, DIMS);

                curs[sel] = txt_cursor_create_coord(buffer, coord);

                verify_n_by_m(buffer, DIMS, DIMS);
            }

            for (size_t n = 0; n < NUM_CURS; ++n)
            {
                if (curs[n])
                   txt_cursor_destroy(curs[n]);
            }
        }
    }
}

#endif
