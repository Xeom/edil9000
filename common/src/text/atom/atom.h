#if !defined(TXT_TEXT_ATOM_ATOM_H)
# define TXT_TEXT_ATOM_ATOM_H
# include "glyph.h"
# include "tree.h"
# include "iter.h"
# include "index.h"
# include <stdbool.h>

typedef enum txt_nudge txt_nudge_e;
typedef struct txt_atom txt_atom_s;
typedef struct txt_atom_tree txt_atom_tree_s;

struct txt_atom
{
    // Atoms are stored in as nodes in a tree.
    txt_tree_node_s thead;
    txt_atom_tree_s *tree;

    /// A ringbuffer of glyphs.
    size_t          allocated;
    size_t          offset;
    txt_glyph_s    *glyphs;
};

/**
 * Create a new atom at an iterator.
 *
 * The atom will be created after all other atoms at the same position.
 *
 */
void txt_atom_init(txt_atom_s *atom, txt_atom_iter_s iter);

/**
 * Create a new atom at an iterator.
 *
 * The atom will be created before all other atoms at the same position.
 *
 */
void txt_atom_init_nudged_backward(txt_atom_s *atom, txt_atom_iter_s iter);

void txt_atom_kill(txt_atom_s *atom);

bool txt_atom_is_linebreak(const txt_atom_s *atom);

int txt_atom_cmp(const txt_atom_s *a, const txt_atom_s *b);

void txt_atom_insert_forward(txt_atom_s *atom, txt_glyph_s glyph);

void txt_atom_insert_backward(txt_atom_s *atom, txt_glyph_s glyph);

bool txt_atom_pop_front(txt_atom_s *atom, txt_glyph_s *glyph_res);

void txt_atom_clear(txt_atom_s *a, txt_atom_s *b);

#endif
