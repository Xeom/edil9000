#if !defined(TXT_TEXT_ATOM_INDEX_H)
# define TXT_TEXT_ATOM_INDEX_H
# include <stddef.h>
# include "iter.h"
# include "txt_cursor.h"

size_t txt_atom_distance(txt_atom_iter_s a, txt_atom_iter_s b);

txt_coord_s txt_atom_coord(txt_atom_s *atom);

size_t txt_atom_row(txt_atom_s *atom);

size_t txt_atom_utf8_byte(txt_atom_s *atom);

bool txt_atom_iter_at_offset(
    txt_atom_iter_s *iter,
    txt_atom_tree_s *tree,
    size_t           off
);

bool txt_atom_iter_at_utf8_byte(
    txt_atom_iter_s *iter,
    txt_atom_tree_s *tree,
    size_t           off
);

bool txt_atom_iter_at_coord(
    txt_atom_iter_s *iter,
    txt_atom_tree_s *tree,
    txt_coord_s      coord
);

#endif
