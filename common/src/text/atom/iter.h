#if !defined(TXT_TEXT_ATOM_ITER_H)
# define TXT_TEXT_ATOM_ITER_H
# include "tree.h"
# include "glyph.h"
# include <stddef.h>
# include <stdbool.h>

typedef struct txt_atom txt_atom_s;
typedef struct txt_atom_iter txt_atom_iter_s;
typedef struct txt_atom_tree txt_atom_tree_s;

/**
 * An iterator through the glyphs of atoms.
 *
 * { NULL, 0 } is a valid iterator representing the end of the sequence.
 *
 */
struct txt_atom_iter
{
    txt_atom_tree_s *tree;
    txt_atom_s *atom;
    size_t      idx;
};

/**
 * Create an iterator at the position of an atom.
 *
 */
txt_atom_iter_s txt_atom_iter(txt_atom_s *atom);

/**
 * Seek an iterator forward or backwards by a number of glyphs.
 *
 */
bool txt_atom_iter_seek(txt_atom_iter_s *iter, long n);

/**
 * Return the glyph currently at the position of an iterator, and move the
 * iterator forward by one glyph.
 *
 */
txt_glyph_s *txt_atom_iter_next(txt_atom_iter_s *iter);

static inline bool txt_atom_iter_eq(txt_atom_iter_s a, txt_atom_iter_s b)
{
    return a.idx == b.idx && a.atom == b.atom;
}

int txt_atom_iter_cmp(txt_atom_iter_s a, txt_atom_iter_s b);

#endif
