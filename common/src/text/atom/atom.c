#include "atom.h"

#include "atom_tree.h"
#include "atom_private.h"

#include <stdlib.h>
#include <string.h>

#include <snow/snow.h>

static void grow(txt_atom_s *atom)
{
    size_t prev_size = atom->allocated;
    size_t new_size  = prev_size * 2;

    txt_glyph_s *glyphs = realloc(atom->glyphs, sizeof(*glyphs) * new_size);

    if (atom->offset + get_len(atom) > prev_size)
    {
        size_t to_copy = atom->offset + get_len(atom) - prev_size;
        memcpy(glyphs + prev_size, glyphs, sizeof(*glyphs) * to_copy);
    }

    atom->allocated = new_size;
    atom->glyphs    = glyphs;
}

static void incr_len(txt_atom_s *atom, txt_glyph_s glyph)
{
    txt_tree_set_weight(&(atom->thead), TXT_ATOM_GLYPHS, get_len(atom) + 1);
    txt_tree_set_weight(&(atom->thead), TXT_ATOM_UTF8_BYTES,
        txt_tree_get_weight(&(atom->thead), TXT_ATOM_UTF8_BYTES) +
        txt_glyph_utf8_bytes(glyph));
}

static void decr_len(txt_atom_s *atom, txt_glyph_s glyph)
{
    txt_tree_set_weight(&(atom->thead), TXT_ATOM_GLYPHS, get_len(atom) - 1);
    txt_tree_set_weight(&(atom->thead), TXT_ATOM_UTF8_BYTES,
        txt_tree_get_weight(&(atom->thead), TXT_ATOM_UTF8_BYTES) -
        txt_glyph_utf8_bytes(glyph));
}

static void reset_len(txt_atom_s *atom)
{
    txt_tree_set_weight(&(atom->thead), TXT_ATOM_GLYPHS, 0);
    txt_tree_set_weight(&(atom->thead), TXT_ATOM_GLYPHS, 0);
}

static void push_front(txt_atom_s *atom, txt_glyph_s glyph)
{
    if (get_len(atom) == atom->allocated)
        grow(atom);

    if (atom->offset >= 1)
        atom->offset -= 1;
    else
        atom->offset = atom->allocated - 1;

    *get_index(atom, 0) = glyph;
    incr_len(atom, glyph);
}

static txt_glyph_s pop_front(txt_atom_s *atom)
{
    txt_glyph_s rtn = *get_index(atom, 0);

    atom->offset += 1;
    atom->offset %= atom->allocated;
    decr_len(atom, rtn);

    return rtn;
    // TODO: shrink();
}

static void push_back(txt_atom_s *atom, txt_glyph_s glyph)
{
    if (get_len(atom) == atom->allocated)
        grow(atom);

    *get_index(atom, get_len(atom)) = glyph;
    incr_len(atom, glyph);

    txt_tree_set_weight(&(atom->thead), TXT_ATOM_GLYPHS, get_len(atom));
}

static txt_glyph_s pop_back(txt_atom_s *atom)
{
    txt_glyph_s rtn = *get_index(atom, get_len(atom) - 1);
    decr_len(atom, rtn);

    return rtn;
    // TODO: shrink();
}

typedef enum init_kind init_kind_e;
enum init_kind
{
    INIT_BACKWARD,
    INIT_FORWARD
};

static void init_inner(txt_atom_s *atom, txt_atom_iter_s iter, init_kind_e kind)
{
    // Initialize the new atom...
    atom->tree             = iter.tree;
    atom->allocated        = 16;
    atom->offset           = 0;
    atom->glyphs           = malloc(16 * sizeof(*atom->glyphs));

    txt_atom_s *base  = iter.atom;
    size_t      index = iter.idx;
    txt_tree_s *tree  = &(iter.tree->tree);

    if (!base)
    {
        base = node_to_atom(txt_tree_last(tree));
        if (base)
        {
            index = get_len(base);
        }
        else
        {
            // Insert the atom at the end
            txt_tree_insert_before(tree, NULL, &(atom->thead));
            return;
        }
    }

    if (index == 0 && kind == INIT_BACKWARD)
    {
        base = nudge_backward(base);
        if (txt_atom_is_linebreak(base))
            txt_tree_insert_after(tree, &(base->thead), &(atom->thead));
        else
            txt_tree_insert_before(tree, &(base->thead), &(atom->thead));
    }
    else
    {
        base = nudge_forward(base);
        txt_tree_insert_after(tree, &(base->thead), &(atom->thead));

        while (get_len(base) > (size_t)index)
            push_front(atom, pop_back(base));
    }
}

static void init_linebreak(txt_atom_s *atom, txt_atom_iter_s iter)
{
    init_inner(atom, iter, INIT_BACKWARD);
    txt_tree_set_weight(&(atom->thead), TXT_ATOM_LINES, 1);
}

static void split_line(txt_atom_iter_s iter)
{
    txt_atom_s *new = malloc(sizeof(*new));
    init_linebreak(new, iter);
}

static void join_line(txt_atom_s *loc)
{
    txt_atom_s *next = get_next(loc);
#if SNOW_ENABLED
    assert(next);
    assert(txt_atom_is_linebreak(next));
#endif
    txt_atom_kill(next);
    free(next);
}

void txt_atom_init(txt_atom_s *atom, txt_atom_iter_s iter)
{
    init_inner(atom, iter, INIT_FORWARD);
}

void txt_atom_init_nudged_backward(txt_atom_s *atom, txt_atom_iter_s iter)
{
    init_inner(atom, iter, INIT_BACKWARD);
}

void txt_atom_kill(txt_atom_s *atom)
{
    txt_atom_s *prev = get_prev(atom);
    if (prev)
    {
        while (get_len(atom) > 0)
            push_back(prev, pop_front(atom));
    }

    txt_tree_remove(&(atom->tree->tree), &(atom->thead));
    free(atom->glyphs);
}

bool txt_atom_is_linebreak(const txt_atom_s *atom)
{
    return txt_tree_get_weight(&(atom->thead), TXT_ATOM_LINES) != 0;
}

int txt_atom_cmp(const txt_atom_s *a, const txt_atom_s *b)
{
    return txt_tree_cmp(&(a->thead), &(b->thead));
}

void txt_atom_insert_forward(txt_atom_s *atom, txt_glyph_s glyph)
{
    push_front(atom, glyph);

    if (glyph.codepoint == '\n')
    {
        txt_atom_iter_s iter = txt_atom_iter(atom);
        txt_atom_iter_seek(&iter, 1);
        split_line(iter);
    }
}

void txt_atom_insert_backward(txt_atom_s *atom, txt_glyph_s glyph)
{
    txt_atom_s *prev = get_prev(atom);
    if (prev)
    {
        push_back(prev, glyph);

        if (glyph.codepoint == '\n')
            split_line(txt_atom_iter(atom));
    }
}

bool txt_atom_pop_front(txt_atom_s *atom, txt_glyph_s *glyph_res)
{
    atom = nudge_forward(atom);
    if (get_len(atom) != 0)
    {
        txt_glyph_s glyph = pop_front(atom);

        if (glyph.codepoint == '\n')
            join_line(atom);

        if (glyph_res)
            *glyph_res = glyph;

        return true;
    }
    else
    {
        return false;
    }
}

void txt_atom_clear(txt_atom_s *start, txt_atom_s *end)
{
    for (txt_atom_s *iter = start; iter && iter != end; iter = get_next(iter))
        reset_len(iter);

    for (txt_atom_s *iter = start; iter && iter != end; iter = get_next(iter))
    {
        while (get_next(iter) && txt_atom_is_linebreak(get_next(iter)))
            join_line(iter);
    }
}

#include <snow/snow.h>
#if SNOW_ENABLED

static void init_at_seek(txt_atom_s *atom, txt_atom_s *base, long n)
{
    txt_atom_iter_s iter = txt_atom_iter(base);
    assert(txt_atom_iter_seek(&iter, n));
    txt_atom_init(atom, iter);
}

describe(atom)
{
    txt_atom_tree_s tree;

    subdesc(circular_buffer)
    {
        txt_atom_s atom;

        before_each()
        {
            txt_atom_tree_init(&tree);
            txt_atom_init(&atom, txt_atom_tree_start(&tree));
        }

        after_each()
        {
            txt_atom_kill(&atom);
            txt_atom_tree_kill(&tree);
        }

        it ("Pushes to front and pops from back")
        {
            for (uint32_t g = 0; g < 1024; ++g)
            {
                txt_glyph_s glyph = TXT_GLYPH(g);
                push_front(&atom, glyph);
            }

            for (uint32_t g = 0; g < 1024; ++g)
                asserteq(pop_back(&atom).codepoint, g);
        }

        it ("Pushes to back and pops from front")
        {
            for (uint32_t g = 0; g < 1024; ++g)
            {
                txt_glyph_s glyph = TXT_GLYPH(g);
                push_back(&atom, glyph);
            }

            for (uint32_t g = 0; g < 1024; ++g)
                asserteq(pop_front(&atom).codepoint, g);
        }

        it ("Pushes to front while popping from back")
        {
            size_t orig = atom.allocated;

            for (uint32_t g = 0; g < 1024; ++g)
            {
                txt_glyph_s glyph = TXT_GLYPH(g);
                push_back(&atom, glyph);
                if (g >= 3)
                    asserteq(pop_front(&atom).codepoint, g - 3);
            }

            asserteq(atom.allocated, orig);
        }

        it ("Pushes to back while popping from front")
        {
            size_t orig = atom.allocated;

            for (uint32_t g = 0; g < 1024; ++g)
            {
                txt_glyph_s glyph = TXT_GLYPH(g);
                push_front(&atom, glyph);
                if (g >= 3)
                    asserteq(pop_back(&atom).codepoint, g - 3);
            }

            asserteq(atom.allocated, orig);
        }
    }

    subdesc(with_text)
    {
        txt_atom_s atom_1;

        before_each()
        {
            txt_atom_tree_init(&tree);
            txt_atom_init(&atom_1, txt_atom_tree_start(&tree));
            for (uint32_t g = 5; g > 0; --g)
            {
                txt_glyph_s glyph = TXT_GLYPH(g);
                txt_atom_insert_forward(&atom_1, glyph);
            }
        }

        after_each()
        {
            txt_atom_kill(&atom_1);
            txt_atom_tree_kill(&tree);
        }

        it ("Inits with positive offset")
        {
            txt_atom_s atom_2;
            init_at_seek(&atom_2, &atom_1, 3);

            asserteq(get_len(&atom_1), 3);
            asserteq(get_len(&atom_2), 2);

            asserteq(get_index(&atom_1, 0)->codepoint, 1);
            asserteq(get_index(&atom_1, 1)->codepoint, 2);
            asserteq(get_index(&atom_1, 2)->codepoint, 3);
            asserteq(get_index(&atom_2, 0)->codepoint, 4);
            asserteq(get_index(&atom_2, 1)->codepoint, 5);

            txt_atom_kill(&atom_2);
        }

        it ("Inits with negative offset")
        {
            txt_atom_s atom_2;
            txt_atom_s atom_3;
            init_at_seek(&atom_3, &atom_1,  4);
            init_at_seek(&atom_2, &atom_3, -2);

            asserteq(get_len(&atom_1), 2);
            asserteq(get_len(&atom_2), 2);
            asserteq(get_len(&atom_3), 1);

            asserteq(get_index(&atom_1, 0)->codepoint, 1);
            asserteq(get_index(&atom_1, 1)->codepoint, 2);
            asserteq(get_index(&atom_2, 0)->codepoint, 3);
            asserteq(get_index(&atom_2, 1)->codepoint, 4);
            asserteq(get_index(&atom_3, 0)->codepoint, 5);

            txt_atom_kill(&atom_2);
            txt_atom_kill(&atom_3);
        }

        it ("Accesses glyphs from subsequent atoms")
        {
            txt_atom_s atom_2;
            init_at_seek(&atom_2, &atom_1, 3);

            const txt_glyph_s *glyph_1, *glyph_2;

            txt_atom_iter_s iter_1 = txt_atom_iter(&atom_1);
            assert(txt_atom_iter_seek(&iter_1, 3));
            txt_atom_iter_s iter_2 = txt_atom_iter(&atom_2);

            assert(glyph_1 = txt_atom_iter_next(&iter_1));
            assert(glyph_2 = txt_atom_iter_next(&iter_2));
            asserteq(glyph_1->codepoint, glyph_2->codepoint);

            assert(glyph_1 = txt_atom_iter_next(&iter_1));
            assert(glyph_2 = txt_atom_iter_next(&iter_2));
            asserteq(glyph_1->codepoint, glyph_2->codepoint);

            assert(!txt_atom_iter_next(&iter_1));
            assert(!txt_atom_iter_next(&iter_2));

            txt_atom_kill(&atom_2);
        }

        it ("Orders atoms correctly")
        {
            txt_atom_s atom_2;
            txt_atom_s atom_3;
            init_at_seek(&atom_2, &atom_1, 1);
            init_at_seek(&atom_3, &atom_1, 2);

            asserteq(txt_atom_cmp(&atom_1, &atom_1),  0);
            asserteq(txt_atom_cmp(&atom_1, &atom_2), -1);
            asserteq(txt_atom_cmp(&atom_1, &atom_3), -1);
            asserteq(txt_atom_cmp(&atom_2, &atom_1),  1);
            asserteq(txt_atom_cmp(&atom_2, &atom_2),  0);
            asserteq(txt_atom_cmp(&atom_2, &atom_3), -1);
            asserteq(txt_atom_cmp(&atom_3, &atom_1),  1);
            asserteq(txt_atom_cmp(&atom_3, &atom_2),  1);
            asserteq(txt_atom_cmp(&atom_3, &atom_3),  0);

            txt_atom_kill(&atom_2);
            txt_atom_kill(&atom_3);
        }

        it ("Can be created before and after one another")
        {
            txt_atom_s before;
            txt_atom_s after;

            txt_atom_init(&after, txt_atom_iter(&atom_1));
            txt_atom_init_nudged_backward(&before, txt_atom_iter(&atom_1));

            asserteq(txt_atom_cmp(&atom_1, &before), 1);
            asserteq(txt_atom_cmp(&after, &atom_1), 1);
            asserteq(get_len(&before), 0);
            asserteq(get_len(&atom_1), 0);
            asserteq(get_len(&after), 5);

            txt_glyph_s glyph = TXT_GLYPH('x');

            txt_atom_insert_backward(&before, glyph);
            txt_atom_insert_backward(&atom_1, glyph);
            txt_atom_insert_backward(&after, glyph);

            asserteq(get_len(&before), 1);
            asserteq(get_len(&atom_1), 1);
            asserteq(get_len(&after), 5);

            txt_atom_kill(&before);
            txt_atom_kill(&after);
        }
    }
}

#endif
