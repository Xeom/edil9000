#include "atom_private.h"
#include "atom_tree.h"
#include "index.h"

#include <stdio.h>

size_t txt_atom_distance(txt_atom_iter_s a, txt_atom_iter_s b)
{
    // If both ends are specified, ensure they're properly ordered.
    if (txt_atom_iter_cmp(a, b) > 0)
        SWAP(a, b);

    size_t a_off = a.atom ?
        txt_tree_find(&(a.atom->thead), TXT_ATOM_GLYPHS) :
        txt_tree_size_by(&(b.tree->tree), TXT_ATOM_GLYPHS);

    size_t b_off = b.atom ?
        txt_tree_find(&(b.atom->thead), TXT_ATOM_GLYPHS) :
        txt_tree_size_by(&(b.tree->tree), TXT_ATOM_GLYPHS);

    return (b_off + b.idx) - (a_off + a.idx);
}

bool txt_atom_iter_at_offset(
    txt_atom_iter_s *iter,
    txt_atom_tree_s *tree,
    size_t           off
)
{
    // Atom trees always contain at least one node, so atom != null.
    txt_atom_s *atom = node_to_atom(txt_tree_index(
        &(tree->tree),
        off,
        TXT_ATOM_GLYPHS
    ));
    size_t idx = off - txt_atom_distance(
        txt_atom_tree_start(tree),
        txt_atom_iter(atom)
    );

    if (idx >= get_len(atom))
    {
        *iter = txt_atom_tree_end(tree);
        return idx == get_len(atom);
    }
    else
    {
        iter->tree = tree;
        iter->atom = atom;
        iter->idx  = idx;
        return true;
    }
}

txt_coord_s txt_atom_coord(txt_atom_s *atom)
{
    size_t row = txt_atom_row(atom);
    txt_atom_s *sol = txt_atom_tree_sol(atom->tree, row);

    return (txt_coord_s){
        .row = row,
        .col = txt_atom_distance(txt_atom_iter(sol), txt_atom_iter(atom))
    };
}

size_t txt_atom_row(txt_atom_s *atom)
{
    txt_atom_s *next = get_next(atom);

    return next ?
        txt_tree_find(&(next->thead), TXT_ATOM_LINES) - 1 :
        txt_tree_size_by(&(atom->tree->tree), TXT_ATOM_LINES) - 1;
}

size_t txt_atom_utf8_byte(txt_atom_s *atom)
{
    return txt_tree_find(&(atom->thead), TXT_ATOM_UTF8_BYTES);
}

bool txt_atom_iter_at_utf8_byte(
    txt_atom_iter_s *iter,
    txt_atom_tree_s *atom_tree,
    size_t           off_utf8
)
{
    txt_atom_s *atom = node_to_atom(txt_tree_index(
        &(atom_tree->tree),
        off_utf8,
        TXT_ATOM_UTF8_BYTES
    ));

    *iter = txt_atom_iter(atom);

    // Find the UTF8 index within this atom of our desired iterator location.
    long idx_utf8 = off_utf8 -
        txt_tree_find(&(atom->thead), TXT_ATOM_UTF8_BYTES);

    const txt_glyph_s *glyph = NULL;
    while (idx_utf8 > 0 && (glyph = txt_atom_iter_next(iter)))
        idx_utf8 -= txt_glyph_utf8_bytes(*glyph);

    return idx_utf8 == 0;
}

bool txt_atom_iter_at_coord(
    txt_atom_iter_s *iter,
    txt_atom_tree_s *atom_tree,
    txt_coord_s      coord
)
{
    txt_atom_s *sol = txt_atom_tree_sol(atom_tree, coord.row);
    if (sol)
    {
        *iter = txt_atom_iter(sol);
        return txt_atom_iter_seek(iter, coord.col);
    }
    else
    {
        *iter = txt_atom_tree_end(atom_tree);
        return false;
    }
}

#include <snow/snow.h>
#if SNOW_ENABLED

describe(atom_index)
{
    txt_atom_tree_s tree;
    const txt_glyph_s g = TXT_GLYPH('x');

    subdesc(without_text)
    {
        before_each()
        {
            txt_atom_tree_init(&tree);
        }

        after_each()
        {
            txt_atom_tree_kill(&tree);
        }

        it ("Calculates distance")
        {
            txt_atom_s atom_1;
            txt_atom_s atom_2;
            txt_atom_s atom_3;

            txt_atom_init(&atom_1, txt_atom_tree_end(&tree));
            txt_atom_insert_forward(&atom_1, g);
            txt_atom_insert_forward(&atom_1, g);

            txt_atom_init(&atom_2, txt_atom_tree_end(&tree));
            txt_atom_insert_forward(&atom_2, g);
            txt_atom_insert_forward(&atom_2, g);

            txt_atom_init(&atom_3, txt_atom_tree_end(&tree));

            txt_atom_iter_s iter_1 = txt_atom_iter(&atom_1);
            txt_atom_iter_s iter_2 = txt_atom_iter(&atom_2);
            txt_atom_iter_s iter_3 = txt_atom_iter(&atom_3);

            asserteq(txt_atom_distance(iter_1, iter_2), 2);
            asserteq(txt_atom_distance(iter_2, iter_3), 2);
            asserteq(txt_atom_distance(iter_1, iter_3), 4);

            // Try with reversed parameters
            asserteq(txt_atom_distance(iter_2, iter_1), 2);
            asserteq(txt_atom_distance(iter_3, iter_2), 2);
            asserteq(txt_atom_distance(iter_3, iter_1), 4);

            txt_atom_kill(&atom_1);
            txt_atom_kill(&atom_2);
            txt_atom_kill(&atom_3);
        }
    }

    subdesc(with_text)
    {
        const uint32_t CONTENTS[] = U"Row 0\nRow 1\nüñîçøđè ŕǫẃ 2\n";

        before_each()
        {
            txt_atom_tree_init(&tree);
            txt_atom_s atom;
            txt_atom_init(&atom, txt_atom_tree_end(&tree));
            for (const uint32_t *it = CONTENTS; *it; ++it)
            {
                txt_glyph_s g = TXT_GLYPH(*it);
                txt_atom_insert_backward(&atom, g);
            }
            txt_atom_kill(&atom);
        }

        after_each()
        {
            txt_atom_tree_kill(&tree);
        }

        it ("Calculates distance to end of buffer")
        {
            txt_atom_iter_s middle;
            const size_t LEN = DIM(CONTENTS) - 1;
            assert(txt_atom_iter_at_offset(&middle, &tree, LEN / 2));

            txt_atom_iter_s start = txt_atom_tree_start(&tree);
            txt_atom_iter_s end = txt_atom_tree_end(&tree);
            asserteq(txt_atom_distance(start, middle), LEN / 2);
            asserteq(txt_atom_distance(middle, end), LEN - (LEN / 2));
            asserteq(txt_atom_distance(start, end), LEN);
        }

        it ("Creates iterators at offsets")
        {
            for (size_t off = 0; off < DIM(CONTENTS); ++off)
            {
                txt_atom_iter_s it;
                assert(txt_atom_iter_at_offset(&it, &tree, off));
                asserteq(
                    txt_atom_distance(txt_atom_tree_start(&tree), it),
                    off
                );
            }
        }

        it ("Creates iterators at coords")
        {
            txt_coord_s expect_coord = { 0 };

            for (size_t off = 0; off < DIM(CONTENTS); ++off)
            {
                txt_atom_iter_s it;
                assert(txt_atom_iter_at_coord(&it, &tree, expect_coord));
                asserteq(
                    txt_atom_distance(txt_atom_tree_start(&tree), it),
                    off
                );

                if (CONTENTS[off] == '\n')
                {
                    expect_coord.row++;
                    expect_coord.col = 0;
                }
                else
                {
                    expect_coord.col++;
                }
            }
        }

        it ("Creates iterators at utf8 bytes")
        {
            size_t expect_off = 0;

            for (size_t off = 0; off < DIM(CONTENTS); ++off)
            {
                txt_atom_iter_s it;
                assert(txt_atom_iter_at_utf8_byte(&it, &tree, expect_off));

                asserteq(
                    txt_atom_distance(txt_atom_tree_start(&tree), it),
                    off
                );

                txt_glyph_s g = TXT_GLYPH(CONTENTS[off]);
                expect_off += txt_glyph_utf8_bytes(g);
            }
        }
    }
}
#endif
