#if !defined(LIBTXT_CORE_GLYPH_H)
# define LIBTXT_CORE_GLYPH_H
# include <stdint.h>

# define TXT_GLYPH(_cp) (txt_glyph_s){ .codepoint = _cp, .font = 0 }

typedef struct txt_glyph txt_glyph_s;

struct txt_glyph
{
    uint32_t font      : 11;
    uint32_t codepoint : 21;
};

static inline uint8_t txt_glyph_utf8_bytes(txt_glyph_s g)
{
    if (g.codepoint <= 0x00007f)
        return 1;
    if (g.codepoint <= 0x0007ff)
        return 2;
    if (g.codepoint <= 0x00ffff)
        return 3;
    return 4;
}

#endif
