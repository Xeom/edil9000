#include "atom_tree.h"
#include "atom_private.h"

void txt_atom_tree_init(txt_atom_tree_s *atom_tree)
{
    txt_tree_init(&(atom_tree->tree));
    txt_atom_init(
        &(atom_tree->start_of_text),
        (txt_atom_iter_s){ .tree = atom_tree }
    );
    txt_tree_set_weight(&(atom_tree->start_of_text.thead), TXT_ATOM_LINES, 1);
}

void txt_atom_tree_kill(txt_atom_tree_s *atom_tree)
{
    txt_atom_clear(&(atom_tree->start_of_text), NULL);
    txt_atom_kill(&(atom_tree->start_of_text));
}

txt_atom_iter_s txt_atom_tree_start(txt_atom_tree_s *atom_tree)
{
    return txt_atom_iter(&(atom_tree->start_of_text));
}

txt_atom_iter_s txt_atom_tree_end(txt_atom_tree_s *atom_tree)
{
    return (txt_atom_iter_s){ .tree = atom_tree };
}

size_t txt_atom_tree_rows(txt_atom_tree_s *atom_tree)
{
    return txt_tree_size_by(&(atom_tree->tree), TXT_ATOM_LINES);
}

size_t txt_atom_tree_cols(txt_atom_tree_s *atom_tree, size_t row)
{
    txt_atom_s *this_line = txt_atom_tree_sol(atom_tree, row);
    txt_atom_s *next_line = txt_atom_tree_sol(atom_tree, row + 1);

    // Handle the case where this line doesn't exist.
    if (!this_line)
        return 0;

    if (next_line)
    {
        return txt_atom_distance(
            txt_atom_iter(this_line),
            txt_atom_iter(next_line)
        ) - 1;
    }
    else
    {
        return txt_atom_distance(
            txt_atom_iter(this_line),
            txt_atom_tree_end(atom_tree)
        );
    }
}

txt_atom_s *txt_atom_tree_sol(txt_atom_tree_s *atom_tree, size_t row)
{
    return (row >= txt_atom_tree_rows(atom_tree)) ?
        NULL :
        node_to_atom(txt_tree_index(&(atom_tree->tree), row, TXT_ATOM_LINES));
}

