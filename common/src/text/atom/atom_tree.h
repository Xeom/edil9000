#if !defined(TXT_TEXT_ATOM_ATOMTREE_H)
# define TXT_TEXT_ATOM_ATOMTREE_H
# include "atom.h"

struct txt_atom_tree
{
    txt_tree_s tree;
    txt_atom_s start_of_text;
};

void txt_atom_tree_init(txt_atom_tree_s *atom_tree);

void txt_atom_tree_kill(txt_atom_tree_s *atom_tree);

/**
 * Get an iterator to the start of the tree.
 *
 */
txt_atom_iter_s txt_atom_tree_start(txt_atom_tree_s *atom_tree);

/**
 * Get an iterator to the end of the tree.
 *
 */
txt_atom_iter_s txt_atom_tree_end(txt_atom_tree_s *atom_tree);

/**
 * Get the start of a line.
 *
 */
txt_atom_s *txt_atom_tree_sol(txt_atom_tree_s *atom_tree, size_t row);

/**
 * Get the number of rows in a tree.
 *
 */
size_t txt_atom_tree_rows(txt_atom_tree_s *atom_tree);

/**
 * Get the number of columns in a row of a tree.
 *
 */
size_t txt_atom_tree_cols(txt_atom_tree_s *atom_tree, size_t row);

#endif
