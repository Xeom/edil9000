#if !defined(TXT_TEXT_ATOM_PRIVATE_H)
# define TXT_TEXT_ATOM_PRIVATE_H
# include "atom.h"
# include "macros.h"

static const txt_weight_s TXT_ATOM_LINES = { 1 };
static const txt_weight_s TXT_ATOM_GLYPHS = { 2 };
static const txt_weight_s TXT_ATOM_UTF8_BYTES = { 3 };

static inline txt_atom_s *node_to_atom(txt_tree_node_s *node)
{
    return node ? CONTAINER_OF(node, txt_atom_s, thead) : NULL;
}

static inline txt_atom_s *get_next(txt_atom_s *atom)
{
    return node_to_atom(txt_tree_next(&(atom->thead)));
}

static inline txt_atom_s *get_prev(txt_atom_s *atom)
{
    return node_to_atom(txt_tree_prev(&(atom->thead)));
}

static inline size_t get_len(const txt_atom_s *atom)
{
    return txt_tree_get_weight(&(atom->thead), TXT_ATOM_GLYPHS);
}

static inline txt_glyph_s *get_index(txt_atom_s *atom, size_t ind)
{
    return &atom->glyphs[(atom->offset + ind) % atom->allocated];
}

static inline txt_atom_s *nudge_forward(txt_atom_s *atom)
{
    txt_atom_s *next;
    while (get_len(atom) == 0 && (next = get_next(atom)))
        atom = next;

    return atom;
}

static inline txt_atom_s *nudge_backward(txt_atom_s *atom)
{
    txt_atom_s *prev;
    while ((prev = get_prev(atom)) && get_len(prev) == 0)
        atom = prev;

    return atom;
}

#endif
