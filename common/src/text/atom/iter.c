#include <snow/snow.h>

#include "atom_private.h"
#include "atom_tree.h"
#include "index.h"
#include "iter.h"

// Enable a fast path for short iterations
#define SHORT_ITER_FAST_PATH 1

static txt_atom_iter_s create(txt_atom_s *atom, size_t idx)
{
#if SNOW_ENABLED
    assert(atom, "Tried to create an iterator from NULL atom.");
#endif

    txt_atom_tree_s *tree = atom->tree;

    while (atom && get_len(atom) <= idx)
    {
        idx -= get_len(atom);
        atom = get_next(atom);
    }

    return (txt_atom_iter_s){ .tree = tree, .atom = atom, .idx = idx };
}

txt_atom_iter_s txt_atom_iter(txt_atom_s *atom)
{
    return create(atom, 0);
}

__attribute__(( unused ))
static bool iter_nearby(txt_atom_iter_s *rtn, txt_atom_s *atom, long off)
{
    // For negative n, try going back by one atom. This is a fast path for small
    // backwards seeks.
    if (off < 0)
    {
        txt_atom_s *prev = get_prev(nudge_backward(atom));
        if (prev)
        {
            atom = prev;
            off += get_len(prev);
        }
    }
    // For n too big, try going forward by one atom. This is a fast path for
    // small forwards seeks.
    else if (off >= (long)get_len(atom))
    {
        txt_atom_s *next = get_next(atom);
        if (next)
        {
            off -= get_len(atom);
            atom = nudge_forward(next);
        }
    }

    // Fast path for seeks within the current atom.
    if (off >= 0 && off < (long)get_len(atom))
    {
        *rtn = create(atom, (size_t)off);
        return true;
    }
    else
    {
        return false;
    }
}

static bool iter_faraway(txt_atom_iter_s *rtn, txt_atom_s *atom, long off)
{
    // O(log N) path for seeks anywhere in text.
    //
    // Get the absolute offset of the new desired seek.
    long tree_offset = (long)txt_atom_distance(
        txt_atom_tree_start(atom->tree),
        create(atom, 0)) + off;

    // If we try to seek before the start of the text, give up.
    if (tree_offset < 0)
    {
        *rtn = txt_atom_tree_start(atom->tree);
        return false;
    }

    // Perform an absolute seek
    return txt_atom_iter_at_offset(rtn, atom->tree, tree_offset);
}

bool txt_atom_iter_seek(txt_atom_iter_s *iter, long n)
{
    txt_atom_s *atom = iter->atom;
    long off = n + (long)iter->idx;

    // Handle the case where the iterator is at the end of the tree.
    if (!atom)
    {
        atom = node_to_atom(txt_tree_last(&iter->tree->tree));
        if (!atom)
            return false;

        off += get_len(atom);
    }

#if SHORT_ITER_FAST_PATH
    // Try to use a fast-path for nearby iteration
    if (iter_nearby(iter, atom, off))
        return true;
#endif

    // Use a general method for iterating anywhere in the text
    return iter_faraway(iter, atom, off);
}

txt_glyph_s *txt_atom_iter_next(txt_atom_iter_s *iter)
{
    if (!iter->atom || iter->idx >= get_len(iter->atom))
        return NULL;

    txt_glyph_s *rtn = get_index(iter->atom, iter->idx);
    txt_atom_iter_seek(iter, 1);

    return rtn;
}

int txt_atom_iter_cmp(txt_atom_iter_s a, txt_atom_iter_s b)
{
    if (a.atom == NULL && b.atom == NULL)
        return 0;

    if (a.atom == NULL)
        return 1;

    if (b.atom == NULL)
        return -1;

    int atom_cmp = txt_atom_cmp(a.atom, b.atom);
    if (atom_cmp != 0)
        return atom_cmp;

    if (a.idx > b.idx)
        return 1;

    if (a.idx < b.idx)
        return -1;

    return 0;
}

#include <stdio.h>

describe(atom_iter)
{
    txt_atom_tree_s tree;

    before_each()
    {
        txt_atom_tree_init(&tree);
    }

    after_each()
    {
        txt_atom_tree_kill(&tree);
    }

    it ("Compares the start and end of an empty buffer as equal.")
    {
        asserteq(
            txt_atom_iter_cmp(
                txt_atom_tree_start(&tree),
                txt_atom_tree_end(&tree)),
            0
        );
    }

    it ("Compares iterators in atom tree")
    {
        const char CONTENTS[] = "L1\nL2\nL3";

        txt_atom_s inserter;
        txt_atom_init(&inserter, txt_atom_tree_start(&tree));
        for (size_t i = 0; i < sizeof(CONTENTS); ++i)
        {
            txt_glyph_s g = TXT_GLYPH(CONTENTS[i]);
            txt_atom_insert_backward(&inserter, g);
        }
        txt_atom_kill(&inserter);

        txt_atom_iter_s iter[sizeof(CONTENTS) + 1];
        for (size_t i = 0; i < DIM(iter); ++i)
        {
            iter[i] = txt_atom_tree_start(&tree);
            txt_atom_iter_seek(&iter[i], i);
        }

        for (size_t a = 0; a < DIM(iter); ++a)
        for (size_t b = 0; b < DIM(iter); ++b)
        {
            if (a > b)
            {
                asserteq(txt_atom_iter_cmp(iter[a], iter[b]), 1);
            }
            else if (a < b)
            {
                asserteq(txt_atom_iter_cmp(iter[a], iter[b]), -1);
            }
            else
            {
                asserteq(txt_atom_iter_cmp(iter[a], iter[b]), 0);
            }
        }
    }
}
