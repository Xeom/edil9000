#if !defined(TXT_EVENT_EXECUTOR_H)
# define TXT_EVENT_EXECUTOR_H
# include "txt_executor.h"
# include "list.h"
# include <pthread.h>
# include <stdbool.h>

typedef struct txt_work txt_work_s;

struct txt_executor
{
    pthread_t       thread;
    bool            alive;
    txt_list_s      work;
    pthread_mutex_t lock;
    pthread_cond_t  ready;
};

void txt_executor_init(txt_executor_s *exec);

void txt_executor_kill(txt_executor_s *exec);

void txt_executor_enqueue(txt_executor_s *exec, txt_work_s *work);

#endif
