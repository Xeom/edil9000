#if !defined(TXT_EVENT_WORK_H)
# define TXT_EVENT_WORK_H
# include "list.h"
# include "cb.h"

typedef struct txt_work txt_work_s;

struct txt_work
{
    txt_list_s lhead;
    txt_cb_t   funct;
    char data[];
};

txt_work_s *txt_work_create(
    txt_cb_t    funct,
    const void *params,
    size_t      param_len
);

#define TXT_CREATE_WORK(_name, ...) \
    txt_work_create( \
        _name, \
        &(_name ## _params_t){ __VA_ARGS__ }, \
        sizeof(_name ## _params_t) \
    )

#endif
