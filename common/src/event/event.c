#include "event.h"

#include "refcount.h"
#include "executor.h"
#include "handler.h"
#include "macros.h"
#include "work.h"

#include <stdatomic.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

static inline void prune_all(txt_event_s *evt);
static inline void prune_dead(txt_event_s *evt);

txt_event_s *txt_event_create(size_t param_len, txt_handler_flags_e flags)
{
    txt_event_s *rtn = malloc(sizeof(txt_event_s));
    if (rtn)
    {
        txt_event_init(rtn, param_len);
        rtn->flags = flags;
    }

    return rtn;
}

void txt_event_destroy(txt_event_s *evt)
{
    txt_event_kill(evt);
    free(evt);
}

void txt_event_init(txt_event_s *evt, size_t param_len)
{
    evt->flags        = TXT_EVENT_SYNC | TXT_EVENT_ASYNC;
    evt->param_len    = param_len;

    txt_list_init(&(evt->sync_handlers));
    txt_list_init(&(evt->async_handlers));

    pthread_mutex_init(&(evt->trigger_lock), NULL);
    pthread_mutex_init(&(evt->handler_lock), NULL);
}

void txt_event_kill(txt_event_s *evt)
{
    prune_all(evt);

    pthread_mutex_destroy(&(evt->handler_lock));
}

static txt_list_s *next_handler(txt_event_s *evt, txt_list_s *curr)
{
    txt_list_s *rtn;
    WITH_MTX(&(evt->handler_lock))
    {
        rtn = curr->next;
    }

    if (rtn == &(evt->async_handlers) ||
        rtn == &(evt->sync_handlers))
    {
        rtn = NULL;
    }

    return rtn;
}

struct cb_sync_iface
{
    txt_handler_s *handler;
    atomic_int    *running;
    const void    *params;
};

TXT_DECLARE_CB_3(
    sync_cb, struct cb_sync_iface,
    txt_handler_s *, handler,
    atomic_int *,    running,
    const void *,    params
)
{
    txt_handler_trigger(handler, params);
    (*running)--;
    txt_refcount_decr(handler);
}

struct cb_async_iface
{
    txt_handler_s *handler;
    void          *params;
};

TXT_DECLARE_CB_2(
    async_cb, struct cb_async_iface,
    txt_handler_s *, handler,
    void *,          params
)
{
    txt_handler_trigger(handler, params);

    if (params)
        txt_refcount_decr(params);

    txt_refcount_decr(handler);
}

txt_err_e txt_event_trigger(txt_event_s *evt, const void *params, size_t len)
{
    txt_list_s *iter;
    pthread_t self = pthread_self();
    atomic_int running = 0;

    // This event is meant to have a specific size.
    if (evt->param_len != len)
        return TXT_ERR_INV_SIZE;

    // Ensure we treat zero-len params as NULL.
    if (len == 0) params = NULL;

    WITH_MTX(&(evt->trigger_lock))
    {
        // Prune any dead handlers first.
        prune_dead(evt);

        // Handle synchronous handlers.
        iter = &(evt->sync_handlers);
        while ((iter = next_handler(evt, iter)))
        {
            txt_handler_s *handler = CONTAINER_OF(iter, txt_handler_s, lhead);
            txt_executor_s *exec = handler->exec;

            if (
                pthread_equal(self, exec->thread) ||
                handler->flags & TXT_EVENT_THREAD_SAFE
            )
            {
                txt_handler_trigger(handler, params);
            }
            else
            {
                running++;
                // While the handler is enqueued, it needs to have a ref.
                txt_refcount_incr(handler);
                txt_executor_enqueue(
                    exec,
                    TXT_CREATE_WORK(
                        sync_cb,
                        .handler = handler,
                        .running = &running,
                        .params  = params
                    )
                );
            }
        }

        void *heap_params = NULL;

        // Handle asynchronous handlers.
        iter = &(evt->async_handlers);
        while ((iter = next_handler(evt, iter)))
        {
            txt_handler_s *handler = CONTAINER_OF(iter, txt_handler_s, lhead);
            txt_executor_s *exec = handler->exec;

            if (heap_params == NULL && len != 0)
            {
                heap_params = txt_refcount_create(len);
                memcpy(heap_params, params, len);
            }

            if (txt_handler_begin_async_task(handler, heap_params))
            {
                txt_refcount_incr(handler);
                if (heap_params)
                    txt_refcount_incr(heap_params);

                txt_executor_enqueue(
                    exec,
                    TXT_CREATE_WORK(
                        async_cb,
                        .handler = handler,
                        .params  = heap_params
                    )
                );
            }
        }

        if (heap_params)
            txt_refcount_decr(heap_params);

        // Wait for synchronous handlers in other threads to finish.
        while (running > 0)
        {
            // TODO: Actual sync mechanism
            usleep(1);
        }
    }

    return TXT_OK;
}

txt_err_e txt_event_listen(txt_event_s *evt, txt_handler_s *handler)
{
    if (!txt_list_empty(&(handler->lhead)))
    {
        // This handler is already in use!
        return TXT_ERR_IN_USE;
    }

    txt_err_e rtn = TXT_OK;

    WITH_MTX(&(evt->handler_lock))
    {
        if (handler->flags & evt->flags & TXT_EVENT_ASYNC)
        {
            txt_list_join(&(evt->async_handlers), &(handler->lhead));
            txt_refcount_incr(handler);
        }
        else if (handler->flags & evt->flags & TXT_EVENT_SYNC)
        {
            txt_list_join(&(evt->sync_handlers), &(handler->lhead));
            txt_refcount_incr(handler);
        }
        else
        {
            // We can't agree whether to be sync or not.
            rtn = TXT_ERR_INV_FLAGS;
        }
    }

    return rtn;
}

static void prune(txt_event_s *evt, txt_list_s *list, bool all)
{
    WITH_MTX(&(evt->handler_lock))
    TXT_LIST_FOREACH(list, iter)
    {
        txt_handler_s *handler = CONTAINER_OF(iter, txt_handler_s, lhead);
        if (handler->dead || all)
        {
            iter = iter->prev;
            txt_list_grab(&(handler->lhead));
            txt_refcount_decr(handler);
        }
    }
}

// Remove all handlers from our lists.
static inline void prune_all(txt_event_s *evt)
{
    prune(evt, &(evt->sync_handlers),  true);
    prune(evt, &(evt->async_handlers), true);
}

// Remove only dead handlers from our lists.
static inline void prune_dead(txt_event_s *evt)
{
    prune(evt, &(evt->sync_handlers),  false);
    prune(evt, &(evt->async_handlers), false);
}

#include <snow/snow.h>
#if SNOW_ENABLED
#include "cb.h"

struct trigger_params
{
    txt_event_s *event;
    const void  *params;
    size_t       len;
    atomic_int  *to_observe;
    atomic_int  *observed;
};

#define WAIT(_n) usleep(_n * 10000)

static void test_cb(
    txt_handler_s *handler, void *handler_params, const void *evt_params
)
{
    (void)handler;
    WAIT(1);
    *(atomic_int *)handler_params = *(const int *)evt_params;
}

static void slow_cb(
    txt_handler_s *handler, void *handler_params, const void *evt_params
)
{
    (void)handler; (void)evt_params;
    WAIT(2);
    *(atomic_bool *)handler_params = true;
}

static void remove_self_cb(
    txt_handler_s *handler, void *handler_params, const void *evt_params
)
{
    (void)handler_params; (void)evt_params;
    txt_handler_destroy(handler);
}

static const int TEST_VAL = 99;

TXT_DECLARE_CB_5(
    trigger_cb, struct trigger_params,
    txt_event_s *, event,
    const void  *, params,
    size_t,        len,
    atomic_int  *, to_observe,
    atomic_int  *, observed
)
{
    if (txt_event_trigger(event, params, len) != TXT_OK)
    {
        puts("HELP");
    }
    if (observed && to_observe)
        *observed = *to_observe;
}

describe(event)
{
    subdesc(threading_situations)
    {
        txt_event_s event;
        txt_handler_s *handler;
        txt_executor_s executor;
        atomic_int out;

        before_each()
        {
            txt_executor_init(&executor);
            txt_event_init(&event,  sizeof(int));
            handler = txt_handler_create(&executor);
            txt_handler_set_cb(handler, test_cb, &out);
            out = 0;
        }

        after_each()
        {
            txt_executor_kill(&executor);
            txt_handler_destroy(handler);
            txt_event_kill(&event);
        }

        it ("Triggers synchronously in different thread")
        {
            txt_handler_set_flags(handler, TXT_EVENT_SYNC);
            asserteq(txt_event_listen(&event, handler), TXT_OK);

            asserteq(txt_event_trigger(&event, &TEST_VAL, sizeof(int)), 0);
            asserteq(out, TEST_VAL);
        }

        it ("Triggers asynchronously in different thread")
        {
            txt_handler_set_flags(handler, TXT_EVENT_ASYNC);
            asserteq(txt_event_listen(&event, handler), TXT_OK);

            asserteq(txt_event_trigger(&event, &TEST_VAL, sizeof(int)), 0);
            asserteq(out, 0);
            WAIT(2);
            asserteq(out, TEST_VAL);
        }

        it ("Triggers synchronously thread-safe in different thread")
        {
            txt_handler_set_flags(
                handler, TXT_EVENT_SYNC | TXT_EVENT_THREAD_SAFE
            );
            asserteq(txt_event_listen(&event, handler), TXT_OK);

            asserteq(txt_event_trigger(&event, &TEST_VAL, sizeof(int)), 0);
            asserteq(out, TEST_VAL);
        }

        it ("Triggers synchronously in same thread")
        {
            atomic_int observed = 0;

            txt_handler_set_flags(handler, TXT_EVENT_SYNC);
            asserteq(txt_event_listen(&event, handler), TXT_OK);

            txt_executor_enqueue(
                &executor,
                TXT_CREATE_WORK(
                    trigger_cb, &event, &TEST_VAL, sizeof(int), &out, &observed
                )
            );

            WAIT(2);
            asserteq(out, TEST_VAL);
            asserteq(observed, TEST_VAL);
        }

        it ("Triggers asynchronously in same thread")
        {
            atomic_int observed = 0;

            txt_handler_set_flags(handler, TXT_EVENT_ASYNC);
            asserteq(txt_event_listen(&event, handler), TXT_OK);

            txt_executor_enqueue(
                &executor,
                TXT_CREATE_WORK(
                    trigger_cb, &event, &TEST_VAL, sizeof(int), &out, &observed
                )
            );

            WAIT(2);
            asserteq(out, TEST_VAL);
            asserteq(observed, 0);
        }

        it ("Triggers ignoring multiple")
        {
            txt_handler_set_flags(
                handler, TXT_EVENT_ASYNC | TXT_EVENT_IGNORE_MULTIPLE);
            asserteq(txt_event_listen(&event, handler), TXT_OK);

            int i;
            for (i = 0; i < 400; ++i)
            {
                asserteq(txt_event_trigger(&event, &i, sizeof(int)), 0);
            }

            WAIT(2);
            asserteq(out, i - 1);

            asserteq(txt_event_trigger(&event, &i, sizeof(int)), 0);

            WAIT(2);
            asserteq(out, i);
        }
    }

    subdesc(add_remove_handlers)
    {
        txt_event_s event;
        txt_executor_s executor;

        before_each()
        {
            txt_executor_init(&executor);
            txt_event_init(&event, 0);
        }

        after_each()
        {
            txt_executor_kill(&executor);
            txt_event_kill(&event);
        }

        it ("Allows a handler to remove itself")
        {
            txt_handler_s *handler = txt_handler_create(&executor);
            txt_handler_set_cb(handler, remove_self_cb, &event);
            txt_handler_set_flags(
                handler,
                TXT_EVENT_SYNC | TXT_EVENT_THREAD_SAFE
            );
            asserteq(txt_event_listen(&event, handler), TXT_OK);
            asserteq(txt_event_trigger(&event, NULL, 0), TXT_OK);
        }

        it ("Triggers after handler removed")
        {
            txt_handler_s *handler = txt_handler_create(&executor);
            txt_handler_set_flags(handler, TXT_EVENT_SYNC);
            asserteq(txt_event_listen(&event, handler), TXT_OK);
            asserteq(txt_event_trigger(&event, NULL, 0), TXT_OK);
            txt_handler_destroy(handler);
            asserteq(txt_event_trigger(&event, NULL, 0), TXT_OK);
        }

        it ("Allows a handler to be removed while it is executing")
        {
            atomic_bool *signal = malloc(sizeof(atomic_bool));
            txt_handler_s *handler = txt_handler_create(&executor);
            txt_handler_set_cb(handler, slow_cb, signal);
            txt_handler_set_flags(handler, TXT_EVENT_ASYNC);
            asserteq(txt_event_listen(&event, handler), TXT_OK);
            asserteq(txt_event_trigger(&event, NULL, 0), TXT_OK);
            WAIT(1);
            txt_handler_destroy(handler);
            free(signal);
        }

        it ("Can be triggered from multiple threads at once")
        {
            txt_executor_s executor2;
            txt_executor_s executor3;
            txt_executor_init(&executor2);
            txt_executor_init(&executor3);

            for (int i = 0; i < 100; ++i)
            {
                for (int i = 0; i < 100; ++i)
                {
                    txt_handler_s *handler = txt_handler_create(&executor);
                    txt_handler_set_cb(handler, remove_self_cb, &event);
                    txt_handler_set_flags(
                        handler,
                        TXT_EVENT_SYNC | TXT_EVENT_THREAD_SAFE
                    );
                    asserteq(txt_event_listen(&event, handler), TXT_OK);
                }

                txt_executor_enqueue(
                    &executor2,
                    TXT_CREATE_WORK(trigger_cb, &event, 0, 0, 0, 0)
                );
                txt_executor_enqueue(
                    &executor3,
                    TXT_CREATE_WORK(trigger_cb, &event, 0, 0, 0, 0)
                );

                asserteq(txt_event_trigger(&event, 0, 0), TXT_OK);
            }

            txt_executor_kill(&executor2);
            txt_executor_kill(&executor3);
        }
    }
}
#endif
