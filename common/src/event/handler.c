#include "handler.h"

#include "refcount.h"
#include "macros.h"

#include <string.h>

static void destructor(void *mem)
{
    txt_handler_s *handler = mem;
    pthread_mutex_destroy(&(handler->lock));

    if (handler->override_params)
        txt_refcount_decr(handler->override_params);
}

txt_handler_s *txt_handler_create(txt_executor_s *exec)
{
    txt_handler_s *rtn = txt_refcount_create(sizeof(txt_handler_s));

    if (rtn)
    {
        memset(rtn, '\0', sizeof(*rtn));

        txt_refcount_set_destructor(rtn, destructor);

        // This mutex must be recursive as the user-supplied callback may
        // perform operations on its own handler.
        pthread_mutexattr_t attr;
        pthread_mutexattr_init(&attr);
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
        pthread_mutex_init(&(rtn->lock), &attr);
        pthread_mutexattr_destroy(&attr);

        txt_list_init(&(rtn->lhead));
        rtn->exec  = exec;
        rtn->flags = TXT_EVENT_SYNC | TXT_EVENT_ASYNC;
    }

    return rtn;
}

void txt_handler_destroy(txt_handler_s *handler)
{
    WITH_MTX(&(handler->lock))
    {
        handler->dead = true;
    }

    txt_refcount_decr(handler);
}

void txt_handler_set_cb(
    txt_handler_s   *handler,
    txt_handler_cb_t cb,
    void            *params
)
{
    handler->cb             = cb;
    handler->handler_params = params;
}

void txt_handler_set_flags(txt_handler_s *handler, txt_handler_flags_e flags)
{
    handler->flags = flags;
}

txt_handler_flags_e txt_handler_get_flags(const txt_handler_s *handler)
{
    return handler->flags;
}

bool txt_handler_begin_async_task(txt_handler_s *handler, void *params)
{
    bool rtn = true;

    WITH_MTX(&(handler->lock))
    {
        if (
            (handler->flags & TXT_EVENT_IGNORE_MULTIPLE) &&
            handler->async_task_active
        )
        {
            if (handler->override_params)
                txt_refcount_decr(handler->override_params);

            if (params)
                txt_refcount_incr(params);

            handler->override_params = params;
            rtn = false;
        }
        handler->async_task_active = true;
    }

    return rtn;
}

void txt_handler_trigger(txt_handler_s *handler, const void *evt_params)
{
    WITH_MTX(&(handler->lock))
    {
        if (handler->override_params)
        {
            evt_params = handler->override_params;
        }

        if (handler->cb && !handler->dead)
        {
            (handler->cb)(handler, handler->handler_params, evt_params);
        }

        if (handler->override_params)
        {
            txt_refcount_decr(handler->override_params);
            handler->override_params = NULL;
        }

        handler->async_task_active = false;
    }
}

#include <snow/snow.h>

#if SNOW_ENABLED
static void test_params_cb(
    txt_handler_s *handler,
    void          *handler_params,
    const void    *evt_params
)
{
    (void)handler;
    *(int *)handler_params = *(int *)evt_params;
}

static void test_handler_cb(
    txt_handler_s *handler,
    void          *handler_params,
    const void    *evt_params
)
{
    (void)handler_params;
    *(txt_handler_s **)evt_params = handler;
}

describe(handler)
{
    txt_handler_s *handler;

    before_each()
    {
        handler = txt_handler_create(NULL);
    }

    after_each()
    {
        if (handler)
            txt_handler_destroy(handler);
    }

    it ("Does nothing with no callback")
    {
        txt_handler_trigger(handler, NULL);
    }

    it ("Passes params")
    {
        int inp = 99;
        int out = 0;

        txt_handler_set_cb(handler, test_params_cb, &out);
        txt_handler_trigger(handler, &inp);

        asserteq(out, inp);
    }

    it ("Passes handler")
    {
        txt_handler_s *out = NULL;

        txt_handler_set_cb(handler, test_handler_cb, NULL);
        txt_handler_trigger(handler, &out);

        asserteq(out, handler);
    }

    it ("Only gets freed when it is derefed")
    {
        txt_refcount_incr(handler);
        txt_handler_destroy(handler);
        txt_refcount_decr(handler);
        handler = NULL;
    }

    it ("Doesn't trigger when destroyed")
    {
        int inp = 99;
        int out = 0;

        txt_refcount_incr(handler);
        txt_handler_destroy(handler);

        txt_handler_set_cb(handler, test_params_cb, &out);
        txt_handler_trigger(handler, &inp);

        asserteq(out, 0);

        txt_refcount_decr(handler);
        handler = NULL;
    }

}
#endif
