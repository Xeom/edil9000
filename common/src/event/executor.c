#include "executor.h"

#include "macros.h"
#include "work.h"

#include <stdlib.h>
#include <snow/snow.h>

static txt_work_s *grab_work(txt_executor_s *exec)
{
    txt_list_s *work_lhead = exec->work.next;
    txt_list_grab(work_lhead);

    return CONTAINER_OF(work_lhead, txt_work_s, lhead);
}

static void *txt_executor_main(void *arg)
{
    txt_executor_s *exec = arg;

    WITH_MTX(&(exec->lock)) while (exec->alive)
    {
        if (txt_list_empty(&(exec->work)))
        {
            pthread_cond_wait(&(exec->ready), &(exec->lock));
        }
        else
        {
            txt_work_s *work = grab_work(exec);

            WITHOUT_MTX(&(exec->lock))
            {
                (work->funct)(&(work->data));
                free(work);
            }
        }
    }

    return NULL;
}

void txt_executor_init(txt_executor_s *exec)
{
    txt_list_init(&(exec->work));

    exec->alive = true;

    pthread_mutex_init(&(exec->lock), NULL);
    pthread_cond_init(&(exec->ready), NULL);
    pthread_create(&(exec->thread), 0, txt_executor_main, exec);
}

void txt_executor_kill(txt_executor_s *exec)
{
    WITH_MTX(&(exec->lock))
    {
        exec->alive = false;
        pthread_cond_signal(&(exec->ready));
    }

    pthread_join(exec->thread, NULL);

    while (!txt_list_empty(&(exec->work)))
    {
        txt_work_s *work = grab_work(exec);
        (work->funct)(&(work->data));
        free(work);
    }

    txt_list_grab(&(exec->work));
}

txt_executor_s *txt_executor_create(void)
{
    txt_executor_s *rtn = malloc(sizeof(*rtn));
    if (rtn)
        txt_executor_init(rtn);

    return rtn;
}

void txt_executor_destroy(txt_executor_s *exec)
{
    txt_executor_kill(exec);
    free(exec);
}

void txt_executor_enqueue(txt_executor_s *exec, txt_work_s *work)
{
    WITH_MTX(&(exec->lock))
    {
        txt_list_join(&(exec->work), &(work->lhead));
        pthread_cond_signal(&(exec->ready));
    }
}

#if SNOW_ENABLED
#include <stdatomic.h>
#include "cb.h"

struct test_params { atomic_int *ctr; };

TXT_DECLARE_CB_1(test_cb, struct test_params, atomic_int *, ctr)
{
    (*ctr)++;
}

#define WAIT() usleep(10000)

describe(executor)
{
    txt_executor_s exec;

    before_each() { txt_executor_init(&exec); }
    after_each() { txt_executor_kill(&exec); }

    it ("Enqueues and runs work added immediately")
    {
        atomic_int ctr = 0;
        for (int i = 0; i < 100; ++i)
            txt_executor_enqueue(&exec, TXT_CREATE_WORK(test_cb, &ctr));
        WAIT();
        asserteq(ctr, 100);
    }

    it ("Enqueues and runs work added after delay")
    {
        atomic_int ctr = 0;
        for (int i = 0; i < 100; ++i)
            txt_executor_enqueue(&exec, TXT_CREATE_WORK(test_cb, &ctr));
        WAIT();
        for (int i = 0; i < 100; ++i)
            txt_executor_enqueue(&exec, TXT_CREATE_WORK(test_cb, &ctr));
        WAIT();
        asserteq(ctr, 200);
    }

    it ("Doesn't leak pending work")
    {
        static atomic_int ctr = 0;
        for (int i = 0; i < 100; ++i)
            txt_executor_enqueue(&exec, TXT_CREATE_WORK(test_cb, &ctr));
    }

}
#endif
