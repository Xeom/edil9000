#if !defined(TXT_EVENT_HANDLER_H)
# define TXT_EVENT_HANDLER_H
# include "txt_handler.h"
# include "list.h"

# include <stdatomic.h>
# include <pthread.h>

typedef struct txt_executor      txt_executor_s;
typedef struct txt_event         txt_event_s;

struct txt_handler
{
    txt_list_s          lhead;
    txt_handler_flags_e flags;
    txt_handler_cb_t    cb;
    void               *handler_params;
    txt_executor_s     *exec;
    pthread_mutex_t     lock;
    /**
     * A flag to indicate this event has been destroyed, and can no longer be
     * triggered.
     */
    bool                dead;
    /**
     * A flag indicating that an async job trigging this handler is coming.
     */
    bool                async_task_active;
    /**
     * Parameters to override the passed parameters with when this handler is
     * triggered.
     */
    void               *override_params;
};

bool txt_handler_begin_async_task(txt_handler_s *handler, void *params);

void txt_handler_trigger(txt_handler_s *handler, const void *params);

#endif
