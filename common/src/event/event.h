#if !defined(TXT_EVENT_EVENT_H)
# define TXT_EVENT_EVENT_H
# include "txt_event.h"
# include "list.h"

# include <pthread.h>

struct txt_event
{
    txt_handler_flags_e  flags;
    size_t               param_len;
    // A lock to be held when triggering.
    pthread_mutex_t      trigger_lock;
    // A lock to be held when modifying or reading the attached handlers.
    pthread_mutex_t      handler_lock;
    txt_list_s           sync_handlers;
    txt_list_s           async_handlers;
};

void txt_event_init(txt_event_s *evt, size_t param_len);

void txt_event_kill(txt_event_s *evt);

# if SNOW_ENABLED
#  include <stdatomic.h>
#  include "txt_executor.h"

__attribute__ (( unused ))
static void expect_event_cb(
    txt_handler_s *handler,
    void          *handler_params,
    const void    *evt_params
)
{
    (void)handler; (void)evt_params;
    *((atomic_bool *)handler_params) = true;
}

#  define EXPECT_EVENT(_evt, _code) \
    do \
    { \
        atomic_bool called_ = false; \
        txt_executor_s *executor_ = txt_executor_create(); \
        txt_handler_s *handler_ = txt_handler_create(executor_); \
        txt_handler_set_flags(handler_, TXT_EVENT_SYNC | TXT_EVENT_ASYNC); \
        txt_handler_set_cb(handler_, expect_event_cb, &called_); \
        asserteq(txt_event_listen(_evt, handler_), TXT_OK); \
        { _code; } \
        for (int x = 1000; x > 0 && !called_; --x) usleep(100); \
        assert(called_, "Expected event did not arrive"); \
        txt_handler_destroy(handler_); \
        txt_executor_destroy(executor_); \
    } while (0)

# endif

#endif
