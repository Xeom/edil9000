#include "work.h"

#include <string.h>
#include <stdlib.h>

txt_work_s *txt_work_create(
    txt_cb_t    cb,
    const void *params,
    size_t      param_len
)
{
    txt_work_s *work = malloc(sizeof(txt_work_s) + param_len);

    if (work)
    {
        txt_list_init(&(work->lhead));
        work->funct = cb;
        memcpy(work->data, params, param_len);
    }

    return work;
}
