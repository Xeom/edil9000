#if !defined(TXT_MACROS_H)
# define TXT_MACROS_H

#define CONTAINER_OF(_inst, _struct, _member) \
    ((_struct *)((char *)(_inst) - offsetof(_struct, _member)))

#define BEFORE_AFTER(_before, _after) \
    for (int _once = 1; _once&&((_before),1); _after,_once = 0)

#define WITH_MTX(_mtx) \
    BEFORE_AFTER(pthread_mutex_lock(_mtx), pthread_mutex_unlock(_mtx))

#define WITHOUT_MTX(_mtx) \
    BEFORE_AFTER(pthread_mutex_unlock(_mtx), pthread_mutex_lock(_mtx))

#define WITH_RDLOCK(_rw) \
    BEFORE_AFTER(pthread_rwlock_rdlock(_rw), pthread_rwlock_unlock(_rw))

#define WITH_WRLOCK(_rw) \
    BEFORE_AFTER(pthread_rwlock_wrlock(_rw), pthread_rwlock_unlock(_rw))

#define MIN(_a, _b) \
    ((_a <= _b) ? (_a) : (_b))

#define MAX(_a, _b) \
    ((_a >= _b) ? (_a) : (_b))

#define SWAP(_a, _b) \
    do { typeof(_a) tmp = (_a); (_a) = (_b); (_b) = tmp; } while (0)

#define DIM(_x) \
    (sizeof(_x) / sizeof(_x[0]))

#endif
