#if !defined(LIBTXT_CORE_CB_H)
# define LIBTXT_CORE_CB_H

typedef void (*txt_cb_t)(const void *);

#define _TXT_DECL_CB(_name, _if, _a, _b) \
    typedef _if _name ## _params_t; \
    static inline void _name ## _inner_ _b; \
    static void _name(const void *ptr) \
    { \
        const _if *p = ptr; \
        _name ## _inner_ _a; \
    } \
    static inline void _name ## _inner_ _b

#define TXT_DECLARE_CB_1(_name, _if, _t1, _v1) \
    _TXT_DECL_CB(_name, _if, (p->_v1), (_t1 _v1))

#define TXT_DECLARE_CB_2(_name, _if, _t1, _v1, _t2, _v2) \
    _TXT_DECL_CB(_name, _if, (p->_v1,p->_v2), (_t1 _v1,_t2 _v2))

#define TXT_DECLARE_CB_3(_name, _if, _t1, _v1, _t2, _v2, _t3, _v3) \
    _TXT_DECL_CB(_name, _if, (p->_v1,p->_v2,p->_v3), (_t1 _v1,_t2 _v2,_t3 _v3))

#define TXT_DECLARE_CB_4(_name, _if, _t1, _v1, _t2, _v2, _t3, _v3, _t4, _v4) \
    _TXT_DECL_CB( \
        _name, _if, \
        (p->_v1,p->_v2,p->_v3,p->_v4), \
        (_t1 _v1,_t2 _v2,_t3 _v3,_t4 _v4) \
    )

#define TXT_DECLARE_CB_5(_name, _if, _t1,_v1,_t2,_v2,_t3,_v3,_t4,_v4,_t5,_v5) \
    _TXT_DECL_CB( \
        _name, _if, \
        (p->_v1,p->_v2,p->_v3,p->_v4,p->_v5), \
        (_t1 _v1,_t2 _v2,_t3 _v3,_t4 _v4,_t5 _v5) \
    )
#endif
