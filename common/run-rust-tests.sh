#!/bin/bash

set -o pipefail

CARGO=${CARGO:-cargo}
VALGRIND=${VALGRIND:-valgrind}

EXECUTABLES=$(
    ${CARGO:-cargo} test --no-run --message-format json |
        grep -oP '(?<="executable":")[^"]+(?=")'
)

if [ $? -ne 0 ]
then
    $CARGO test --no-run
    exit 1
fi

for EXE in $EXECUTABLES
do
    echo === Running $(basename $EXE) under VALGRIND ===
    $VALGRIND $VGFLAGS -- $EXE $TEST_RUST_FLAGS || exit 1
done
