use std::{env, path::{Path, PathBuf}};

fn option(opt: &str, path: impl AsRef<Path>)
{
    println!("cargo:{opt}={}", path.as_ref().as_os_str().to_str().unwrap());
}

fn add_lib(path: &Path)
{
    option("rerun-if-changed", path);

    if let Some(dir) = path.parent()
    {
        option("rustc-link-search", dir);
    }

    if let Some(lib) = path.file_stem()
        .and_then(|p| p.to_str().unwrap().strip_prefix("lib"))
    {
        option("rustc-link-lib", lib);
    }
}

fn find_headers(path: impl AsRef<Path>) -> impl Iterator<Item=String>
{
    option("rerun-if-changed", &path);
    std::fs::read_dir(path)
        .expect("Could not read header directory.")
        .map(|e| e.unwrap().path().to_str().unwrap().to_string())
        .filter(|path| path.ends_with(".h"))
}

fn main()
{
    let common_dir = PathBuf::from(env::var("CARGO_MANIFEST_DIR").unwrap())
        .canonicalize()
        .expect("Could not canonicalize common directory");

    let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap())
        .canonicalize()
        .expect("Could not canonicalize out dir");

    let inc_dir = common_dir.join("inc");

    option("rerun-if-changed", &common_dir.join("build.rs"));
    add_lib(&common_dir.join("build/libedil_common.a"));

    bindgen::Builder::default()
        .headers(
            find_headers("inc")
                .inspect(|p| option("rerun-if-changed", p))
        )
        .clang_arg(format!("-I{inc_dir:?}"))
        .parse_callbacks(Box::new(bindgen::CargoCallbacks::new()))
        .generate()
        .expect("Could not generate bindings")
        .write_to_file(out_dir.join("bindings.rs"))
        .expect("Could not write bindings");
}
