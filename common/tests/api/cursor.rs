use super::*;

use std::fmt::Write as _;
use std::io::Write as _;

#[test]
fn create_at_coord()
{
    let mut buf = Buffer::default();
    write!(&mut buf, "L1\nL2\nL3\n").unwrap();

    for row in 0..3
    {
        for col in 0..3
        {
            let coord = Coord { row, col };
            let new = buf.cursor(&coord);
            assert_eq!(coord, new.location());
            assert_eq!(row, new.row());
            assert_eq!(col, new.col());
        }
    }
}

#[test]
fn create_dup()
{
    let mut buf = Buffer::default();
    write!(&mut buf, "TEST").unwrap();

    let cur = buf.cursor(&Offset(2));
    let dup = cur.dup();
    assert_eq!(cur.location::<Coord>(), dup.location::<Coord>());
}

#[test]
fn move_over_rows()
{
    let mut buf = Buffer::default();
    write!(&mut buf, "L1\nL2\nL3").unwrap();

    let expect: &[(usize, usize)] = &[
        (0, 0), (0, 1), (0, 2),
        (1, 0), (1, 1), (1, 2),
        (2, 0), (2, 1), (2, 2)
    ];

    let cur = buf.cursor(&StartOfText);

    eprintln!("Moving forward...");
    for e in expect.iter()
    {
        assert_eq!(&(cur.row(), cur.col()), e);
        cur.move_by(1);
    }

    eprintln!("Moving backward...");
    for e in expect.iter().rev()
    {
        assert_eq!(&(cur.row(), cur.col()), e);
        cur.move_by(-1);
    }
}

#[test]
fn read_and_write()
{
    let buf = Buffer::default();
    write!(&mut buf.cursor(&EndOfText), "Hello world").unwrap();

    assert_eq!(buf.cursor(&StartOfText).read().to_string(), "Hello world");
}

#[test]
fn read_and_write_codepoints()
{
    let buf = Buffer::default();
    write!(&mut buf.cursor(&EndOfText), "Hello world").unwrap();

    assert_eq!(
        buf.cursor(&StartOfText).read().to_codepoints(),
        "Hello world".chars().collect::<Vec<_>>()
    );
}

#[test]
fn read_and_write_bytes()
{
    let buf = Buffer::default();
    write!(&mut buf.cursor(&EndOfText).write(), "Hello world").unwrap();

    assert_eq!(buf.cursor(&StartOfText).read().to_string(), "Hello world");
}

#[test]
fn range_read()
{
    let mut buf = Buffer::default();
    write!(&mut buf, "Hello world").unwrap();

    assert_eq!(
        buf.cursor(&Offset(1)).read()
            .until(&buf.cursor(&Offset(5))).to_string(),
        "ello"
    );
}

#[test]
fn read_until_eol()
{
    let mut buf = Buffer::default();
    write!(&mut buf, "Hello\nWorld\n").unwrap();

    let cur = buf.cursor(&StartOfText);

    assert_eq!(cur.read().until_eol().to_string(), "Hello");
    assert_eq!(cur.read().until_eol().to_string(), "World");
    assert_eq!(cur.read().until_eol().to_string(), "");
}

#[test]
fn can_be_used_after_buffer_destroyed()
{
    let mut buf = Buffer::default();
    write!(&mut buf, "Hello world").unwrap();

    let cur = buf.cursor(&StartOfText);
    std::mem::drop(buf);

    // Check we can still read from the cursor.
    assert_eq!(cur.read().to_string(), "Hello world")
}

#[test]
fn on_move_event()
{
    let mut buf = Buffer::default();
    write!(&mut buf, "Hello world").unwrap();

    let cur = buf.cursor(&StartOfText);

    expect_event!(cur.on_move(), { cur.move_by(1); });
    expect_event!(cur.on_move(), { cur.move_by(-1); });
    expect_event!(cur.on_move(), { cur.move_by(0); });
}

#[test]
fn on_modify_text()
{
    let buf = Buffer::default();
    let cur = buf.cursor(&EndOfText);

    expect_event!(buf.on_modify_text(), { cur.insert_forward('X') });
    expect_event!(buf.on_modify_text(), { cur.insert_backward('X') });
    expect_event!(buf.on_modify_text(), { cur.delete_forward() });
    expect_event!(buf.on_modify_text(), { cur.delete_backward() });
}

#[test]
fn on_modify_font()
{
    let mut buf = Buffer::default();
    write!(&mut buf, "Hello world").ok();

    let cur = buf.cursor(&StartOfText);

    let font = Fonts::default().get_id("test");
    expect_event!(buf.on_modify_font(), { cur.set_font(font, 4) });
}

#[test]
fn delete_between()
{
    let mut buf = Buffer::default();
    write!(&mut buf, "Hello world").unwrap();

    Cursor::delete_between(&buf.cursor(&Offset(2)), &buf.cursor(&Offset(9)));

    assert_eq!(buf.cursor(&StartOfText).read().to_string(), "Held");
}

#[test]
fn delete_between_lines()
{
    let mut buf = Buffer::default();
    write!(&mut buf, "L1\nL2\nL3").unwrap();

    assert_eq!(buf.num_lines(), 3);
    Cursor::delete_between(&buf.cursor(&Row(1)), &buf.cursor(&Row(2)));
    assert_eq!(buf.num_lines(), 2);
}

#[test]
fn insert_and_delete_lines()
{
    let mut buf = Buffer::default();
    write!(&mut buf, "L1\nL2\nL3").unwrap();

    let read_line = |row| buf.cursor(&Row(row)).read().until_eol().to_string();

    assert_eq!(read_line(0), "L1");
    assert_eq!(read_line(1), "L2");
    assert_eq!(read_line(2), "L3");

    Cursor::delete_between(&buf.cursor(&Row(1)), &buf.cursor(&Row(2)));

    assert_eq!(read_line(0), "L1");
    assert_eq!(read_line(1), "L3");

    write!(&mut buf.cursor(&Row(1)), "L2\n").unwrap();

    assert_eq!(read_line(0), "L1");
    assert_eq!(read_line(1), "L2");
    assert_eq!(read_line(2), "L3");
}

#[test]
fn insert_and_delete_lines_by_single_char()
{
    let mut buf = Buffer::default();
    write!(&mut buf, "L1\nL2\nL3").unwrap();

    let read_line = |row| buf.cursor(&Row(row)).read().until_eol().to_string();

    assert_eq!(read_line(0), "L1");
    assert_eq!(read_line(1), "L2");
    assert_eq!(read_line(2), "L3");

    let cur = buf.cursor(&Row(1));
    cur.delete_forward();
    cur.delete_forward();
    cur.delete_forward();

    assert_eq!(read_line(0), "L1");
    assert_eq!(read_line(1), "L3");

    write!(&mut buf.cursor(&Row(1)), "L2\n").unwrap();

    assert_eq!(read_line(0), "L1");
    assert_eq!(read_line(1), "L2");
    assert_eq!(read_line(2), "L3");
}

#[test]
fn cursor_ordering()
{
    let mut buf = Buffer::default();
    write!(&mut buf, "L1\nL2\nL3").unwrap();

    let c1 = buf.cursor(&Coord { row: 0, col: 0 });
    let c2 = buf.cursor(&Coord { row: 0, col: 1 });
    let c3 = buf.cursor(&Coord { row: 2, col: 0 });

    assert!(c1 < c2);
    assert!(c2 < c3);
    assert!(c1 < c3);
    assert!(c2 > c1);
    assert!(c3 > c2);
    assert!(c3 > c1);
}

#[test]
fn cursor_locations()
{
    let mut buf = Buffer::default();
    write!(&mut buf, "L1\nL2\nL3").unwrap();

    fn check<L>(buf: &Buffer, loc: L)
        where L: QueryLocation + CreateLocation + std::fmt::Debug + PartialEq
    {
        assert_eq!(buf.cursor(&loc).location::<L>(), loc);
    }

    for off in 0..=8
    {
        check(&buf, Offset(off));
        check(&buf, Utf8Byte(off));
    }

    assert_eq!(buf.cursor(&StartOfText).location::<Offset>().0, 0);
    assert_eq!(buf.cursor(&EndOfText).location::<Offset>().0, 8);

    assert_eq!(buf.cursor(&Row(0)).location::<Offset>().0, 0);
    assert_eq!(buf.cursor(&Row(1)).location::<Offset>().0, 3);
    assert_eq!(buf.cursor(&Row(2)).location::<Offset>().0, 6);
}
