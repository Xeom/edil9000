use super::*;

use std::fmt::Write;

#[test]
fn create_and_destroy_buffer()
{
    let buf = Buffer::default();
    std::mem::drop(buf);
}

#[test]
fn empty_buffer_has_empty_contents()
{
    assert_eq!(Buffer::default().contents(), "");
}

#[test]
fn writes()
{
    use std::fmt::Write;

    let mut buf = Buffer::default();
    write!(buf, "Hello world!").unwrap();
    assert_eq!(buf.contents(), "Hello world!");
}

#[test]
fn multiple_writes()
{
    use std::fmt::Write;

    let mut buf = Buffer::default();
    write!(buf, "a").unwrap();
    write!(buf, "b").unwrap();
    write!(buf, "c").unwrap();

    assert_eq!(buf.contents(), "abc");
}

#[test]
fn writes_multiple_lines()
{
    use std::fmt::Write;

    let mut buf = Buffer::default();
    write!(buf, "Hello world 1\nHello world 2").unwrap();
    write!(buf, "\nHello world 3").unwrap();
    assert_eq!(buf.contents(), "Hello world 1\nHello world 2\nHello world 3");
}

#[test]
fn num_lines()
{
    let mut buf = Buffer::default();

    assert_eq!(buf.num_lines(), 1);
    write!(buf, "Hello world!\n").unwrap();
    assert_eq!(buf.num_lines(), 2);
    write!(buf, "Hello world!\n").unwrap();
    assert_eq!(buf.num_lines(), 3);
}
