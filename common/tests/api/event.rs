use super::*;

use std::{sync::Arc, future::Future};

async fn assert_times_out(fut: impl Future)
{
    use tokio::time::*;
    assert!(timeout(Duration::from_millis(100), fut).await.is_err());
}

#[tokio::test]
async fn async_with_sync_flag()
{
    let exec: Executor = Default::default();
    let event: Event<i32> = Default::default();
    let handler: AsyncHandler<i32> =  Handler::builder(&exec)
        .with_event(&event)
        .with_sync()
        .into();

    event.trigger(&1);
    event.trigger(&2);
    event.trigger(&3);

    assert_eq!(handler.next().await, 1);
    assert_eq!(handler.next().await, 2);
    assert_eq!(handler.next().await, 3);
}

#[tokio::test]
async fn async_with_async_flag()
{
    let exec: Executor = Default::default();
    let event: Event<i32> = Default::default();
    let handler: AsyncHandler<i32> =  Handler::builder(&exec)
        .with_event(&event)
        .with_async()
        .into();

    event.trigger(&1);
    event.trigger(&2);
    event.trigger(&3);

    assert_eq!(handler.next().await, 1);
    assert_eq!(handler.next().await, 2);
    assert_eq!(handler.next().await, 3);
    assert_times_out(handler.next()).await;
}

#[tokio::test]
async fn async_no_param()
{
    let exec: Executor = Default::default();
    let event: Event<()> = Default::default();
    let handler: AsyncHandler<()> =  Handler::builder(&exec)
        .with_event(&event)
        .into();

    event.trigger(&());
    event.trigger(&());
    event.trigger(&());

    assert_eq!(handler.next().await, ());
    assert_eq!(handler.next().await, ());
    assert_eq!(handler.next().await, ());
    assert_times_out(handler.next()).await;
}

#[tokio::test]
async fn async_ignore_multiple()
{
    let exec: Executor = Default::default();
    let event: Event<i32> = Default::default();
    let handler: AsyncHandler<i32> =  Handler::builder(&exec)
        .with_event(&event)
        .with_async()
        .with_ignore_multiple()
        .into();

    event.trigger(&1);
    event.trigger(&2);
    event.trigger(&3);

    assert_eq!(handler.next().await, 3);
    assert_times_out(handler.next()).await;
}

#[tokio::test]
async fn async_angry_ignore_multiple()
{
    let exec: Executor = Default::default();
    let event: Arc<Event<i32>> = Default::default();
    let handler: AsyncHandler<i32> =  Handler::builder(&exec)
        .with_event(&*event)
        .with_async()
        .with_ignore_multiple()
        .into();

    let handle = tokio::task::spawn(async move {
        for i in 0_i32..=10000 { event.trigger(&i); }
    });

    let val = handler.next().await;
    handle.await.unwrap();

    if val != 10000
    {
        assert_eq!(handler.next().await, 10000);
    }
    assert_times_out(handler.next()).await;
}

#[test]
fn get_params()
{
    let exec = Executor::default();
    assert_eq!(
        Handler::builder(&exec)
            .with_async()
            .build(move |_: &()| {})
            .get_flags(),
        ASYNC
    );
    assert_eq!(
        Handler::builder(&exec)
            .with_sync()
            .build(move |_: &()| {})
            .get_flags(),
        SYNC
    );
    assert_eq!(
        Handler::builder(&exec)
            .with_ignore_multiple()
            .build(move |_: &()| {})
            .get_flags(),
        SYNC | ASYNC | IGNORE_MULTIPLE
    );
    assert_eq!(
        Handler::builder(&exec)
            .with_ignore_multiple()
            .build_thread_safe(move |_: &()| {})
            .get_flags(),
        SYNC | ASYNC | IGNORE_MULTIPLE | THREAD_SAFE
    );
    assert_eq!(
        Handler::builder(&exec)
            .build(move |_: &()| {})
            .get_flags(),
        SYNC | ASYNC
    );
}

#[test]
fn trigger()
{
    let exec = Executor::default();
    let evt = Event::<i32>::default();

    let (tx, rx) = std::sync::mpsc::channel();
    let _handler = Handler::builder(&exec)
        .with_event(&evt)
        .with_async()
        .build(move |i| tx.send(*i).unwrap());

    evt.trigger(&1);
    assert_eq!(rx.recv().unwrap(), 1);
    evt.trigger(&2);
    assert_eq!(rx.recv().unwrap(), 2);
    evt.trigger(&3);
    assert_eq!(rx.recv().unwrap(), 3);
}

#[test]
fn trigger_no_params()
{
    let exec = Executor::default();
    let evt = Event::<()>::default();

    let (tx, rx) = std::sync::mpsc::channel();
    let _handler = Handler::builder(&exec)
        .with_event(&evt)
        .with_async()
        .build(move |_| tx.send(()).unwrap());

    evt.trigger(&());
    rx.recv().unwrap();
    evt.trigger(&());
    rx.recv().unwrap();
}

#[test]
fn can_be_expected()
{
    let evt = Event::<i32>::default();
    expect_event!(&evt, &[100], { evt.trigger(&100) });
    expect_event!(&evt, &[1, 2, 3], { for i in 1..=3 { evt.trigger(&i) } });
    expect_no_event!(&evt, {});

    let evt2 = Event::<()>::default();
    expect_event!(&evt2, { evt2.trigger(&()) });
    expect_event!(
        &evt2, &[(), (), ()],
        { for _ in 1..=3 { evt2.trigger(&()) } }
    );
    expect_no_event!(&evt2, {});
}
