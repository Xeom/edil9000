#if !defined(LIBTXT_CORE_BUFFER_H)
# define LIBTXT_CORE_BUFFER_H
# include "txt_event.h"

# include <stddef.h>

typedef struct txt_buffer       txt_buffer_s;
typedef struct txt_coord        txt_coord_s;
typedef struct txt_modification txt_modification_s;
typedef struct txt_pos          txt_pos_s;

/** Line based coordinates of a glyph. */
struct txt_coord
{
    size_t row; /**< Zero-indexed row */
    size_t col; /**< Zero-indexed column in glyphs */
};

struct txt_pos
{
    txt_coord_s coord;
    size_t      glyph_off;
    size_t      utf8_off;
};

/**
 * Information summarising a change made to a buffer.
 *
 */
struct txt_modification
{
    /** The new revision ID of the buffer. */
    size_t new_revision_id;
    /** The position of the start of the change. */
    txt_pos_s start;
    /** The position of the end of the change, prior to the change. */
    txt_pos_s end_orig;
    /** The position of the end of the change, after the change. */
    txt_pos_s end_new;
};

txt_buffer_s *txt_buffer_create(void);

void txt_buffer_destroy(txt_buffer_s *buffer);

size_t txt_buffer_num_lines(txt_buffer_s *buffer);

/**
 * Get the length of a line in glyphs, excluding any newlines.
 *
 */
size_t txt_buffer_line_len(txt_buffer_s *buffer, size_t row);

/**
 * Return an event that is triggered when the text of the buffer is modified.
 *
 * The event parameter is of type txt_modification_s.
 *
 */
txt_event_s *txt_buffer_on_modify_text(txt_buffer_s *buffer);

/**
 * Return an event that is triggered when a font is modified in the buffer.
 *
 */
txt_event_s *txt_buffer_on_modify_font(txt_buffer_s *buffer);

/**
 * Begin a buffer transaction.
 *
 * Return the current revision ID of the buffer.
 *
 */
void txt_buffer_transaction_begin(txt_buffer_s *buffer);

void txt_buffer_transaction_end(txt_buffer_s *buffer);

/**
 * Get the current revision ID of the buffer.
 *
 */
size_t txt_buffer_revision_id(txt_buffer_s *buffer);

#endif
