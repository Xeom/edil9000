#if !defined(LIBTXT_CORE_HANDLER_H)
# define LIBTXT_CORE_HANDLER_H
# include <stddef.h>

typedef struct txt_executor      txt_executor_s;
typedef struct txt_handler       txt_handler_s;
typedef enum   txt_handler_flags txt_handler_flags_e;

typedef void (*txt_handler_cb_t)(
    txt_handler_s *handler,
    void          *handler_params,
    const void    *evt_params
);

/// An OR'd set of properties describing how a handler callback can be run.
enum txt_handler_flags
{
    /// Must complete before txt_event_trigger() returns.
    TXT_EVENT_SYNC            = 0x01,

    /// Must complete after txt_event_trigger() returns.
    TXT_EVENT_ASYNC           = 0x02,

    /// Can be called by ANY thread.
    ///
    /// Only relevant with the SYNC option.
    ///
    TXT_EVENT_THREAD_SAFE     = 0x04,

    /// Multiple events can be ignored, and only the last one handled.
    ///
    /// Only relevant with the ASYNC option.
    ///
    TXT_EVENT_IGNORE_MULTIPLE = 0x08
};

__attribute__((warn_unused_result))
txt_handler_s *txt_handler_create(txt_executor_s *exec);

void txt_handler_destroy(txt_handler_s *handler);

void txt_handler_set_cb(
    txt_handler_s   *handler,
    txt_handler_cb_t cb,
    void            *handler_params
);

void txt_handler_set_flags(txt_handler_s *handler, txt_handler_flags_e flags);

txt_handler_flags_e txt_handler_get_flags(const txt_handler_s *handler);

#endif
