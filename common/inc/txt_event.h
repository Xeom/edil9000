#if !defined(LIBTXT_CORE_EVENT_H)
# define LIBTXT_CORE_EVENT_H
# include "txt_handler.h"
# include "txt_err.h"

typedef struct txt_event txt_event_s;

__attribute__((warn_unused_result))
txt_event_s *txt_event_create(size_t param_len, txt_handler_flags_e flags);

void txt_event_destroy(txt_event_s *evt);

void txt_event_set_flags(txt_handler_s *handler, txt_handler_flags_e flags);

__attribute__((warn_unused_result))
txt_err_e txt_event_trigger(txt_event_s *evt, const void *params, size_t len);

__attribute__((warn_unused_result))
txt_err_e txt_event_listen(txt_event_s *evt, txt_handler_s *handler);

#endif
