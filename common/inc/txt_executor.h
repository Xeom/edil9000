#if !defined(LIBTXT_CORE_EXECUTOR_H)
# define LIBTXT_CORE_EXECUTOR_H

typedef struct txt_executor txt_executor_s;

__attribute__((warn_unused_result))
txt_executor_s *txt_executor_create(void);

void txt_executor_destroy(txt_executor_s *exec);

#endif
