#if !defined(LIBTXT_CORE_CURSOR_H)
# define LIBTXT_CORE_CURSOR_H
# include <stddef.h>
# include <stdint.h>
# include "txt_buffer.h"

typedef struct txt_cursor txt_cursor_s;

/* --- Create and destroy --------------------------------------------------- */

/* Note that all newly created cursors will be after all other cursors at the *
 * same row and column.                                                       */

/** Create a cursor at the end of a buffer. */
__attribute__((warn_unused_result))
txt_cursor_s *txt_cursor_create_end_of_text(txt_buffer_s *buffer);

/** Create a cursor at a coordinate */
__attribute__((warn_unused_result))
txt_cursor_s *txt_cursor_create_coord(txt_buffer_s *buffer, txt_coord_s coord);

/** Create a cursor at the Nth glyph of a buffer. */
__attribute__((warn_unused_result))
txt_cursor_s *txt_cursor_create_offset(txt_buffer_s *buffer, size_t offset);

/** Create a cursor at the Nth UTF-8 byte of a buffer. */
__attribute__((warn_unused_result))
txt_cursor_s *txt_cursor_create_utf8_byte(txt_buffer_s *buffer, size_t offset);

/** Duplicate a cursor. */
__attribute__((warn_unused_result))
txt_cursor_s *txt_cursor_create_dup(txt_cursor_s *oth);

/**
 * Create a cursor at the start of a line relative to the position of another.
 *
 */
__attribute__((warn_unused_result))
txt_cursor_s *txt_cursor_create_relative_line(txt_cursor_s *oth, long n);

/** Destroy the cursor. */
void txt_cursor_destroy(txt_cursor_s *cursor);

/* --- Moving --------------------------------------------------------------- */

/** Move a cursor a number of codepoints forward or backward. */
void txt_cursor_move_by(txt_cursor_s *cursor, long n);

/** Move a cursor to the location of another. */
__attribute__((warn_unused_result))
txt_err_e txt_cursor_move_to(txt_cursor_s *cursor, txt_cursor_s *oth);

/* --- Editing Text --------------------------------------------------------- */

/** Insert a codepoint forward from a cursor. */
void txt_cursor_insert_forward(txt_cursor_s *cursor, uint32_t codepoint);

/** Insert a codepoint backward from a cursor. */
void txt_cursor_insert_backward(txt_cursor_s *cursor, uint32_t codepoint);

/** Delete one codepoint forward from a cursor. */
void txt_cursor_delete_forward(txt_cursor_s *cursor);

/** Delete one codepoint backward from a cursor. */
void txt_cursor_delete_backward(txt_cursor_s *cursor);

/** Delete between two cursors. */
__attribute__((warn_unused_result))
txt_err_e txt_cursor_delete_between(txt_cursor_s *a, txt_cursor_s *b);

/* --- Fonts ---------------------------------------------------------------- */

void txt_cursor_get_font(txt_cursor_s *cur, uint16_t *fonts, size_t n);

void txt_cursor_set_font(txt_cursor_s *cur, uint16_t font, size_t n);

/* --- Querying Cursor ------------------------------------------------------ */

/**
 * Get the row and column of a cursor.
 *
 * Both are zero-indexed.
 *
 */
__attribute__((warn_unused_result))
txt_coord_s txt_cursor_coord(txt_cursor_s *cursor);

/**
 * Get an event that is triggered when the cursor is moved but the buffer is not
 * modified.
 *
 */
__attribute__((warn_unused_result))
txt_event_s *txt_cursor_on_move(txt_cursor_s *cursor);

size_t txt_cursor_offset(txt_cursor_s *cursor);

size_t txt_cursor_utf8_byte(txt_cursor_s *cursor);

__attribute__((warn_unused_result))
txt_err_e txt_cursor_cmp(txt_cursor_s *a, txt_cursor_s *b, int32_t *res);

/** Get the buffer of this cursor. */
txt_buffer_s *txt_cursor_buffer(txt_cursor_s *cur);

#endif

