#if !defined(LIBTXT_CORE_ERR_H)
# define LIBTXT_CORE_ERR_H

typedef enum txt_err txt_err_e;

enum txt_err
{
    TXT_OK = 0,
    TXT_ERR_IN_USE = -1,
    TXT_ERR_INV_SIZE = -2,
    TXT_ERR_INV_FLAGS = -3,
    TXT_ERR_WRONG_BUF = -4,
    TXT_ERR_PYTHON    = -5,
};

static inline void txt_err_ignore(txt_err_e err) { (void)err; }

#endif
