#if !defined(LIBTXT_CORE_CURSOR_IO_H)
# define LIBTXT_CORE_CURSOR_IO_H
# include <stddef.h>
# include <stdint.h>

typedef struct txt_cursor_io   txt_cursor_io_s;
typedef struct txt_cursor      txt_cursor_s;
typedef enum   txt_encoding    txt_encoding_e;
typedef enum   txt_line_ending txt_line_ending_e;

/**
 * The encoding of text.
 *
 */
enum txt_encoding
{
    TXT_ENCODING_ASCII, /**< ASCII encoding (7-bit) */
    TXT_ENCODING_UTF8,  /**< UTF-8 encoding. This is the default. */
    TXT_ENCODING_UTF32  /**< UTF-32 encoding (little-endian) */
};

/**
 * The type of line ending encoding.
 *
 */
enum txt_line_ending
{
    TXT_LINE_ENDING_UNIX, /**< Sensible (\n) newlines. This is the default. */
    TXT_LINE_ENDING_DOS,  /**< CRLF newlines */
    TXT_LINE_ENDING_NONE  /**< No newlines */
};

/**
 * Create a cursor IO handler.
 *
 * An IO handler can be used to read and write encoded bytes to or from a
 * cursor, seeking the cursor through the file as the bytes are read.
 *
 * This is a little analogous to a file descriptor moving through a file.
 *
 * @param cursor The cursor to attach the IO handler to.
 *
 */
txt_cursor_io_s *txt_cursor_io_create(txt_cursor_s *cursor);

/**
 * Destroy a cursor IO handler.
 *
 */
void txt_cursor_io_destroy(txt_cursor_io_s *io);

/**
 * Set the encoding of this cursor.
 *
 */
void txt_cursor_io_set_encoding(txt_cursor_io_s *io, txt_encoding_e enc);

/**
 * Set the type of line ending to use for this IO handler.
 *
 */
void txt_cursor_io_set_line_ending(txt_cursor_io_s *io, txt_line_ending_e le);

/**
 * Do not allow this IO handler to read past another cursor.
 *
 * This is useful for encoding a selection of text. The cursor moves forward
 * until it is in the same position as @p until.
 *
 */
void txt_cursor_io_set_until(txt_cursor_io_s *io, txt_cursor_s *until);

/**
 * Do not allow this IO handler to read past the end of the line.
 *
 * The newline is consumed by the cursor.
 *
 */
void txt_cursor_io_set_until_eol(txt_cursor_io_s *io);

void txt_cursor_io_set_font(txt_cursor_io_s *io, uint16_t font);

/**
 * Read bytes from an IO handler.
 *
 * IO handlers must be either read from or written to, but never both.
 *
 */
size_t txt_cursor_io_read(txt_cursor_io_s *io, uint8_t *buf, size_t len);

/**
 * Write bytes to an IO handler.
 *
 * IO handlers must be either read from or written to, but never both.
 *
 */
void txt_cursor_io_write(txt_cursor_io_s *io, const uint8_t *buf, size_t len);

#endif
