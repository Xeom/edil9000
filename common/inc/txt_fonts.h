#if !defined(LIBTXT_CORE_FONT_H)
# define LIBTXT_CORE_FONT_H
# include <stdint.h>
# include <stdbool.h>
# include "txt_event.h"

typedef struct txt_fonts      txt_fonts_s;
typedef struct txt_colour     txt_colour_s;
typedef struct txt_font_attrs txt_font_attrs_s;

typedef struct txt_fonts txt_fonts_s;

struct txt_colour
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

struct txt_font_attrs
{
    bool bold;
    bool dull;
    bool under;
    bool blink;
    bool invert;
    bool italic;
    bool has_fg;
    bool has_bg;
    txt_colour_s fg;
    txt_colour_s bg;
};

extern const txt_colour_s TXT_BLACK;
extern const txt_colour_s TXT_RED;
extern const txt_colour_s TXT_GREEN;
extern const txt_colour_s TXT_YELLOW;
extern const txt_colour_s TXT_BLUE;
extern const txt_colour_s TXT_MAGENTA;
extern const txt_colour_s TXT_CYAN;
extern const txt_colour_s TXT_WHITE;

extern const txt_colour_s TXT_GREY;
extern const txt_colour_s TXT_BRIGHT_RED;
extern const txt_colour_s TXT_BRIGHT_GREEN;
extern const txt_colour_s TXT_BRIGHT_YELLOW;
extern const txt_colour_s TXT_BRIGHT_BLUE;
extern const txt_colour_s TXT_BRIGHT_MAGENTA;
extern const txt_colour_s TXT_BRIGHT_CYAN;
extern const txt_colour_s TXT_BRIGHT_WHITE;

txt_fonts_s *txt_fonts_create(void);

void txt_fonts_destroy(txt_fonts_s *fonts);

uint16_t txt_fonts_get_id(txt_fonts_s *fonts, const char *name);

void txt_fonts_set_attrs(
    txt_fonts_s            *fonts,
    uint16_t                id,
    const txt_font_attrs_s *attrs
);

void txt_fonts_get_attrs(
    txt_fonts_s      *fonts,
    uint16_t          id,
    txt_font_attrs_s *attrs
);

txt_event_s *txt_fonts_on_set_attrs(txt_fonts_s *fonts);

uint8_t txt_colour_to_3bit(txt_colour_s col);

uint8_t txt_colour_to_8bit(txt_colour_s col);

bool txt_colour_is_bright(txt_colour_s col);

bool txt_font_attrs_eq(txt_font_attrs_s a, txt_font_attrs_s b);

#endif
