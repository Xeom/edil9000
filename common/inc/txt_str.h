#if !defined(LIBTXT_CORE_STR_H)
# define LIBTXT_CORE_STR_H
# include <stdint.h>
# include <stddef.h>

/// A convenience type for passing UTF-8 strings between APIs.
typedef struct txt_utf8 txt_utf8_s;

txt_utf8_s *txt_utf8_create(const uint8_t *bytes, size_t len);

void txt_utf8_destroy(txt_utf8_s *s);

size_t txt_utf8_len(const txt_utf8_s *s);

const uint8_t *txt_utf8_bytes(const txt_utf8_s *s);

#endif

