# TODO

## Files

### Display
 * Show non-printable characters escaped. (*DONE*)

### Highlighting
 * Matthewdown
 * Rust (*PARTIALLY DONE*)

### Searching
 * Search backwards (*DONE*)
 * Search result list
 * Search using grep

## Directories
 * List directories (*DONE*)
 * Navigatable directory links (*DONE*)
 * Live updates as directory changes
 * Display directory details

## Windows
 * Windows show titles
 * Display multiple files at once
 * Resize prompt when not in use
 * Add window size adjustment controls
 * Window selection menu
 * Display logged error messages/alerts

### Controls
 * Make page-up/page-down work like they did in palm (Scroll such that cursor is
   at the edge of the screen, or if cursor is already at edge, move cursor to
   other edge).

## Plugin System
### Task System
 * Add a Task trait
 * Add Content/File-scope tasks
 * Add Editor-scope tasks
 * Add a way to display currently running tasks

### Python-Bindings
