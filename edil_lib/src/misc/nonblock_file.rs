use super::*;

use std::task::{ready, Context, Poll};
use std::io::{Write, Read};

use nix::fcntl::{fcntl, FcntlArg::*, OFlag};

pub struct NonblockFile(AsyncFd<StdFile>);

unsafe impl Sync for NonblockFile {}

fn nonblockify(fd: &impl AsRawFd)
{
    let flags = OFlag::from_bits_retain(
        fcntl(fd.as_raw_fd(), F_GETFL).expect("Could not get flags")
    );

    if !flags.contains(OFlag::O_NONBLOCK)
    {
        fcntl(fd.as_raw_fd(), F_SETFL(flags | OFlag::O_NONBLOCK))
            .expect("Could not set flags");
    }
}

impl NonblockFile
{
    pub fn poll_read_inner(
        &self,
        ctx:  &mut Context<'_>,
        buf:  &mut io::ReadBuf<'_>
    )
        -> Poll<io::Result<()>>
    {
        loop
        {
            let mut guard = ready!(self.0.poll_read_ready(ctx))?;

            let unfilled = buf.initialize_unfilled();
            match guard.try_io(|inner| inner.get_ref().read(unfilled))
            {
                Ok(Ok(len)) =>
                {
                    buf.advance(len);
                    return Poll::Ready(Ok(()));
                }
                Ok(Err(err)) =>
                {
                    return Poll::Ready(Err(err));
                }
                Err(_would_block) => { }
            }
        }
    }

    pub fn poll_write_inner(&self, ctx: &mut Context<'_>, buf: &[u8])
        -> Poll<io::Result<usize>>
    {
        loop
        {
            let mut guard = ready!(self.0.poll_write_ready(ctx))?;

            match guard.try_io(|inner| inner.get_ref().write(buf))
            {
                Ok(result)        => return Poll::Ready(result),
                Err(_would_block) => { }
            }
        }
    }
}

impl From<OwnedFd> for NonblockFile
{
    fn from(owned_fd: OwnedFd) -> Self
    {
        Self::from(StdFile::from(owned_fd))
    }
}

impl From<StdFile> for NonblockFile
{
    fn from(std_file: StdFile) -> Self
    {
        nonblockify(&std_file);

        let inner = AsyncFd::new(std_file)
            .expect("Could not create AsyncFd from file.");

        Self(inner)
    }
}

impl AsRawFd for NonblockFile
{
    fn as_raw_fd(&self) -> RawFd { self.0.as_raw_fd() }
}

impl AsFd for NonblockFile
{
    fn as_fd(&self) -> BorrowedFd<'_> { self.0.as_fd() }
}

impl AsyncRead for NonblockFile
{
    fn poll_read(
        self: Pin<&mut Self>,
        ctx:  &mut Context<'_>,
        buf:  &mut io::ReadBuf<'_>
    )
        -> Poll<io::Result<()>>
    {
        self.poll_read_inner(ctx, buf)
    }
}

impl AsyncWrite for NonblockFile
{
    fn poll_write(
        self: Pin<&mut Self>,
        ctx:  &mut Context<'_>,
        buf:  &[u8]
    )
        -> Poll<io::Result<usize>>
    {
        self.poll_write_inner(ctx, buf)
    }

    fn poll_flush(self: Pin<&mut Self>, _ctx: &mut Context<'_>)
        -> Poll<io::Result<()>>
    {
        Poll::Ready(Ok(()))
    }

    fn poll_shutdown(self: Pin<&mut Self>, _ctx: &mut Context<'_>)
        -> Poll<io::Result<()>>
    {
        Poll::Ready(Ok(()))
    }
}
