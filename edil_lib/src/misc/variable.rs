use super::*;

pub(crate) struct Variable<T>
{
    value:     Mutex<Arc<T>>,
    on_change: Event<T>
}

impl<T> Variable<T>
    where T: Sync + Send + Sized + PartialEq + 'static
{
    pub fn set(&self, new_val: impl Into<T>)
    {
        let new_val = Arc::new(new_val.into());

        let changed;
        {
            let mut locked = self.value.lock().unwrap();
            changed = **locked != *new_val;
            *locked = new_val.clone()
        };

        if changed
        {
            self.on_change.trigger(&new_val);
        }
    }
}

impl<T> Variable<T>
    where T: Sync + Send + 'static
{
    pub fn new_sync(val: impl Into<T>) -> Self
    {
        Self
        {
            value:     Mutex::new(Arc::new(val.into())),
            on_change: Event::new_sync()
        }
    }

    pub fn get_ref(&self) -> Arc<T> { self.value.lock().unwrap().clone() }

    pub fn on_change<'var>(&'var self) -> NonOwned<'var, Event<T>>
    {
        self.on_change.as_non_owned()
    }
}

impl<T> Default for Variable<T>
    where T: Sync + Send + Copy + Default + 'static
{
    fn default() -> Self { Self::new(T::default()) }
}

impl<T> Variable<T>
    where T: Sync + Send + Copy + 'static
{
    pub fn get(&self) -> T { *self.get_ref() }

    pub fn new(val: T) -> Self
    {
        Self
        {
            value:     Mutex::new(Arc::new(val)),
            on_change: Event::default()
        }
    }
}

#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    fn initial_value()
    {
        let var = Variable::<i32>::new(100);
        assert_eq!(var.get(), 100);
    }

    #[test]
    fn set_value()
    {
        let var = Variable::<i32>::new(100);
        var.set(101);
        assert_eq!(var.get(), 101);
    }

    #[test]
    fn triggers_event()
    {
        let var = Variable::<i32>::new(100);
        expect_event!(var.on_change(), &[ 101 ], { var.set(101); });
        expect_event!(var.on_change(), &[ 102 ], { var.set(102); });
        expect_event!(var.on_change(), &[ 103 ], { var.set(103); });
    }

    #[test]
    fn triggers_no_event_on_equal_value()
    {
        let var = Variable::<i32>::new(100);
        expect_no_event!(var.on_change(), { var.set(100); });
        var.set(101);
        expect_no_event!(var.on_change(), { var.set(101); });
    }
}
