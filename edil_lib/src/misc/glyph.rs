use super::*;

#[derive(Copy, Clone)]
pub struct Glyph(pub char, pub FontId);

impl Default for Glyph
{
    fn default() -> Self { Self::from(' ') }
}

impl From<char> for Glyph
{
    fn from(ch: char) -> Glyph { Self(ch, Default::default()) }
}

impl From<&char> for Glyph
{
    fn from(ch: &char) -> Glyph { Self(*ch, Default::default()) }
}

impl From<&Glyph> for Glyph
{
    fn from(g: &Glyph) -> Glyph { *g }
}

impl Glyph
{
    pub fn parse(s: &str, editor: &Editor) -> Vec<Glyph>
    {
        parse::Parser::new(s, &editor.fonts).collect()
    }

    pub fn is_tab(&self) -> bool { self.0 == '\t' }

    pub fn is_print(&self) -> bool
    {
        // TODO: Unicode printables
        matches!(self.0, '\x20'..='\x7e')
    }

    pub fn expand<'a>(glyphs: &'a [Glyph], editor: &'a Editor)
        ->  impl Iterator<Item=Glyph> + 'a
    {
        let mut column = 0;
        glyphs.iter()
            .flat_map(
                move |glyph| expand::expand_glyph(glyph, &mut column, editor)
            )
    }
}

/// Functions to expand glyphs into a pretty form
mod expand
{
    use super::*;
    use std::borrow::Cow;

    pub(super) struct GlyphIter<'a>(usize, Cow<'a, [Glyph]>);

    impl<'a> Iterator for GlyphIter<'a>
    {
        type Item = Glyph;

        fn next(&mut self) -> Option<Glyph>
        {
            let rtn = self.1.get(self.0).cloned();
            self.0 += 1;
            rtn
        }
    }

    impl<'a> GlyphIter<'a>
    {
        pub fn len(&self) -> usize { self.1.len() }
    }

    impl<'a, T> From<T> for GlyphIter<'a>
        where Cow<'a, [Glyph]>: From<T>
    {
        fn from(x: T) -> Self { Self(0, From::from(x)) }
    }

    pub(super) fn expand_glyph<'a>(
        glyph:  &'a Glyph,
        column: &mut usize,
        editor: &'a Editor
    )
        -> GlyphIter<'a>
    {
        let rtn: GlyphIter<'a>;

        let config = &editor.config;

        if glyph.is_tab()
        {
            let tab = config.tabs.get_display(&editor);
            let width = tab.len() - (*column % tab.len());
            rtn = (&tab[..width]).into();
        }
        else if !glyph.is_print()
        {
            rtn = Glyph::parse(&config.hex.get_display(glyph.0), editor).into();
        }
        else
        {
            rtn = std::slice::from_ref(glyph).into();
        }

        *column += rtn.len();
        rtn
    }
}

/// Functions to parse strings into vectors of glyphs.
mod parse
{
    use super::*;

    enum Token
    {
        Char(char),
        Open,
        Close,
        Sep
    }

    struct Tokens<'a>(std::str::Chars<'a>);

    impl<'a> Iterator for Tokens<'a>
    {
        type Item = Token;

        fn next(&mut self) -> Option<Token>
        {
            match self.0.next()?
            {
                '\\' => Some(Token::Char(self.0.next()?)),
                '{'  => Some(Token::Open),
                '}'  => Some(Token::Close),
                ':'  => Some(Token::Sep),
                ch   => Some(Token::Char(ch))
            }
        }
    }

    pub(super) struct Parser<'a>
    {
        tokens:    Tokens<'a>,
        current:   FontId,
        stack:     Vec<FontId>,
        font_name: Option<String>,
        fonts:     &'a Fonts
    }

    impl<'a> Parser<'a>
    {
        pub fn new(s: &'a str, fonts: &'a Fonts) -> Self
        {
            Self
            {
                tokens:    Tokens(s.chars()),
                current:   Default::default(),
                stack:     vec![],
                font_name: None,
                fonts
            }
        }
    }

    impl<'a> Iterator for Parser<'a>
    {
        type Item = Glyph;

        fn next(&mut self) -> Option<Glyph>
        {
            match self.tokens.next()?
            {
                Token::Char(ch) =>
                {
                    if let Some(ref mut name) = &mut self.font_name
                    {
                        name.push(ch);
                    }
                    else
                    {
                        return Some(Glyph(ch, self.current));
                    }
                }
                Token::Open =>
                {
                    self.font_name = Some(String::new())
                }
                Token::Close =>
                {
                    if let Some(name) = self.font_name.take()
                    {
                        self.current = self.fonts.get_id(&name);
                    }
                    else
                    {
                        self.current = self.stack.pop()
                            .unwrap_or(Default::default());
                    }
                }
                Token::Sep =>
                {
                    if let Some(name) = self.font_name.take()
                    {
                        self.stack.push(self.current);
                        self.current = self.fonts.get_id(&name);
                    }
                }
            }

            // Recurse until we run out of tokens
            self.next()
        }
    }
}
