use super::*;

pub struct Writer<'a>
{
    glyphs: &'a mut Vec<Glyph>,
    font:   Option<FontId>
}

fn make_glyph(glyph: impl Into<Glyph>, font: Option<FontId>) -> Glyph
{
    match font
    {
        None       => glyph.into(),
        Some(font) => Glyph(glyph.into().0, font)
    }
}

impl<'a> Writer<'a>
{
    pub fn with_font(&mut self, font: FontId) -> &mut Self
    {
        self.font = Some(font);
        self
    }

    pub fn write_fmt(&mut self, args: std::fmt::Arguments<'_>) -> &mut Self
    {
        let _ = std::fmt::Write::write_fmt(self, args);
        self
    }

    pub fn write_str(&mut self, s: &str) -> &mut Self
    {
        let _ = std::fmt::Write::write_str(self, s);
        self
    }

    pub fn write_char(&mut self, ch: char) -> &mut Self
    {
        let _ = std::fmt::Write::write_char(self, ch);
        self
    }
}

impl<'a, G> Extend<G> for Writer<'a>
    where Glyph: From<G>
{
    fn extend<It>(&mut self, iter: It)
        where It: IntoIterator<Item=G>
    {
        self.glyphs.extend(iter.into_iter().map(|g| make_glyph(g, self.font)));
    }

//  fn extend_one(&mut self, item: G)
//  {
//      self.glyphs.extend_one(self.make_glyph(item));
//  }
}

impl<'a> std::fmt::Write for Writer<'a>
{
    fn write_str(&mut self, s: &str) -> std::fmt::Result
    {
        self.extend(s.chars());
        Ok(())
    }

    fn write_char(&mut self, ch: char) -> std::fmt::Result
    {
        self.glyphs.push(make_glyph(ch, self.font));
        Ok(())
    }
}

#[derive(Default, Clone)]
pub struct Line
{
    left: Vec<Glyph>,
    fill: Glyph
}

impl From<&str> for Line
{
    fn from(s: &str) -> Line { Line::new(s.chars()) }
}

impl Line
{
    pub fn new<Iter, G>(iter: Iter) -> Self
        where Glyph: From<G>, Iter: IntoIterator<Item=G>
    {
        let mut rtn = Self::default();
        rtn.left().extend(iter);
        rtn
    }

    pub fn left<'line>(&'line mut self) -> Writer<'line>
    {
        Writer { glyphs: &mut self.left, font: None }
    }

    pub fn with_fill(self, fill: Glyph) -> Self
    {
        Self { fill, ..self }
    }

    pub fn highlight(
        &mut self,
        cols: impl std::ops::RangeBounds<usize>,
        font: FontId
    )
    {
        let mut pushed = false;

        let start = match cols.start_bound()
            {
                std::ops::Bound::Included(n) => *n,
                std::ops::Bound::Excluded(n) => n + 1,
                std::ops::Bound::Unbounded   => 0
            };

        let end = match cols.end_bound()
            {
                std::ops::Bound::Included(n) => n + 1,
                std::ops::Bound::Excluded(n) => *n,
                std::ops::Bound::Unbounded   => self.left.len()
            };

        for col in start..end
        {
            if self.left.len() == col && !pushed
            {
                self.left.push(Default::default());
                pushed = true;
            }

            self.left.get_mut(col).map(|i| i.1 = font);
        }
    }

    pub fn fill(&self) -> Glyph
    {
        self.fill
    }

    pub fn expand_left<'a>(&'a self, editor: &'a Editor)
        -> impl Iterator<Item=Glyph> + 'a
    {
        Glyph::expand(&self.left, editor)
    }

    pub fn len(&self, editor: &Editor) -> usize
    {
        self.expand_left(editor).count()
    }
}

