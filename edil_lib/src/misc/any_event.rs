use super::*;

/// An adaptor that triggers an event when any of a number of others are
/// triggered.
///
#[derive(Default)]
pub struct AnyEvent
{
    handlers: Vec<Handler>,
    event:    Arc<Event<()>>
}

impl AnyEvent
{
    pub fn add_trigger<T>(&mut self, exec: &Executor, evt: &Event<T>)
        where T: 'static
    {
        let event = self.event.clone();
        self.handlers.push(
            Handler::builder(exec)
                .with_event(evt)
                .with_ignore_multiple()
                .build_thread_safe(move |_| event.trigger(&()))
        )
    }

    pub fn with_trigger<T>(mut self, exec: &Executor, evt: &Event<T>) -> Self
        where T: 'static
    {
        self.add_trigger(exec, evt);
        self
    }

    pub fn event(&self) -> &Event<()> { &self.event }
}
