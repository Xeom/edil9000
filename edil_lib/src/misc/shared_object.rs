use super::*;

use std::ptr::NonNull;
use std::ffi::{OsStr, CString, c_void};

pub struct SharedObject(NonNull<c_void>);

fn c_str(s: impl AsRef<OsStr>) -> io::Result<CString>
{
    std::ffi::CString::new(s.as_ref().as_encoded_bytes())
        .map_err(|_| io::Error::new(
            io::ErrorKind::InvalidInput,
            "Filename contained NUL byte."
        ))
}

unsafe fn dlopen(path: impl AsRef<Path>) -> io::Result<*mut c_void>
{
    let c_path = c_str(path.as_ref())?;
    Ok(nix::libc::dlopen(c_path.as_ptr(), nix::libc::RTLD_LAZY))
}

unsafe fn dlsym(handle: &NonNull<c_void>, symbol: impl AsRef<OsStr>)
    -> io::Result<*mut c_void>
{
    let c_symbol = c_str(symbol)?;
    Ok(nix::libc::dlsym(handle.as_ptr(), c_symbol.as_ptr()))
}

impl SharedObject
{
    pub fn new(path: impl AsRef<Path>) -> io::Result<Self>
    {
        if let Some(handle) = NonNull::new(unsafe { dlopen(path)? })
        {
            Ok(Self(handle))
        }
        else
        {
            Err(io::Error::new(
                io::ErrorKind::NotFound, "Could not load language file."
            ))
        }
    }

    pub fn symbol_ptr(&self, symbol: impl AsRef<OsStr>)
        -> io::Result<NonNull<c_void>>
    {
        NonNull::new(unsafe { dlsym(&self.0, &symbol)? })
            .ok_or_else(|| io::Error::new(
                io::ErrorKind::NotFound,
                format!(
                    "Could not find symbol {:?} in shared object.",
                    symbol.as_ref()
                )
            ))
    }

    pub unsafe fn symbol_function<T>(&self, symbol: impl AsRef<OsStr>)
        -> io::Result<unsafe extern "C" fn() -> T>
    {
        Ok(std::mem::transmute(self.symbol_ptr(symbol)?.as_ptr()))
    }
}
