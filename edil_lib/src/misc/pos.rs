//use super::*;

#[derive(PartialOrd, Ord, PartialEq, Eq, Clone, Copy, Debug)]
pub struct Pos<T>
{
    pub row: T,
    pub col: T
}

impl<T> std::ops::Add for Pos<T>
    where T: std::ops::Add<Output=T>
{
    type Output = Pos<T>;

    fn add(self, oth: Pos<T>) -> Pos<T>
    {
        Pos {
            row: self.row + oth.row,
            col: self.col + oth.col
        }
    }
}

impl<T> Pos<T>
    where T: Default + Copy, std::ops::Range<T>: Iterator<Item=T>
{
    pub fn iter(&self) -> impl Iterator<Item=Pos<T>>
    {
        let row = self.row;
        let col = self.col;

        (Default::default()..row)
            .flat_map(
                move |row|
                (Default::default()..col)
                    .map(move |col| Pos::<T> { row, col })
            )
    }

    pub fn cast<U>(&self) -> Pos<U>
        where U: From<T>
    {
        Pos { row: self.row.into(), col: self.col.into() }
    }
}

#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    fn test_pos_iter()
    {
        let mut iter = Pos { row: 3, col: 2 }.iter();

        assert_eq!(iter.next(), Some(Pos { row: 0, col: 0 }));
        assert_eq!(iter.next(), Some(Pos { row: 0, col: 1 }));
        assert_eq!(iter.next(), Some(Pos { row: 1, col: 0 }));
        assert_eq!(iter.next(), Some(Pos { row: 1, col: 1 }));
        assert_eq!(iter.next(), Some(Pos { row: 2, col: 0 }));
        assert_eq!(iter.next(), Some(Pos { row: 2, col: 1 }));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn test_pos_add()
    {
        assert_eq!(
            Pos { row: 1, col: 2 } + Pos { row: 3, col: 4 },
            Pos { row: 4, col: 6 }
        );
    }
}
