use super::*;

pub(crate) struct DerivedVariable<T>
{
    inner:    Arc<Variable<T>>,
    _handler: Handler
}

impl<T> DerivedVariable<T>
    where T: Sync + Send + Copy + PartialEq + 'static
{
    pub fn new<'a, I, F>(exec: &Executor, oth: &'a Variable<I>, cb: F)
        -> Self
        where
            F: Sync + Send + 'static + Fn(&I) -> T,
            I: Sync + Send + 'static
    {
        let inner = Arc::new(Variable::new(cb(&oth.get_ref())));

        Self
        {
            inner: inner.clone(),
            _handler: Handler::builder(exec)
                .with_ignore_multiple()
                .with_event(oth.on_change())
                .build(move |x| inner.set(cb(x)))
        }
    }

    pub fn on_change<'var>(&'var self) -> NonOwned<'var, Event<T>>
    {
        self.inner.on_change()
    }
}

#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    fn derived_variable()
    {
        let exec = Executor::default();
        let var = Variable::<i32>::new(1);
        let derived = DerivedVariable::<i32>::new(&exec, &var, |x| x + 1);
        expect_event!(derived.on_change(), &[ 3 ], { var.set(2); });
        expect_no_event!(derived.on_change(), { var.set(3); });
    }
}
