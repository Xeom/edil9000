//use super::*;

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Mode
{
    Read,
    Prompt,
    Window,
    Buffer
    //Edit,
    //Insert,
    //Command,
}

impl std::fmt::Display for Mode
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        <Mode as std::fmt::Debug>::fmt(self, f)
    }
}
