use super::*;

#[derive(Copy, Clone, Debug)]
pub struct Location
{
    pub pos:  Pos<u16>,
    pub size: Pos<u16>
}

impl Location
{
    pub fn iter_pos(&self) -> impl Iterator<Item=Pos<u16>> + '_
    {
        self.size.iter().map(move |it| it + self.pos)
    }

    pub fn split_vertical(&self, at: u16) -> Option<(Location, Location)>
    {
        if at > 0 && self.size.row > at
        {
            Some((
                Location
                {
                    pos: self.pos,
                    size: Pos { col: self.size.col, row: at }
                },
                Location
                {
                    pos: self.pos + Pos { col: 0, row: at },
                    size: Pos { col: self.size.col, row: self.size.row - at }
                }
            ))
        }
        else
        {
            None
        }
    }

    pub fn split_off_bottom(&mut self, at: u16) -> Option<Location>
    {
        if self.size.row > at
        {
            let (top, bottom) = self.split_vertical(self.size.row - at)?;
            *self = top;
            Some(bottom)
        }
        else
        {
            None
        }
    }
}
