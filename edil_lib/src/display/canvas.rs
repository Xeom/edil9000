use super::*;

#[async_trait]
pub trait Canvas: Sync + Send
{
    fn set_glyph(&mut self, pos: Pos<u16>, glyph: (char, FontAttrs));

    async fn begin(&mut self) -> io::Result<()>;

    async fn end(&mut self) -> io::Result<()>;

    async fn reset(&mut self) -> io::Result<()>;

    async fn draw(&mut self) -> io::Result<()>;
}
