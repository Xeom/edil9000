use super::*;

pub struct DisplayTask<O>
{
    editor: Arc<Editor>,
    output: O
}

impl<O> DisplayTask<O>
    where O: Canvas
{
    pub fn new(editor: Arc<Editor>, output: O) -> Self
    {
        Self { editor, output }
    }

    async fn end(&mut self)
    {
        self.output.end().await.expect("Could not end display output");
    }

    async fn begin(&mut self)
    {
        self.output.begin().await.expect("Could not begin display output");
        self.redraw().await;
    }

    async fn reset(&mut self)
    {
        debug!("Resetting display");
        self.output.reset().await
            .expect("Could not reset display output");
        self.redraw().await;
    }

    async fn redraw(&mut self)
    {
        for disp in self.editor.windows.get_displayable(&self.editor)
        {
            draw(&*disp, &mut self.output, &self.editor);
        }

        self.output.draw().await
            .expect("Could not draw to display output");
    }

    pub async fn task(mut self)
    {
        let resets: AsyncHandler<_> = Handler::builder(&self.editor.exec)
            .with_event(self.editor.windows.on_display_reset())
            .with_ignore_multiple()
            .into();

        let redraws: AsyncHandler<_> = Handler::builder(&self.editor.exec)
            .with_event(self.editor.windows.on_display_redraw())
            .with_ignore_multiple()
            .into();

        let set_attrs: AsyncHandler<_> = Handler::builder(&self.editor.exec)
            .with_event(self.editor.fonts.on_set_attrs())
            .with_ignore_multiple()
            .into();

        self.begin().await;

        task_loop!
        {
            (self.editor);

            // If the fonts have been modified...
            _ = set_attrs.next() =>
                self.reset().await,

            // Redraws issued by the window manager
            _ = redraws.next() =>
                self.redraw().await,

            // Resets issued by the window manager
            _ = resets.next() =>
                self.reset().await
        }

        self.end().await;
    }
}
