use super::*;

pub(crate) trait Displayable
{
    fn get_line(&self, n: usize, editor: &Editor) -> Option<Line>;

    fn get_scroll(&self) -> usize;

    fn get_location(&self) -> Location;
}
