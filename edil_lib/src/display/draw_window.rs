use super::*;

fn iter_content<'a>(disp: &'a dyn Displayable, editor: &'a Editor)
    -> impl Iterator<Item=Line> + 'a
{
    (disp.get_scroll()..)
        .map(|n| disp.get_line(n, editor))
        .take_while(|line| line.is_some())
        .map(|line| line.unwrap_or_default())
}

pub fn draw(
    disp:   &dyn Displayable,
    canvas: &mut impl Canvas,
    editor: &Editor
)
{
    let lookup_font = |g: Glyph| (g.0, editor.fonts.get_attrs(g.1));

    let mut content = iter_content(disp, editor);
    let location = disp.get_location();
    let mut pos = location.iter_pos().peekable();

    while let (Some(line), Some(&orig_p)) = (content.next(), pos.peek())
    {
        // Emit characters for this line.
        for glyph in line.expand_left(editor)
        {
            if let Some(p) = pos.next()
            {
                canvas.set_glyph(p, lookup_font(glyph));
            }
        }

        // Fill the remainder of the line
        while let Some(p) = pos
            .next_if(|p| p.col != orig_p.col || p.row == orig_p.row)
        {
            canvas.set_glyph(p, lookup_font(line.fill()));
        }
    }

    // Fill the remainder of the window
    while let Some(p) = pos.next()
    {
        canvas.set_glyph(p, lookup_font(Default::default()));
    }
}
