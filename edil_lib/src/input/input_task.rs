use super::*;

pub struct InputTask<I>
{
    input:  I,
    editor: Arc<Editor>,
    plugin: Arc<dyn plugin::Plugin>
}

impl<I> InputTask<I>
    where I: UserInput
{
    pub fn new(editor: Arc<Editor>, input: I, plugin: Box<dyn plugin::Plugin>)
        -> Self
    {
        Self { input, editor, plugin: plugin.into() }
    }

    fn window_height(&self) -> isize
    {
        if let Some(size) = self.editor.windows.get_size()
        {
            size.row as isize
        }
        else
        {
            0
        }
    }

    fn read_mode(&mut self, key: &Key)
    {
        if let Some(content) = self.editor.windows.selected_content()
        {
            if content.inner().handle_key(key)
            {
                return;
            }
        }

        match key
        {
            // Navigate the current window
            key!('g') => self.editor.windows.scroll(isize::MIN),
            key!('G') => self.editor.windows.scroll(isize::MAX),
            key!(Up)     | key!('k') => self.editor.windows.scroll(-1),
            key!(Down)   | key!('j') => self.editor.windows.scroll(1),
            key!(PageUp) | key!('K') =>
                self.editor.windows.scroll(-self.window_height() / 2),
            key!(PageDown) | key!('J') =>
                self.editor.windows.scroll(self.window_height() / 2),

            // Window-mode
            key!(CTRL, 'W') => self.editor.mode.set(Mode::Window),

            // Buffer-mode
            key!(CTRL, 'B') => self.editor.mode.set(Mode::Buffer),

            // Exit the editor
            key!('q') | key!(Escape) => self.editor.stop(),

            key!(CTRL, 'P') =>
                if let Some(curr) =
                    self.editor.windows.selected_content()
                {
                    let plugin = self.plugin.clone();
                    curr.get_context().ask(
                        "Python",
                        move |_, ctx, to_eval|
                            plugin::eval_single(
                                plugin.clone(),
                                ctx.to_owned(),
                                to_eval.to_owned()
                            )
                    );
                },

            _ => error!(%key, "Unknown keypress")
        }
    }

    fn prompt_mode(&mut self, key: &Key)
    {
        if let Some(prompt) = self.editor.windows.prompt()
        {
            if !prompt.inner().handle_key(key)
            {
                error!(%key, "Unknown keypress");
            }
        }
        else
        {
            error!("Missing prompt");
        }
    }

    fn buffer_mode(&self, key: &Key)
    {
        match key
        {
            // Open debug log
            key!('d') =>
            {
                let log_params = content::LogParams(Buffer::default());
                self.editor.windows.open(
                    &self.editor,
                    log_params,
                    window::OpenAs::Frame
                );
            }
            key!('b') =>
                error!("TODO: Open buffer list"),
            key!(Right) | key!('n') =>
                self.editor.windows
                    .switch_content(&self.editor, window::SwitchType::Next),
            key!(Left) | key!('p') =>
                self.editor.windows
                    .switch_content(&self.editor, window::SwitchType::Prev),
            key!('x') =>
                error!("TODO: close window"),
            key!('l') =>
            {
                self.editor.windows.open(
                    &self.editor,
                    content::BufferListParams,
                    window::OpenAs::Frame
                );
            }
            _ =>
                error!(?key, "Unknown keypress")
        }
        self.editor.mode.set(Mode::Read);
    }

    fn window_mode(&self, key: &Key)
    {
        match key
        {
            key!(Up) | key!('k') => error!("TODO: Move window upwards"),
            key!(Down) | key!('j') => error!("TODO: Move window downwards"),
            key!(Right) | key!('l') => error!("TODO: Move window right"),
            key!(Left) | key!('h') => error!("TODO: Move window left"),
            key!('n') =>
                self.editor.windows.switch_frame(1),
            key!('p') =>
                self.editor.windows.switch_frame(-1),
            key!('x') =>
                self.editor.windows.close_selected(),
            key!('s') | key!('-') =>
                self.editor.windows
                    .split(&self.editor, window::layout::SplitType::Vertical),
            key!('v') | key!('|') =>
                self.editor.windows
                    .split(&self.editor, window::layout::SplitType::Horizontal),
            key!(CTRL, 'N') =>
                self.editor.windows.set_notifications(None),
            _ =>
                error!(%key, "Unknown keypress")
        }
        self.editor.mode.set(Mode::Read);
    }

    fn resize(&mut self)
    {
        self.editor.windows.resize_display(self.input.win_size());
    }

    pub async fn task(mut self)
    {
        self.resize();

        task_loop!
        {
            (self.editor);
            evt = self.input.next_event() => match evt
            {
                UserEvent::Resize => self.resize(),
                UserEvent::Key(key) =>
                {
                    debug!(%key, "Received keypress");
                    match self.editor.mode.get()
                    {
                        Mode::Read   => self.read_mode(&key),
                        Mode::Prompt => self.prompt_mode(&key),
                        Mode::Window => self.window_mode(&key),
                        Mode::Buffer => self.buffer_mode(&key)
                    }
                }
            }
        }
    }
}
