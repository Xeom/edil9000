use super::*;

pub trait UserInput: Sync + Send
{
    fn win_size(&self) -> Pos<u16>;

    fn next_event(&mut self) -> impl Future<Output=UserEvent> + Send;
}

#[derive(Clone, Debug, PartialEq)]
pub enum UserEvent
{
    Resize,
    Key(Key)
}

