use super::*;

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Button
{
    Char(char),
    Escape,
    Enter,
    Backspace,
    Delete,
    Up,
    Down,
    Right,
    Left,
    PageUp,
    PageDown,
    Insert
}

impl std::fmt::Display for Button
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        if let Self::Char(ch) = self
        {
            use std::fmt::Write;
            f.write_char(*ch)
        }
        else
        {
            <Button as std::fmt::Debug>::fmt(&self, f)
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub struct Key
{
    pub button: Button,
    pub ctrl:   bool,
    pub alt:    bool,
    pub shift:  bool
}

impl From<char> for Key
{
    fn from(ch: char) -> Key
    {
        Key
        {
            button: Button::Char(ch),
            ctrl:   false,
            alt:    false,
            shift:  false
        }
    }
}

impl std::str::FromStr for Key
{
    type Err = ();
    fn from_str(s: &str) -> Result<Key, ()>
    {
        if let Some(remaining) = s.strip_prefix("C-")
        {
            Ok(Key { ctrl: true, ..Self::from_str(remaining)? })
        }
        else if let Some(remaining) = s.strip_prefix("S-")
        {
            Ok(Key { shift: true, ..Self::from_str(remaining)? })
        }
        else if let Some(remaining) = s.strip_prefix("M-")
        {
            Ok(Key { alt: true, ..Self::from_str(remaining)? })
        }
        else
        {
            let button = match s
                {
                    s if s.chars().count() == 1 =>
                        Button::Char(s.chars().next().unwrap()),
                    "Escape"    => Button::Escape,
                    "Enter"     => Button::Enter,
                    "Backspace" => Button::Backspace,
                    "Delete"    => Button::Delete,
                    "Up"        => Button::Up,
                    "Down"      => Button::Down,
                    "Right"     => Button::Right,
                    "Left"      => Button::Left,
                    "PageUp"    => Button::PageUp,
                    "PageDown"  => Button::PageDown,
                    "Insert"    => Button::Insert,
                    "Space"     => Button::Char(' '),
                    _ => return Err(())
                };

            Ok(Key { button, ctrl: false, alt: false, shift: false })
        }
    }
}

impl std::fmt::Display for Key
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        if self.ctrl  { write!(f, "C-")?; }
        if self.alt   { write!(f, "M-")?; }
        if self.shift { write!(f, "S-")?; }

        <Button as std::fmt::Display>::fmt(&self.button, f)
    }
}

macro_rules! key_impl
{
    ($c:expr, $m:expr, $s:expr, $button:ident) =>
    {
        Key { button: Button::$button, ctrl: $c, alt: $m, shift: $s }
    };
    ($c:expr, $m:expr, $s:expr, $ch:expr) =>
    {
        Key { button: Button::Char($ch), ctrl: $c, alt: $m, shift: $s }
    };
    ($c:expr, $m:expr, $s:expr, CTRL, $($nxt:tt)+) =>
    {
        key_impl!(true, $m, $s, $($nxt)+)
    };
    ($c:expr, $m:expr, $s:expr, ALT, $($nxt:tt)+) =>
    {
        key_impl!($c, true, $s, $($nxt)+)
    };
    ($c:expr, $m:expr, $s:expr, SHIFT, $($nxt:tt)+) =>
    {
        key_impl!($c, $m, true, $($nxt)+)
    };
}

macro_rules! key
{
    ($($tok:tt)+) =>
    {
        key_impl!(false, false, false, $($tok)+)
    };
}

pub(crate) use key;
pub(crate) use key_impl;

#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    fn test_key_to_from_str()
    {
        for (k, s) in [
            (key!('a'),                    "a"),
            (key!('b'),                    "b"),
            (key!(CTRL, 'A'),              "C-A"),
            (key!(CTRL, ALT, 'A'),         "C-M-A"),
            (key!(ALT, 'a'),               "M-a"),
            (key!(Up),                     "Up"),
            (key!(Down),                   "Down"),
            (key!(CTRL, Down),             "C-Down"),
            (key!(CTRL, SHIFT, Down),      "C-S-Down"),
            (key!(CTRL, SHIFT, ALT, Down), "C-M-S-Down")
        ]
        {
            assert_eq!(k, s.parse().unwrap());
            assert_eq!(k.to_string(), s);
        }
    }
}
