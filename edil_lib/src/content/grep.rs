use super::*;

pub struct GrepParams;

impl Params for GrepParams
{
    type Content = List<GrepContent>;

    fn create(self, context: Context<Self::Content>) -> Self::Content
    {
        let _span = info_span!("Creating Grep content").entered();
        let rtn = Self::Content::new(
            context.clone(),
            GrepContent
            {
                context,
                entries:           Mutex::new(vec![]),
                on_entries_change: Event::default()
            }
        );
        debug!("Built Grep content");
        rtn
    }
}

pub struct Entry
{
    location: Option<(PathBuf, usize)>,
    line:     Line
}

pub struct GrepContent
{
    context:           Context<List<GrepContent>>,
    entries:           Mutex<Vec<Entry>>,
    on_entries_change: Event<()>
}

// TODO: When MutexGuard::map is stable, use that instead
pub struct EntriesGuard<'a>(std::sync::MutexGuard<'a, Vec<Entry>>);

impl<'a> std::ops::Deref for EntriesGuard<'a>
{
    type Target = [Entry];

    fn deref(&self) -> &[Entry] { self.0.as_slice() }
}

impl ListEntry<GrepContent> for Entry
{
    fn get_line(&self, _context: &Context<List<GrepContent>>) -> Line
    {
        self.line.clone()
    }

    fn handle_key(&self, key: &Key, context: &Context<List<GrepContent>>)
        -> bool
    {
        match key
        {
            key!(Enter) =>
            {
                if let Some((path, row)) = self.location.clone()
                {
                    context.spawn_falliable(
                        context.editor().open_path(path, row)
                    );
                }
                true
            }
            _ => false
        }
    }
}

impl ListContent for GrepContent
{
    type Entry = Entry;
    type Entries<'a> = EntriesGuard<'a>;

    fn get_title(&self) -> String { "Grep".to_owned() }

    fn get_entries<'a>(&'a self) -> EntriesGuard<'a>
    {
        EntriesGuard(self.entries.lock().unwrap())
    }

    fn on_entries_change(&self) -> NonOwned<'_, Event<()>>
    {
        self.on_entries_change.as_non_owned()
    }
}

impl GrepContent
{
    pub fn load(&self, cmd: process::Command)
    {
        let this = self.context.get_this();
        self.context.spawn(
            async move
            {
                match this.inner().try_load(cmd).await
                {
                    Ok(()) => info!("Finished loading results"),
                    Err(err) => error!(?err, "Error loading results")
                }
            }
        );
    }

    async fn try_load(&self, mut cmd: process::Command)
        -> io::Result<()>
    {
        let mut child = cmd
            .stdin(std::process::Stdio::null())
            .stdout(std::process::Stdio::piped())
            .stderr(std::process::Stdio::piped())
            .kill_on_drop(true)
            .spawn()?;

        let stdout: NonblockFile = child.stdout.take()
            .expect("Where is my stdout???")
            .into_owned_fd()?
            .into();

        let mut lines = io::BufReader::new(stdout).split(b'\n');

        while let Some(line) = lines.next_segment().await?
        {
            let entry = Entry::parse(&line, &self.context.as_any());
            self.entries.lock().unwrap().push(entry);
            self.on_entries_change.trigger(&());
        }

        Ok(())
    }
}

impl Entry
{
    fn new(content: &str, path: &OsStr, row: usize, ctx: &Context) -> Self
    {
        let mut line = Line::from(content);
        write!(line.left().with_font(ctx.font_id("dull")), " {path:?}:{row}");

        Self
        {
            location: Some((PathBuf::from(path), row.saturating_sub(1))),
            line
        }
    }

    fn parse(line: &[u8], ctx: &Context) -> Entry
    {
        Self::try_parse_nicely(line, ctx)
            .unwrap_or_else(
                || Self
                {
                    location: None,
                    line: Line::new(String::from_utf8_lossy(line).chars()) 
                }
            )
    }

    fn try_parse_nicely(line: &[u8], ctx: &Context) -> Option<Entry>
    {
        let mut iter = line.iter().enumerate();
        let null_pos = iter.find(|(_, &b)| b == b'\0')?.0;
        let colon_pos = iter.find(|(_, &b)| b == b':')?.0;

        let path_bytes = &line[0..null_pos];
        let row_bytes  = &line[(null_pos + 1)..colon_pos];
        let line_bytes = &line[(colon_pos + 1)..];

        use std::os::unix::ffi::OsStrExt;

        let parsed_row = std::str::from_utf8(row_bytes).ok()?
            .parse::<usize>().ok()?;

        Some(Self::new(
            &String::from_utf8_lossy(line_bytes),
            OsStr::from_bytes(path_bytes),
            parsed_row,
            ctx
        ))
    }
}
