use super::*;

pub struct Entry
{
    path:     PathBuf,
    name:     String,
    is_dir:   bool,
    metadata: Option<std::fs::Metadata>
}

impl PartialEq for Entry
{
    fn eq(&self, oth: &Self) -> bool { self.path == oth.path }
}

impl Eq for Entry {}

impl PartialOrd for Entry
{
    fn partial_cmp(&self, oth: &Self) -> Option<std::cmp::Ordering>
    {
        Some(self.cmp(oth))
    }
}

impl Ord for Entry
{
    fn cmp(&self, oth: &Self) -> std::cmp::Ordering
    {
        self.is_dir.cmp(&oth.is_dir).reverse()
            .then_with(|| self.path.cmp(&oth.path))
    }
}

impl ListEntry<Dir> for Entry
{
    /// Get a line that should be displayed for this entity.
    fn get_line(&self, context: &Context<List<Dir>>) -> Line
    {
        let mut line = Line::default();

        line.left()
            .with_font(context.font_id(self.get_font()))
            .write_str(&self.name);

        // For directories, add a dull slash to the end.
        if self.is_dir
        {
            line.left()
                .with_font(context.font_id("dull"))
                .write_char('/');
        }

        line
    }

    fn handle_key(&self, key: &Key, context: &Context<List<Dir>>) -> bool
    {
        match key
        {
            key!(Enter) =>
            {
                context.spawn_falliable(
                    // Open the path at line 0
                    context.editor().open_path(self.path.clone(), 0)
                );
                true
            }
            _ => false
        }
    }
}

impl Entry
{
    /// Is this file an executable?
    fn is_exec(&self) -> bool
    {
        use std::os::unix::fs::MetadataExt;

        self.metadata.as_ref()
            .map(|meta| meta.mode() & 0o111 != 0)
            .unwrap_or(false)
    }

    /// Choose a font for the name of this entry.
    fn get_font(&self) -> &'static str
    {
        // Directories
        if self.is_dir
        {
            "dir_entry.dir"
        }
        // If the executable bit is set
        else if self.is_exec()
        {
            "dir_entry.file.exec"
        }
        // Hidden files
        else if self.name.starts_with(".")
        {
            "dir_entry.file.hidden"
        }
        // Normal files
        else
        {
            "dir_entry.file"
        }
    }

    /// Get an entry corresponding to the parent of a path.
    ///
    /// The entry will appear as `..`.
    ///
    pub async fn new_parent(path: &Path) -> io::Result<Option<Self>>
    {
        let mut path = tokio::fs::canonicalize(path).await?;
        if !path.pop() { return Ok(None); }

        Ok(Some(
            Self
            {
                path,
                name:     "..".to_owned(),
                is_dir:   true,
                metadata: None
            }
        ))
    }

    pub async fn from_dir_entry(dir_entry: tokio::fs::DirEntry)
        -> io::Result<Self>
    {
        let metadata = dir_entry.metadata().await?;
        Ok(
            Self
            {
                path:     dir_entry.path(),
                name:     dir_entry.file_name().to_string_lossy().into_owned(),
                is_dir:   metadata.is_dir(),
                metadata: Some(metadata)
            }
        )
    }
}
