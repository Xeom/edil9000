use super::*;

mod entry;
use entry::Entry;

pub struct DirParams(pub PathBuf);

impl Params for DirParams
{
    type Content = List<Dir>;

    fn create(self, context: Context<Self::Content>) -> Self::Content
    {
        let inner = Dir
            {
                path:              self.0,
                context:           context.clone(),
                entries:           Mutex::new(Arc::from(vec![])),
                on_entries_change: Event::default()
            };

        Self::Content::new(context, inner)
    }

    fn unique_id(&self) -> Option<String>
    {
        Some(format!("dir({:?})", self.0))
    }
}

pub struct Dir
{
    path:              PathBuf,
    context:           Context<List<Dir>>,
    entries:           Mutex<Arc<[Entry]>>,
    on_entries_change: Event<()>,
}

impl Dir
{
    fn set_entries(&self, entries_from: impl Into<Arc<[Entry]>>)
    {
        let entries = entries_from.into();

        *(self.entries.lock().unwrap()) = entries.into();
        self.on_entries_change.trigger(&());
    }

    pub fn load(&self)
    {
        let this = self.context.get_this();
        self.context.spawn_falliable(
            async move { this.inner().load_inner().await }
        )
    }

    /// An async task to load the contents of a directory.
    async fn load_inner(&self) -> io::Result<()>
    {
        let mut entries = Vec::new();

        if let Some(parent_entry) = Entry::new_parent(&self.path).await?
        {
            entries.push(parent_entry);
        }

        let mut dir = tokio::fs::read_dir(&self.path).await?;
        while let Some(dir_entry) = dir.next_entry().await?
        {
            entries.push(Entry::from_dir_entry(dir_entry).await?);
        }

        entries.sort();

        self.set_entries(entries);

        Ok(())
    }
}

impl ListContent for Dir
{
    type Entry = Entry;
    type Entries<'a> = Arc<[Entry]>;

    fn get_title(&self) -> String
    {
        format!("Dir {path:?}", path=self.path)
    }

    fn get_entries(&self) -> Arc<[Entry]>
    {
        self.entries.lock().unwrap().clone()
    }

    fn on_entries_change(&self) -> NonOwned<'_, Event<()>>
    {
        self.on_entries_change.as_non_owned()
    }

    fn handle_key(&self, key: &Key) -> bool
    {
        match key
        {
            key!('G') =>
            {
                self.context.ask(
                    "Grep",
                    |this, ctx, needle|
                    {
                        let dir = this.inner().path.to_owned();
                        ctx.editor().open_grep(needle, dir, &["-R"]);
                    }
                );
                true
            }
            _ => false
        }
    }
}

