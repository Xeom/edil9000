use super::*;

pub struct NotificationsParams;
pub struct Notifications
{
    context: Context<Notifications>,
    buffer:  Buffer,
    redraw:  AnyEvent
}

impl Params for NotificationsParams
{
    type Content = Notifications;

    fn create(self, context: Context<Self::Content>) -> Self::Content
    {
        debug!("Creating notifications...");

        let buffer = Buffer::default();
        let fonts  = context.fonts();

        logging::Logger::instance()
            .add_sink(
                "info",
                logging::BufferSink::new(&buffer, fonts)
                    .with_timestamps(false)
            );

        let mut redraw = AnyEvent::default();
        redraw.add_trigger(context.exec(), &buffer.on_modify_text());

        Notifications { context, buffer, redraw }
    }
}

impl Notifications
{
    async fn clear_old_entries(&self)
    {
        let modified: AsyncHandler<_> = Handler::builder(&self.context.exec())
            .with_event(self.buffer.on_modify_text())
            .with_ignore_multiple()
            .into();

        use tokio::time::*;

        let mut to_clear = VecDeque::<(Instant, usize)>::default();

        const LONG_TIME: Duration = Duration::from_secs(100);
        let sleep = sleep(LONG_TIME);
        tokio::pin!(sleep);

        loop
        {
            tokio::select!
            {
                _ = modified.next() =>
                {
                    let _lock = self.buffer.transaction();

                    let empty =
                        self.buffer.num_lines() == 1 &&
                        self.buffer.line_len(0) == 0;

                    let total = to_clear.iter().map(|(_, rows)| rows).sum();
                    if !empty && self.buffer.num_lines() > total
                    {
                        to_clear.push_back((
                            Instant::now() + Duration::from_secs(5),
                            self.buffer.num_lines() - total
                        ));
                    }
                }
                _ = &mut sleep =>
                    if let Some((_, num_rows)) = to_clear.pop_front()
                    {
                        let _lock = self.buffer.transaction();
                        let start = self.buffer.cursor(&StartOfText);
                        let end = start.relative_line(num_rows as i64)
                            .unwrap_or(self.buffer.cursor(&EndOfText));

                        Cursor::delete_between(&start, &end);
                    }
            }

            sleep.as_mut()
                .reset(
                    to_clear.front()
                        .map(|&(instant, _)| instant)
                        .unwrap_or(Instant::now() + LONG_TIME)
                );
        }
    }
}

impl Content for Notifications
{
    fn open(&self)
    {
        let this = self.context.get_this();
        self.context.spawn(
            async move { this.clear_old_entries().await }
        );
    }

    fn get_line(&self, n: usize) -> Option<Line>
    {
        get_buffer_line(&self.buffer, n)
    }

    fn num_lines(&self) -> usize
    {
        self.buffer.num_lines()
    }

    fn on_redraw_needed(&self) -> Option<NonOwned<'_, Event<()>>>
    {
        Some(self.redraw.event().as_non_owned())
    }
}
