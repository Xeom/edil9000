use super::*;

pub struct PromptParams;

pub struct Prompt
{
    context:    Context<Self>,
    buffer:     Buffer,
    cursor:     Cursor,
    redraw:     AnyEvent,
}

impl Params for PromptParams
{
    type Content = Prompt;

    fn create(self, context: Context<Self::Content>) -> Self::Content
    {
        debug!("Creating prompt...");

        let buffer     = Buffer::default();
        let cursor     = buffer.cursor(&EndOfText);

        let mut redraw = AnyEvent::default();
        redraw.add_trigger(context.exec(), &buffer.on_modify_text());
        redraw.add_trigger(context.exec(), &cursor.on_move());

        Prompt { context, buffer, cursor, redraw }
    }
}

impl Prompt
{
    fn consume(&self) -> String
    {
        let reader = self.buffer.cursor(&StartOfText);
        let rtn    = reader.read().to_string();

        Cursor::delete_between(&self.buffer.cursor(&StartOfText), &reader);

        rtn
    }
}

impl Content for Prompt
{
    fn get_line(&self, n: usize) -> Option<Line>
    {
        let _lock = self.buffer.transaction();

        let mut maybe_line = get_buffer_line(&self.buffer, n);

        // Highlight the cursor!
        if let Some(line) = maybe_line.as_mut()
        {
            if n == 0
            {
                if let Some(prompt) = self.context.editor().oracle.get_prompt()
                {
                    let font = self.context.font_id("title");
                    write!(line.left().with_font(font), " ({prompt})");
                }
            }

            if n == self.cursor.row()
            {
                let col = self.cursor.col();
                line.highlight(col..=col, self.context.font_id("cursor"));
            }
        }

        maybe_line
    }

    fn num_lines(&self) -> usize
    {
        self.buffer.num_lines()
    }

    fn on_redraw_needed(&self) -> Option<NonOwned<'_, Event<()>>>
    {
        Some(self.redraw.event().as_non_owned())
    }

    fn handle_key(&self, evt: &Key) -> bool
    {
        use Button::*;
        match evt
        {
            Key { button: Char(c), ctrl: false, shift: false, alt: false } =>
            {
                self.cursor.insert_backward(*c);
                true
            },
            key!(Backspace) => { self.cursor.delete_backward(); true },
            key!(Delete)    => { self.cursor.delete_forward(); true },
            key!(Left)      => { self.cursor.move_by(-1); true },
            key!(Right)     => { self.cursor.move_by(1); true },
            key!(Escape) =>
            {
                self.context.editor().oracle.handle_cancel();
                true
            },
            key!(Enter) =>
            {
                self.context.editor().oracle.handle_answer(self.consume());
                true
            }
            _ => false
        }
    }
}
