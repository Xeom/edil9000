use super::*;

pub struct LogParams(pub Buffer);

impl Params for LogParams
{
    type Content = Text<Log>;

    fn create(self, context: Context<Text<Log>>) -> Text<Log>
    {
        let cursor = self.0.cursor(&EndOfText);
        Text::new(context, Log { buffer: self.0, cursor })
    }

    fn unique_id(&self) -> Option<String> { Some("log".to_owned()) }
}

pub struct Log
{
    buffer: Buffer,
    cursor: Cursor
}

impl TextContent for Log
{
    fn get_title(&self) -> String { "Log".to_owned() }

    fn get_buffer(&self) -> &Buffer { &self.buffer }

    fn get_cursor(&self) -> &Cursor { &self.cursor }
}
