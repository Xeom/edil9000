use super::*;

pub struct BufferListParams;

impl Params for BufferListParams
{
    type Content = List<BufferList>;

    fn create(self, context: Context<Self::Content>) -> Self::Content
    {
        Self::Content::new(context.clone(), BufferList { context })
    }

    fn unique_id(&self) -> Option<String>
    {
        Some("buffer_list".to_owned())
    }
}

pub struct BufferList
{
    context: Context<List<Self>>,
}

impl ListContent for BufferList
{
    type Entry = Entry;
    type Entries<'a> = Vec<Entry>;

    fn get_title(&self) -> String { "Buffer List".to_string() }

    fn get_entries(&self) -> Vec<Entry>
    {
        self.context.windows().all_content_ids().into_iter()
            .map(|content_id| Entry { content_id })
            .collect()
    }

    fn on_entries_change(&self) -> NonOwned<'_, Event<()>>
    {
        self.context.windows().on_content_change()
    }
}

pub struct Entry
{
    content_id: window::ContentId
}

impl ListEntry<BufferList> for Entry
{
    fn get_line(&self, context: &Context<List<BufferList>>) -> Line
    {
        let mut line = Line::default();
        let mut wtr  = line.left();

        write!(wtr, "{} ", self.content_id);

        if let Some(content) = context.windows().get_content(self.content_id)
        {
            write!(wtr, "{} ", content.inner().title());
        }
        else
        {
            line.left().with_font(context.font_id("dull")).write_str("-");
        }

        line
    }

    fn handle_key(&self, key: &Key, context: &Context<List<BufferList>>) -> bool
    {
        match key
        {
            key!(Enter) =>
            {
                context.windows().switch_content(
                    &context.editor(),
                    window::SwitchType::Id(self.content_id)
                );
                true
            }
            _ => false
        }
    }
}

