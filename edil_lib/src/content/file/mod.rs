use super::*;

mod syntax;
mod load;

const NUM_HANDLERS: usize = 1;

pub struct FileParams(pub Option<PathBuf>);

impl Params for FileParams
{
    type Content = Text<File>;

    fn create(self, context: Context<Text<File>>) -> Text<File>
    {
        let buffer   = Buffer::default();
        let cursor   = buffer.cursor(&EndOfText);
        let handlers = [
            Handler::builder(&context.exec())
                .with_event(buffer.on_modify_text())
                .with_ignore_multiple()
                .build(context.make_callback(
                    |this, _, modification|
                    {
                        this.inner().handle_modify(modification);
                    }
                ))
        ];

        let file = File
            {
                context:     context.clone(),
                path:        self.0,
                buffer,
                cursor,
                tree_sitter: Mutex::new(None),
                handlers:    Mutex::new(Some(handlers))
            };

        Text::new(context, file)
    }

    fn unique_id(&self) -> Option<String>
    {
        Some(format!("file({:?})", self.0.clone()?))
    }
}

/// Content representing a file.
pub struct File
{
    context:     Context<Text<Self>>,
    path:        Option<PathBuf>,
    buffer:      Buffer,
    cursor:      Cursor,
    tree_sitter: Mutex<Option<tsitter::TreeSitter>>,
    handlers:    Mutex<Option<[Handler; NUM_HANDLERS]>>
}

impl TextContent for File
{
    fn get_title(&self) -> String
    {
        match self.path.as_ref().and_then(|p| p.file_name())
        {
            Some(filename) => format!("File {filename:?}"),
            None           => "Untitled File".to_owned()
        }
    }

    fn get_buffer(&self) -> &Buffer { &self.buffer }

    fn get_cursor(&self) -> &Cursor { &self.cursor }

    fn close(&self)
    {
        // Remove the tree-sitter and all handlers before closing the file,
        // ensuring there aren't circular references to the file.
        self.handlers.lock().unwrap().take();
        self.tree_sitter.lock().unwrap().take();
    }
}

impl File
{
    fn select(&self, pos: &Cursor) { self.cursor.move_to(pos); }
}

