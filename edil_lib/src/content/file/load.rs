use super::*;

impl File
{
    pub fn load(
        &self,
        file: impl AsyncRead + Unpin + Send + Sync + 'static,
        row:  usize
    )
    {
        let this = self.context.get_this();
        self.context.spawn_falliable(
            async move { this.inner().load_impl(file, row).await }
        );
    }

    async fn load_impl(
        &self,
        mut file: impl AsyncRead + Unpin,
        row:      usize
    )
        -> io::Result<()>
    {
        let mut buffer = [0; 4096];
        let mut first_read = true;
        let mut done_scroll = false;

        let cur = self.buffer.cursor(&EndOfText);
        let mut wtr = cur.write();

        loop
        {
            let maybe_n = file.read(&mut buffer).await;
            let n = maybe_n?;

            // Use the first read from the file to set the filetype.
            //
            // TODO: Also deduce the file encoding here.
            if first_read
            {
                first_read = false;

                if let Some(file_type) = self.context.config()
                    .guess_file_type(self.path.as_deref(), &buffer[..n])
                {
                    debug!(?file_type, "Guessed file type");
                    self.set_file_type(file_type).await;
                }
            }

            if n == 0 { break; }

            std::io::Write::write(&mut wtr, &buffer[..n])
                .expect("Should not have failed to write");

            if !done_scroll && self.buffer.num_lines() > row
            {
                self.select(&self.buffer.cursor(&Row(row)));
                done_scroll = true;
            }

            tokio::task::yield_now().await;
        }

        info!("Loaded file");

        Ok(())
    }
}
