use super::*;

#[api_macros::plugin_api]
fn set_file_type(file_type: &str, ctx: &Context)
{
    // Clone and own!
    let owned_ctx       = ctx.clone();
    let owned_file_type = file_type.to_owned();

    ctx.spawn(
        async move
        {
            if let Some(file) = owned_ctx.get_as::<Text<File>>()
            {
                file.inner().set_file_type(&owned_file_type).await;
            }
            else
            {
                error!("Cannot set file_type in non-file context");
            }
        }
    );
}

impl File
{
    pub async fn set_file_type(&self, file_type: &str)
    {
        debug!(%file_type, "Setting file type");

        let Some(language) = self.context.config()
            .get_language(file_type, &self.context.as_any()).await
            else
            {
                error!(file_type, "Could not create language");
                return;
            };

        let Ok(new) = tsitter::TreeSitter::new(language, &self.buffer)
            else
            {
                error!(file_type, "Error creating tree-sitter");
                return;
            };

        self.apply_tsitter(|tsitter| *tsitter = Some(new));
    }

    pub fn handle_modify(&self, m: &Modification)
    {
        self.apply_tsitter(
            |maybe_tsitter| if let Some(ref mut tsitter) = maybe_tsitter
            {
                tsitter.handle_modify(m);
            }
        )
    }

    /// Perform an action on the file's tree-sitter, potentially causing it to
    /// need to launch a worker.
    fn apply_tsitter(&self, cb: impl FnOnce(&mut Option<tsitter::TreeSitter>))
    {
        let mut guard = self.tree_sitter.lock().unwrap();

        let needed_worker = guard.as_ref().is_some_and(|ts| ts.needs_worker());

        cb(&mut guard);

        let needs_worker = guard.as_ref().is_some_and(|ts| ts.needs_worker());
        if needs_worker && !needed_worker
        {
            self.launch_tsitter_worker()
        }
        else if needed_worker && !needs_worker
        {
            panic!("Tree sitter work completed, but not by a worker.");
        }
    }

    fn launch_tsitter_worker(&self)
    {
        let this = self.context.get_this();
        self.context.spawn(
            async move
            {
                this.inner().tsitter_worker_impl().await;
            }
        );
    }

    async fn tsitter_worker_impl(&self)
    {
        let mut running = true;

        while running
        {
            tokio::task::yield_now().await;

            let mut guard = self.tree_sitter.lock().unwrap();
            if let Some(ref mut tsitter) = *guard
            {
                tsitter.work(&self.buffer);
                running = tsitter.needs_worker();
            }
            else
            {
                running = false;
            }
        }

        debug!("Tree-sitter worker task complete");
    }
}

