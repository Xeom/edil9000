use super::*;

mod log;
pub use log::LogParams;

mod text;
pub use text::{Text, TextContent};

mod grep;
pub use grep::GrepParams;

mod list;
use list::{List, ListContent, ListEntry};

mod prompt;
pub use prompt::PromptParams;

mod file;
pub use file::FileParams;

mod dir;
pub use dir::DirParams;

mod notifications;
pub use notifications::NotificationsParams;

mod buffer_list;
pub use buffer_list::BufferListParams;

pub trait Params: Sized
{
    type Content: Content + Sized;

    fn create(self, context: Context<Self::Content>) -> Self::Content;

    /// Get a unique string describing this content.
    ///
    /// When creating this content, if the string matches other content that
    /// already exists, that content will be substituted.
    fn unique_id(&self) -> Option<String> { None }
}

/// A trait representing something that can be shown in a window.
///
/// TODO: Rework this trait to allow it to work with buffer transactions, and
///       handle the case where content changes while it is being drawn.
///
/// TODO: Add an extern C interface for this.
///
pub trait Content: Send + Sync + 'static
{
    /// Called after the content
    fn open(&self) { }

    /// Called before the content is closed and destroyed.
    ///
    /// Any cleanup should be done here.
    fn close(&self) { }

    fn title(&self) -> String { "...".to_owned() }

    fn get_line(&self, n: usize) -> Option<Line>;

    /// Get the number of lines in this content.
    fn num_lines(&self) -> usize;

    /// Get an efficient lower bound for the length of a line.
    fn line_len_lower_bound(&self, n: usize) -> usize
    {
        let _ = n;
        0
    }

    fn on_redraw_needed(&self) -> Option<NonOwned<'_, Event<()>>>
    {
        None
    }

    fn on_focus(&self) -> Option<NonOwned<'_, Event<usize>>>
    {
        None
    }

    fn handle_key(&self, key: &Key) -> bool { let _ = key; false }
}

fn get_buffer_line(buf: &Buffer, n: usize) -> Option<Line>
{
    let _lock = buf.transaction();

    if n < buf.num_lines()
    {
        let sol = buf.cursor(&Row(n));
        let codepoints = sol.dup().read().until_eol().to_codepoints();
        let fonts      = sol.get_fonts(codepoints.len());

        Some(Line::new(
            codepoints.into_iter()
                .zip(fonts.into_iter())
                .map(|(cp, font)| Glyph(cp, font))
        ))
    }
    else
    {
        None
    }
}
