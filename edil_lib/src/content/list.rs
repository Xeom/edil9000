use super::*;

pub trait ListContent: Sync + Send + Sized + 'static
{
    type Entry: ListEntry<Self>;
    type Entries<'a>: std::ops::Deref<Target=[Self::Entry]> + 'a;

    fn get_title(&self) -> String;

    fn get_entries<'a>(&'a self) -> Self::Entries<'a>;

    fn on_entries_change(&self) -> NonOwned<'_, Event<()>>;

    fn handle_key(&self, key: &Key) -> bool { let _ = key; false }
}

pub trait ListEntry<Content>
{
    fn get_line(&self, context: &Context<List<Content>>) -> Line;

    fn handle_key(&self, key: &Key, context: &Context<List<Content>>)
        -> bool;
}

pub struct List<L>
{
    context:           Context<Self>,
    selected_row:      Variable<usize>,
    focused_row:       DerivedVariable<usize>,
    redraw:            AnyEvent,
    inner:             L
}

impl<L> List<L>
    where L: ListContent
{
    pub fn new(context: Context<Self>, inner: L) -> Self
    {
        let selected_row = Variable::new(0_usize);
        let redraw =
            AnyEvent::default()
                .with_trigger(context.exec(), &inner.on_entries_change())
                .with_trigger(context.exec(), &selected_row.on_change());

        let focused_row =
            DerivedVariable::new(context.exec(), &selected_row, |&x| x + 1);

        Self { context, selected_row, focused_row, redraw, inner }
    }

    pub fn inner(&self) -> &L { &self.inner }

    fn get_title_line(&self) -> Line
    {
        let dull_font  = self.context.font_id("dull");
        let title_font = self.context.font_id("title");

        let mut line = Line::default()
            .with_fill(Glyph('*', dull_font));

        line.left()
            .with_font(dull_font)
            .write_str("* ")
            .with_font(title_font)
            .write_str(&self.inner.get_title())
            .write_str(" ");

        line
    }

    fn get_entry_line(&self, n: usize) -> Option<Line>
    {
        let entries = self.inner.get_entries();
        let mut line = entries.get(n)?.get_line(&self.context);

        // If this is the currently selected item, highlight it
        if n == self.selected_row.get()
        {
            line.highlight(.., self.context.font_id("cursor"));
        }

        Some(line)
    }

    fn handle_entry_key(&self, key: &Key) -> bool
    {
        let entries = self.inner.get_entries();
        if let Some(selected) = entries.get(self.selected_row.get())
        {
            selected.handle_key(key, &self.context)
        }
        else
        {
            false
        }
    }

    fn move_row(&self, n: isize)
    {
        use std::cmp::{min, max};

        let orig_row = self.selected_row.get() as isize;
        let num_entries = self.inner.get_entries().len() as isize;
        let new_row = max(min(orig_row + n, num_entries - 1), 0) as usize;
        self.selected_row.set(new_row);
    }
}

impl<L> Content for List<L>
    where L: ListContent
{
    fn title(&self) -> String { self.inner.get_title() }

    fn get_line(&self, n: usize) -> Option<Line>
    {
        if n == 0
        {
            Some(self.get_title_line())
        }
        else
        {
            self.get_entry_line(n - 1)
        }
    }

    fn num_lines(&self) -> usize { self.inner.get_entries().len() + 1 }

    fn on_redraw_needed(&self) -> Option<NonOwned<'_, Event<()>>>
    {
        Some(self.redraw.event().as_non_owned())
    }

    fn on_focus(&self) -> Option<NonOwned<'_, Event<usize>>>
    {
        Some(self.focused_row.on_change())
    }

    fn handle_key(&self, key: &Key) -> bool
    {
        match key
        {
            key!(Up)   => { self.move_row(-1); true }
            key!(Down) => { self.move_row(1);  true }
            _ => self.handle_entry_key(key) || self.inner.handle_key(key)
        }
    }
}
