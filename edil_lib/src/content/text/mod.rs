use super::*;

mod search;

pub trait TextContent: Sync + Send + Sized + 'static
{
    fn get_title(&self) -> String;

    fn get_buffer(&self) -> &Buffer;

    fn get_cursor(&self) -> &Cursor;

    fn handle_key(&self, key: &Key) -> bool { let _ = key; false }

    fn close(&self) {}
}

const NUM_HANDLERS: usize = 1;

pub struct Text<T>
{
    context:         Context<Self>,
    on_search_begin: Event<()>,
    on_focus:        Event<usize>,
    redraw:          AnyEvent,
    search_pattern:  Mutex<Option<Arc<dyn search::Pattern>>>,
    handlers:        Mutex<Option<[Handler; NUM_HANDLERS]>>,
    inner:           T
}

impl<T> Text<T>
    where T: TextContent
{
    pub fn new(context: Context<Self>, inner: T) -> Self
    {
        let on_search_begin = Event::default();
        let on_focus        = Event::default();
        let redraw          = AnyEvent::default()
            .with_trigger(context.exec(), &inner.get_buffer().on_modify_text())
            .with_trigger(context.exec(), &inner.get_buffer().on_modify_font())
            .with_trigger(context.exec(), &inner.get_cursor().on_move())
            .with_trigger(context.exec(), &on_search_begin);

        let handlers = [
            Handler::builder(&context.exec())
                .with_event(inner.get_cursor().on_move())
                .with_ignore_multiple()
                .build(context.make_callback(
                    |this, _, _|
                    {
                        this.on_focus.trigger(&this.inner.get_cursor().row());
                    }
                )),
        ];

        Text
        {
            context,
            on_focus,
            on_search_begin,
            redraw,
            search_pattern: Mutex::new(None),
            handlers:       Mutex::new(Some(handlers)),
            inner
        }

    }

    pub fn inner(&self) -> &T { &self.inner }
}

impl<T> Content for Text<T>
    where T: TextContent
{
    fn close(&self)
    {
        // Before closing the file, remove all handlers!
        self.handlers.lock().unwrap().take();

        // Close the inner content
        self.inner.close();
    }

    fn title(&self) -> String { self.inner.get_title() }

    fn get_line(&self, row: usize) -> Option<Line>
    {
        let buffer = self.inner.get_buffer();
        let cursor = self.inner.get_cursor();
        let _lock = buffer.transaction();

        if let Some(mut line) = get_buffer_line(buffer, row)
        {
            // Highlight any search pattern instances
            if let Some(pattern) = self.get_pattern()
            {
                let sol = buffer.cursor(&Row(row));
                for highlight in pattern.match_line(sol)
                {
                    let start_col = highlight.start.col();
                    let end_col   = highlight.end.col();
                    let font      = self.context.font_id("highlight");
                    line.highlight(start_col..end_col, font);
                }
            }

            // Highlight the cursor
            if cursor.row() == row
            {
                let col  = cursor.col();
                let font = self.context.font_id("cursor");
                line.highlight(col..=col, font);
            }

            Some(line)
        }
        else
        {
            let eof_display = self.context.config().eof
                .get_display(&self.context.editor());

            if let Some(disp) = usize::checked_sub(row, buffer.num_lines())
                .and_then(|ind| eof_display.get(ind))
                .or_else(|| eof_display.last())
            {
                Some(Line::new(disp))
            }
            else
            {
                Some(Line::default())
            }
        }
    }

    fn num_lines(&self) -> usize { self.inner.get_buffer().num_lines() }

    fn line_len_lower_bound(&self, n: usize) -> usize
    {
        self.inner.get_buffer().line_len(n)
    }

    fn on_redraw_needed(&self) -> Option<NonOwned<'_, Event<()>>>
    {
        Some(self.redraw.event().as_non_owned())
    }

    fn on_focus(&self) -> Option<NonOwned<'_, Event<usize>>>
    {
        Some(self.on_focus.as_non_owned())
    }

    fn handle_key(&self, key: &Key) -> bool
    {
        match key
        {
            key!('/') => self.search_begin(),
            key!('n') => self.search_forward(),
            key!('N') => self.search_backward(),
            _ => return self.inner.handle_key(key)
        }

        true
    }
}

