use super::*;

#[derive(Debug)]
pub(super) struct Match { pub start: Cursor, pub end: Cursor }

pub(super) trait Pattern: Sync + Send + 'static
{
    fn match_line(&self, sol: Cursor) -> Vec<Match>;
}

impl Pattern for String
{
    fn match_line(&self, sol: Cursor) -> Vec<Match>
    {
        let line = sol.dup().read().until_eol().to_string();
        let byte_to_char = |b| (&line[..b]).chars().count();
        let mut matches = Vec::new();

        for (start_byte, span) in line.match_indices(self)
        {
            let start = sol.moved(byte_to_char(start_byte) as i64);
            let end   = start.moved(span.chars().count() as i64);

            matches.push(Match { start, end });
        }

        matches
    }
}

#[derive(Debug, Clone, Copy)]
enum Direction
{
    Forward,
    Backward
}

impl Direction
{
    fn as_sign(&self) -> i64
    {
        match self
        {
            Self::Forward => 1,
            Self::Backward => -1
        }
    }
}

impl<T> Text<T>
    where T: TextContent
{
    pub(super) fn get_pattern(&self) -> Option<Arc<dyn search::Pattern>>
    {
        self.search_pattern.lock().unwrap().clone()
    }

    pub(super) fn search_backward(&self)
    {
        self.context.spawn(
            self.context.get_this().search_continue_impl(Direction::Backward)
        );
    }

    pub(super) fn search_forward(&self)
    {
        self.context.spawn(
            self.context.get_this().search_continue_impl(Direction::Forward)
        );
    }

    pub(super) fn search_begin(&self)
    {
        self.context.ask(
            "Search",
            |this, ctx, needle|
            {
                debug!(?needle, "Beginning search");
                ctx.spawn(this.search_begin_impl(needle.to_owned()));
            }
        );
    }

    async fn search_impl(
        &self,
        pattern:   &dyn Pattern,
        start:     Cursor,
        direction: Direction
    )
        -> Option<Cursor>
    {
        debug!(?direction, "Starting search loop");

        let mut iter = start.dup();
        loop
        {
            let mut matches = self.inner.get_buffer().transaction()
                .run(|| pattern.match_line(iter.start_of_line()))
                .into_iter();

            let maybe_found = match direction
                {
                    Direction::Forward =>
                        matches.find(|found| found.start >= start),
                    Direction::Backward =>
                        matches.rfind(|found| found.start <= start)
                };

            if let Some(found) = maybe_found
            {
                return Some(found.start);
            }

            iter = iter.relative_line(direction.as_sign())?;
        }
    }

    async fn search_begin_impl(self: Arc<Self>, needle: impl Pattern)
    {
        let needle = Arc::new(needle);
        let cur = self.inner.get_buffer().cursor(&StartOfText);
        self.search_pattern.lock().unwrap().replace(needle.clone());
        self.on_search_begin.trigger(&());

        let maybe_found =
            self.search_impl(&*needle, cur, Direction::Forward).await;

        if let Some(found) = maybe_found
        {
            self.inner.get_cursor().move_to(&found);
        }
        else
        {
            error!("No match found.");
        }
    }

    async fn search_continue_impl(self: Arc<Self>, direction: Direction)
    {
        let cur = self.inner.get_cursor().moved(direction.as_sign());

        if let Some(needle) = self.get_pattern()
        {
            if let Some(found) = self.search_impl(&*needle, cur, direction)
                .await
            {
                self.inner.get_cursor().move_to(&found);
            }
            else
            {
                error!(?direction, "Reached last match.");
            }
        }
        else
        {
            error!("No active search to continue.");
        }
    }
}
