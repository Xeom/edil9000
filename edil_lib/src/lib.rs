mod content;
use content::Content;
pub use content::LogParams;

mod display;
pub use display::DisplayTask;

mod input;
pub use input::{UserInput, InputTask};
use input::{UserEvent, Key, Button, key, key_impl};

mod state;
pub use state::{Editor, Context};
use state::*;

mod window;

mod misc;
use misc::*;

mod config;
pub use config::Config;

pub mod logging;
pub mod tty;

pub mod plugin;

mod tsitter;

use async_trait::async_trait;

use tokio::{
    io::{
        self,
        AsyncWrite, AsyncWriteExt,
        AsyncBufReadExt,
        AsyncRead, AsyncReadExt,
        unix::AsyncFd
    },
    process,
    signal::unix::{Signal, SignalKind, signal}
};

use std::{
    future::Future,
    collections::{BTreeMap, BTreeSet, VecDeque},
    os::fd::{AsRawFd, AsFd, RawFd, FromRawFd, BorrowedFd, OwnedFd},
    ffi::OsStr,
    sync::{Mutex, Weak, Arc, RwLock},
    fs::File as StdFile,
    pin::Pin,
    path::{Path, PathBuf},
    marker::PhantomData
};

pub use edil_common::*;
pub use nix;
pub use tokio;
pub use tracing::{info, error, warn, debug, info_span};

macro_rules! task_loop
{
    {($editor:expr);$($inner:tt)*} =>
    {
        let running: AsyncHandler<_> = Handler::builder(&$editor.exec)
            .with_event($editor.running.on_change())
            .into();

        if $editor.running.get()
        {
            'run_task: loop
            {
                tokio::select!
                {
                    val = running.next() => { if !val { break 'run_task } }
                    $($inner)*
                }
            }
        }
    }
}

use task_loop;
