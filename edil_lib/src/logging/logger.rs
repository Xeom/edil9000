use super::*;

use tracing_core::LevelFilter;

struct Inner
{
    sinks: RwLock<Arc<[(LevelFilter, Arc<dyn Sink>)]>>,
    next_id: std::sync::atomic::AtomicU64
}

impl Inner
{
    fn get_sinks(&self) -> Arc<[(LevelFilter, Arc<dyn Sink>)]>
    {
        self.sinks.read().unwrap().clone()
    }

    fn max_level(&self) -> LevelFilter
    {
        match self.get_sinks().first()
        {
            None            => LevelFilter::OFF,
            Some(&(lvl, _)) => lvl
        }
    }

    fn add_sink(&self, lvl: LevelFilter, sink: Arc<dyn Sink>)
    {
        let mut new_sinks = self.get_sinks().iter().cloned()
            .chain(std::iter::once((lvl, sink)))
            .collect::<Vec<_>>();

        new_sinks.sort_by(|&(l, _), &(r, _)| r.cmp(&l));
        *self.sinks.write().unwrap() = new_sinks.into();

        tracing_core::callsite::rebuild_interest_cache();
    }

    fn alloc_id(&self) -> u64
    {
        self.next_id.fetch_add(1, std::sync::atomic::Ordering::Relaxed)
    }
}

struct Subscriber(Arc<Inner>);

impl tracing::Subscriber for Subscriber
{
    fn enabled(&self, metadata: &tracing::Metadata<'_>) -> bool
    {
        *metadata.level() <= self.0.max_level()
    }

    fn new_span(&self, span: &tracing::span::Attributes<'_>) -> tracing::Id
    {
        let _ = span;
        tracing::Id::from_u64(self.0.alloc_id())
    }

    fn record(&self, span: &tracing::Id, values: &tracing::span::Record<'_>)
    {
        let _ = span;
        let _ = values;
    }

    fn record_follows_from(&self, span: &tracing::Id, follows: &tracing::Id)
    {
        let _ = span;
        let _ = follows;
    }

    fn event(&self, event: &tracing::Event<'_>)
    {
        for (_, sink) in self.0.get_sinks().into_iter()
            .take_while(|&(lvl, _)| event.metadata().level() <= lvl)
        {
            sink.on_event(event);
        }
    }

    fn enter(&self, span: &tracing::Id)
    {
        let _ = span;
    }

    fn exit(&self, span: &tracing::Id)
    {
        let _ = span;
    }

    fn max_level_hint(&self) -> Option<LevelFilter>
    {
        Some(self.0.max_level())
    }
}

pub struct Logger(Arc<Inner>);

impl Logger
{
    pub fn instance() -> &'static Self
    {
        static CELL: std::sync::OnceLock<Logger> = std::sync::OnceLock::new();
        CELL.get_or_init(|| {
            let inner = Arc::new(
                Inner
                {
                    sinks: RwLock::new(Arc::new([])),
                    next_id: 1.into()
                }
            );

            let subscriber = Subscriber(inner.clone());

            tracing::subscriber::set_global_default(subscriber)
                .expect("Could not create global logger");

            Self(inner)
        })
    }

    pub fn add_sink(&self, lvl: &str, sink: impl Sink + Sized)
    {
        self.0.add_sink(
            lvl.parse().unwrap_or(LevelFilter::ERROR),
            Arc::new(sink)
        )
    }
}


