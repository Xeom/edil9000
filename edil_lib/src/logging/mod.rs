use super::*;

mod sink;
pub use sink::*;

mod redirect_stdio;
pub use redirect_stdio::*;

mod redirect_panic;
pub use redirect_panic::*;

mod logger;
pub use logger::*;

// Plugin API to allow plugins to emit basic log messages.

#[api_macros::plugin_api]
fn log_debug(info: &str) { debug!("{}", info); }

#[api_macros::plugin_api]
fn log_info(info: &str) { info!("{}", info); }

#[api_macros::plugin_api]
fn log_warn(warn: &str) { warn!("{}", warn); }

#[api_macros::plugin_api]
fn log_error(err: &str) { error!("{}", err); }
