use super::*;

pub enum StdStream
{
    StdOut,
    StdErr
}

pub async fn redirect_stdstream_to_log_task(
    editor: Arc<Editor>,
    stream: StdStream,
    span:   tracing::Span
)
{
    let fd = match stream
        {
            StdStream::StdOut => nix::libc::STDOUT_FILENO,
            StdStream::StdErr => nix::libc::STDERR_FILENO
        };

    let (rd_end, wr_end) = nix::unistd::pipe()
        .expect("Could not open a pipe to redirect to log");

    // Redirect stdout and stderr to the pipe.
    nix::unistd::dup2(wr_end.as_raw_fd(), fd)
        .expect("Could not redirect file to pipe");

    // The write end of the pipe is redirected to STDOUT. We forget about the
    // inital FD.
    std::mem::forget(wr_end);

    let to_read = StdFile::from(rd_end);
    let mut lines = io::BufReader::new(NonblockFile::from(to_read)).lines();

    task_loop!
    {
        (editor);
        maybe_line = lines.next_line() =>
        {
            if let Ok(Some(line)) = maybe_line
            {
                let _entered = span.enter();
                info!("{}", line);
            }
            else
            {
                return;
            }
        }
    }
}
