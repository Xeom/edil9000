use super::*;

pub fn redirect_panics_to_log()
{
    std::panic::set_hook(Box::new(
        |info|
        {
            let thread_name = std::thread::current()
                .name()
                .unwrap_or("<unnamed>")
                .to_string();

            let msg =
                if let Some(s) = info.payload().downcast_ref::<&'static str>()
                {
                    *s
                }
                else if let Some(s) = info.payload().downcast_ref::<String>()
                {
                    &**s
                }
                else
                {
                    "Box<dyn Any>"
                };

            let location =
                match info.location()
                {
                    Some(loc) => format!("{}:{}", loc.file(), loc.line()),
                    None      => "<unknown location>".to_string()
                };

            let backtrace =
                std::backtrace::Backtrace::force_capture().to_string();

            error!(location, thread_name, %msg, %backtrace, "Thread panicked");
        }
    ));
}
