use super::*;

pub(super) struct Line<'a, W, C>
{
    out:        &'a mut W,
    event:      &'a tracing::Event<'a>,
    colours:    C,
    timestamps: bool
}

impl<'a, W> Line<'a, W, colours::NoCol>
{
    pub fn new(out: &'a mut W, event: &'a tracing::Event<'a>)
        -> Self
    {
        Self { out, event, colours: colours::NoCol, timestamps: true }
    }
}

impl<'a, W, C> Line<'a, W, C>
{
    pub fn with_colours<C2>(self, colours: C2) -> Line<'a, W, C2>
    {
        Line
        {
            out:        self.out,
            event:      self.event,
            timestamps: self.timestamps,
            colours
        }
    }

    pub fn with_timestamps(self, timestamps: bool) -> Self
    {
        Line { timestamps, ..self }
    }
}

impl<'a, W, C> Line<'a, W, C>
    where W: std::io::Write, C: colours::Colours<W>
{
    pub fn write(mut self) -> std::io::Result<()>
    {
        if self.timestamps
        {
            self.write_time()?;
        }
        self.write_level()?;
        self.write_message()?;
        self.write_fields()?;

        self.out.flush()
    }

    fn field_indent(&self) -> usize { if self.timestamps { 15 } else { 3 } }

    fn write_message(&mut self) -> std::io::Result<()>
    {
        field_visitor::FieldVisitor::new(
            |f, value| if f.name() == "message"
            {
                write!(self.out, "{:?} ", value)?;
                self.colours.dull(self.out)?;
                write!(
                    self.out,
                    "({}:{})",
                    self.event.metadata().file().unwrap_or("???"),
                    self.event.metadata().line().as_ref()
                        .map(|line| -> &dyn std::fmt::Display { &*line })
                        .unwrap_or(&"???")
                )?;
                self.colours.reset(self.out)
            }
            else
            {
                Ok(())
            }
        )
            .visit(self.event)
    }

    fn write_fields(&mut self) -> std::io::Result<()>
    {
        let indent = self.field_indent();
        field_visitor::FieldVisitor::new(
            |f, value| if f.name() != "message"
            {
                let key = f.name();
                self.colours.dull(self.out)?;
                write!(self.out, "\n{:indent$}{key}: ", "")?;
                self.colours.reset(self.out)?;
                write!(self.out, "{value:?}")
            }
            else
            {
                Ok(())
            }
        )
            .visit(self.event)
    }

    fn write_level(&mut self) -> std::io::Result<()>
    {
        let level = *self.event.metadata().level();
        let letter = match level
            {
                tracing::Level::TRACE => 'T',
                tracing::Level::DEBUG => 'D',
                tracing::Level::INFO  => 'I',
                tracing::Level::WARN  => 'W',
                tracing::Level::ERROR => 'E'
            };

        self.colours.level(self.out, level)?;
        write!(self.out, " {letter} ")?;
        self.colours.reset(self.out)
    }

    fn write_time(&mut self) -> std::io::Result<()>
    {
        write!(self.out, "{}", chrono::Local::now().format("%H:%M:%S.%3f"))
    }
}
