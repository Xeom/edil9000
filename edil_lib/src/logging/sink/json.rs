use super::*;

use serde::ser::{Serialize, Serializer, SerializeMap};

pub(super) struct Json<'a, W>
{
    out:   &'a mut W,
    event: &'a tracing::Event<'a>
}

impl<'a, W> Json<'a, W>
    where W: std::io::Write
{
    pub fn new(out: &'a mut W, event: &'a tracing::Event<'a>) -> Self
    {
        Self { out, event }
    }

    pub fn write(mut self) -> std::io::Result<()>
    {
        let mut serializer = serde_json::Serializer::new(&mut self.out);
        let mut map = serializer.serialize_map(None)?;

        map.serialize_entry("level", self.event.metadata().level().as_str())?;
        map.serialize_entry("fields", &Fields(self.event))?;
        map.end()?;

        Ok(())
    }
}

pub(super) struct Fields<'a>(&'a tracing::Event<'a>);

impl<'a> Serialize for Fields<'a>
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where S: Serializer
    {
        let mut map = serializer.serialize_map(None)?;
        field_visitor::FieldVisitor::new(
            |f, value| map.serialize_entry(f.name(), &format!("{:?}", value))
        )
            .visit(self.0)?;

        map.end()
    }
}
