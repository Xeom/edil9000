use super::*;

pub struct BufferSink
{
    cursor:     Cursor,
    fonts:      colours::BufferFonts,
    timestamps: bool
}

impl BufferSink
{
    pub fn new(buf: &Buffer, fonts: &Fonts) -> Self
    {
        Self
        {
            cursor:     buf.cursor(&EndOfText),
            fonts:      colours::BufferFonts::new(fonts),
            timestamps: true
        }
    }

    pub fn with_timestamps(self, timestamps: bool) -> Self
    {
        Self { timestamps, ..self }
    }
}

impl logging::Sink for BufferSink
{
    fn on_event(&self, event: &tracing::Event<'_>)
    {
        let buffer = self.cursor.buffer();
        let _xact = buffer.transaction();

        let mut writer = self.cursor.write();

        // If the current line is not empty, move to a new one.
        //
        // This avoids adding an extra newline at the end of the buffer.
        if self.cursor.location::<Coord>().col > 0
        {
            let _ = std::io::Write::write(&mut writer, b"\n");
        }

        // Ignore all errors since buffers can't produce IO errors...
        let _ = line::Line::new(&mut writer, event)
            .with_timestamps(self.timestamps)
            .with_colours(&self.fonts)
            .write();
    }
}


