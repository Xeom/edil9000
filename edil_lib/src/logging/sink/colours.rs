use super::*;

pub(super) trait Colours<W>
{
    fn level(&self, out: &mut W, lvl: tracing::Level) -> std::io::Result<()>;

    fn reset(&self, out: &mut W) -> std::io::Result<()>;

    fn dull(&self, out: &mut W) -> std::io::Result<()>;
}

pub(super) struct NoCol;

impl<W> Colours<W> for NoCol
{
    fn level(&self, _: &mut W, _: tracing::Level) -> std::io::Result<()>
    {
        Ok(())
    }

    fn reset(&self, _: &mut W) -> std::io::Result<()> { Ok(()) }

    fn dull(&self, _: &mut W) -> std::io::Result<()> { Ok(()) }
}

pub(super) struct Ansi;

impl<W> Colours<W> for Ansi
    where W: std::io::Write
{
    fn level(&self, out: &mut W, lvl: tracing::Level) -> std::io::Result<()>
    {
        let code = match lvl
            {
                tracing::Level::TRACE => 35,
                tracing::Level::DEBUG => 36,
                tracing::Level::INFO  => 34,
                tracing::Level::WARN  => 33,
                tracing::Level::ERROR => 31
            };

        write!(out, "\x1b[{code}m")
    }

    fn reset(&self, out: &mut W) -> std::io::Result<()>
    {
        write!(out, "\x1b[0m")
    }

    fn dull(&self, out: &mut W) -> std::io::Result<()>
    {
        write!(out, "\x1b[0;2m")
    }
}

pub struct BufferFonts
{
    error:  FontId,
    warn:   FontId,
    info:   FontId,
    debug:  FontId,
    trace:  FontId,
    dull:   FontId
}

impl BufferFonts
{
    pub fn new(fonts: &Fonts) -> Self
    {
        Self
        {
            error:  fonts.get_id("log.error"),
            warn:   fonts.get_id("log.warn"),
            info:   fonts.get_id("log.info"),
            debug:  fonts.get_id("log.debug"),
            trace:  fonts.get_id("log.trace"),
            dull:   fonts.get_id("dull")
        }
    }
}

impl<'cur> Colours<CursorWrite<'cur>> for &BufferFonts
{
    fn level(&self, out: &mut CursorWrite<'cur>, lvl: tracing::Level)
        -> std::io::Result<()>
    {
        out.set_font(
            match lvl
            {
                tracing::Level::TRACE => self.trace,
                tracing::Level::DEBUG => self.debug,
                tracing::Level::INFO  => self.info,
                tracing::Level::WARN  => self.warn,
                tracing::Level::ERROR => self.error
            }
        );

        Ok(())
    }

    fn reset(&self, out: &mut CursorWrite<'cur>) -> std::io::Result<()>
    {
        out.set_font(FontId::default());
        Ok(())
    }

    fn dull(&self, out: &mut CursorWrite<'cur>) -> std::io::Result<()>
    {
        out.set_font(self.dull);
        Ok(())
    }
}

