use super::*;

pub enum FileFormat
{
    Text { ansi: bool, timestamps: bool },
    Json
}

pub struct FileSink<W>
{
    out:    Mutex<W>,
    format: FileFormat
}

impl<W> Sink for FileSink<W>
    where W: std::io::Write + Sync + Send + 'static
{
    fn on_event(&self, event: &tracing::Event<'_>)
    {
        let mut out = self.out.lock().unwrap();

        let result = match self.format
            {
                FileFormat::Text { ansi: true, timestamps } =>
                    line::Line::new(&mut *out, event)
                        .with_timestamps(timestamps)
                        .with_colours(colours::Ansi)
                        .write(),
                FileFormat::Text { ansi: false, timestamps } =>
                    line::Line::new(&mut *out, event)
                        .with_timestamps(timestamps)
                        .write(),
                FileFormat::Json =>
                    json::Json::new(&mut *out, event).write()
            }
            .and_then(|()| write!(out, "\n"))
            .and_then(|()| out.flush());

        if let Err(_err) = result
        {
            // TODO: Handle inability to log?
        }
    }
}

impl<W> FileSink<W>
{
    pub fn new(out: W, format: FileFormat) -> Self
    {
        Self { out: Mutex::new(out), format }
    }
}

