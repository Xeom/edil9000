// use super::*;

use tracing::{field::Field, Event};
use std::fmt::Debug;

pub(super) struct FieldVisitor<E, Cb>
{
    cb:     Cb,
    result: Result<(), E>
}

impl<E, Cb> FieldVisitor<E, Cb>
    where Cb: FnMut(&Field, &dyn Debug) -> Result<(), E>
{
    pub fn new(cb: Cb) -> Self { Self { cb, result: Ok(()) } }

    pub fn visit(mut self, event: &Event<'_>) -> Result<(), E>
    {
        event.record(&mut self);
        self.result
    }
}

impl<E, Cb> tracing::field::Visit for FieldVisitor<E, Cb>
    where Cb: FnMut(&Field, &dyn Debug) -> Result<(), E>
{
    fn record_debug(&mut self, field: &Field, value: &dyn Debug)
    {
        if self.result.is_ok()
        {
            self.result = (self.cb)(field, value);
        }
    }
}
