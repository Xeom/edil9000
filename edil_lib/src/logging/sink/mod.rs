use super::*;

mod json;
mod line;
mod colours;
mod field_visitor;

mod file_sink;
pub use file_sink::*;

mod buffer_sink;
pub use buffer_sink::*;

pub trait Sink: Sync + Send + 'static
{
    fn on_event(&self, event: &tracing::Event<'_>);
}
