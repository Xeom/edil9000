use super::*;

#[derive(Clone, Default, serde::Deserialize)]
pub struct EofConfig
{
    display: Vec<String>,
    #[serde(skip)]
    cached_display: std::sync::OnceLock<Vec<Vec<Glyph>>>
}

impl EofConfig
{
    pub fn get_display(&self, editor: &Editor) -> &[Vec<Glyph>]
    {
        self.cached_display.get_or_init(
            || self.display.iter().map(|it| Glyph::parse(it, editor)).collect()
        )
            .as_slice()
    }

}
