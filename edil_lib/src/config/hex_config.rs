//use super::*;

#[derive(Clone, Default, serde::Deserialize)]
#[serde(deny_unknown_fields)]
pub struct HexConfig
{
    pre:  String,
    post: String
}

impl HexConfig
{
    pub fn get_display(&self, ch: char) -> String
    {
        let as_u32 = ch as u32;
        let width =
            match as_u32
            {
                0..=0xff       => 2,
                0x100..=0xffff => 4,
                0x10000..      => 8
            };

        format!("{pre}{as_u32:0width$x}{post}", pre=self.pre, post=self.post)
    }
}
