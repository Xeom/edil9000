use super::*;

mod font_config;
use font_config::*;

mod file_type_config;
use file_type_config::*;

mod tab_config;
pub use tab_config::*;

mod hex_config;
pub use hex_config::*;

mod eof_config;
pub use eof_config::*;

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Config
{
    file_types: BTreeMap<String, FileTypeConfig>,
    fonts:      BTreeMap<String, FontConfig>,
    pub tabs:   TabConfig,
    pub hex:    HexConfig,
    pub eof:    EofConfig
}

impl Default for Config
{
    fn default() -> Config
    {
        toml::from_str(DEFAULT_CONFIG).expect("Could not parse default config")
    }
}

impl Config
{
    pub async fn load() -> Config
    {
        let conf_dir = get_config_dir();

        let Ok(loaded) =
            tokio::fs::read_to_string(conf_dir.join("edil.toml")).await
            else
            {
                warn!(
                    "Could not find config file - loading default static config"
                );
                return Config::default();
            };

        match toml::from_str(&loaded)
        {
            Ok(config) => config,
            Err(err) =>
            {
                error!(
                    err = err.message(), span =? err.span(),
                    "Error loading config - loading default static config"
                );
                Config::default()
            }
        }
    }

    fn guess_file_type_from_path(&self, maybe_path: Option<&Path>)
        -> Option<&str>
    {
        let path = maybe_path?;
        self.file_types.iter()
            .find(|(_, v)| v.match_path(path))
            .map(|(k, _)| k.as_ref())
    }

    fn guess_file_type_from_content(&self, content: &[u8]) -> Option<&str>
    {
        self.file_types.iter()
            .find(|(_, v)| v.match_content(content))
            .map(|(k, _)| k.as_ref())
    }

    pub fn guess_file_type(&self, maybe_path: Option<&Path>, content: &[u8])
        -> Option<&str>
    {
        self.guess_file_type_from_path(maybe_path)
            .or_else(|| self.guess_file_type_from_content(content))
    }

    pub async fn get_language(&self, file_type: &str, ctx: &Context)
        -> Option<Arc<tsitter::Language>>
    {
        self.file_types.get(file_type)?.get_language(ctx).await
    }

    pub async fn setup_dotfiles() -> io::Result<()>
    {
        let conf_dir = get_config_dir();
        if !exists(&conf_dir).await
        {
            create_dir(&conf_dir).await?;
            create_file(&conf_dir.join("edil.toml"), DEFAULT_CONFIG.as_bytes())
                .await?;
        }

        let cache_dir = get_cache_dir();
        if !exists(&cache_dir).await
        {
            create_dir(&cache_dir).await?;
            create_targz(&cache_dir.join("tree-sitter"), DEFAULT_TREE_SITTER)
                .await?;
        }

        Ok(())
    }

    async fn find_file(&self, path: &Path) -> PathBuf
    {
        for root in [ get_config_dir(), get_cache_dir() ]
        {
            let candidate = root.join(path);
            if exists(&candidate).await
            {
                return candidate;
            }
        }

        path.to_owned()
    }

    pub fn register_fonts(&self, fonts: &Fonts)
    {
        FontConfig::register(&self.fonts, fonts)
    }
}

async fn exists(path: &Path) -> bool
{
    tokio::fs::metadata(path).await.is_ok()
}

async fn create_targz(path: &Path, contents: &[u8]) -> io::Result<()>
{
    if !exists(path).await
    {
        let tar = async_compression::tokio::bufread::GzipDecoder::new(contents);
        tokio_tar::Archive::new(tar).unpack(path).await?;
    }

    Ok(())
}

async fn create_dir(path: &Path) -> io::Result<()>
{
    info!(?path, "Creating directory for configuration files");
    tokio::fs::create_dir_all(path).await
}

async fn create_file(path: &Path, contents: &[u8])
    -> io::Result<()>
{
    if !tokio::fs::metadata(&path).await.is_ok()
    {
        tokio::fs::write(&path, contents).await?;
        info!(?path, "Created default file");
    }

    Ok(())
}

fn get_home() -> PathBuf
{
    std::env::var_os("HOME")
        .unwrap_or(".".into())
        .into()
}

fn get_cache_dir() -> PathBuf
{
    std::env::var_os("XDG_CACHE_HOME")
        .map(PathBuf::from)
        .unwrap_or(get_home().join(".cache/edil"))
}

fn get_config_dir() -> PathBuf
{
    std::env::var_os("XDG_CONFIG_HOME")
        .map(PathBuf::from)
        .unwrap_or(get_home().join(".config/edil"))
}

const DEFAULT_CONFIG: &'static str =
    include_str!("../../../runtime/edil.toml");

const DEFAULT_TREE_SITTER: &'static [u8] =
    include_bytes!("../../../runtime/tree-sitter.tar.gz");

