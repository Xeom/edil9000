use super::*;

#[derive(Clone, Default, serde::Deserialize)]
#[serde(deny_unknown_fields)]
pub struct TabConfig
{
    display: String,
    #[serde(skip)]
    cached_display: std::sync::OnceLock<Vec<Glyph>>
}

impl TabConfig
{
    pub fn get_display(&self, editor: &Editor) -> &[Glyph]
    {
        self.cached_display.get_or_init(|| Glyph::parse(&self.display, editor))
            .as_slice()
    }
}
