use super::*;

#[derive(serde::Deserialize, Clone, Copy)]
enum ColourConfig
{
    Default,
    Black,
    Red,
    Green,
    Yellow,
    Blue,
    Magenta,
    Cyan,
    White,
    Grey,
    BrightRed,
    BrightGreen,
    BrightYellow,
    BrightBlue,
    BrightMagenta,
    BrightCyan,
    BrightWhite
}

impl ColourConfig
{
    fn to_colour(&self) -> Option<Colour>
    {
        match self
        {
            ColourConfig::Default      =>  None,
            ColourConfig::Black        =>  Some(Colour::black()),
            ColourConfig::Red          =>  Some(Colour::red()),
            ColourConfig::Green        =>  Some(Colour::green()),
            ColourConfig::Yellow       =>  Some(Colour::yellow()),
            ColourConfig::Blue         =>  Some(Colour::blue()),
            ColourConfig::Magenta      =>  Some(Colour::magenta()),
            ColourConfig::Cyan         =>  Some(Colour::cyan()),
            ColourConfig::White        =>  Some(Colour::white()),
            ColourConfig::Grey         =>  Some(Colour::grey()),
            ColourConfig::BrightRed    =>  Some(Colour::bright_red()),
            ColourConfig::BrightGreen  =>  Some(Colour::bright_green()),
            ColourConfig::BrightYellow =>  Some(Colour::bright_yellow()),
            ColourConfig::BrightBlue   =>  Some(Colour::bright_blue()),
            ColourConfig::BrightMagenta=>  Some(Colour::bright_magenta()),
            ColourConfig::BrightCyan   =>  Some(Colour::bright_cyan()),
            ColourConfig::BrightWhite  =>  Some(Colour::bright_white())
        }
    }
}

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
pub(super) struct FontConfig
{
    #[serde(default)] base:   Option<String>,
    #[serde(default)] fg:     Option<ColourConfig>,
    #[serde(default)] bg:     Option<ColourConfig>,
    #[serde(default)] bold:   Option<bool>,
    #[serde(default)] dull:   Option<bool>,
    #[serde(default)] under:  Option<bool>,
    #[serde(default)] blink:  Option<bool>,
    #[serde(default)] invert: Option<bool>,
    #[serde(default)] italic: Option<bool>
}

impl FontConfig
{
    pub fn register(table: &BTreeMap<String, Self>, fonts: &Fonts)
    {
        for (name, conf) in table.iter()
        {
            let mut attrs = FontAttrs::default();
            if let Some(base) = conf.base.as_ref().and_then(|b| table.get(b))
            {
                base.apply(&mut attrs);
            }
            conf.apply(&mut attrs);
            fonts.set_attrs(fonts.get_id(name), &attrs);
        }
    }

    fn apply(&self, attrs: &mut FontAttrs)
    {
        if let Some(bold)   = self.bold   { attrs.set_bold(bold); }
        if let Some(dull)   = self.dull   { attrs.set_dull(dull); }
        if let Some(under)  = self.under  { attrs.set_under(under); }
        if let Some(blink)  = self.blink  { attrs.set_blink(blink); }
        if let Some(invert) = self.invert { attrs.set_invert(invert); }
        if let Some(italic) = self.italic { attrs.set_italic(italic); }
        if let Some(fg)     = self.fg     { attrs.set_fg(fg.to_colour()); }
        if let Some(bg)     = self.bg     { attrs.set_bg(bg.to_colour()); }
    }
}

