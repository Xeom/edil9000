use super::*;

type Regex = regex::bytes::Regex;

#[derive(serde::Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub(super) struct TreeSitterConfig
{
    shared_object:     PathBuf,
    symbol:            String,
    highlight_queries: Vec<PathBuf>,

    #[serde(skip)]
    cached_language: tokio::sync::OnceCell<Arc<tsitter::Language>>
}

impl TreeSitterConfig
{
    async fn load_language(&self, ctx: &Context)
        -> io::Result<Arc<tsitter::Language>>
    {
        let shared_obj = ctx.config().find_file(&self.shared_object).await;

        let mut hl_queries = Vec::new();
        for path in self.highlight_queries.iter()
        {
            hl_queries.push(ctx.config().find_file(path).await);
        }

        tsitter::Language::load(&shared_obj, &self.symbol, &hl_queries, &ctx)
            .await
    }

    pub async fn get_language(&self, ctx: &Context)
        -> io::Result<Arc<tsitter::Language>>
    {
        self.cached_language.get_or_try_init(|| self.load_language(ctx))
            .await
            .cloned()
    }
}

fn deser_regex<'de, D>(deserializer: D) -> Result<Option<Regex>, D::Error>
    where D: serde::Deserializer<'de>
{
    let s = <String as serde::Deserialize>::deserialize(deserializer)?;

    match s.parse()
    {
        Ok(regex) => Ok(Some(regex)),
        Err(e)    => Err(<D::Error as serde::de::Error>::custom(e))
    }
}

#[derive(serde::Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub(super) struct FileTypeConfig
{
    #[serde(default, deserialize_with="deser_regex")]
    match_name: Option<Regex>,

    #[serde(default, deserialize_with="deser_regex")]
    match_content: Option<Regex>,

    #[serde(default)]
    tree_sitter: Option<TreeSitterConfig>
}

impl FileTypeConfig
{
    pub async fn get_language(&self, ctx: &Context)
        -> Option<Arc<tsitter::Language>>
    {
        self.tree_sitter.as_ref()?
            .get_language(ctx).await
            .map_err(|err| error!(?err, "Error loading tree sitter language"))
            .ok()
    }

    pub fn match_path(&self, path: &Path) -> bool
    {
        let Some(file_name) = path.file_name()
            else { return false; };

        self.match_name.as_ref()
            .is_some_and(|r| r.is_match(file_name.as_encoded_bytes()))
    }

    pub fn match_content(&self, content: &[u8]) -> bool
    {
        self.match_content.as_ref().is_some_and(|r| r.is_match(content))
    }
}
