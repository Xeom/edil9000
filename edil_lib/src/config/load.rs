use super::*;

macro_rules! load_file
{
    ($loader:expr, $path:literal) =>
    {
        debug!("Loading default {:?}", $path);
        $loader.add_document(include_str!($path), $path).unwrap();
    }
}

pub fn load_config() -> Config
{
    let fonts = Default::default();

    let mut confiner = confiner::Loader::default();
    load_file!(confiner, "defaults/tabs.conf");
    load_file!(confiner, "defaults/file_types/rust.conf");
    load_file!(confiner, "defaults/file_types/c.conf");
    load_file!(confiner, "defaults/file_types/toml.conf");
    load_file!(confiner, "defaults/file_types/confiner.conf");
    load_file!(confiner, "defaults/fonts.conf");
    load_file!(confiner, "defaults/file_types/common-syntax.conf");

    let data = schema().load(&confiner).unwrap();

    // Register all loaded fonts
    for f in data.fonts.iter()
    {
        f.register(&fonts);
    }

    debug!("Registered {} fonts", data.fonts.len());

    let file_types = data.file_types.iter()
        .map(|ft| FileType::new(ft, &fonts))
        .collect::<Vec<_>>();

    let tabs = data.tabs.last()
        .map(|arc| (**arc).clone())
        .unwrap_or(Default::default());

    let hex = data.hex.last()
        .map(|arc| (**arc).clone())
        .unwrap_or(Default::default());

    let font_cache = FontCache::new(&fonts);

    let eof = data.eof.last()
        .map(|arc| (**arc).clone())
        .unwrap_or(Default::default());

    Config { file_types: file_types.into(), tabs, fonts, eof, font_cache, hex }
}

