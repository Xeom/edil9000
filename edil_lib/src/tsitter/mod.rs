use super::*;

mod language;
pub use language::Language;

mod range_set;
use range_set::RangeSet;

mod painter;
use painter::Painter;

mod text_provider;
use text_provider::*;

use std::ops::Range;

enum State
{
    /// The buffer has been modified, and nothing has been done about it.
    BufferModified,
    /// The tree has been edited to reflect changes to the buffer, but not
    /// parsed.
    TreeEdited,
    /// The tree has been parsed to reflect changes to the buffer.
    Parsed,
    /// The buffer has been fully painted with syntax colours.
    Complete
}

pub struct TreeSitter
{
    tree:        Option<tree_sitter::Tree>,
    parser:      tree_sitter::Parser,
    language:    Arc<Language>,
    to_paint:    RangeSet,
    state:       State,
    revision_id: usize
}

impl TreeSitter
{
    pub fn new(language: Arc<Language>, buf: &Buffer)
        -> Result<Self, tree_sitter::LanguageError>
    {
        let mut parser = tree_sitter::Parser::default();
        parser.set_timeout_micros(200);
        parser.set_language(&language.language)?;

        Ok(
            Self
            {
                tree:         None,
                parser,
                language,
                to_paint:     Default::default(),
                state:        State::TreeEdited,
                revision_id:  buf.revision_id()
            }
        )
    }

    pub fn needs_worker(&self) -> bool
    {
        matches!(self.state, State::TreeEdited | State::Parsed)
    }

    pub fn handle_modify(&mut self, m: &Modification)
    {
        if let Some(ref mut tree) = self.tree
        {
            tree.edit(
                &tree_sitter::InputEdit
                {
                    start_byte:   m.start.utf8_off,
                    old_end_byte: m.end_orig.utf8_off,
                    new_end_byte: m.end_new.utf8_off,
                    start_position:
                        tree_sitter::Point
                        {
                            row:    m.start.coord.row,
                            column: m.start.coord.col
                        },
                    old_end_position:
                        tree_sitter::Point
                        {
                            row:    m.end_orig.coord.row,
                            column: m.end_orig.coord.col
                        },
                    new_end_position:
                        tree_sitter::Point
                        {
                            row:    m.end_new.coord.row,
                            column: m.end_new.coord.col
                        }
                }
            );
        }

        self.to_paint.modify(m);
        self.to_paint.add_range(m.start.utf8_off..m.end_new.utf8_off);

        self.revision_id = m.new_revision_id;
        self.state = State::TreeEdited;
        self.parser.reset();
    }

    pub fn work(&mut self, buf: &Buffer)
    {
        // Lock the buffer
        let _xact = buf.transaction();

        if self.revision_id != buf.revision_id()
        {
            // The buffer has changed but the event handler has not yet been
            // run.
            self.state = State::BufferModified;
        }

        match self.state
        {
            State::TreeEdited => self.do_parse(buf),
            State::Parsed => self.do_repaint(buf),
            _ => {}
        }
    }

    fn update_parsed_tree(&mut self, buf: &Buffer, new_tree: tree_sitter::Tree)
    {
        if let Some(prev_tree) = &self.tree
        {
            // If we had a previous tree highlight any changed regions.
            for change in prev_tree.changed_ranges(&new_tree)
            {
                self.to_paint.add_range(change.start_byte..change.end_byte);
            }
        }
        else
        {
            // If we have a brand new tree, highlight the whole buffer.
            self.to_paint.add_range(
               0..buf.cursor(&EndOfText).location::<Utf8Byte>().0
            )
        }

        self.state = State::Parsed;
        self.tree = Some(new_tree);
    }

    fn do_parse(&mut self, buf: &Buffer)
    {
        if let Some(tree) = self.parser
            .parse_with(&mut TextProvider(buf).callback(), self.tree.as_ref())
        {
            self.update_parsed_tree(buf, tree);
            debug!("Updated tree-sitter parse");
        }
    }

    fn repaint_range(&mut self, buf: &Buffer, range: Range<usize>)
    {
        let node = self.tree.as_ref()
            .expect("Found no tree when doing highlighting")
            .root_node();

        let mut cursor = tree_sitter::QueryCursor::new();
        let mut captures = cursor
            .set_byte_range(range.clone())
            .captures(&self.language.hl_query, node, TextProvider(buf));

        let mut painter = Painter::new(buf, range, &self.language.fonts);

        use streaming_iterator::StreamingIterator;
        while let Some((mtch, _)) = captures.next()
        {
            for capture in mtch.captures
            {
                painter.paint(capture);
            }
        }
    }

    fn do_repaint(&mut self, buf: &Buffer)
    {
        if let Some(range) = self.to_paint.next_range(1024)
        {
            self.repaint_range(buf, range);
        }
        else
        {
            self.state = State::Complete;
            debug!("Completed tree-sitter highlighting");
        }
    }
}

