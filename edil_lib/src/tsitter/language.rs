use super::*;

pub struct Language
{
    pub hl_query: tree_sitter::Query,
    pub language: tree_sitter::Language,
    pub fonts:    Box<[FontId]>
}

fn load_language(path: &Path, symbol: &str) -> io::Result<tree_sitter::Language>
{
    use tree_sitter::ffi::TSLanguage;

    debug!(?path, ?symbol, "Loading tree-sitter shared object...");

    let s_obj = SharedObject::new(path)?;
    let funct = unsafe { s_obj.symbol_function::<*const TSLanguage>(symbol)? };

    let language_ptr = unsafe { funct() };

    Ok(unsafe { tree_sitter::Language::from_raw(language_ptr) })
}

async fn load_query(language: &tree_sitter::Language, paths: &[PathBuf])
    -> io::Result<tree_sitter::Query>
{
    let mut contents = String::new();

    for path in paths
    {
        debug!(?path, "Loading tree-sitter highlights query...");
        contents.push_str(&tokio::fs::read_to_string(path).await?);
    }

    tree_sitter::Query::new(language, &contents)
        .map_err(|_| io::Error::other("Could not parse language query"))
}

impl Language
{
    pub async fn load(
        lang_path:      &Path,
        lang_symbol:    &str,
        hl_query_paths: &[PathBuf],
        ctx:            &Context
    )
        -> io::Result<Arc<Self>>
    {
        debug!(
            ?lang_path, ?lang_symbol, ?hl_query_paths,
            "Loading new tree-sitter language..."
        );

        let language = tokio::task::block_in_place(
            || load_language(&lang_path, &lang_symbol)
        )?;

        let hl_query = load_query(&language, hl_query_paths).await?;

        let fonts = hl_query.capture_names().iter()
            .map(|name| ctx.font_id(&format!("syntax.{name}")))
            .collect();

        Ok(Arc::new(Self { hl_query, language, fonts }))
    }
}
