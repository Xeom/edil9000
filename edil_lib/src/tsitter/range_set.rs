use super::*;

#[derive(Default)]
pub(super) struct RangeSet(Vec<Range<usize>>);

fn overlaps<T: PartialOrd>(a: &Range<T>, b: &Range<T>) -> bool
{
    a.start <= b.end && b.start <= a.end
}

impl RangeSet
{
    pub fn add_range(&mut self, mut to_add: Range<usize>)
    {
        assert!(to_add.start < to_add.end);
        self.0.retain(
            |r| if overlaps(r, &to_add)
            {
                to_add.start = std::cmp::min(r.start, to_add.start);
                to_add.end   = std::cmp::max(r.end,   to_add.end);
                false
            }
            else
            {
                true
            }
        );

        self.0.push(to_add);
    }

    pub fn modify(&mut self, m: &Modification)
    {
        let modify = |off: &mut usize|
            if *off > m.end_orig.utf8_off
            {
                *off += m.end_new.utf8_off - m.end_orig.utf8_off;
            }
            else if *off > m.start.utf8_off
            {
                *off = m.start.utf8_off;
            };

        for it in self.0.iter_mut()
        {
            modify(&mut it.start);
            modify(&mut it.end);
        }
    }

    pub fn next_range(&mut self, max_len: usize) -> Option<Range<usize>>
    {
        let range = self.0.last_mut()?;
        if range.end <= max_len + range.start
        {
            self.0.pop()
        }
        else
        {
            let result = range.start..(range.start + max_len);
            range.start = result.end;

            Some(result)
        }
    }
}
