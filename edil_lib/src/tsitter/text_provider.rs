
use super::*;

pub struct TextProvider<'a>(pub &'a Buffer);

impl<'a> tree_sitter::TextProvider<Vec<u8>> for TextProvider<'a>
{
    // TODO: Consider making a proper iterator type?
    type I = std::iter::Once<Vec<u8>>;
    fn text(&mut self, node: tree_sitter::Node<'_>) -> Self::I
    {
        let start = self.0.cursor(&Utf8Byte(node.start_byte()));
        let end   = self.0.cursor(&Utf8Byte(node.end_byte()));

        std::iter::once(start.read().until(&end).to_bytes())
    }
}

impl<'a> TextProvider<'a>
{
    pub fn callback(self) ->
        impl FnMut(usize, tree_sitter::Point) -> Vec<u8> + 'a
    {
        |_, tree_sitter::Point { row, column }|
        {
            let start = self.0.cursor(&Coord { row, col: column });
            start.read().until(&start.moved(256)).to_bytes()
        }
    }
}
