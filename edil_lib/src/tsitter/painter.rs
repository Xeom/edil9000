use super::*;

pub(super) struct Painter<'a>
{
    fonts:  &'a [FontId],
    range:  Range<usize>,
    cursor: Cursor
}

impl<'a> Drop for Painter<'a>
{
    fn drop(&mut self)
    {
        self.paint_until(self.range.end, FontId::default());
    }
}

impl<'a> Painter<'a>
{
    pub fn new(buf: &Buffer, range: Range<usize>, fonts: &'a [FontId]) -> Self
    {
        let cursor = buf.cursor(&Utf8Byte(range.start));
        Self { fonts, range, cursor }
    }

    pub fn paint(&mut self, capture: &tree_sitter::QueryCapture)
    {
        let font_id = self.fonts[capture.index as usize];
        self.paint_until(capture.node.start_byte(), FontId::default());
        self.paint_until(capture.node.end_byte(), font_id);
    }

    fn paint_until(&mut self, new_byte: usize, font: FontId)
    {
        let curr_byte = self.cursor.location::<Utf8Byte>().0;
        if new_byte > curr_byte
        {
            self.cursor.set_font(font, new_byte - curr_byte);
            self.cursor.move_by((new_byte - curr_byte) as i64);
        }
    }
}

