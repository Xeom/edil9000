use super::*;

use tokio::{
    sync::mpsc,
    task::{JoinSet, JoinError}
};

mod worker;
use worker::{Worker, Command, Async, Blocking};

pub(crate) struct Manager
{
    num_tasks:  Arc<Variable<usize>>,
    command_tx: mpsc::UnboundedSender<Command>
}

impl Manager
{
    pub fn new(editor: &Arc<Editor>) -> Self
    {
        let (command_tx, command_rx) = mpsc::unbounded_channel();
        let num_tasks = Arc::new(Variable::new(0_usize));
        let worker = Worker::new(num_tasks.clone(), editor.clone(), command_rx);
        let manager = Manager { num_tasks, command_tx };

        editor.tokio_rt.spawn(worker.run());

        manager
    }

    pub fn num_tasks(&self) -> &Variable<usize> { &self.num_tasks }

    pub fn spawn_blocking(&self, task: impl FnOnce() + Send + 'static)
    {
        self.command_tx.send(Command::Spawn(Box::new(Blocking(task)))).ok();
    }

    pub fn spawn(&self, task: impl Future<Output=()> + Send + 'static)
    {
        self.command_tx.send(Command::Spawn(Box::new(Async(task)))).ok();
    }

    pub fn shutdown(&self)
    {
        self.command_tx.send(Command::Shutdown).ok();
    }
}
