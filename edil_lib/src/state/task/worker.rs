use super::*;

pub(super) enum Command
{
    Spawn(Box<dyn Spawn>),
    Shutdown
}

pub(super) trait Spawn: Send
{
    fn spawn(self: Box<Self>, set: &mut JoinSet<()>);
}

pub(super) struct Blocking<F>(pub F);
impl<F> Spawn for Blocking<F>
    where F: FnOnce() -> () + Send + 'static
{
    fn spawn(self: Box<Self>, set: &mut JoinSet<()>)
    {
        set.spawn_blocking(self.0);
    }
}

pub(super) struct Async<F>(pub F);
impl<F> Spawn for Async<F>
    where F: Future<Output=()> + Send + 'static
{
    fn spawn(self: Box<Self>, set: &mut JoinSet<()>)
    {
        set.spawn(self.0);
    }
}

enum State
{
    Running,
    Aborting,
    Done
}

pub(super) struct Worker
{
    num_tasks:  Arc<Variable<usize>>,
    editor:     Arc<Editor>,
    command_rx: mpsc::UnboundedReceiver<Command>,
    state:      State
}

async fn await_join(set: &mut JoinSet<()>) -> Result<(), JoinError>
{
    if let Some(rtn) = set.join_next().await
    {
        rtn
    }
    else
    {
        std::future::pending().await
    }
}

impl Worker
{
    pub fn new(
        num_tasks:  Arc<Variable<usize>>,
        editor:     Arc<Editor>,
        command_rx: mpsc::UnboundedReceiver<Command>
    )
        -> Self
    {
        Self { num_tasks, editor, command_rx, state: State::Running }
    }

    fn update_tasks(&mut self, tasks: &JoinSet<()>)
    {
        self.num_tasks.set(tasks.len());
        if matches!(self.state, State::Aborting) && tasks.len() == 0
        {
            self.state = State::Done;
        }
    }

    async fn await_command(&mut self) -> Command
    {
        match self.state
        {
            State::Running =>
                self.command_rx.recv().await.unwrap_or(Command::Shutdown),
            State::Aborting | State::Done =>
                std::future::pending().await
        }
    }

    pub async fn run(mut self)
    {
        let mut tasks = JoinSet::new();

        task_loop!
        {
            (self.editor);
            cmd = self.await_command() =>
            {
                match cmd
                {
                    Command::Spawn(to_spawn) =>
                        to_spawn.spawn(&mut tasks),
                    Command::Shutdown =>
                    {
                        tasks.abort_all();
                        self.state = State::Aborting;
                    }
                }
                self.update_tasks(&tasks);
                if matches!(self.state, State::Done)
                {
                    return;
                }
            }
            _ = await_join(&mut tasks) =>
            {
                self.update_tasks(&tasks);
                if matches!(self.state, State::Done)
                {
                    return;
                }
            }
        }
    }
}
