use super::*;

pub struct Question
{
    prompt:    String,
    on_answer: Event<String>,
    on_cancel: Event<()>,
    handlers:  Vec<Handler>
}

pub struct Oracle
{
    mode: Arc<Variable<Mode>>,
    queue: Mutex<VecDeque<Question>>
}

impl Question
{
    pub fn new(prompt: impl Into<String>) -> Self
    {
        Question
        {
            prompt:    prompt.into(),
            on_answer: Event::new_sync(),
            on_cancel: Event::new_sync(),
            handlers:  Default::default()
        }
    }

    fn with_handler(mut self, handler: Handler) -> Self
    {
        self.handlers.push(handler);
        self
    }

    pub fn with_answer_handler(
        self,
        exec: &Executor,
        cb:   impl FnMut(&String) + Send + 'static
    )
        -> Self
    {
        let handler = Handler::builder(exec)
            .with_event(&self.on_answer)
            .build(cb);

        self.with_handler(handler)
    }

    #[allow(dead_code)] // TODO: Use this somewhere, or remove :)
    pub fn with_cancel_handler(
        self,
        exec: &Executor,
        cb:   impl FnMut(&()) + Send + 'static
    )
        -> Self
    {
        let handler = Handler::builder(exec)
            .with_event(&self.on_cancel)
            .build(cb);

        self.with_handler(handler)
    }
}

impl Oracle
{
    pub fn new(mode: Arc<Variable<Mode>>) -> Self
    {
        Self { mode, queue: Default::default() }
    }

    pub fn ask(&self, question: Question)
    {
        self.queue.lock().unwrap().push_back(question);
        self.mode.set(Mode::Prompt);
    }

    pub fn handle_answer(&self, ans: String)
    {
        if let Some(question) = self.next_question()
        {
            question.on_answer.trigger(&ans);
        }
    }

    pub fn handle_cancel(&self)
    {
        if let Some(question) = self.next_question()
        {
            question.on_cancel.trigger(&());
        }
    }

    pub fn get_prompt(&self) -> Option<String>
    {
        let queue = self.queue.lock().unwrap();
        queue.front()
            .map(|question| question.prompt.clone())
    }

    fn next_question(&self) -> Option<Question>
    {
        let mut queue = self.queue.lock().unwrap();
        if queue.len() <= 1
        {
            self.mode.set(Mode::Read);
        }
        queue.pop_front()
    }
}
