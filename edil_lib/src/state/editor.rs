use super::*;

pub struct Editor
{
    pub(crate) exec:      Arc<Executor>,

    pub(crate) tokio_rt:  tokio::runtime::Handle,

    pub(crate) fonts:     Fonts,

    pub(crate) config:    Arc<config::Config>,
    pub(crate) windows:   Arc<window::Manager>,

    pub(crate) running:   Arc<Variable<bool>>,
    pub(crate) mode:      Arc<Variable<Mode>>,

    pub(crate) oracle:    Arc<Oracle>,
}

impl Editor
{
    pub fn new(display_size: Pos<u16>, config: Config, fonts: Fonts)
        -> Arc<Self>
    {
        let config  = Arc::new(config);
        let exec    = Arc::new(Executor::default());
        debug!("Created executor");

        let running = Arc::new(Variable::new(true));
        let mode    = Arc::new(Variable::new(Mode::Read));
        let oracle  = Arc::new(Oracle::new(mode.clone()));
        let windows = window::Manager::new(exec.clone(), display_size, &mode);

        config.register_fonts(&fonts);

        debug!("Created window manager");

        let editor = Arc::new(
            Editor
            {
                exec,
                tokio_rt: tokio::runtime::Handle::current(),
                fonts,
                config,
                windows,
                running,
                mode,
                oracle
            }
        );

        debug!("Created editor");

        editor.windows.open(
            &editor,
            content::PromptParams,
            window::OpenAs::Prompt
        );
        editor.windows.open(
            &editor,
            content::NotificationsParams,
            window::OpenAs::Notifications
        );

        debug!("Created default windows");

        editor
    }

    pub fn stop(&self)
    {
        info!("Stopping editor");
        self.running.set(false);
        self.windows.close_all();
    }

    /// Disble the notification window at the bottom of the screen
    pub fn disable_notifications(&self)
    {
        self.windows.set_notifications(Some(false));
    }

    pub fn open_pipe(self: &Arc<Self>, fd: &impl AsRawFd) -> io::Result<()>
    {
        info!("Opening pipe");
        let dupped = nix::unistd::dup(fd.as_raw_fd())?;
        let file = NonblockFile::from(unsafe { StdFile::from_raw_fd(dupped) });

        let (_, content) = self.windows.open(
            &self,
            content::FileParams(None),
            window::OpenAs::Frame
        );
        content.inner().load(file, 0);

        Ok(())
    }

    pub async fn open_path(
        self: Arc<Self>,
        path: impl Into<PathBuf>,
        row:  usize
    )
        -> io::Result<()>
    {
        let path = path.into();

        debug!(?path, ?row, "Opening path...");

        // If the path is a directory
        if tokio::fs::metadata(&path).await?.is_dir()
        {
            info!(?path, "Opening directory...");
            let (_, content) = self.windows.open(
                &self,
                content::DirParams(path),
                window::OpenAs::Frame
            );
            content.inner().load();
        }
        // If the path is a normal file
        else
        {
            info!(?path, "Opening file...");
            let file = tokio::fs::File::open(&path).await?;
            let (_, content) = self.windows.open(
                &self,
                content::FileParams(Some(path)),
                window::OpenAs::Frame
            );
            content.inner().load(file, row);
        }

        Ok(())
    }

    pub fn open_grep<A, I>(
        self: &Arc<Self>,
        pattern: &str,
        path: impl AsRef<Path>,
        args: I
    )
        where
            I: IntoIterator<Item=A>,
            A: AsRef<OsStr>
    {
        let mut cmd = process::Command::new("grep");
        cmd
            .arg("-nHIRZ")
            .arg("--color=never")
            .args(args)
            .arg("--")
            .arg(pattern)
            .arg(path.as_ref());

        let (_, content) =
            self.windows.open(self, content::GrepParams, window::OpenAs::Frame);
        content.inner().load(cmd);
    }

    pub fn open_window<P>(self: &Arc<Self>, content_params: P)
        where P: content::Params
    {
        self.windows.open(self, content_params, window::OpenAs::Frame);
    }
}

impl Drop for Editor
{
    fn drop(&mut self) { info!("Destroyed editor"); }
}

#[api_macros::plugin_api]
fn open_path(path: &str, ctx: &Context)
{
    open_path_at(path, &0, ctx);
}

#[api_macros::plugin_api]
fn open_path_at(path: &str, row: &usize, ctx: &Context)
{
    if let Err(e) = ctx.block_on(ctx.editor().open_path(path, *row))
    {
        error!(?path, ?e, "Error opening path");
    }
}

#[api_macros::plugin_api]
fn get_mode(ctx: &Context) -> String
{
    match ctx.editor().mode.get()
    {
        Mode::Read   => "Read",
        Mode::Window => "Window",
        Mode::Prompt => "Prompt",
        Mode::Buffer => "Buffer"
    }
        .to_owned()
}
