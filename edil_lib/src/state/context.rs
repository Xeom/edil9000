use super::*;

pub struct Context<T: ?Sized = dyn std::any::Any + Sync + Send + 'static>
{
    this:        Weak<T>,
    editor:      Arc<Editor>,
    content_id:  window::ContentId,
    tasks:       Arc<task::Manager>
}

// Manually implement Clone to avoid T:Clone
impl<T: ?Sized> Clone for Context<T>
{
    fn clone(&self) -> Self
    {
        Self
        {
            this:       self.this.clone(),
            editor:     self.editor.clone(),
            content_id: self.content_id,
            tasks:      self.tasks.clone()
        }
    }
}

impl<T: ?Sized> Context<T>
{
    pub(crate) fn new(
        this:       &Weak<T>,
        editor:     &Arc<Editor>,
        content_id: window::ContentId,
        tasks:      Arc<task::Manager>
    )
        -> Self
    {
        Self
        {
            this: this.clone(),
            editor: editor.clone(),
            content_id,
            tasks
        }
    }

    pub(crate) fn try_get_this(&self) -> Option<Arc<T>> { self.this.upgrade() }

    pub(crate) fn get_this(&self) -> Arc<T> { self.this.upgrade().unwrap() }

    pub(crate) fn config(&self) -> &'_ Config { &self.editor.config }

    pub(crate) fn content_id(&self) -> window::ContentId { self.content_id }

    pub(crate) fn editor(&self) -> Arc<Editor> { self.editor.clone() }

    pub(crate) fn windows(&self) -> &'_ window::Manager { &self.editor.windows }

    pub(crate) fn exec(&self) -> &'_ Executor { &self.editor.exec }

    pub(crate) fn font_id(&self, name: &str) -> FontId { self.fonts().get_id(name) }

    pub(crate) fn fonts(&self) -> &'_ Fonts { &self.editor.fonts }

    pub(crate) fn tasks(&self) -> &task::Manager { &self.tasks }

    pub(crate) fn block_on<R>(&self, task: impl Future<Output=R>) -> R
    {
        self.editor.tokio_rt.block_on(task)
    }

    pub(crate) fn spawn_blocking(&self, task: impl FnOnce() + Send + 'static)
    {
        self.tasks.spawn_blocking(task);
    }

    pub(crate) fn spawn(&self, task: impl Future<Output=()> + Send + 'static)
    {
        self.tasks.spawn(task)
    }

    pub(crate) fn spawn_falliable<E>(
        &self,
        task: impl Future<Output=Result<(), E>> + Send + 'static
    )
        where E: std::error::Error
    {
        self.tasks.spawn(
            async move
            {
                if let Err(err) = task.await
                {
                    error!(?err, "Error executing task");
                }
            }
        )
    }

    pub(crate) fn as_any(&self) -> Context
        where T: Sized + Sync + Send + 'static
    {
        Context
        {
            this:       self.this.clone(),
            editor:     self.editor.clone(),
            content_id: self.content_id,
            tasks:      self.tasks.clone()
        }
    }

    pub(crate) fn make_callback<P>(
        &self,
        mut cb: impl FnMut(Arc<T>, &Self, &P) + Send + 'static
    )
        -> impl FnMut(&P) + Send + 'static
        where T: Send + Sync + 'static
    {
        let ctx = self.clone();
        move |x| if let Some(this) = ctx.try_get_this()
        {
            cb(this, &ctx, x)
        }
    }

    pub(crate) fn ask(
        &self,
        prompt: impl Into<String>,
        cb:     impl FnMut(Arc<T>, &Self, &String) + Send + 'static
    )
        where T: Send + Sync + 'static
    {
        self.editor.oracle.ask(
            Question::new(prompt)
                .with_answer_handler(self.exec(), self.make_callback(cb))
        );
    }
}

impl Context
{
    pub(crate) fn get_as<T>(&self) -> Option<Arc<T>>
        where T: Sync + Send + 'static
    {
        self.try_get_this()?.downcast::<T>().ok()
    }
}

#[api_macros::plugin_api]
fn get_content_id(context: &Context) -> u64 { context.content_id().into() }
