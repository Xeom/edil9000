use super::*;

use std::task::{Context, Poll};

use nix::{
    sys::termios::{self, Termios, SetArg::*},
};

pub struct Handle
{
    inner:        NonblockFile,
    termios_orig: Termios
}

unsafe impl Sync for Handle {}

pub struct WriteHandle(Arc<Handle>);
pub struct ReadHandle(Arc<Handle>);

fn rawify(fd: &impl AsFd) -> Termios
{
    let orig = termios::tcgetattr(fd).expect("Could not get termios");

    let mut ios = orig.clone();
    termios::cfmakeraw(&mut ios);
    termios::tcsetattr(fd, TCSANOW, &ios)
        .expect("Could not set termios");

    orig
}

impl From<StdFile> for Handle
{
    fn from(std_file: StdFile) -> Self
    {
        let termios_orig = rawify(&std_file);
        let inner = std_file.into();

        Self { inner, termios_orig }
    }
}

impl Handle
{
    pub fn split(self) -> (ReadHandle, WriteHandle)
    {
        let arc = Arc::new(self);
        (ReadHandle(arc.clone()), WriteHandle(arc))
    }
}

impl AsRawFd for ReadHandle
{
    fn as_raw_fd(&self) -> RawFd { self.0.inner.as_raw_fd() }
}

impl AsyncRead for ReadHandle
{
    fn poll_read(
        self: Pin<&mut Self>,
        ctx:  &mut Context<'_>,
        buf:  &mut io::ReadBuf<'_>
    )
        -> Poll<io::Result<()>>
    {
        Pin::new(&self.0.inner).poll_read_inner(ctx, buf)
    }
}

impl AsyncWrite for WriteHandle
{
    fn poll_write(
        self: Pin<&mut Self>,
        ctx:  &mut Context<'_>,
        buf:  &[u8]
    )
        -> Poll<io::Result<usize>>
    {
        Pin::new(&self.0.inner).poll_write_inner(ctx, buf)
    }

    fn poll_flush(self: Pin<&mut Self>, _ctx: &mut Context<'_>)
        -> Poll<io::Result<()>>
    {
        Poll::Ready(Ok(()))
    }

    fn poll_shutdown(self: Pin<&mut Self>, _ctx: &mut Context<'_>)
        -> Poll<io::Result<()>>
    {
        Poll::Ready(Ok(()))
    }
}

impl Drop for Handle
{
    fn drop(&mut self)
    {
        termios::tcsetattr(&self.inner, TCSANOW, &self.termios_orig).ok();
    }
}
