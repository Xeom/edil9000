use super::*;

#[derive(Clone)]
pub struct Vt220<C=Col16>(PhantomData<C>);

pub const fn vt220<C>() -> Vt220<C> { Vt220(PhantomData) }

macro_rules! emit
{
    ($out:expr, $($f:expr),*) =>
    {
        $out.write_all(format!($($f),*).as_bytes()).await
    }
}

macro_rules! tty
{
    (ESC $($rem:tt)* )           => { tty!("\x1b" $($rem)*) };
    (CSI $($rem:tt)* )           => { tty!(ESC "[" $($rem)*) };
    (SET($n:expr) $($rem:tt)*)   => { tty!(CSI "?" $n "h" $($rem)*) };
    (RESET($n:expr) $($rem:tt)*) => { tty!(CSI "?" $n "l" $($rem)*) };
    ($oth:tt $($rem:tt)*)        => { concat!($oth, tty!($($rem)*)) };
    ()                           => { "" }
}

impl TtyInput for Vt220
{
    fn keys(&self) -> BTreeMap<&'static [u8], Key>
    {
        [
            (tty!(ESC),      key!(Escape)),
            (tty!("\x7f"),   key!(Backspace)),
            (tty!("\r"),     key!(Enter)),
            (tty!("\n"),     key!(Enter)),
            (tty!(CSI "A"),  key!(Up)),
            (tty!(CSI "B"),  key!(Down)),
            (tty!(CSI "C"),  key!(Right)),
            (tty!(CSI "D"),  key!(Left)),
            (tty!(CSI "2~"), key!(Insert)),
            (tty!(CSI "3~"), key!(Delete)),
            (tty!(CSI "5~"), key!(PageUp)),
            (tty!(CSI "6~"), key!(PageDown))
        ]
            .into_iter()
            .map(|(k, v)| (k.as_bytes(), v))
            .collect()
    }
}

#[async_trait]
impl<O, C> TtyOutput<O> for Vt220<C>
    where
        O: AsyncWrite + Unpin + Send,
        C: Palette + Send + Sync
{
    async fn set_cursor_pos(&mut self, p: Pos<u16>, out: &mut O)
        -> io::Result<()>
    {
        emit!(out, tty!(CSI "{};{}H"), p.row + 1, p.col + 1)
    }

    async fn set_font(&mut self, f: FontAttrs, out: &mut O) -> io::Result<()>
    {
        let mut codes = vec![0];

        if let Some(colour) = f.fg()
        {
            codes.append(&mut C::encode(colour, false))
        }

        if let Some(colour) = f.bg()
        {
            codes.append(&mut C::encode(colour, true))
        }

        if f.bold()   { codes.push(1); }
        if f.dull()   { codes.push(2); }
        if f.italic() { codes.push(3); }
        if f.under()  { codes.push(4); }
        if f.blink()  { codes.push(5); }
        if f.invert() { codes.push(7); }

        emit!(
            out,
            tty!(CSI "{}m"),
            codes.iter().map(u8::to_string).collect::<Vec<_>>().join(";")
        )
    }

    async fn set_char(&mut self, ch: char, out: &mut O) -> io::Result<()>
    {
        emit!(out, "{ch}")
    }

    async fn begin(&mut self, out: &mut O) -> io::Result<()>
    {
        emit!(out, tty!(
            ESC 7     // Save cursor position
            SET(1049) // Use alternate screen
            RESET(25) // Hide the cursor
        ))
    }

    async fn clear(&mut self, out: &mut O) -> io::Result<()>
    {
        emit!(out, tty!(
            CSI "0m" // Reset font
            CSI "2J" // Clear the screen
        ))
    }

    async fn end(&mut self, out: &mut O) -> io::Result<()>
    {
        emit!(out, tty!(
            RESET(1049) // Use primary screen
            ESC 8       // Restore cursor position
            SET(25)     // Show the cursor
        ))
    }
}
