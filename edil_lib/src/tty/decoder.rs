use super::*;

pub struct Decoder<I>
{
    input:   I,
    buffer:  Vec<u8>,
    keys:    BTreeMap<&'static [u8], Key>
}

#[derive(Debug)]
enum Decoded<T>
{
    No,
    Maybe,
    YesButWait(T),
    Yes(T)
}

impl<T> Decoded<T>
{
    fn map(self, f: impl FnOnce(T) -> T) -> Self
    {
        match self
        {
            Self::No            => Self::No,
            Self::Maybe         => Self::Maybe,
            Self::YesButWait(v) => Self::YesButWait(f(v)),
            Self::Yes(v)        => Self::Yes(f(v))
        }
    }

    fn combine(self, oth: Self) -> Self
    {
        match (self, oth)
        {
            (Self::No, x) | (x, Self::No)           => x,
            (Self::Maybe, Self::Maybe)              => Self::Maybe,
            (Self::YesButWait(x) | Self::Yes(x), _) => Self::YesButWait(x),
            (_, Self::YesButWait(x) | Self::Yes(x)) => Self::YesButWait(x),
        }
    }
}

fn match_utf8_keys(buf: &[u8]) -> Decoded<Key>
{
    match std::str::from_utf8(buf)
    {
        Ok(s) => match s.chars().next()
        {
            // We decoded a UTF-8 character
            Some(ch) if s.chars().count() == 1 =>
                // Handle CTRL characters
                if ch.is_control()
                {
                    if let Some(new_ch) = char::from_u32(ch as u32 + 64)
                    {
                        Decoded::Yes(Key { ctrl: true, ..Key::from(new_ch) })
                    }
                    else
                    {
                        Decoded::No
                    }
                }
                // Handle normal characters
                else
                {
                    Decoded::Yes(Key::from(ch))
                }
            _ =>
                // Did not contain a single character
                Decoded::No
        }
        // If error_len() is not set, we have an incomplete UTF-8 codepoint.
        Err(e) if e.error_len().is_none() =>
            Decoded::Maybe,
        // If error_len() is set, we have a non-UTF-8 sequence.
        Err(_) =>
            Decoded::No
    }
}

fn match_special_keys(buf: &[u8], keys: &BTreeMap<&[u8], Key>) -> Decoded<Key>
{
    let mut matching = keys.range(buf..)
        .take_while(|kv| kv.0.starts_with(buf));

    match matching.next()
    {
        // A perfect match, but there are possibly other matches
        Some(kv) if kv.0 == &buf && matching.next().is_some() =>
            Decoded::YesButWait(kv.1.clone()),
        // A perfect match and there are no other possible matches
        Some(kv) if kv.0 == &buf =>
            Decoded::Yes(kv.1.clone()),
        // No perfect match, but a possible match
        Some(kv) if kv.0.starts_with(buf) =>
            Decoded::Maybe,
        // No possible match
        _ =>
            Decoded::No
    }
}

fn match_alt_keys(buf: &[u8], keys: &BTreeMap<&'static [u8], Key>)
    -> Decoded<Key>
{
    if buf.get(0) == Some(&b'\x1b')
    {
        match_special_keys(buf, keys)
            .combine(match_utf8_keys(&buf[1..]))
            .map(|k| Key { alt: true, ..k })
    }
    else
    {
        Decoded::No
    }
}

fn decode_buf(buf: &[u8], keys: &BTreeMap<&'static [u8], Key>)
    -> Decoded<Key>
{
    if buf.len() == 0 { return Decoded::Maybe; }

    match_special_keys(buf, keys)
        .combine(match_alt_keys(buf, keys))
        .combine(match_utf8_keys(buf))
}

impl<I> Decoder<I>
    where I: AsyncRead + Unpin
{
    pub fn new(input: I, keys: BTreeMap<&'static [u8], Key>) -> Self
    {
        Self { input, buffer: Vec::new(), keys }
    }

    pub fn inner(&self) -> &I { &self.input }

    async fn read(&mut self) -> io::Result<()>
    {
        if self.input.read_buf(&mut self.buffer).await? == 0
        {
            Err(io::Error::new(io::ErrorKind::UnexpectedEof, "TTY reached EOF"))
        }
        else
        {
            Ok(())
        }
    }

    fn consume(&mut self, n: usize)
    {
        self.buffer.drain(..n);
    }

    async fn try_decode(&mut self, n: usize, timeout: bool)
        -> io::Result<Decoded<Key>>
    {
        while self.buffer.len() < n
        {
            if timeout
            {
                let dur = tokio::time::Duration::from_millis(20);
                match tokio::time::timeout(dur, self.read()).await
                {
                    Ok(res) => res?,
                    Err(_)  => return Ok(Decoded::No)
                }
            }
            else
            {
                self.read().await?;
            }
        }

        Ok(decode_buf(&self.buffer[..n], &self.keys))
    }

    pub async fn next(&mut self) -> io::Result<Key>
    {
        loop
        {
            let mut found: Option<(Key, usize)> = None;
            'decode: for n in 1..
            {
                match self.try_decode(n, found.is_some()).await?
                {
                    Decoded::No              => break 'decode,
                    Decoded::Maybe           => {}
                    Decoded::YesButWait(evt) => found = Some((evt, n)),
                    Decoded::Yes(evt) =>
                    {
                        found = Some((evt, n));
                        break 'decode;
                    }
                }
            }

            if let Some((evt, n)) = found
            {
                self.consume(n);
                return Ok(evt);
            }
            else
            {
                self.consume(1)
            }
        }
    }
}

#[cfg(test)]
mod test
{
    use super::*;
    use test_harness::PtyHarness;

    fn setup() -> (PtyHarness, Decoder<ReadHandle>)
    {
        let (pty_harness, mut pty_client) = test_harness::make_pty();

        let input   = Handle::from(pty_client.file()).split().0;
        let decoder = Decoder::new(input, vt220().keys());

        (pty_harness, decoder)
    }

    #[tokio::test]
    async fn decode()
    {
        let (mut harness, mut decoder) = setup();

        harness.type_bytes("xyz");
        assert_eq!(decoder.next().await.unwrap(), key!('x'));
        assert_eq!(decoder.next().await.unwrap(), key!('y'));
        assert_eq!(decoder.next().await.unwrap(), key!('z'));
    }

    #[tokio::test]
    async fn decode_all_keys()
    {
        let (mut harness, mut decoder) = setup();

        for (input, expect) in vt220().keys()
        {
            harness.type_bytes(input);
            assert_eq!(decoder.next().await.unwrap(), expect);
        }
    }

    #[tokio::test]
    async fn survives_fuzzing_slow()
    {
        let (mut harness, mut decoder) = setup();

        let bg = tokio::task::spawn_blocking(move || {
            for _ in 0..1024
            {
                let bytes = (0..1024)
                    .map(|_| rand::random::<u8>())
                    .map(|b| if b == b'x' { b'y' } else { b })
                   .collect::<Vec<_>>();

                harness.type_bytes(&bytes);
            }
            harness.type_bytes(b"xxxxx");
            harness
        });

        loop
        {
            match decoder.next().await.unwrap()
            {
                key!('x') => break,
                _ => {}
            }
        }

        bg.await.unwrap();
    }

    #[tokio::test]
    async fn decode_alt()
    {
        let (mut harness, mut decoder) = setup();

        harness.type_bytes("\x1bx\x1by\x1bz");
        assert_eq!(decoder.next().await.unwrap(), key!(ALT, 'x'));
        assert_eq!(decoder.next().await.unwrap(), key!(ALT, 'y'));
        assert_eq!(decoder.next().await.unwrap(), key!(ALT, 'z'));
    }

    #[tokio::test]
    async fn decode_ctrl()
    {
        let (mut harness, mut decoder) = setup();

        harness.type_bytes("\x18\x19\x1a");
        assert_eq!(decoder.next().await.unwrap(), key!(CTRL, 'X'));
        assert_eq!(decoder.next().await.unwrap(), key!(CTRL, 'Y'));
        assert_eq!(decoder.next().await.unwrap(), key!(CTRL, 'Z'));
    }

    #[tokio::test]
    async fn decode_ctrl_alt()
    {
        let (mut harness, mut decoder) = setup();

        harness.type_bytes("\x1b\x18\x1b\x19\x1b\x1a");
        assert_eq!(decoder.next().await.unwrap(), key!(CTRL, ALT, 'X'));
        assert_eq!(decoder.next().await.unwrap(), key!(CTRL, ALT, 'Y'));
        assert_eq!(decoder.next().await.unwrap(), key!(CTRL, ALT, 'Z'));
    }
}
