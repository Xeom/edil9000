use super::*;

pub struct InputWrapper<I>
{
    winch:   Signal,
    decoder: Decoder<I>,
}

pub trait TtyInput: Sync + Send
{
    fn keys(&self) -> BTreeMap<&'static [u8], Key>;
}

use nix::{libc::TIOCGWINSZ, pty::Winsize};
nix::ioctl_read_bad!(get_win_size_ptr, TIOCGWINSZ, Winsize);

fn get_win_size(file: &impl AsRawFd) -> Pos<u16>
{
    let mut winsz = std::mem::MaybeUninit::<Winsize>::uninit();
    match unsafe { get_win_size_ptr(file.as_raw_fd(), winsz.as_mut_ptr()) }
    {
        Ok(_) =>
        {
            let winsz = unsafe { winsz.assume_init() };
            Pos { row: winsz.ws_row, col: winsz.ws_col }
        }
        Err(_) =>
        {
            Pos { row: 30, col: 80 }
        }
    }
}

impl<I> InputWrapper<I>
    where I: AsyncRead + Unpin
{
    pub fn new<T>(input: I, tty: T) -> Self
        where T: TtyInput
    {
        Self
        {
            winch: signal(SignalKind::window_change())
                .expect("Could not listen for SIGWINCH"),
            decoder: Decoder::new(input, tty.keys())
        }
    }
}

impl<I> input::UserInput for InputWrapper<I>
    where I: AsyncRead + AsRawFd + Unpin + Sync + Send
{
    fn win_size(&self) -> Pos<u16> { get_win_size(self.decoder.inner()) }

    async fn next_event(&mut self) -> UserEvent
    {
        tokio::select!
        {
            evt = self.decoder.next() =>
                UserEvent::Key(evt.expect("Error getting event from TTY")),
            Some(_) = self.winch.recv() =>
                UserEvent::Resize
        }
    }
}

#[cfg(test)]
mod test
{
    use super::*;
    use test_harness::PtyHarness;

    fn setup() -> (PtyHarness, InputWrapper<ReadHandle>)
    {
        let (harness, mut term) = test_harness::make_pty();
        let input   = Handle::from(term.file()).split().0;

        (harness, InputWrapper::new(input, vt220()))
    }

    #[tokio::test]
    async fn resize()
    {
        let (mut harness, mut input) = setup();

        harness.resize(50, 100);
        assert_eq!(input.next_event().await, UserEvent::Resize);
        assert_eq!(input.win_size(), Pos { row: 50, col: 100 });

        harness.resize(25, 50);
        assert_eq!(input.next_event().await, UserEvent::Resize);
        assert_eq!(input.win_size(), Pos { row: 25, col: 50 });
    }

    #[tokio::test]
    async fn keys()
    {
        let (mut harness, mut inp) = setup();

        harness.type_bytes(b"ABC");
        assert_eq!(inp.next_event().await, UserEvent::Key(key!('A')));
        assert_eq!(inp.next_event().await, UserEvent::Key(key!('B')));
        assert_eq!(inp.next_event().await, UserEvent::Key(key!('C')));
    }
}
