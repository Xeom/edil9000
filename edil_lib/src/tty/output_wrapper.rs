use super::*;

pub struct OutputWrapper<O, T>
{
    pos:     Option<Pos<u16>>,
    font:    Option<FontAttrs>,
    glyphs:  BTreeMap<Pos<u16>, (char, FontAttrs)>,
    to_draw: BTreeMap<Pos<u16>, (char, FontAttrs)>,
    output:  O,
    tty:     T
}

#[async_trait]
pub trait TtyOutput<O>: Sync + Send
    where O: AsyncWrite
{
    async fn set_cursor_pos(&mut self, p: Pos<u16>, out: &mut O)
        -> io::Result<()>;

    async fn set_font(&mut self, f: FontAttrs, out: &mut O) -> io::Result<()>;

    async fn set_char(&mut self, ch: char, out: &mut O) -> io::Result<()>;

    async fn clear(&mut self, out: &mut O) -> io::Result<()>;

    async fn begin(&mut self, out: &mut O) -> io::Result<()>;

    async fn end(&mut self, out: &mut O) -> io::Result<()>;
}

impl<O, T> OutputWrapper<O, T>
    where O: AsyncWrite + Unpin, T: TtyOutput<O>
{
    pub fn new(output: O, tty: T) -> Self
    {
        Self
        {
            pos:     None,
            font:    None,
            glyphs:  Default::default(),
            to_draw: Default::default(),
            output,
            tty
        }
    }

    fn already_has(&self, pos: Pos<u16>, glyph: (char, FontAttrs)) -> bool
    {
        let found = self.glyphs.get(&pos);
        let blank = (' ', FontAttrs::default());

        found == Some(&glyph) || (glyph == blank && found == None)
    }

    async fn draw_glyph(&mut self, pos: Pos<u16>, glyph: (char, FontAttrs))
        -> io::Result<()>
    {
        if !self.already_has(pos, glyph)
        {
            if self.pos != Some(pos)
            {
                self.tty.set_cursor_pos(pos, &mut self.output).await?;
            }

            if self.font != Some(glyph.1)
            {
                self.tty.set_font(glyph.1, &mut self.output).await?;
                self.font = Some(glyph.1);
            }

            self.tty.set_char(glyph.0, &mut self.output).await?;

            self.glyphs.insert(pos, glyph);
            self.pos = Some(Pos { row: pos.row, col: pos.col + 1 });
        }

        Ok(())
    }
}

#[async_trait]
impl<O, T> display::Canvas for OutputWrapper<O, T>
    where O: AsyncWrite + Unpin + Sync + Send, T: TtyOutput<O>
{
    fn set_glyph(&mut self, pos: Pos<u16>, glyph: (char, FontAttrs))
    {
        self.to_draw.insert(pos, glyph);
    }

    async fn begin(&mut self) -> io::Result<()>
    {
        self.tty.begin(&mut self.output).await?;
        self.output.flush().await
    }

    async fn end(&mut self) -> io::Result<()>
    {
        self.tty.end(&mut self.output).await?;
        self.output.flush().await
    }

    async fn reset(&mut self) -> io::Result<()>
    {
        self.pos = None;
        self.font = None;
        self.glyphs.clear();
        self.tty.clear(&mut self.output).await?;
        self.output.flush().await
    }

    async fn draw(&mut self) -> io::Result<()>
    {
        let to_draw = std::mem::take(&mut self.to_draw);

        for (pos, glyph) in to_draw.into_iter()
        {
            self.draw_glyph(pos, glyph).await?;
        }

        self.output.flush().await
    }
}

