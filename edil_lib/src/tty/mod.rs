use super::*;

mod palette;
use palette::*;

mod decoder;
use decoder::*;

mod output_wrapper;
use output_wrapper::*;

mod input_wrapper;
use input_wrapper::*;

mod vt220;
pub use vt220::*;

mod handle;
use handle::*;

pub fn open_tty<T>(tty: T) -> (impl UserInput, impl display::Canvas)
    where T: TtyInput + TtyOutput<io::BufWriter<WriteHandle>> + Clone
{
    let std_file = std::fs::File::options()
        .read(true)
        .write(true)
        .open("/dev/tty")
        .expect("Could not open TTY");

    let (reader, writer) = Handle::from(std_file).split();

    (
        InputWrapper::new(reader, tty.clone()),
        OutputWrapper::new(io::BufWriter::new(writer), tty)
    )
}
