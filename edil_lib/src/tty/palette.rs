use super::*;

pub trait Palette: Clone
{
    fn encode(colour: Colour, bg: bool) -> Vec<u8>;
}

#[derive(Clone)]
pub struct NoColour;

impl Palette for NoColour
{
    fn encode(_: Colour, _: bool) -> Vec<u8> { vec![] }
}

#[derive(Clone)]
pub struct Rgb;

impl Palette for Rgb
{
    fn encode(colour: Colour, bg: bool) -> Vec<u8>
    {
        vec![
            if bg { 48 } else { 38 }, 2,
            colour.r(),
            colour.g(),
            colour.b()
        ]
    }
}

#[derive(Clone)]
pub struct Col8;

impl Palette for Col8
{
    fn encode(colour: Colour, bg: bool) -> Vec<u8>
    {
        vec![ if bg { 40 } else { 30 } + colour.to_3bit() ]
    }
}

#[derive(Clone)]
pub struct Col16;

impl Palette for Col16
{
    fn encode(colour: Colour, bg: bool) -> Vec<u8>
    {
        vec![
            if colour.is_bright() { 90 } else { 30 } +
            if bg { 10 } else { 0 } +
            colour.to_3bit()
        ]
    }
}

#[derive(Clone)]
pub struct Col256;

impl Palette for Col256
{
    fn encode(colour: Colour, bg: bool) -> Vec<u8>
    {
        vec![
            if bg { 48 } else { 38 }, 5,
            16 + colour.to_8bit()
        ]
    }
}
