use super::*;

use KeyIter<'map> =
    std::collections::btree_map::Iter<&'map, &'static [u8], Key>;



enum Match<T>
{
    Partial<T>,
    Perfect<T>
}

struct MatchingKeys<'a>
{
    keys:     Option<KeyIter<'a>>,
    alt_keys: Option<KeyIter<'a>>,
    buf:      &'a [u8]
}



impl Iterator for MatchingKeys<'a, KeyIt, AltKeyIt>
    where KeyIt
{
    type Item = Match<Key>;

    fn next(&mut self) -> Option<Match<Key>>
    {
        if let Some((bytes, key)) = self.keys.as_ref().map(|it| it.next())
        {
            if self.buf == bytes
            {
                return Match::Perfect(key);
            }
            else if bytes.starts_with(self.buf)
            {
                return Match::Partial(key);
            }
            else
            {
                self.keys = None;
            }
        }

        if let Some((bytes, key)) = self.alt_keys.as_ref().map(|it| it.next())
        {
            if self.buf == 
        }
        else if let Some((ty, alt_key)) = self.alt_keys.as_ref()
            .map(|it| it.next())
        {
            alt_key.alt = true;
            Some((ty, alt_key))
        }
        else if let Some((ty, utf8_key)) = decode_utf8(self.buf)
        {
            Some((ty, utf8_key))
        }
        else
        {
            None
        }
    }
}
