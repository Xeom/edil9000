use super::*;

#[derive(Clone)]
pub enum SplitType
{
    Horizontal,
    Vertical
}

impl SplitType
{
    pub fn split(&self, loc: &Location) -> Option<(Location, Location)>
    {
        match self
        {
            Self::Vertical =>
                loc.split_vertical(loc.size.row / 2),
            Self::Horizontal => todo!()
        }
    }
}

