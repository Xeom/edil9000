//use super::*;

use std::sync::atomic::AtomicU64;

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub struct FrameId(u64);
static NEXT: AtomicU64 = AtomicU64::new(0);

impl std::fmt::Display for FrameId
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        f.debug_tuple("FId")
            .field(&self.0)
            .finish()
    }
}

impl std::str::FromStr for FrameId
{
    type Err = ();
    fn from_str(s: &str) -> Result<FrameId, ()>
    {
        fn parse(s: &str) -> Option<u64>
        {
            s.strip_prefix("FId(")?.strip_suffix(")")?.parse().ok()
        }

        match parse(s)
        {
            None    => Err(()),
            Some(i) => Ok(Self(i))
        }
    }
}

impl FrameId
{
    pub fn next() -> Self
    {
        FrameId(NEXT.fetch_add(1, std::sync::atomic::Ordering::Relaxed))
    }
}
