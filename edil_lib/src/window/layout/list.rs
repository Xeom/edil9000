use super::*;

pub(crate) struct List
{
    tree:               tree::Tree,
    prompt:             Option<Arc<Record>>,
    notifications:      Option<Arc<Record>>,
    show_notifications: bool
}

impl Default for List
{
    fn default() -> Self
    {
        Self
        {
            tree:               Default::default(),
            prompt:             None,
            notifications:      None,
            show_notifications: true
        }
    }
}

impl List
{
    pub fn clear(&mut self)
    {
        self.tree = tree::Tree::Empty;
        self.prompt = None;
    }

    pub fn create(
        &mut self,
        frame:            Arc<Record>,
        target:           Option<FrameId>,
        maybe_split_type: Option<SplitType>
    )
    {
        match maybe_split_type
        {
            Some(split_type) =>
                self.tree.split(target, frame, split_type),
            None =>
                self.tree.replace(target, frame)
        }
    }

    pub fn create_prompt(&mut self, frame: Arc<Record>)
    {
        self.prompt = Some(frame);
    }

    pub fn create_notifications(&mut self, frame: Arc<Record>)
    {
        self.notifications = Some(frame);
    }

    pub fn set_notifications(&mut self, value: Option<bool>)
    {
        self.show_notifications = value.unwrap_or(!self.show_notifications);
        if self.show_notifications
        {
            info!("Enabled notifications");
        }
        else
        {
            info!("Disabled notifications");
        }
    }

    pub fn close_frame(&mut self, id: FrameId)
    {
        self.tree.close(id);
    }

    pub fn get_ids(&self) -> Vec<(FrameId, ContentId)>
    {
        let mut rtn = Vec::new();
        self.tree.walk(
            &mut |frame|
            {
                rtn.push((frame.get_id(), frame.get_content().get_id()));
            }
        );

        rtn
    }

    pub fn get_prompt(&self) -> Option<Arc<Record>>
    {
        self.prompt.clone()
    }

    pub fn get_frame(&self, id: FrameId) -> Option<Arc<Record>>
    {
        self.tree.find_record_by_id(id)
    }

    pub fn get_displayable(&self, size: Pos<u16>, mode: Mode)
        -> Vec<Box<dyn display::Displayable>>
    {
        let mut rtn = Vec::<Box<dyn display::Displayable>>::new();

        let mut location = Location { pos: Pos { row: 0, col: 0 }, size };

        // Handle the bottom frame...
        if let Some(bottom) = self.get_bottom_frame(mode)
        {
            let bottom_height = std::cmp::min(
                6,
                bottom.get_content().inner().num_lines()
            ) as u16;

            // If we can make space for the bottom frame, add it.
            if let Some(bottom_loc) = location.split_off_bottom(bottom_height)
            {
                bottom.size().set(bottom_loc.size.cast::<usize>());
                rtn.push(Box::new(body::Body::new(&bottom, bottom_loc)));
            }
        }

        self.tree.walk_locations(
            &mut |frame, &(mut loc)|
            {
                if let Some(bar_loc) = loc.split_off_bottom(1)
                {
                    rtn.push(Box::new(bar::Bar::new(frame, bar_loc)));
                }

                frame.size().set(loc.size.cast::<usize>());
                rtn.push(Box::new(body::Body::new(&frame, loc)));
            },
            &location
        );

        rtn
    }

    /// Get the frame to be displayed at the bottom of the layout.
    fn get_bottom_frame(&self, mode: Mode) -> Option<Arc<Record>>
    {
        if matches!(mode, Mode::Prompt)
        {
            self.prompt.clone()
        }
        else if self.show_notifications
        {
            self.notifications.clone()
        }
        else
        {
            None
        }
    }
}

