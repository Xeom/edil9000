use super::*;

#[derive(Default)]
pub(super) enum Tree
{
    #[default]
    Empty,
    Frame(Arc<Record>),
    Split(SplitType, Box<Tree>, Box<Tree>)
}

impl Tree
{
    pub fn find_record_by_id(&self, id: FrameId) -> Option<Arc<Record>>
    {
        match self
        {
            Self::Frame(frame) if frame.get_id() == id =>
                Some(frame.clone()),
            Self::Frame(_) | Self::Empty =>
                None,
            Self::Split(_, l, r) =>
                l.find_record_by_id(id).or_else(|| r.find_record_by_id(id))
        }
    }

    pub fn split(
        &mut self,
        target:     Option<FrameId>,
        new:        Arc<Record>,
        split_type: SplitType
    )
    {
        let found = self.find_by_maybe_id(target);
        if matches!(found, Self::Empty)
        {
            *found = Self::Frame(new);
        }
        else
        {
            *found = Self::Split(
                split_type,
                Box::new(Self::Frame(new)),
                Box::new(found.take())
            );
        }
    }

    pub fn close(&mut self, target: FrameId)
    {
        debug!(?target, "Closing frame in layout");

        let should_close = |arm: &Self| match arm
            {
                Self::Frame(record) => record.get_id() == target,
                Self::Split(_, _, _) | Self::Empty => false
            };

        match self
        {
            Self::Split(_, l, _) if should_close(&l) =>
                *self = l.take(),
            Self::Split(_, _, r) if should_close(&r) =>
                *self = r.take(),
            Self::Split(_, l, r) =>
            {
                l.close(target);
                r.close(target);
            }
            _ => {}
        }
    }

    pub fn replace(&mut self, target: Option<FrameId>, new: Arc<Record>)
    {
        *self.find_by_maybe_id(target) = Self::Frame(new);
    }

    pub fn walk(&self, cb: &mut impl FnMut(&Arc<Record>))
    {
        match self
        {
            Self::Frame(frame) =>
                cb(&frame),
            Self::Split(_, l, r) =>
            {
                l.walk(cb);
                r.walk(cb);
            }
            Self::Empty => {}
        }
    }

    pub fn walk_locations(
        &self,
        cb: &mut impl FnMut(&Arc<Record>, &Location),
        loc: &Location
    )
    {
        match self
        {
            Self::Frame(frame) =>
                cb(frame, loc),
            Self::Split(split_type, l, r) =>
            {
                if let Some((l_loc, r_loc)) = split_type.split(loc)
                {
                    l.walk_locations(cb, &l_loc);
                    r.walk_locations(cb, &r_loc);
                }
                else
                {
                    l.walk_locations(cb, loc);
                }
            }
            Self::Empty => {}
        }
    }

    fn find_rightmost<'a>(&'a mut self) -> &'a mut Self
    {
        match self
        {
            Self::Frame(_) | Self::Empty => self,
            Self::Split(_, l, _) => l.find_rightmost()
        }
    }

    fn find_by_maybe_id<'a>(&'a mut self, maybe_id: Option<FrameId>)
        -> &'a mut Self
    {
        if let Some(id) = maybe_id
        {
            // TODO: The borrow checker is idiotic here.
            //
            // We should be able to replace this with
            //
            // if let Some(found) = self.find_by_id(...)
            //
            if self.contains_id(id)
            {
                return self.find_by_id(id).unwrap();
            }
        }

        self.find_rightmost()
    }

    fn take(&mut self) -> Self
    {
        std::mem::replace(self, Self::Empty)
    }

    fn contains_id(&self, id: FrameId) -> bool
    {
        match self
        {
            Self::Frame(frame) => frame.get_id() == id,
            Self::Empty => false,
            Self::Split(_, l, r) => l.contains_id(id) || r.contains_id(id)
        }
    }

    fn find_by_id<'a>(&'a mut self, id: FrameId) -> Option<&'a mut Self>
    {
        match self
        {
            Self::Frame(frame) if frame.get_id() == id =>
                Some(self),
            Self::Frame(_) | Self::Empty =>
                None,
            Self::Split(_, l, r) =>
                l.find_by_id(id).or_else(|| r.find_by_id(id))
        }
    }

}

