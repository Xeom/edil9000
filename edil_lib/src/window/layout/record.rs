use super::*;

use std::cmp::{min, max};

pub struct Record
{
    id:       FrameId,
    editor:   Arc<Editor>,
    content:  Arc<content::Record>,
    scroll:   Variable<usize>,
    size:     Variable<Pos<usize>>,

    // Handlers for this record.
    _handlers: [Handler; 4]
}

impl Record
{
    pub fn new(editor: &Arc<Editor>, content: Arc<content::Record>) -> Arc<Self>
    {
        let id = FrameId::next();
        debug!(?id, "Creating new frame record");

        Arc::new_cyclic(|this: &Weak<Self>|
            {
                let this      = this.clone();
                let mngr      = Arc::downgrade(&editor.windows);
                let scroll    = Variable::new(0_usize);
                let size      = Variable::new(Pos { row: 30, col: 80 });
                let _handlers = [
                    // Redraw if the scroll changes
                    make_redraw_handler(
                        &mngr, &editor.exec, scroll.on_change()
                    ),
                    // Redraw if the content requests it
                    make_redraw_handler(
                        &mngr, &editor.exec, content.inner().on_redraw_needed()
                    ),
                    // Redraw if the number of active tasks changes
                    make_redraw_handler(
                        &mngr, &editor.exec, content.num_tasks().on_change()
                    ),
                    // Scroll the window if the content focuses on something
                    Handler::builder(&editor.exec)
                        .with_event(content.inner().on_focus())
                        .with_ignore_multiple()
                        .build(
                            move |row| if let Some(this) = this.upgrade()
                            {
                                this.scroll_to_show(*row)
                            }
                        )
                ];

                Self
                {
                    id,
                    editor: editor.clone(),
                    content,
                    scroll,
                    size,
                    _handlers
                }
            }
        )
    }

    pub fn get_id(&self) -> FrameId
    {
        self.id
    }

    pub fn get_content(&self) -> Arc<content::Record>
    {
        self.content.clone()
    }

    pub fn get_scroll(&self) -> usize
    {
        let rtn = self.scroll.get();
        if rtn < self.max_scroll_lower_bound()
        {
            rtn
        }
        else
        {
            min(rtn, self.max_scroll())
        }
    }

    pub fn scroll(&self, n: isize)
    {
        let new = self.get_scroll().saturating_add_signed(n);
        if new < self.max_scroll_lower_bound() || new < self.max_scroll()
        {
            self.scroll.set(new);
        }
        else
        {
            self.scroll.set(usize::MAX);
        }
    }

    pub fn scroll_to_show(&self, n: usize)
    {
        let min_scroll = self.min_scroll_showing(n);
        let max_scroll = n;

        self.scroll.set(min(max_scroll, max(self.get_scroll(), min_scroll)));
    }

    pub fn size(&self) -> &Variable<Pos<usize>> { &self.size }

    fn max_scroll(&self) -> usize
    {
        // The maximum scroll is the minimum scroll that shows the last line.
        self.min_scroll_showing(
            self.content.inner().num_lines().saturating_sub(1)
        )
    }

    fn max_scroll_lower_bound(&self) -> usize
    {
        self.content.inner().num_lines().saturating_sub(self.size.get().row)
    }

    fn min_scroll_showing(&self, to_show: usize) -> usize
    {
        let content = self.content.inner();
        let size = self.size.get();

        if size.col == 0 { return 0; }

        let mut available_rows = size.row;

        let cols_to_rows = |cols| 1 + cols / size.col;

        // TODO: Engage some kind of transaction??
        for row_iter in (0..=to_show).rev()
        {
            // Escape hatch for very long lines
            let this_line_rows_lower_bound = cols_to_rows(
                content.line_len_lower_bound(row_iter)
            );

            if this_line_rows_lower_bound > available_rows
            {
                return min(to_show, row_iter + 1);
            }

            let this_line_rows = cols_to_rows(
                content.get_line(row_iter)
                    .map(|l| l.len(&self.editor))
                    .unwrap_or(0)
            );

            // If we scroll to this row, we cannot fit everything on the screen.
            if this_line_rows > available_rows
            {
                return min(to_show, row_iter + 1);
            }

            available_rows -= this_line_rows;
        }

        return 0;
    }
}
