use super::*;

struct BarItem
{
    glyphs: Vec<Glyph>
}

impl BarItem
{
    fn new(s: impl ToString, editor: &Editor) -> Self
    {
        let frame_fg = editor.fonts.get_id("frame_fg");
        BarItem
        {
            glyphs: s.to_string().chars()
                .map(|ch| Glyph(ch, frame_fg))
                .collect()
        }
    }
}

/// A bar to be drawn at the bottom of a window.
pub struct Bar
{
    location: Location,
    frame:    Arc<layout::Record>
}

impl Bar
{
    pub fn new(frame: &Arc<layout::Record>, location: Location) -> Self
    {
        Self { location, frame: frame.clone() }
    }

    fn get_items(&self, editor: &Editor) -> impl Iterator<Item=BarItem>
    {
        let mut items = Vec::new();

        // For the currently selected window, include extra items
        if Some(self.frame.get_id()) == editor.windows.selected_frame()
            .map(|frame| frame.get_id())
        {
            items.push(BarItem::new(editor.mode.get(), editor));
        }

        let content = self.frame.get_content();
        items.push(BarItem::new(content.inner().title(), editor));

        match content.num_tasks().get()
        {
            0 => {}
            1 => items.push(BarItem::new("1 Task", editor)),
            n => items.push(BarItem::new(format!("{n} Tasks"), editor))
        }

        items.into_iter()
    }
}

impl display::Displayable for Bar
{
    fn get_line(&self, _n: usize, editor: &Editor) -> Option<Line>
    {
        let mut rtn = Line::default()
            .with_fill(Glyph('_', editor.fonts.get_id("frame_bg")));

        let sep = Glyph('/', editor.fonts.get_id("frame_bg"));

        let mut wtr = rtn.left();
        for (index, item) in self.get_items(editor).enumerate()
        {
            wtr.extend(if index != 0 { Some(sep) } else { None });
            wtr.extend(&item.glyphs);
        }

        Some(rtn)
    }

    fn get_scroll(&self) -> usize { 0 }

    fn get_location(&self) -> Location { self.location }
}
