use super::*;

pub enum SwitchType
{
    Next,
    Prev,
    Id(ContentId)
}

#[derive(Debug)]
pub(crate) enum OpenAs
{
    Frame,
    Prompt,
    Notifications
}

pub(crate) struct Manager
{
    /// A list of content.
    content:           RwLock<content::List>,
    /// A list of displayed frames.
    layout:            RwLock<layout::List>,
    /// The current size of the terminal.
    display_size:      Variable<Pos<u16>>,
    /// The currently selected frame of the layout.
    selected:          Variable<Option<FrameId>>,

    on_display_redraw: Event<()>,
    on_display_reset:  Event<()>,
    on_content_change: Event<()>,

    /// Handlers for the window manager
    handlers:         Mutex<Option<[Handler; 2]>>
}

pub(super) fn make_redraw_handler<'evt, E: 'static>(
    this: &Weak<Manager>,
    exec: &Executor,
    evt:  impl Into<Option<NonOwned<'evt, Event<E>>>>
)
    -> Handler
{
    let weak = this.clone();
    Handler::builder(exec)
        .with_event(evt.into())
        .with_ignore_multiple()
        .build(
            move |_| if let Some(this) = weak.upgrade()
            {
                this.on_display_redraw.trigger(&())
            }
        )
}

pub fn get_next<T>(items: BTreeSet<T>, maybe_start: Option<T>, n: isize)
    -> Option<T>
    where T: Clone + Ord + Copy
{
    // Start from the specified start, or the first item.
    let start = maybe_start
        .or(items.first().cloned())?;

    let range = items.range(start..)
        .chain(items.range(..=start))
        .cloned();

    match n
    {
        0.. => range.cycle().nth(n as usize),
        _   => range.rev().cycle().nth(-n as usize)
    }
}


impl Manager
{
    pub fn new(
        exec:         Arc<Executor>,
        display_size: Pos<u16>,
        mode:         &Variable<Mode>
    )
        -> Arc<Self>
    {
        Arc::new_cyclic(|this|
            {
                let selected = Variable::new(None);

                let handlers = [
                    make_redraw_handler(this, &exec, mode.on_change()),
                    make_redraw_handler(this, &exec, selected.on_change()),
                ];

                Manager
                {
                    layout:            Default::default(),
                    content:           Default::default(),
                    on_display_redraw: Default::default(),
                    on_display_reset:  Default::default(),
                    on_content_change: Default::default(),
                    display_size:      Variable::new(display_size),
                    selected,
                    handlers:          Mutex::new(Some(handlers))
                }
            }
        )
    }

    /// Create and display some content.
    ///
    /// The new content is shown in place of the currently selected window, and
    /// the new content is selected.
    ///
    /// If matching content already exists, that is used instead.
    ///
    /// TODO: If matching content is already shown on the screen, jump to it.
    ///
    pub fn open<P>(&self, editor: &Arc<Editor>, content_params: P, ty: OpenAs)
        -> (ContentId, Arc<P::Content>)
        where P: crate::content::Params
    {
        let _span = info_span!("Opening content").entered();

        let (id, inner) =
            self.content.write().unwrap().create(editor, content_params);

        match ty
        {
            OpenAs::Frame =>
            {
                self.create_frame(editor, id, self.selected.get(), None);
            }
            OpenAs::Prompt | OpenAs::Notifications =>
            {
                // Handle opening hidden content
                if let Some(content) = self.get_content(id)
                {
                    self.content.write().unwrap().hide_content(id);
                    let frame = layout::Record::new(editor, content);
                    if matches!(ty, OpenAs::Prompt)
                    {
                        self.layout.write().unwrap().create_prompt(frame);
                    }
                    else if matches!(ty, OpenAs::Notifications)
                    {
                        self.layout.write().unwrap()
                            .create_notifications(frame);
                    }
                }
            }
        }

        self.on_content_change.trigger(&());
        debug!(?id, "Finished opening new content");

        (id, inner)
    }

    pub fn split(&self, editor: &Arc<Editor>, split_type: layout::SplitType)
    {
        let _span = info_span!("Splitting window").entered();

        // Get the IDs of currently displayed content.
        let shown_ids: BTreeSet<_> = self.layout.read().unwrap()
            .get_ids().iter()
            .map(|(_, content_id)| *content_id)
            .collect();

        // Get all Content IDs.
        let all_ids = self.content.read().unwrap().get_ids();

        // Get the Content IDs that are not currently displayed.
        let mut hidden = all_ids.difference(&shown_ids).cloned();

        if let Some(to_show) = hidden.next().or(self.selected_content_id())
        {
            debug!(?to_show, "Found new content to show");
            let selected = self.selected.get();
            self.create_frame(editor, to_show, selected, Some(split_type));
        }
    }

    pub fn switch_content(&self, editor: &Arc<Editor>, ty: SwitchType)
    {
        let _span = info_span!("Switching content in window").entered();

        let current_id = self.selected_content_id();
        let maybe_next_id = match ty
            {
                SwitchType::Id(id) =>
                    Some(id),
                SwitchType::Next =>
                    get_next(self.all_content_ids(), current_id, 1),
                SwitchType::Prev =>
                    get_next(self.all_content_ids(), current_id, -1),
            };

        if let Some(next_id) = maybe_next_id
        {
            debug!(
                ?current_id, ?next_id,
                "Switching content of currently selected window."
            );

            let selected = self.selected.get();

            // Replace the selected window with this one...
            self.create_frame(editor, next_id, selected, None);
        }
    }

    pub fn switch_frame(&self, n: isize)
    {
        let frame_ids = self.layout.read().unwrap().get_ids().iter()
            .map(|(frame_id, _)| *frame_id)
            .collect();

        self.selected.set(get_next(frame_ids, self.selected.get(), n));
    }

    pub fn close_selected(&self)
    {
        if let Some(id) = self.selected.get()
        {
            self.layout.write().unwrap().close_frame(id);
            self.on_display_redraw.trigger(&());
        }
    }

    pub fn close_all(&self)
    {
        self.selected.set(None);
        self.content.write().unwrap().clear();
        self.layout.write().unwrap().clear();
        self.handlers.lock().unwrap().take();
    }

    pub fn get_size(&self) -> Option<Pos<usize>>
    {
        Some(self.selected_frame()?.size().get())
    }

    pub fn scroll(&self, n: isize)
    {
        if let Some(frame) = self.selected_frame()
        {
            frame.scroll(n)
        }
    }

    pub fn prompt(&self) -> Option<Arc<content::Record>>
    {
        let frame = self.layout.read().unwrap().get_prompt()?;
        Some(frame.get_content())
    }

    pub fn selected_content(&self) -> Option<Arc<content::Record>>
    {
        Some(self.selected_frame()?.get_content())
    }

    pub fn get_displayable(&self, editor: &Arc<Editor>)
        -> Vec<Box<dyn display::Displayable>>
    {
        let mode = editor.mode.get();
        self.layout.read().unwrap()
            .get_displayable(self.display_size.get(), mode)
    }

    pub fn resize_display(&self, size: Pos<u16>)
    {

        self.display_size.set(size);
        self.on_display_reset.trigger(&());
    }

    /// Get an event that is triggered when the display must be reset.
    pub fn on_display_reset(&self) -> NonOwned<'_, Event<()>>
    {
        self.on_display_reset.as_non_owned()
    }

    /// Get an event that is triggered when the display must be redrawn.
    pub fn on_display_redraw(&self) -> NonOwned<'_, Event<()>>
    {
        self.on_display_redraw.as_non_owned()
    }

    /// Get an event that is triggered when the content list changes.
    pub fn on_content_change(&self) -> NonOwned<'_, Event<()>>
    {
        self.on_content_change.as_non_owned()
    }

    pub fn set_notifications(&self, value: Option<bool>)
    {
        self.layout.write().unwrap().set_notifications(value);
        self.on_display_redraw.trigger(&());
    }

    pub fn all_content_ids(&self) -> BTreeSet<ContentId>
    {
        self.content.read().unwrap().get_ids()
    }

    pub fn get_content(&self, id: ContentId) -> Option<Arc<content::Record>>
    {
        self.content.read().unwrap().get(id)
    }

    pub(super) fn selected_frame(&self) -> Option<Arc<layout::Record>>
    {
        self.get_frame(self.selected.get()?)
    }

    fn selected_content_id(&self) -> Option<ContentId>
    {
        Some(self.selected_content()?.get_id())
    }

    fn get_frame(&self, id: FrameId) -> Option<Arc<layout::Record>>
    {
        self.layout.read().unwrap().get_frame(id)
    }

    fn create_frame(
        &self,
        editor:           &Arc<Editor>,
        content_id:       ContentId,
        target:           Option<FrameId>,
        maybe_split_type: Option<layout::SplitType>
    )
        -> Option<FrameId>
    {
        let _span = info_span!("Creating frame").entered();
        debug!(?content_id, "Creating frame...");

        // Get the content and create a new frame for it in the layout
        let content = self.get_content(content_id)?;
        let frame = layout::Record::new(editor, content);
        let id = frame.get_id();

        // Add the new frame to the layout
        self.layout.write().unwrap().create(frame, target, maybe_split_type);

        // Select the new frame
        self.selected.set(Some(id));
        self.on_display_redraw.trigger(&());

        debug!(?content_id, ?id, "Creating window frame done");

        Some(id)
    }
}
