use super::*;

pub struct Record
{
    content: Arc<dyn Content>,
    context: Context,
    unique_id: Option<String>
}

impl Record
{
    pub fn new<P>(editor: &Arc<Editor>, content_params: P, id: ContentId)
        -> (Self, Arc<P::Content>)
        where P: crate::content::Params
    {
        let _span = info_span!("Creating window record", ?id).entered();

        let unique_id = content_params.unique_id();
        let mut context: Option<Context<P::Content>> = None;

        let content = Arc::new_cyclic(|this|
            {
                let tasks = Arc::new(task::Manager::new(editor));
                context = Some(Context::new(this, editor, id, tasks));
                content_params.create(context.clone().unwrap())
            }
        );

        debug!("Constructed window record handlers");

        let record = Self
            {
                content: content.clone(),
                context: context.unwrap().as_any(),
                unique_id
            };

        record.content.open();

        (record, content)
    }

    pub fn close(&self)
    {
        self.content.close();
        self.context.tasks().shutdown();
    }

    pub fn num_tasks(&self) -> &Variable<usize>
    {
        self.context.tasks().num_tasks()
    }

    pub fn get_id(&self) -> ContentId { self.context.content_id() }

    pub fn get_context(&self) -> Context { self.context.clone() }

    pub fn inner(&self) -> &Arc<dyn Content> { &self.content }

    pub fn get_unique_id(&self) -> Option<&String>
    {
        self.unique_id.as_ref()
    }
}
