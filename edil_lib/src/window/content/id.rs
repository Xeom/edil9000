//use super::*;

#[derive(Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Hash)]
pub struct ContentId(u64);

impl Into<u64> for ContentId
{
    fn into(self) -> u64 { self.0 }
}

impl std::fmt::Display for ContentId
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        f.debug_tuple("CId")
            .field(&self.0)
            .finish()
    }
}

impl std::str::FromStr for ContentId
{
    type Err = ();
    fn from_str(s: &str) -> Result<ContentId, ()>
    {
        fn parse(s: &str) -> Option<u64>
        {
            s.strip_prefix("CId(")?.strip_suffix(")")?.parse().ok()
        }

        match parse(s)
        {
            None    => Err(()),
            Some(i) => Ok(Self(i))
        }
    }
}

impl ContentId
{
    pub fn grab_next(&mut self) -> Self
    {
        let rtn = *self;
        self.0 += 1;
        rtn
    }
}

