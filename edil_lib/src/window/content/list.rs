use super::*;

#[derive(Default)]
pub struct List
{
    next_id:   ContentId,
    by_id:     BTreeMap<ContentId, Arc<Record>>,
    hidden:    BTreeSet<ContentId>
}

impl List
{
    pub fn hide_content(&mut self, id: ContentId)
    {
        self.hidden.insert(id);
    }

    pub fn clear(&mut self)
    {
        for (_, record) in self.by_id.iter()
        {
            record.close();
        }

        self.by_id.clear();
    }

    pub fn get(&self, id: ContentId) -> Option<Arc<Record>>
    {
        self.by_id.get(&id).cloned()
    }

    /// Find some existing content that can be used for some content params
    /// rather than creating new window contents.
    ///
    /// For example, a copy of a file may already be open, so we should use that
    /// rather than opening the same file twice.
    pub fn find_existing<P>(&self, content_params: &P)
        -> Option<(ContentId, Arc<P::Content>)>
        where P: crate::content::Params
    {
        if let Some(description) = content_params.unique_id()
        {
            for (id, record) in self.by_id.iter()
            {
                if record.get_unique_id() == Some(&description)
                {
                    let content = record.get_context().get_as::<P::Content>()?;
                    return Some((*id, content));
                }
            }
        }

        None
    }

    pub fn create<P>(&mut self, editor: &Arc<Editor>, content_params: P)
        -> (ContentId, Arc<P::Content>)
        where P: crate::content::Params
    {
        let _span = info_span!("Creating content").entered();

        if let Some((id, content)) = self.find_existing(&content_params)
        {
            debug!(
                ?id, unique_id=content_params.unique_id(),
                "Not creating window content - found existing"
            );
            (id, content)
        }
        else
        {
            let id = self.next_id.grab_next();
            debug!(
                ?id, unique_id=content_params.unique_id(),
                "Creating new window content"
            );

            let (record, content) = Record::new(editor, content_params, id);
            self.by_id.insert(id, Arc::new(record));

            (id, content)
        }
    }

    pub fn get_ids(&self) -> BTreeSet<ContentId>
    {
        &self.by_id.keys().cloned().collect::<BTreeSet<_>>() - &self.hidden
    }
}
