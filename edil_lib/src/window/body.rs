use super::*;

/// Information necessary to draw a window.
pub struct Body
{
    // The location of the window.
    location:  Location,
    // The number of rows of scroll of the window.
    scroll:    usize,
    // The content to draw.
    content:   Arc<dyn Content>
}

impl Body
{
    pub fn new(frame: &layout::Record, location: Location) -> Self
    {
        Self
        {
            location,
            scroll:   frame.get_scroll(),
            content:  frame.get_content().inner().clone()
        }
    }
}

impl display::Displayable for Body
{
    fn get_line(&self, n: usize, _: &Editor) -> Option<Line>
    {
        self.content.get_line(n)
    }

    fn get_scroll(&self) -> usize { self.scroll }

    fn get_location(&self) -> Location { self.location }
}
