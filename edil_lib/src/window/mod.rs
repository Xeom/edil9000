use super::*;

mod manager;
pub(crate) use manager::{Manager, OpenAs, SwitchType};
use manager::make_redraw_handler;

pub mod content;
pub use content::ContentId;

pub mod layout;
pub use layout::FrameId;

mod body;
mod bar;

