use super::*;

pub(crate) trait Input: Sync + Send + 'static
{
    type CType;
    type RustType<'a>: std::ops::Deref<Target=Self> + 'a;

    fn c_type_name() -> &'static str;

    unsafe fn from_c<'a>(c: &'a Self::CType) -> Self::RustType<'a>;
}

pub trait DynInput: Sync + Send
{
    fn c_type_name(&self) -> &'static str;
}

pub struct InputImpl<T: ?Sized>(std::marker::PhantomData<T>);

impl<T: ?Sized> DynInput for InputImpl<T>
    where T: Input
{
    fn c_type_name(&self) -> &'static str { T::c_type_name() }
}

impl<T: ?Sized> InputImpl<T>
{
    pub const fn new() -> Self { Self(std::marker::PhantomData) }
}

impl Input for Context
{
    type CType = *mut Context;
    type RustType<'a> = &'a Context;

    fn c_type_name() -> &'static str { "txt_context_s *" }

    unsafe fn from_c<'a>(c: &'a Self::CType) -> Self::RustType<'a> { &**c }
}

impl Input for Cursor
{
    type CType = <Cursor as OwnedPtr>::Ptr;
    type RustType<'a> = NonOwned<'a, Cursor>;

    fn c_type_name() -> &'static str { "txt_cursor_s *" }

    unsafe fn from_c<'a>(c: &'a Self::CType) -> Self::RustType<'a>
    {
        Cursor::from_non_owned_ptr(*c)
    }
}

impl Input for str
{
    type CType = <ApiUtf8 as OwnedPtr>::Ptr;
    type RustType<'a> = std::borrow::Cow<'a, str>;

    fn c_type_name() -> &'static str { "txt_utf8_s *" }

    unsafe fn from_c<'a>(c: &'a Self::CType) -> Self::RustType<'a>
    {
        ApiUtf8::from_non_owned_ptr(*c).as_str()
    }
}

impl Input for usize
{
    type CType = usize;
    type RustType<'a> = &'a usize;

    fn c_type_name() -> &'static str { "size_t" }

    unsafe fn from_c<'a>(c: &'a Self::CType) -> Self::RustType<'a> { c }
}
