use super::*;

mod input;
pub(crate) use input::{Input, InputImpl};
pub use input::DynInput;

mod output;
pub(crate) use output::{Output, OutputImpl};
pub use output::DynOutput;

pub struct Info<'a>
{
    /// The human-friendly name of this function.
    pub name:   &'a str,
    /// The name of the symbol containing this function.
    pub symbol: &'a str,
    /// The input types to this function, and their names.
    pub args:   &'a [(&'a str, &'a dyn DynInput)],
    /// The result type of this function.
    pub result: Option<&'a dyn DynOutput>
}

#[linkme::distributed_slice]
pub static PLUGIN_API: [Info<'static>];
