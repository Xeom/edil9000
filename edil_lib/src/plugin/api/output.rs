use super::*;

pub(crate) trait Output: Sync + Send + Sized + 'static
{
    type CType;

    fn c_type_name() -> &'static str;

    unsafe fn from_rust(self) -> Self::CType;
}

pub trait DynOutput: Sync + Send
{
    fn c_type_name(&self) -> &'static str;
}

pub struct OutputImpl<T>(std::marker::PhantomData<T>);

impl<T> DynOutput for OutputImpl<T>
    where T: Output
{
    fn c_type_name(&self) -> &'static str { T::c_type_name() }
}

impl<T> OutputImpl<T>
{
    pub const fn new() -> Self { Self(std::marker::PhantomData) }
}

impl Output for Cursor
{
    type CType = <Cursor as OwnedPtr>::Ptr;

    fn c_type_name() -> &'static str { "txt_cursor_s *" }

    unsafe fn from_rust(self) -> Self::CType
    {
        self.to_owned_ptr()
    }
}

// Add optional APIs with nullable pointers
impl<T, Ptr> Output for Option<T>
    where T: Output<CType = *mut Ptr>
{
    type CType = T::CType;

    fn c_type_name() -> &'static str { T::c_type_name() }

    unsafe fn from_rust(self) -> Self::CType
    {
        match self
        {
            Some(this) => this.from_rust(),
            None => std::ptr::null_mut()
        }
    }
}

impl Output for u64
{
    type CType = u64;

    fn c_type_name() -> &'static str { "uint64_t" }

    unsafe fn from_rust(self) -> Self::CType { self }
}

impl Output for String
{
    type CType = ApiUtf8;

    fn c_type_name() -> &'static str { "txt_utf8_s *" }

    unsafe fn from_rust(self) -> Self::CType { ApiUtf8::new(&self) }
}
