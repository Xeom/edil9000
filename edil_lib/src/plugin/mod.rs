use super::*;

pub fn eval_single(plugin: Arc<dyn Plugin>, context: Context, line: String)
{
    context.clone().spawn_blocking(
        move ||
        {
            let mut handle = plugin.create(&context);
            handle.eval(&line);
        }
    );
}

pub trait Plugin: Sync + Send + 'static
{
    fn create<'a>(&self, context: &'a Context) -> Box<dyn PluginHandle + 'a>;
}

pub trait PluginHandle
{
    fn eval(&mut self, s: &str);
}

pub mod api;
