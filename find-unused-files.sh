#!/bin/bash

set -e
set -o pipefail

TARGET=debug

get_files_in_package()
{
    local PACKAGE=$1
    cargo rustc -p $PACKAGE -- --emit dep-info 2>/dev/null
    cat target/$TARGET/deps/$PACKAGE-*.d |
        cut -d: -f1- |
        tr ' ' '\n' |
        grep -P '\.rs$' |
        sort -u
}

get_files_in_directory()
{
    local DIR=$1
    find $DIR -name '*.rs' | sort -u
}

print_missing()
{
    local PACKAGE=$1
    local DIR=$2

    echo "=== Checking package $PACKAGE against files in $DIR ==="

    comm --check-order --total -13 \
        <(get_files_in_package $PACKAGE) \
        <(get_files_in_directory $DIR)
}

print_missing edil edil/src
print_missing edil_common common/src_rs
print_missing edil_lib edil_lib/src
print_missing test_harness test_harness/src
