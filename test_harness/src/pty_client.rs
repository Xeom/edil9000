use super::*;

pub struct PtyClient
{
    term: File
}

impl PtyClient
{
    pub(super) fn new(term: File) -> Self { Self { term } }

    pub fn file(&mut self) -> File
    {
        self.term.try_clone().expect("Could not clone PTY client end")
    }
}
