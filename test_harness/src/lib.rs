use nix::{
    pty::*,
    sys::{signal::*, wait::*, termios},
    unistd::*,
    libc,
};

use std::time::{Duration, Instant};
use std::ffi::CString;
use std::os::fd::{AsRawFd, OwnedFd, FromRawFd};
use std::fs::File;
use std::sync::{Mutex, Arc};
use std::path::{Path, PathBuf};
use std::collections::HashMap;

mod child_harness;
pub use child_harness::{ChildBuilder, ChildHarness};

mod pty_harness;
pub use pty_harness::PtyHarness;

mod pty_client;
pub use pty_client::PtyClient;

mod command;
use command::Command;

mod test_harness;
pub use test_harness::TestHarness;

mod file_harness;
pub use file_harness::FileHarness;

fn to_winsize(rows: u16, cols: u16) -> Winsize
{
    Winsize
    {
        ws_row:    rows,
        ws_col:    cols,
        ws_xpixel: cols * 8,
        ws_ypixel: rows * 8
    }
}

const DEFAULT_ROWS: u16 = 8;
const DEFAULT_COLS: u16 = 20;

pub fn make_pty() -> (PtyHarness, PtyClient)
{
    let pty = openpty(&to_winsize(DEFAULT_ROWS, DEFAULT_COLS), None)
        .expect("Could not optn PTY");

    (PtyHarness::new(pty.master.into()), PtyClient::new(pty.slave.into()))
}
