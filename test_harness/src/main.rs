use test_harness::*;

use std::path::{Path, PathBuf};

struct Info
{
    current:        Option<String>,
    complete:       usize,
    total:          usize,
    complete_steps: usize,
    total_steps:    usize
}

fn print_bar(
    f:        &mut std::fmt::Formatter<'_>,
    complete: usize,
    total:    usize,
    col:      &str
)
    -> std::fmt::Result
{
    const WIDTH: usize = 25;

    let w = (WIDTH * complete) / total;
    let bar = std::iter::repeat('=').take(w)
        .chain(
            std::iter::once('>')
                .chain(std::iter::repeat(' '))
                .take(WIDTH - w)
        )
        .collect::<String>();

    const BOLD:  &'static str = "\x1b[0;1m";
    const RESET: &'static str = "\x1b[0m";

    write!(f, "{BOLD}[{col}{bar}{BOLD}]{RESET} ")?;

    Ok(())
}

impl std::fmt::Display for Info
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        const GREEN:  &'static str = "\x1b[0;32m";
        const YELLOW: &'static str = "\x1b[0;33m";
        print_bar(f, self.complete_steps, self.total_steps, YELLOW)?;
        print_bar(f, self.complete, self.total, GREEN)?;
        write!(
            f, "({:03}.{:03}/{:03})",
            self.complete, self.complete_steps, self.total
        )?;

        if let Some(current) = &self.current
        {
            write!(f, " {current}")?;
        }

        Ok(())
    }
}

impl Info
{
    fn new(total: usize) -> Self
    {
        println!("Running {total} cases...");
        Info
        {
            current:  None,
            complete: 0,
            total,
            complete_steps: 0,
            total_steps:    0
        }
    }

    fn print(&mut self)
    {
        print!("\x1b[2K {}\r", self);
        std::io::Write::flush(&mut std::io::stdout()).unwrap();
    }

    fn end_step(&mut self)
    {
        self.complete_steps += 1;
        self.print();
    }

    fn begin_case(&mut self, name: &str, num_steps: usize)
    {
        self.current = Some(name.to_owned());
        self.complete_steps = 0;
        self.total_steps = num_steps;
        self.print();
    }

    fn end_case(&mut self)
    {
        if let Some(current) = self.current.as_ref()
        {
            print!("\x1b[2K");
            println!("{:>3}. Completed {current}...", self.complete);
        }
        self.complete += 1;
        self.current = None;
        self.print();
    }
}

fn for_each_case(dir: impl AsRef<Path>, cb: &mut impl FnMut(&Path))
    -> std::io::Result<()>
{
    for maybe_entry in std::fs::read_dir(dir)?
    {
        let path = maybe_entry?.path();
        if path.is_dir()
        {
            for_each_case(path, cb)?;
        }
        else if let Some(ext) = path.extension()
        {
            if ext == "ron" { cb(&path) }
        }
    }

    Ok(())
}

fn find_cases(dir: impl AsRef<Path>) -> std::io::Result<Vec<PathBuf>>
{
    let mut rtn = Vec::<PathBuf>::new();
    for_each_case(dir, &mut |found| rtn.push(found.to_owned()))?;
    Ok(rtn)
}

/// Test harness for Edil editor.
#[derive(clap::Parser)]
#[command(version, about, long_about=None)]
struct CliArgs
{
    /// Path to directory of test cases to run.
    cases: PathBuf,
    /// Executable and arguments to invoke.
    args: Vec<String>,
    /// Number of times to repeat the test run.
    #[arg(long, default_value="1")]
    repeats: usize
}

fn main() -> std::io::Result<()>
{
    let args = <CliArgs as clap::Parser>::parse();

    println!("*** Edil test harness ***");
    let cases = find_cases(Path::new(&args.cases))?;

    let total_cases = cases.len() * args.repeats;
    let mut info = Info::new(total_cases);
    for case_path in cases.iter().cycle().take(total_cases)
    {
        let mut harness = TestHarness::new(&case_path, &args.args);
        info.begin_case(harness.name(), harness.num_steps());
        for ind in 0..harness.num_steps()
        {
            harness.run_step(ind);
            info.end_step();
        }
        info.end_case();
    }

    println!("\nAll tests passed");

    Ok(())
}
