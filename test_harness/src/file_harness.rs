use super::*;

pub struct FileHarness
{
    path: PathBuf,
    file: std::io::BufReader<File>
}

fn get_json_field<'j>(mut json: &'j serde_json::Value, key: &str)
    -> Option<&'j str>
{
    for component in key.split('.')
    {
        json = json.as_object()?.get(component)?;
    }

    json.as_str()
}

fn match_json<'a, It>(json: &str, fields: It) -> bool
    where It: Iterator<Item=(&'a String, &'a String)>
{
    let map: serde_json::Value = serde_json::from_str(json)
        .expect("Could not parse JSON");

    for (key, val) in fields
    {
        if let Some(cmp) = get_json_field(&map, key)
        {
            if cmp != val
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    true
}

impl FileHarness
{
    pub fn new(path: impl AsRef<Path>) -> Self
    {
        Self
        {
            path: path.as_ref().to_owned(),
            file: std::io::BufReader::new(
                std::fs::OpenOptions::new()
                    .read(true)
                    .write(true)
                    .create(true)
                    .open(path)
                    .unwrap()
            )
        }
    }

    fn has_matching_line(&mut self, mut pred: impl FnMut(&str) -> bool) -> bool
    {
        loop
        {
            use std::io::BufRead;

            let mut s = String::new();
            if self.file.read_line(&mut s).unwrap() == 0
            {
                return false;
            }

            if pred(&s)
            {
                return true;
            }
        }
    }

    pub fn has_matching_json(&mut self, fields: &HashMap<String, String>)
        -> bool
    {
        self.has_matching_line(|line| match_json(line, fields.iter()))
    }

    pub fn log_state(&self)
    {
        let src = &self.path;
        if let Some(dst) = src.file_name()
        {
            std::fs::copy(src, &dst).unwrap();

            const RED: &'static str = "\x1b[33m";
            const B:   &'static str = "\x1b[1m";
            const NB:  &'static str = "\x1b[22m";

            eprintln!(
                "{RED} * File {B}{src:?}{NB} copied to {B}{dst:?}\x1b[0m"
            );
        }
    }
}
