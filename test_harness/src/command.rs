use super::*;

#[derive(serde::Deserialize, Debug, Clone)]
#[serde(deny_unknown_fields)]
pub enum Command
{
    ExpectTerminates,
    ExpectDisplayedLines(Vec<String>),
    ExpectDisplayedRegex(String),
    ExpectLog(HashMap<String, String>),
    Resize
    {
        rows: u16,
        cols: u16
    },
    SendStdin(String),
    SendStdinLines(Vec<String>),
    SendPty(String),
}

impl Command
{
    pub fn run(&self, test: &mut TestHarness)
    {
        match self
        {
            Self::ExpectTerminates => {}
            Self::ExpectDisplayedLines(_) => {}
            Self::ExpectDisplayedRegex(_) => {}
            Self::ExpectLog(_) => {}
            Self::Resize { rows, cols } =>
                test.pty().resize(*rows, *cols),
            Self::SendStdin(bytes) =>
                test.child().send_stdin_bytes(bytes),
            Self::SendStdinLines(lines) =>
                test.child().send_stdin_bytes(
                    lines.iter().fold(String::new(), |s, line| s + line + "\n")
                ),
            Self::SendPty(bytes) =>
                test.pty().type_bytes(bytes),
        }
    }

    pub fn is_satisfied(&self, test: &mut TestHarness) -> bool
    {
        match self
        {
            Self::ExpectTerminates =>
                test.child().is_terminated(),
            Self::ExpectDisplayedLines(lines) =>
                test.pty().contents().lines()
                    .take(lines.len())
                    .eq(lines.iter().map(|l| l.trim_end())),
            Self::ExpectDisplayedRegex(regex) =>
                regex::Regex::new(regex).unwrap()
                    .is_match(&test.pty().contents()),
            Self::ExpectLog(fields) =>
                test.log().has_matching_json(fields),
            Self::Resize { .. } => true,
            Self::SendStdin(_) => true,
            Self::SendStdinLines(_) => true,
            Self::SendPty(_) => true
        }
    }
}
