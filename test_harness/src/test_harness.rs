use super::*;

use std::path::{Path, PathBuf};

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
enum FileContents
{
    Bytes(String),
    Copied(PathBuf)
}

fn default_log_level() -> String { "debug".to_owned() }

#[derive(serde::Deserialize)]
#[serde(deny_unknown_fields)]
struct Config
{
    #[serde(default)]
    use_stdin_pipe: bool,
    #[serde(default)]
    files: HashMap<PathBuf, FileContents>,
    #[serde(default)]
    args: Vec<String>,
    steps: Vec<Command>,
    #[serde(default="default_log_level")]
    log_level: String
}

struct Builder
{
    name:   String,
    config: Config,
    child:  ChildBuilder
}

pub struct TestHarness
{
    name:   String,
    config: Config,
    pty:    PtyHarness,
    child:  ChildHarness,
    log:    FileHarness
}

impl Builder
{
    fn new(config_path: &Path, args: &[String]) -> Self
    {
        let config: Config = ron::de::from_bytes(
            &std::fs::read(config_path)
                .expect("Could not read test harness file")
        )
            .expect("Could not deserialize test harness file");

        let name = config_path.file_stem()
            .map(|name| name.to_string_lossy().into_owned())
            .unwrap_or("unknown".to_owned());

        Self { name, config, child: ChildBuilder::new(args) }
    }

    fn create_file(&self, path: &Path, contents: &FileContents)
    {
        let path = self.child.temp_path(path);
        if let Some(parent) = path.parent()
        {
            std::fs::create_dir_all(parent)
                .expect("Could not create parent directory");
        }

        match contents
        {
            FileContents::Bytes(bytes) =>
            {
                std::fs::write(path, bytes)
                    .expect("Could not write bytes to temp dir");
            }
            FileContents::Copied(src) =>
            {
                std::fs::copy(src, path)
                    .expect("Could not copy file to temp dir");
            }
        }
    }

    fn build(mut self) -> TestHarness
    {
        // Setup temporary files
        for (path, contents) in self.config.files.iter()
        {
            self.create_file(path, contents);
        }

        // Setup STDIN pipe
        if self.config.use_stdin_pipe
        {
            self.child = self.child.using_stdin_pipe();
        }

        // Setup arguemnts
        for arg in self.config.args.iter()
        {
            self.child = self.child.using_arg(arg);
        }

        // Decompose self
        let Self { name, child, config } = self;

        // Setup log file
        let log = FileHarness::new(child.temp_path("log"));
        let (pty, child) = child
            .using_arg("--log-file=log")
            .using_arg(&format!("--log-level={}", &config.log_level))
            .using_arg("--log-format=json")
            .using_arg("--disable-notifications")
            .build();

        TestHarness { name, config, pty, child, log }
    }
}

impl TestHarness
{
    pub fn new(config_path: &Path, args: &[String]) -> Self
    {
        Builder::new(config_path, args).build()
    }

    pub fn num_steps(&self) -> usize { self.config.steps.len() }

    pub fn name(&self) -> &str { &self.name }

    pub fn run_step(&mut self, ind: usize)
    {
        let step = self.config.steps[ind].clone();
        step.run(self);

        let end = Instant::now() + std::time::Duration::from_secs(100);
        while !step.is_satisfied(self)
        {
            if Instant::now() > end
            {
                println!("\n");
                self.pty.log_state();
                self.log.log_state();
                panic!("Expectation timed out: {step:?}");
            }

            std::thread::sleep(std::time::Duration::from_millis(10));
        }
    }

    pub fn pty(&mut self) -> &mut PtyHarness { &mut self.pty }

    pub fn child(&mut self) -> &mut ChildHarness { &mut self.child }

    pub fn log(&mut self) -> &mut FileHarness { &mut self.log }
}
