use super::*;

use pretty_assertions::assert_eq;

nix::ioctl_write_ptr_bad!(set_win_size, libc::TIOCSWINSZ, Winsize);

fn normalize_str(s: impl AsRef<str>) -> String
{
    s.as_ref()
        .lines()
        .map(|line| line.trim_end().to_owned() + "\n")
        .collect::<String>()
        .trim_end()
        .to_owned()
}

pub struct PtyHarness
{
    term:   File,
    parser: Arc<Mutex<vt100::Parser>>
}

fn read_thread(parser: Arc<Mutex<vt100::Parser>>, mut term: File)
{
    let mut buf: [u8; 2048] = [0; 2048];

    loop
    {
        use std::io::Read;
        match term.read(&mut buf)
        {
            Ok(n) =>
            {
                // debug!("RECV {:?}", &buf[..n]);
                parser.lock().unwrap().process(&buf[..n]);
            }
            Err(e) if e.raw_os_error() == Some(5) =>
            {
                // info!("Child hung up");
                return;
            }
            Err(e) =>
                panic!("Could not read from PTY: {:?}", e)
        }
    }
}

impl PtyHarness
{
    pub(super) fn new(term: File) -> Self
    {
        let parser = Arc::new(Mutex::new(
            vt100::Parser::new(DEFAULT_ROWS, DEFAULT_COLS, 0)
        ));

        let rtn = Self
            {
                term:   term.try_clone().unwrap(),
                parser: parser.clone()
            };

        std::thread::spawn(move || read_thread(parser, term));

        rtn
    }

    pub fn type_bytes(&mut self, b: impl AsRef<[u8]>)
    {
        use std::io::Write;
        // debug!("SEND {:?}", b.as_ref());
        self.term.write_all(b.as_ref())
            .expect("Could not type bytes to harness");
    }

    pub fn resize(&mut self, rows: u16, cols: u16)
    {
        self.parser.lock().unwrap().set_size(rows, cols);

        unsafe
        {
            set_win_size(self.term.as_raw_fd(), &to_winsize(rows, cols))
                .unwrap();
        }

        raise(Signal::SIGWINCH).unwrap();
    }

    pub fn contents(&self) -> String
    {
        let parser = self.parser.lock().unwrap();
        normalize_str(
            parser.screen()
                .contents_between(
                    0,
                    0,
                    parser.screen().size().0,
                    0
                )
        )
    }

    pub fn line(&self, n: u16) -> String
    {
        let parser = self.parser.lock().unwrap();
        let size = parser.screen().size();
        normalize_str(
            parser.screen()
                .contents_between(n, 0, n, size.1)
        )
    }

    pub fn size(&self) -> (u16, u16)
    {
        self.parser.lock().unwrap().screen().size()
    }

    pub fn assert_shows(&self, s: impl AsRef<str>)
    {
        let s = normalize_str(s);

        let end = Instant::now() + Duration::from_secs(10);
        while Instant::now() < end
        {
            if self.contents() == s
            {
                return
            }
            std::thread::sleep(Duration::from_micros(10));
        }

        assert_eq!(s, self.contents(), "Terminal does not show expected.");
    }

    // Assert a predicate on the contents of the body of the terminal (skip
    // status and search)
    pub fn assert_ignoring_bottom(&mut self, pred: impl Fn(&str) -> bool)
    {
        self.assert(
            |this| match this.contents().rsplitn(1, '\n').last()
            {
                Some(body) => pred(body),
                None =>false
            },
            Duration::from_secs(10)
        );
    }

    pub fn assert(&mut self, pred: impl Fn(&Self) -> bool, timeout: Duration)
    {
        let end = Instant::now() + timeout;
        while Instant::now() < end
        {
            if pred(self)
            {
                return
            }
            std::thread::sleep(Duration::from_micros(10));
        }

        self.log_state();

        panic!(
            "Terminal output fails predicate.\nCurrent terminal contents:\n\
            >>>\n{}\n<<<",
            self.contents()
        );
    }

    pub fn log_state(&self)
    {
        let parser = self.parser.lock().unwrap();
        let screen = parser.screen();
        eprintln!("\x1b[32m>>> \x1b[1mTerminal Contents");
        for row in screen.rows_formatted(0, screen.size().1)
        {
            eprintln!("\x1b[32m|\x1b[0m\x1b[s{}\x1b[0m", &String::from_utf8_lossy(&row));
        }
        eprintln!("\x1b[32m<<<\x1b[0m");
    }
}
