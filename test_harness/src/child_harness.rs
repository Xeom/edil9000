use super::*;

use tempdir::TempDir;

struct Stdin
{
    read:  OwnedFd,
    write: OwnedFd
}

pub struct ChildBuilder
{
    temp_dir:   TempDir,
    stdin:      Option<Stdin>,
    stdout:     OwnedFd,
    stderr:     OwnedFd,
    args:       Vec<String>
}

impl ChildBuilder
{
    pub fn new<S>(args: impl IntoIterator<Item=S>) -> Self
        where S: AsRef<str>
    {
        let stdout = dup(libc::STDOUT_FILENO)
            .expect("Could not dup stdout for child");

        let stderr = dup(libc::STDERR_FILENO)
            .expect("Could not dup stderr for child");

        Self
        {
            temp_dir: TempDir::new("edil-test-harness")
                .expect("Could not make temporary cache"),
            stdin: None,
            stdout: unsafe { OwnedFd::from_raw_fd(stdout) },
            stderr: unsafe { OwnedFd::from_raw_fd(stderr) },
            args:   args.into_iter().map(|s| s.as_ref().to_owned()).collect()
        }
    }

    pub fn using_stdin_pipe(self) -> Self
    {
        let (read, write) = pipe().expect("Could not make pipe to child");
        let stdin = Some(Stdin { read, write });
        Self { stdin, ..self }
    }

    pub fn using_arg(mut self, arg: &str) -> Self
    {
        self.args.push(arg.to_owned());
        self
    }

    pub(crate) fn temp_path(&self, relative: impl AsRef<Path>) -> PathBuf
    {
        assert!(relative.as_ref().is_relative());
        self.temp_dir.path().join(relative)
    }

    pub fn build(self) -> (PtyHarness, ChildHarness)
    {
        let mut ios = termios::tcgetattr(std::io::stdin())
            .expect("Could not get termios");

        termios::cfmakeraw(&mut ios);

        let result =
            unsafe
            {
                forkpty(&to_winsize(DEFAULT_ROWS, DEFAULT_COLS), Some(&ios))
            }
            .expect("Could not fork PTY");

        match result
        {
            ForkptyResult::Parent { master, child } =>
                (PtyHarness::new(master.into()), self.handle_parent(child)),
            ForkptyResult::Child =>
                self.handle_child()
        }
    }

    fn handle_parent(self, pid: Pid) -> ChildHarness
    {
        ChildHarness
        {
            _temp_dir: self.temp_dir,
            pid:       Some(pid),
            stdin:     self.stdin.map(|Stdin { write, .. }| write.into())
        }
    }

    fn handle_child(mut self) -> !
    {
        // Setup STDIN
        if let Some(stdin) = &self.stdin
        {
            // The STDIN is currently the PTY. If we're redirecting the STDIN
            // to a pipe, duplicate the PTY so we don't hang up on it.
            dup(libc::STDIN_FILENO).unwrap();
            dup2(stdin.read.as_raw_fd(), libc::STDIN_FILENO).unwrap();
        }

        // Setup the STDOUT and STDERR
        dup2(self.stdout.as_raw_fd(), libc::STDOUT_FILENO).unwrap();
        dup2(self.stderr.as_raw_fd(), libc::STDERR_FILENO).unwrap();

        // Setup temporary directories
        let Self { temp_dir, .. } = self;

        if let Ok(found) =
            AsRef::<Path>::as_ref(self.args.get(0).expect("Missing executable"))
            .canonicalize()
        {
            self.args[0] = found.into_os_string().into_string().unwrap();
        }

        std::env::set_current_dir(temp_dir.path())
            .expect("Could not set current directory to temp dir");

        let cache_dir = temp_dir.path().join(".cache");
        let config_dir = temp_dir.path().join(".config");

        unsafe { std::env::set_var("HOME", temp_dir.path()); }
        unsafe { std::env::set_var("XDG_CONFIG_HOME", cache_dir); }
        unsafe { std::env::set_var("XDG_CACHE_HOME", config_dir); }

        let args = self.args.iter()
            .map(|a| CString::new(a.as_str()).unwrap())
            .collect::<Vec<_>>();


        execvp(&args[0], &args).expect("Could not exec child");
        panic!("This line should not be reached!")
    }
}

pub struct ChildHarness
{
    _temp_dir: TempDir,
    pid:       Option<Pid>,
    stdin:     Option<File>
}

impl Drop for ChildHarness
{
    fn drop(&mut self) { self.terminate() }
}

impl ChildHarness
{
    pub fn send_stdin_bytes(&mut self, b: impl AsRef<[u8]>)
    {
        use std::io::Write;
        self.stdin
            .as_mut()
            .expect("Child has no stdin pipe")
            .write_all(b.as_ref())
            .expect("Could not send bytes in harness");
    }

    pub fn terminate(&mut self)
    {
        if self.is_terminated() { return; }

        if let Some(pid) = self.pid
        {
            kill(pid, Signal::SIGINT).expect("Could not kill child");
            if self.check_terminates() { return; }

            kill(pid, Signal::SIGKILL).expect("Could not kill child");
            assert!(self.check_terminates(), "SIGKILL did not kill child");
        }
    }

    pub fn is_terminated(&mut self) -> bool
    {
        if let Some(pid) = self.pid
        {
            match waitpid(pid, Some(WaitPidFlag::WNOHANG))
                .expect("Could not wait for child")
            {
                WaitStatus::Exited(_, 0) | WaitStatus::Signaled(_, _, _) =>
                {
                    self.pid = None;
                    true
                }
                WaitStatus::Exited(_, exit) =>
                    panic!("Child process exited with code {exit}"),
                _ =>
                    false
            }
        }
        else
        {
            true
        }
    }

    fn check_terminates(&mut self) -> bool
    {
        let end = Instant::now() + Duration::from_secs(30);
        while Instant::now() < end
        {
            if self.is_terminated() { return true; }
            std::thread::sleep(Duration::from_millis(10));
        }

        false
    }
}
