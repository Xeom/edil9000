use edil_lib::*;
use quote::{quote, format_ident};

use proc_macro2::{Ident, TokenStream};

struct Details
{
    method_name: String,
    extern_name: Ident,
    static_name: Ident,
    res_c_type:  TokenStream,
    res_exists:  bool,
    arg_names:   Vec<Ident>,
    arg_types:   Vec<TokenStream>
}

fn details(funct: &plugin::api::Info<'_>) -> Details
{
    Details
    {
        method_name:
            funct.name.to_string(),
        extern_name:
            format_ident!("{}", funct.symbol),
        static_name:
            format_ident!("_static_{}", funct.name),
        res_c_type:
            match funct.result
            {
                Some(ty) => ty.c_type_name().parse().unwrap(),
                None => quote! { void }
            },
        res_exists:
            funct.result.is_some(),
        arg_names:
            funct.args.iter()
                .map(|(name, _)| format_ident!("{}", name))
                .collect(),
        arg_types:
            funct.args.iter()
                .map(|(_, ty)| ty.c_type_name().parse().unwrap())
                .collect()
    }
}

fn extern_signature(details: &Details) -> TokenStream
{
    let Details { res_c_type, extern_name, arg_types, arg_names, .. } = details;
    quote! { extern #res_c_type #extern_name( #( #arg_types #arg_names ),* ); }
}

fn static_preamble(details: &Details) -> TokenStream
{
    let Details { arg_types, arg_names, .. } = details;

    quote!
    {
        (void)self;

        Py_ssize_t arg_ind = 0;
        Py_ssize_t num_args = PyTuple_Size(args);
        if (num_args < 0)
        {
            return NULL;
        }

        #( #arg_types #arg_names; )*;

        #({
            PyObject *py_arg = (num_args > arg_ind) ?
                PyTuple_GetItem(args, arg_ind) :
                Py_None;

            arg_ind = arg_ind + 1;

            Py_INCREF(py_arg);
            bool ok = TXT_CONVERT_ARG_FROM_PY(py_arg, &#arg_names);
            Py_DECREF(py_arg);

            if (!ok)
            {
                return NULL;
            }
        })*
    }
}

fn static_call(details: &Details) -> TokenStream
{
    let Details { arg_names, extern_name, .. } = details;

    quote!
    {
        ( Py_INCREF(args), #extern_name( #( #arg_names ),* ));
        Py_DECREF(args);
    }
}

fn static_body(details: &Details) -> TokenStream
{
    let preamble = static_preamble(details);
    let call = static_call(details);
    if let Details { res_c_type, res_exists: true, .. } = details
    {
        quote!
        {
            #preamble
            #res_c_type _result = #call;
            return TXT_CONVERT_RESULT_TO_PY(_result);
        }
    }
    else
    {
        quote! { #preamble #call; Py_RETURN_NONE; }
    }
}

fn static_function(details: &Details) -> TokenStream
{
    let Details { static_name, .. } = details;

    let body = static_body(details);

    quote!
    {
        static PyObject * #static_name (PyObject *self, PyObject *args)
        {
            #body
        }
    }
}

fn method_definitions(details_all: &[Details]) -> TokenStream
{
    let definitions = details_all.iter().map(
        |details|
        {
            let Details { method_name, static_name, .. } = details;
            quote! { #method_name, #static_name, METH_VARARGS, "..." }
        });

    quote!
    {
        PyMethodDef TXT_PY_METHODS[] =
        {
            #( { #definitions }, )*
            { NULL, NULL, 0, NULL }
        };
    }
}

fn main()
{
    use std::fmt::Write;

    let args: Vec<String> = std::env::args().collect();
    assert_eq!(args.len(), 2, "Expected one argument - <OUTPUT>");

    let mut output = String::new();

    writeln!(output, "// Auto-generated file").ok();
    writeln!(output, r#"#include "py_types.h""#).ok();

    let all_details = plugin::api::PLUGIN_API.iter()
        .map(details)
        .collect::<Vec<_>>();

    for details in all_details.iter()
    {
        writeln!(output, "{}", extern_signature(details)).ok();
    }

    for details in all_details.iter()
    {
        writeln!(output, "{}", static_function(details)).ok();
    }

    writeln!(output, "{}", method_definitions(&all_details)).ok();

    if Some(true) == std::fs::read_to_string(&args[1])
        .map(|extant| extant == output)
        .ok()
    {
        // Already up-to-date
        return;
    }

    std::fs::write(&args[1], &output).unwrap();
}
